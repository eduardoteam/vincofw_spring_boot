package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_dbscript
 */
@Entity
@Table(name = "ad_dbscript")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdDbscript extends AdEntityBean {

    private String idDbscript;
    private String name;
    private String description;
    private Long seqno;

    List<AdDbscriptSql> sql;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idDbscript;
    }

    @Override
    public void setId(String id) {
            this.idDbscript = id;
    }

    @Id
    @Column(name = "id_dbscript")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdDbscript() {
        return idDbscript;
    }

    public void setIdDbscript(String idDbscript) {
        this.idDbscript = idDbscript;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 1000)
    @Size(min = 1, max = 1000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dbscript", referencedColumnName = "id_dbscript", insertable = false, updatable = false)
    @OrderBy("seqno ASC")
    @JsonIgnore
    public List<AdDbscriptSql> getSql() {
        return sql;
    }

    public void setSql(List<AdDbscriptSql> sql) {
        this.sql = sql;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdDbscript)) return false;

        final AdDbscript that = (AdDbscript) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idDbscript == null) && (that.idDbscript == null)) || (idDbscript != null && idDbscript.equals(that.idDbscript)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

}

