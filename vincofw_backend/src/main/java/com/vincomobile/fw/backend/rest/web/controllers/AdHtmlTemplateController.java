package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.persistence.model.AdHtmlTemplate;
import com.vincomobile.fw.backend.persistence.services.AdHtmlTemplateService;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.exception.FWBadRequestException;
import com.vincomobile.fw.basic.persistence.model.AdModule;
import com.vincomobile.fw.basic.persistence.model.AdPreference;
import com.vincomobile.fw.basic.persistence.services.AdModuleService;
import com.vincomobile.fw.basic.persistence.services.AdPreferenceService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Vincomobile FW on 03/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Controller for table ad_html_template
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_html_template/{idClient}")
public class AdHtmlTemplateController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdHtmlTemplateController.class);

    @Autowired
    AdHtmlTemplateService service;

    @Autowired
    AdModuleService moduleService;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdHtmlTemplate> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdHtmlTemplate.class);
        Page<AdHtmlTemplate> result = service.findAll(pageReq);
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (!Converter.isEmpty(apacheServerCacheUrl)) {
            for (AdHtmlTemplate item : result.getContent()) {
                item.setImageUrl(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/ad_html_template/" + item.getIdHtmlTemplate() + "/" + item.getImage());
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdHtmlTemplate get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(
            @PathVariable("idClient") String idClient,
            @RequestPart AdHtmlTemplate entity,
            @RequestParam(value = "file_image") MultipartFile image,
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        if (Converter.isEmpty(entity.getIdModule())) {
            AdModule module = moduleService.getClientModule(entity.getIdClient());
            entity.setIdModule(module != null ? module.getIdModule() : AdModuleService.MODULE_ID_FW);
        }
        entity.setImage(FileTool.cleanFileName(image.getOriginalFilename()));
        String result = (String) createEntity(entity);
        if (!bindingResults.hasErrors()) {
            ControllerResult valid = saveFile(idClient, "ad_html_template", entity.getIdHtmlTemplate(), "image/jpeg;image/png;image/gif", 50L, null, image);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("image", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setImage(valid.getProperties().get("filename").toString());
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestPart AdHtmlTemplate entity,
            @RequestParam(value = "file_image", required = false) MultipartFile image,
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdHtmlTemplate item = service.findById(entity.getId());
        RestPreconditions.checkNotNull(item);
        if (!bindingResults.hasErrors() && image != null) {
            entity.setImage(item.getImage());
            ControllerResult valid = saveFile(idClient, "ad_html_template", entity.getId(), "image/jpeg;image/png;image/gif", 50L, item.getImage(), image);
            if (!valid.isSuccess()) {
                bindingResults.rejectValue("image", valid.getMessage(), "" + valid.getProperties().get("value1"));
                throw new FWBadRequestException(bindingResults);
            } else {
                entity.setImage(valid.getProperties().get("filename").toString());
            }
        }
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        AdHtmlTemplate item = service.findById(id);
        RestPreconditions.checkNotNull(item);
        deleteEntity(item);
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            FileTool.deleteFile(baserDir.getString() + "/files/ad_html_template/" + item.getId() + "/" + item.getImage());
        }
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

}