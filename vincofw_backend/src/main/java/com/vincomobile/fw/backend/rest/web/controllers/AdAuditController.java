package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.persistence.model.AdAudit;
import com.vincomobile.fw.basic.persistence.services.AdAuditService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Vincomobile FW on 02/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Controller for table ad_audit
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_audit/{idClient}")
public class AdAuditController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdAuditController.class);

    @Autowired
    AdAuditService service;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdAudit> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdAudit.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdAudit get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdAudit entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdAudit entity) {
        logger.debug("PUT update(" + entity + ")");
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

}