package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.AdChartInfo;
import com.vincomobile.fw.backend.business.AdChartResult;
import com.vincomobile.fw.backend.business.AdChartTools;
import com.vincomobile.fw.backend.business.AdChartValue;
import com.vincomobile.fw.backend.persistence.charts.ChartDefinition;
import com.vincomobile.fw.backend.persistence.charts.ChartQuery;
import com.vincomobile.fw.backend.persistence.charts.ChartUtils;
import com.vincomobile.fw.backend.persistence.charts.ChartWhere;
import com.vincomobile.fw.backend.persistence.model.AdChartColumn;
import com.vincomobile.fw.backend.persistence.model.AdChartLinked;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.persistence.beans.AdBaseTools;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.AdChart;
import com.vincomobile.fw.basic.persistence.model.AdChartFilter;
import com.vincomobile.fw.basic.persistence.services.AdMessageService;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.Datetool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.persistence.Query;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 * <p>
 * Service layer implementation for ad_chart
 */
@Repository
@Transactional(readOnly = true)
public class AdChartServiceImpl extends BaseServiceImpl<AdChart, String> implements AdChartService {

    private Logger logger = LoggerFactory.getLogger(AdChartServiceImpl.class);

    private final WebApplicationContext applicationContext;
    private final AdMessageService messageService;
    private final AdChartFilterService chartFilterService;
    private final AdChartLinkedService chartLinkedService;
    private final AdChartColumnService chartColumnService;

    int maxSizeXLabel;

    /**
     * Constructor.
     */
    public AdChartServiceImpl(
            WebApplicationContext applicationContext, AdMessageService messageService, AdChartFilterService chartFilterService,
            AdChartLinkedService chartLinkedService, AdChartColumnService chartColumnService
    ) {
        super(AdChart.class);
        this.applicationContext = applicationContext;
        this.messageService = messageService;
        this.chartFilterService = chartFilterService;
        this.chartLinkedService = chartLinkedService;
        this.chartColumnService = chartColumnService;
    }


    /**
     * Evaluate a chart
     *
     * @param idClient    Client identifier (executor)
     * @param idChart     Chart identifier
     * @param idLanguage  Language identifier
     * @param constraints Constraints
     * @return Chart values
     */
    @Override
    public AdChartResult evaluate(String idClient, String idChart, String idLanguage, Map<String, Object> constraints) {
        return evaluate(idClient, idChart, idLanguage, constraints, null, null);
    }

    /**
     * Evaluate a chart
     *
     * @param idClient    Client identifier (executor)
     * @param idChart     Chart identifier
     * @param idLanguage  Language identifier
     * @param constraints Constraints
     * @return Chart values
     */
    @Override
    public AdChartResult evaluate(String idClient, String idChart, String idLanguage, Map<String, Object> constraints, Integer page, String sort) {
        AdChartResult result = new AdChartResult();
        AdChart chart = findById(idChart);
        if (chart != null) {
            logger.debug("Chart: " + chart.getName());
            if (AdChartService.TYPE_LIST.equals(chart.getCtype())) {
                if (page == null)
                    page = 1;
                return this.evaluateList(chart, idClient, idLanguage, constraints, page);
            }
            maxSizeXLabel = Converter.getInt(CacheManager.getPreferenceString(chart.getIdClient(), PREF_CHART_MAX_SIZE_X_LABEL), 50);
            List<AdChartFilter> filters = chartFilterService.getChartFilters(chart.getIdClient(), chart.getIdTab());
            result.setChart(chart);
            if (!Converter.isEmpty(chart.getValue2())) {
                result.setItems(2);
                if (!Converter.isEmpty(chart.getValue3())) {
                    result.setItems(3);
                    if (!Converter.isEmpty(chart.getValue4())) {
                        result.setItems(4);
                        if (!Converter.isEmpty(chart.getValue5())) {
                            result.setItems(5);
                            if (!Converter.isEmpty(chart.getValue6())) {
                                result.setItems(6);
                                if (!Converter.isEmpty(chart.getValue7())) {
                                    result.setItems(7);
                                    if (!Converter.isEmpty(chart.getValue8())) {
                                        result.setItems(8);
                                        if (!Converter.isEmpty(chart.getValue9())) {
                                            result.setItems(9);
                                            if (!Converter.isEmpty(chart.getValue10())) {
                                                result.setItems(10);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            List<AdChartLinked> chartLinkeds = chartLinkedService.getChartLinkeds(chart.getIdClient(), chart.getIdChart());
            AdChartInfo chartInfo = new AdChartInfo();
            if (AdChartService.MODE_CUSTOM.equals(chart.getMode())) {
                Method mExecute = null;
                ChartDefinition chartClass = null;
                try {
                    // Instantiate a chart class
                    chartClass = (ChartDefinition) applicationContext.getBean(chart.getClassname());
                    chartClass.setIdClient(idClient);
                    chartClass.setIdLanguage(idLanguage);
                    chartClass.setChart(chart);
                    chartClass.setFilters(filters);
                    chartClass.setChartLinkeds(chartLinkeds);
                    chartClass.setChartInfo(chartInfo);
                    chartClass.setChartResult(result);
                    chartClass.setConstraints(constraints);
                    Method[] allMethods = chartClass.getClass().getDeclaredMethods();
                    for (final Method m : allMethods) {
                        if (m.getName().equals(chart.getClassmethod())) {
                            mExecute = m;
                        }
                    }
                } catch (Exception e) {
                    String error = "Error loading class: " + e.getMessage();
                    logger.error(error);
                }
                try {
                    if (mExecute != null) {
                        mExecute.invoke(chartClass);
                    } else {
                        logger.error("Not found method: '" + chart.getClassmethod() + "' in class: '" + chart.getClassname() + "'");
                    }
                } catch (Exception e) {
                    String error = "Execution error (" + chart.getClassname() + "." + chart.getClassmethod() +"): " + e.getMessage();
                    logger.error(error);
                }
            } else {
                ChartQuery chartQuery = getQuery(idClient, chart, filters, chartLinkeds, constraints, chartInfo, idLanguage);
                Query query = entityManager.createNativeQuery(chartQuery.getStandardQuery());
                List<Object[]> queryResult = query.getResultList();
                fillResult(chart, queryResult, result, false, chartInfo, idLanguage, null);
                result.setTitleX(chart.getTitleX());
                String mode = chart.getMode();
                if (mode.startsWith(AdChartService.MODE_DATE_COMPARE)) {
                    String lengend = messageService.getMessage(
                            chart.getIdClient(), idLanguage,
                            AdChartService.MODE_DATE_COMPARE_WEEK.equals(mode) ? "AD_ChartTitleDatePrevWeek" :
                            AdChartService.MODE_DATE_COMPARE_MONTH.equals(mode) ? "AD_ChartTitleDatePrevMonth" :
                            AdChartService.MODE_DATE_COMPARE_QUARTER.equals(mode) ? "AD_ChartTitleDatePrevQuarter" :
                            AdChartService.MODE_DATE_COMPARE_SEMESTER.equals(mode) ? "AD_ChartTitleDatePrevSemester" : "AD_ChartTitleDatePrevYear"
                    );
                    int items = result.getItems();
                    for (int i = 0; i < items; i++) {
                        result.setLegend(items + i + 1, lengend);
                        result.setColor(items + i + 1, chart.getCompareColor(i + 1));
                    }
                    query = entityManager.createNativeQuery(chartQuery.getCompareQuery());
                    queryResult = query.getResultList();
                    fillResult(chart, queryResult, result, true, chartInfo, idLanguage, null);
                    result.setItems(items * 2);
                }
                HookManager.executeHook(FWConfig.HOOK_CHART_EVALUATE, idClient, idLanguage, chart, result);
            }
        }
        return result;
    }


    /**
     * Evaluate a chart
     *
     * @param chart       Chart
     * @param idClient    Client identifier
     * @param idLanguage  Language identifier
     * @param constraints Constraints
     * @param page        Page
     * @return Chart values
     */
    private AdChartResult evaluateList(AdChart chart, String idClient, String idLanguage, Map<String, Object> constraints, int page) {
        AdChartResult result = new AdChartResult();
        logger.debug("Chart: " + chart.getName());
        maxSizeXLabel = Converter.getInt(CacheManager.getPreferenceString(chart.getIdClient(), PREF_CHART_MAX_SIZE_X_LABEL), 50);
        List<AdChartFilter> filters = chartFilterService.getChartFilters(chart.getIdClient(), chart.getIdTab());
        result.setChart(chart);

        List<AdChartLinked> chartLinkeds = chartLinkedService.getChartLinkeds(chart.getIdClient(), chart.getIdChart());
        AdChartInfo chartInfo = new AdChartInfo();
        List<AdChartColumn> columns = chartColumnService.findChartColumns(chart.getIdChart());
        Query query = entityManager.createNativeQuery(getListQuery(idClient, chart, filters, chartLinkeds, constraints, page, columns, false));
        List<Object[]> queryResult = query.getResultList();
        Query queryCount = entityManager.createNativeQuery(getListQuery(idClient, chart, filters, chartLinkeds, constraints, page, columns, true));
        List<Object> queryCountResult = queryCount.getResultList();
        BigInteger total = ((BigInteger) (queryCountResult.get(0)));
        fillResult(chart, queryResult, result, false, chartInfo, idLanguage, columns);
        result.setItems(total.intValue());
        HookManager.executeHook(FWConfig.HOOK_CHART_EVALUATE, idClient, idLanguage, chart, result);
        return result;
    }

    /**
     * Fill result with database result set
     *
     * @param chart Chart information
     * @param queryResult Database result set
     * @param result Result to be filled
     * @param compareDate Is compare date serie or not
     * @param chartInfo Runtime chart information
     * @param idLanguage Language identifier
     * @param columns Columns list
     */
    private void fillResult(AdChart chart, List<Object[]> queryResult, AdChartResult result, boolean compareDate, AdChartInfo chartInfo, String idLanguage, List<AdChartColumn> columns) {
        String mode = chart.getMode();
        String ctype = chart.getCtype();
        Long row = 0L, limit = chart.getRowLimit();
        AdChartValue sum = new AdChartValue("sum-rest-values");

        if (AdChartService.MODE_NORMAL.equals(mode) || AdChartService.TYPE_LIST.equals(ctype)) {
            if (AdChartService.TYPE_EASY_PIE.equals(ctype)) {
                for (Object[] item : queryResult) {
                    AdChartValue value = new AdChartValue("none");
                    AdChartTools.setChartValue(value, item, result.getItems(), 0, 0);
                    result.addValue(value);
                    break;
                }
            } else {
                for (Object[] item : queryResult) {
                    row++;
                    AdChartValue value = new AdChartValue(AdBaseTools.getString(item, 0));

                    if (!AdChartService.TYPE_LIST.equals(ctype)) {
                        value.setX(Converter.truncText(AdBaseTools.getString(item, 1), maxSizeXLabel));
                        value.setFullX(AdBaseTools.getString(item, Converter.isEmpty(chart.getFullnameField()) ? 1 : 2));
                        AdChartTools.setChartValue(value, item, result.getItems(), 0, 3);
                    } else {
                        if (AdChartService.MODE_DATE.equals(mode))
                            value.setId(Converter.truncText(Converter.formatDate(AdBaseTools.getDate(item, 0), "dd/MM/yyyy'T'HH:mm:ss"), maxSizeXLabel));
                        AdChartTools.setChartValue(value, item, columns);
                    }
                    if (!Converter.isEmpty(limit) && limit < row) {
                        sum.add(value);
                    } else {
                        result.addValue(value);
                    }
                }
            }
        } else if (mode.startsWith(AdChartService.MODE_DATE)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(chartInfo.getStartDay());
            result.setYear(cal.get(Calendar.YEAR));
            if (chartInfo.getDays() > WEEK_MAX_DAYS && chartInfo.getDays() < MONTH_MAX_DAYS) {
                if (!compareDate) {
                    int startWeek = Datetool.getWeek(chartInfo.getStartDay());
                    int endWeek = Datetool.getWeek(chartInfo.getEndDay());
                    result.setDateMode(DATE_MODE_WEEK);
                    for (int i = startWeek; i <= endWeek; i++) {
                        AdChartValue value = new AdChartValue("" + i);
                        value.setX("" + i);
                        value.setFullX("" + i);
                        result.addValue(value);
                    }
                }
                for (Object[] item : queryResult) {
                    AdChartValue value = AdChartTools.findChartValue(result.getValues(), AdChartTools.getItemId(mode, DATE_MODE_WEEK, item, compareDate));
                    AdChartTools.setChartValue(value, item, result.getItems(), compareDate ? result.getItems() : 0, 1);
                }
            } else {
                if (chartInfo.getDays() <= MONTH_MAX_DAYS) {
                    if (!compareDate) {
                        result.setDateMode(DATE_MODE_DAY);
                        Date date = chartInfo.getStartDay();
                        for (int i = 0; i <= chartInfo.getDays(); i++) {
                            AdChartValue value = new AdChartValue(Converter.formatDate(date, "yyyyMMdd"));
                            value.setX(Converter.formatDate(date, "dd/MM"));
                            value.setFullX(Converter.formatDate(date, "dd/MM/yyyy"));
                            date = Datetool.getNextDay(date);
                            result.addValue(value);
                        }
                    }
                    for (Object[] item : queryResult) {
                        AdChartValue value = AdChartTools.findChartValue(result.getValues(), AdChartTools.getItemId(mode, DATE_MODE_DAY, item, compareDate));
                        AdChartTools.setChartValue(value, item, result.getItems(), compareDate ? result.getItems() : 0, 3);
                    }
                } else {
                    if (!compareDate) {
                        result.setDateMode(DATE_MODE_MONTH);
                        Date date = chartInfo.getStartDay();
                        for (int i = 0; i < chartInfo.getMonths(); i++) {
                            AdChartValue value = new AdChartValue(Converter.formatDate(date, "yyyyMM"));
                            String monthName = messageService.getMessage(chart.getIdClient(), idLanguage, "AD_MonthName_" + Datetool.getMonth(date));
                            value.setX(monthName.substring(0, 3));
                            value.setFullX(monthName);
                            date = Datetool.getNextMonth(date);
                            result.addValue(value);
                        }
                    }
                    for (Object[] item : queryResult) {
                        AdChartValue value = AdChartTools.findChartValue(result.getValues(), AdChartTools.getItemId(mode, DATE_MODE_MONTH, item, compareDate));
                        AdChartTools.setChartValue(value, item, result.getItems(), compareDate ? result.getItems() : 0, 2);
                    }
                }
            }
        } else if (AdChartService.MODE_HOURS.equals(mode)) {
            for (int i = 0; i <= 24; i++) {
                String id = Converter.leftFill(i, '0', 2);
                AdChartValue value = new AdChartValue(id);
                value.setX(id);
                value.setFullX(id);
                result.addValue(value);
            }
            for (Object[] item : queryResult) {
                AdChartValue value = AdChartTools.findChartValue(result.getValues(), Converter.leftFill(AdBaseTools.getLong(item, 0), '0', 2));
                AdChartTools.setChartValue(value, item, result.getItems(), 0, 1);
            }
        }

        if (AdChartService.TYPE_PIE.equals(ctype) && !Converter.isEmpty(limit) && limit < row) {
            String others = messageService.getMessage(chart.getIdClient(), idLanguage, "AD_ChartPieOthers");
            sum.setX(others);
            sum.setFullX(others);
            result.addValue(sum);
        }
    }

    /**
     * Get query for chart values
     *
     * @param idClient     Client identifier (executor)
     * @param chart        Chart object
     * @param filters      Tab Chart filters
     * @param chartLinkeds Chart linkeds
     * @param constraints  Real constrains
     * @param chartInfo    Runtime chart information
     * @param idLanguage   Language identifier
     * @return Query
     */
    private ChartQuery getQuery(String idClient, AdChart chart, List<AdChartFilter> filters, List<AdChartLinked> chartLinkeds, Map<String, Object> constraints, AdChartInfo chartInfo, String idLanguage) {
        String select = "";
        String where = ChartUtils.appendConditionalExpression(chart.getXwhere(), constraints);
        where = where.replaceAll("@idClient", idClient);
        String groupby = "GROUP BY ";
        String orderby = "ORDER BY ";
        String limit = "";
        String prefix = chart.getPrefix();
        if (prefix == null) {
            prefix = "";
        }
        // Depend on mode
        String mode = chart.getMode();
        ChartWhere chartWhere = new ChartWhere(mode, where);
        chartWhere.replaceParameters(constraints, filters, chartLinkeds);
        if (AdChartService.MODE_NORMAL.equals(mode)) {
            if (AdChartService.TYPE_EASY_PIE.equals(chart.getCtype())) {
                groupby = "";
                orderby = "";
            } else {
                select += prefix + chart.getKeyField() + ", ";
                select += (Converter.isEmpty(chart.getNameField()) ? "'-'" : chart.getNameField()) + ", ";
                select += (Converter.isEmpty(chart.getFullnameField()) ? "'-'" : chart.getFullnameField()) + ", ";
                groupby += "1, 2, 3";
            }
        } else if (mode.startsWith(AdChartService.MODE_DATE) || AdChartService.MODE_HOURS.equals(mode)) {
            if (!Converter.isEmpty(chart.getNameField())) {
                Date begin = Converter.getDate((String) constraints.get(chart.getNameField() + ".begin"));
                Date end = Converter.getDate((String) constraints.get(chart.getNameField() + ".end"));
                int days = Datetool.getDaysBetween(begin, end);
                if (mode.startsWith(AdChartService.MODE_DATE)) {
                    if (days > WEEK_MAX_DAYS && days < MONTH_MAX_DAYS) {
                        select += "week(" + prefix + chart.getKeyField() + "), ";
                        groupby += "1";
                        orderby += "1";
                        chart.setTitleX(messageService.getMessage(chart.getIdClient(), idLanguage, "AD_ChartTitleDateWeeks"));
                    } else {
                        select += "year(" + prefix + chart.getKeyField() + "), month( " + prefix + chart.getKeyField() + "), ";
                        groupby += "1, 2";
                        orderby += "1, 2";
                        if (days <= MONTH_MAX_DAYS) {
                            select += "day(" + prefix + chart.getKeyField() + "), ";
                            groupby += ", 3";
                            orderby += ", 3";
                            chart.setTitleX(messageService.getMessage(chart.getIdClient(), idLanguage, "AD_ChartTitleDateDays"));
                        } else {
                            chartInfo.setMonths(Datetool.getMonthsBetween(begin, end));
                            chart.setTitleX(messageService.getMessage(chart.getIdClient(), idLanguage, "AD_ChartTitleDateMonths"));
                        }
                    }
                } else {
                    select += "hour(" + prefix + chart.getKeyField() + "), ";
                    groupby += "1";
                    orderby += "1";
                }
                chartInfo.setDays(days);
                chartInfo.setStartDay(begin);
                chartInfo.setEndDay(end);
            } else {
                logger.error("Not found: " + chart.getNameField() + " in parameters restrictions");
            }
        } else {
            logger.error("Invalid mode: " + mode);
        }
        // Depend on ctype
        String ctype = chart.getCtype();
        if (AdChartService.TYPE_PIE.equals(ctype)) {
            orderby += "4 DESC";
        } else {
            if (AdChartService.MODE_NORMAL.equals(mode) && !AdChartService.TYPE_EASY_PIE.equals(chart.getCtype())) {
                orderby += "2 ASC";
            }
            if (!Converter.isEmpty(chart.getRowLimit())) {
                limit = "\nLIMIT " + chart.getRowLimit();
            }
        }
        // Values
        String replaceValueParameters = ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue1(), chartInfo);
        if (replaceValueParameters != null)
            select += replaceValueParameters;
        else if (AdChartService.TYPE_PIE.equals(ctype)) {
            select += chart.getValue1();
        } else if (select.endsWith(", ")) {
            select = select.substring(0, select.length() - 2);
            select += " ";
        }
        if (!Converter.isEmpty(chart.getValue2())) {
            select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue2(), chartInfo);
            if (!Converter.isEmpty(chart.getValue3())) {
                select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue3(), chartInfo);
                if (!Converter.isEmpty(chart.getValue4())) {
                    select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue4(), chartInfo);
                    if (!Converter.isEmpty(chart.getValue5())) {
                        select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue5(), chartInfo);
                        if (!Converter.isEmpty(chart.getValue6())) {
                            select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue6(), chartInfo);
                            if (!Converter.isEmpty(chart.getValue7())) {
                                select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue7(), chartInfo);
                                if (!Converter.isEmpty(chart.getValue8())) {
                                    select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue8(), chartInfo);
                                    if (!Converter.isEmpty(chart.getValue9())) {
                                        select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue9(), chartInfo);
                                        if (!Converter.isEmpty(chart.getValue10())) {
                                            select += ", " + ChartUtils.replaceValueParameters(chart.getIdClient(), chart.getValue10(), chartInfo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // FROM
        String from = ChartUtils.appendConditionalExpression(chart.getXfrom(), constraints);

        return new ChartQuery(select, from, chartWhere, groupby, orderby, limit);
    }

    /**
     * Get query for chart values
     *
     * @param idClient     Client identifier (executor)
     * @param chart        Chart object
     * @param filters      Tab Chart filters
     * @param chartLinkeds Chart linkeds
     * @param constraints  Real constrains
     * @param columns      Columns list
     * @param isCount      Checks if the generated query should be a Count(*) query  @return Query
     */
    private String getListQuery(String idClient, AdChart chart, List<AdChartFilter> filters, List<AdChartLinked> chartLinkeds, Map<String, Object> constraints, int page, List<AdChartColumn> columns, boolean isCount) {
        String select = "SELECT ";
        String where = ChartUtils.appendConditionalExpression(chart.getXwhere(), constraints);
        where = where.replaceAll("@idClient", idClient);
        String orderby = "";
        String limit = "";
        where = ChartUtils.replaceRangedParameters(where, constraints, filters);
        where = ChartUtils.replaceDateFilterParameters(where, constraints, chartLinkeds);
        where = ChartUtils.replaceSingleParameters(where, constraints, chartLinkeds);

        // TODO: Parametros condicionales de tipo fecha, cuando el valor pasado es un mes completo


        // FROM
        String from = "\nFROM " + ChartUtils.appendConditionalExpression(chart.getXfrom(), constraints);

        // GROUP BY
        String groupby = "<none>".equalsIgnoreCase(chart.getNameField()) && Converter.isEmpty(chart.getXgroupby()) ? "" : "\nGROUP BY " + chart.getXgroupby();

        boolean containsKey = false;
        for (AdChartColumn column : columns) {
            select += (ChartUtils.appendConditionalExpression(column.getSqlExpression(), constraints) + ", ");
            if (column.getSqlExpression().contains(chart.getKeyField())) {
                containsKey = true;
            }
        }
        select = ChartUtils.replaceRangedParameters(select, constraints, filters);
        select = ChartUtils.replaceSingleParameters(select, constraints, chartLinkeds);
        if (!containsKey) {
            select += (chart.getPrefix() != null ? chart.getPrefix() : "") + chart.getKeyField() + ", ";
        }
        select = select.substring(0, select.length() - 2);

        // LIMIT
        if (!Converter.isEmpty(chart.getRowLimit())) {
            limit = "\nLIMIT " + chart.getRowLimit() * (page - 1) + ", " + chart.getRowLimit();
        }

        // ORDER BY
        if (chart.getNameField() != null && !"<none>".equalsIgnoreCase(chart.getNameField())) {
            String conditionalName = ChartUtils.appendConditionalExpression(chart.getNameField(), constraints);
            conditionalName = ChartUtils.replaceRangedParameters(conditionalName, constraints, filters);
            conditionalName = ChartUtils.replaceSingleParameters(conditionalName, constraints, chartLinkeds);
            orderby += (" ORDER BY " + conditionalName + " DESC\n");
        }
        String query = select + from + "\nWHERE " + where + "\n" + groupby;
        if (isCount) {
            query = "SELECT COUNT(*) FROM (\n" + query + ") AS CNT";
        } else {
            query += " " + orderby + limit;
        }
        return query;
    }

}
