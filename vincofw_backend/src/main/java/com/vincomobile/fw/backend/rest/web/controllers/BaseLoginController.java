package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.business.AdUserRoleValue;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.exception.FWResourceNotFoundException;
import com.vincomobile.fw.basic.persistence.model.AdRole;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.persistence.model.AdUserRoles;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.rest.security.AuthenticationFacade;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Component
public class BaseLoginController extends BaseUserController {

    private Logger logger = LoggerFactory.getLogger(BaseLoginController.class);

    final protected AdUserService usersService;

    final protected AdRoleService roleService;

    final protected AdUserRolesService userRolesService;

    final protected AuthenticationFacade authentication;

    public BaseLoginController(AdUserService usersService, AdRoleService roleService, AdUserRolesService userRolesService, AuthenticationFacade authentication) {
        this.usersService = usersService;
        this.roleService = roleService;
        this.userRolesService = userRolesService;
        this.authentication = authentication;
    }

    @Override
    public BaseService getService() {
        throw new FWResourceNotFoundException();
    }

    /**
     * Login
     *
     * @param idClient Client identifier
     * @param login Login name
     * @return ControllerResult
     */
    protected ControllerResult login(String idClient, String login) {
        ControllerResult result = new ControllerResult();
        result.put("menu", null);
        result.put("user", null);
        result.setSuccess(false);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() != null) {
            AdUser user = userService.findByLogin(idClient, login);
            if (user != null && user.getActive() != null && user.getActive()) {
                setUserPhoto(user);
                result.put("userName", user.getName());
                result.put("idUserLogged", user.getIdUser());
                result.put("user", user);
            }
            loadRoles(result, user);
        }
        return result;
    }

    protected ControllerResult login(String login) {
        return login(null, login);
    }

    /**
     * Set user photo
     *
     * @param user User
     */
    protected void setUserPhoto(AdUser user) {
        String apacheServerCacheUrl = CacheManager.getPreferenceString(user.getIdClient(), AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (Converter.isEmpty(user.getPhoto())) {
            user.setPhotoUrl("app/extensions/vinco_core/images/user_default.png");
        } else {
            user.setPhotoUrl(apacheServerCacheUrl + user.getUpdated().getTime() + "/files/ad_user/" + user.getId() + "/" + user.getPhoto());
        }
    }

    /**
     * Login again
     *
     * @return ControllerResult
     */
    protected ControllerResult doRelogin() {
        return new ControllerResult();
    }

    /**
     * Logout
     *
     * @param httpRequest HTTP Request
     * @return ControllerResult
     */
    protected ControllerResult doLogout(HttpServletRequest httpRequest) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
/*
        if (principal instanceof User) {
            FWConfig.session.removeUser(((User) principal).getUsername(), httpRequest.getSession().getId());
        }
*/
        return new ControllerResult();
    }

    /**
     * Change default user role
     *
     * @param roleId Role identifier
     * @return AdUserRoles
     */
    protected AdUserRoles doChangeRole(String roleId) {
        AdRole role = roleService.findById(roleId);
        RestPreconditions.checkRequestElementNotNull(role);
        AdUser user = usersService.findByLogin(authentication.getMyUser().getUsername());
        user.setDefaultIdRole(roleId);
        usersService.save(user);
        return userRolesService.findUserRole(user.getIdUser(), roleId);
    }

    /**
     * Loads the user roles
     *
     * @param result Controller result
     * @param user   Principal
     */
    protected void loadRoles(ControllerResult result, AdUser user) {
        if (user != null) {
            List<AdUserRoles> rolesOfUser = user.getUserRoles();
            List<AdUserRoleValue> roles = new ArrayList<>();
            for (AdUserRoles userRole : rolesOfUser) {
                roles.add(new AdUserRoleValue(userRole.getIdClient(), userRole.getRole()));
            }
            result.put("isUser", true);
            result.setSuccess(true);
            result.put("roles", roles);
        } else {
            result.setError("AD_NotFoundUser");
        }
    }

}