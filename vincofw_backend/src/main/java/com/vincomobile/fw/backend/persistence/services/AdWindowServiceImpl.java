package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.dto.AdWindowDto;
import com.vincomobile.fw.basic.persistence.model.AdWindow;
import com.vincomobile.fw.basic.persistence.services.AdClientService;
import com.vincomobile.fw.basic.persistence.services.AdTabService;
import com.vincomobile.fw.basic.persistence.services.AdTranslationService;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_WINDOW
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdWindowServiceImpl extends BaseServiceImpl<AdWindow, String> implements AdWindowService {

    private final AdClientService clientService;
    private final AdTabService tabService;
    private final AdTranslationService translationService;

    /**
     * Constructor.
     *
     */
    public AdWindowServiceImpl(AdClientService clientService, AdTabService tabService, AdTranslationService translationService) {
        super(AdWindow.class);
        this.clientService = clientService;
        this.tabService = tabService;
        this.translationService = translationService;
    }

    /**
     * Load meta data for a window (load first tab)
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idWindow Window identifier
     * @param idLanguage Language identifier
     * @return Window meta data
     */
    @Override
    public AdWindowDto load(String idClient, String idUser, String idWindow, String idLanguage) {
        AdWindow item = findById(idWindow);
        if (item != null) {
            AdGUIDto gui = getWindowGUI(idClient, item);
            AdWindowDto result = new AdWindowDto(
                    item,
                    translationService.getTranslation(idClient, FWConfig.TABLE_ID_WINDOW, FWConfig.COLUMN_ID_WINDOW_NAME, idLanguage, item.getIdWindow(), item.getName())
            );
            result.setMainTab(tabService.loadMainTab(idClient, idUser, idWindow, null, idLanguage, gui));
            return result;
        }
        return null;
    }

    /**
     * Get Window GUI
     *
     * @param idClient Client identifier
     * @param idWindow Window identifier
     * @return GUI
     */
    @Override
    public AdGUIDto getWindowGUI(String idClient, String idWindow) {
        return getWindowGUI(idClient, findById(idWindow));
    }

    /**
     * Get Window GUI
     *
     * @param idClient Client identifier
     * @param item Window
     * @return GUI
     */
    private AdGUIDto getWindowGUI(String idClient, AdWindow item) {
        AdGUIDto gui = clientService.getClientGUI(idClient);
        AdGUIDto result = gui.clone();
        result.merge(item);
        return result;
    }

}
