package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.annotations.FWProcess;
import org.springframework.stereotype.Component;

@Component
@FWProcess
public class TestingProcess1 extends TestingBaseProcess {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static TestingProcess1 service = new TestingProcess1();

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Execute test
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void test(String idProcessExec) {
        testProcess(idProcessExec, false);
    }

}