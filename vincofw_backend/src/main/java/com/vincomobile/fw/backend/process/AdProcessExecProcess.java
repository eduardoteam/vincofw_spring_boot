package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.backend.persistence.services.AdProcessExecService;
import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@FWProcess
public class AdProcessExecProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdProcessExecProcess service = new AdProcessExecProcess();

    @Autowired
    AdProcessExecService processExecService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return true;
    }

    /**
     * Clear tables for process execution and execution logs
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void clearProcessExecution(String idProcessExec) {
        AdProcessParam daysParam = getParam("days");
        if (daysParam == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "days"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        int rows = processExecService.clearProcessExec(Converter.getInt((String) daysParam.getValue()));
        info(idProcessExec, getMessage("AD_ProcessProcessExecClear", rows));
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

}