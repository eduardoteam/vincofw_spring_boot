package com.vincomobile.fw.backend.sync;

import com.vincomobile.fw.backend.persistence.model.AdColumnSync;
import com.vincomobile.fw.backend.persistence.services.AdRefSequenceService;

import javax.persistence.EntityManager;
import java.util.List;

public class ActionTrigger_Impl implements ActionTrigger {

    protected EntityManager entityManager;
    protected AdRefSequenceService sequenceService;

    /**
     * Inject DB manager
     *
     * @param entityManager Manager
     */
    @Override
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Inject Sequence service  manager
     *
     * @param sequenceService Manager
     */
    @Override
    public void setSequenceService(AdRefSequenceService sequenceService) {
        this.sequenceService = sequenceService;
    }

    /**
     * Triggers called for UPDATE
     *
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    @Override
    public boolean beforeUpdate(List<AdColumnSync> columns) {
        return true;
    }

    @Override
    public void afterUpdate(List<AdColumnSync> columns) {
    }

    /**
     * Triggers called for INSERT
     *
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    @Override
    public boolean beforeInsert(List<AdColumnSync> columns) {
        return true;
    }

    @Override
    public void afterInsert(List<AdColumnSync> columns) {
    }

    /**
     * Triggers called for DELETE
     *
     * @param columns Table columns (value = new, old_value = value stored in DB)
     * @return If action can be done
     */
    @Override
    public boolean beforeDelete(List<AdColumnSync> columns) {
        return true;
    }

    @Override
    public void afterDelete(List<AdColumnSync> columns) {
    }

    /**
     * Search a column
     *
     * @param columns Table columns
     * @param name Column name
     * @return Column
     */
    protected AdColumnSync getColumn(List<AdColumnSync> columns, String name) {
        for (AdColumnSync column : columns) {
            if (name.equals(column.getName()))
                return column;
        }
        return null;
    }
}
