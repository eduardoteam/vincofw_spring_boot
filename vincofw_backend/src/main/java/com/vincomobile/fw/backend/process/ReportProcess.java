package com.vincomobile.fw.backend.process;

public interface ReportProcess extends ProcessDefinition {

    /**
     * Generates report files
     *
     * @param idProcessExec Process Execution Identifier
     */
    void executeReport(String idProcessExec);
}
