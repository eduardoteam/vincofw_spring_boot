package com.vincomobile.fw.backend.persistence.model;

import com.vincomobile.fw.basic.tools.Converter;

import java.lang.reflect.Method;

/**
 * Created by Devtools.
 * Modelo de ad_offline_device_log
 *
 * Date: 06/03/2016
 */
public class AdDeviceSynchro {

    private String idOfflineDevice;
    private String userName;
    private String device;
    private String version;
    private Long d1;
    private Long d2;
    private Long d3;
    private Long d4;
    private Long d5;
    private Long d6;
    private Long d7;

    /**
     * Set a value for property
     *
     * @param name Property name (CamelCase)
     * @param value Value
     */
    public void setPropertyValue(String name, Object value) {
        name = Converter.capitalize(name);
        Method[] allMethods = this.getClass().getDeclaredMethods();
        for (Method m : allMethods) {
            String mthName = m.getName();
            if (mthName.equals("set" + name)) {
                try {
                    m.setAccessible(true);
                    m.invoke(this, value);
                } catch (Exception e) {
                    System.out.println("Error calling: " + this.getClass().getName() + "." + m.getName());
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * Set/Get Methods
     */

    public String getIdOfflineDevice() {
        return idOfflineDevice;
    }

    public void setIdOfflineDevice(String idOfflineDevice) {
        this.idOfflineDevice = idOfflineDevice;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getD1() {
        return d1;
    }

    public void setD1(Long d1) {
        this.d1 = d1;
    }

    public Long getD2() {
        return d2;
    }

    public void setD2(Long d2) {
        this.d2 = d2;
    }

    public Long getD3() {
        return d3;
    }

    public void setD3(Long d3) {
        this.d3 = d3;
    }

    public Long getD4() {
        return d4;
    }

    public void setD4(Long d4) {
        this.d4 = d4;
    }

    public Long getD5() {
        return d5;
    }

    public void setD5(Long d5) {
        this.d5 = d5;
    }

    public Long getD6() {
        return d6;
    }

    public void setD6(Long d6) {
        this.d6 = d6;
    }

    public Long getD7() {
        return d7;
    }

    public void setD7(Long d7) {
        this.d7 = d7;
    }
}

