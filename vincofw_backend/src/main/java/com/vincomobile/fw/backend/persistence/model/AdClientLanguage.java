package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import com.vincomobile.fw.basic.persistence.model.AdLanguage;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 07/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_client_language
 */
@Entity
@Table(name = "ad_client_language")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdClientLanguage extends AdEntityBean {

    private String idClientLanguage;
    private String idLanguage;
    private Long seqno;

    private AdLanguage language;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idClientLanguage;
    }

    @Override
    public void setId(String id) {
            this.idClientLanguage = id;
    }

    @Id
    @Column(name = "id_client_language")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdClientLanguage() {
        return idClientLanguage;
    }

    public void setIdClientLanguage(String idClientLanguage) {
        this.idClientLanguage = idClientLanguage;
    }

    @Column(name = "id_language", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_language", referencedColumnName = "id_language", insertable = false, updatable = false)
    public AdLanguage getLanguage() {
        return language;
    }

    public void setLanguage(AdLanguage language) {
        this.language = language;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdClientLanguage)) return false;

        final AdClientLanguage that = (AdClientLanguage) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idClientLanguage == null) && (that.idClientLanguage == null)) || (idClientLanguage != null && idClientLanguage.equals(that.idClientLanguage)));
        result = result && (((idLanguage == null) && (that.idLanguage == null)) || (idLanguage != null && idLanguage.equals(that.idLanguage)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

    @Transient
    public String getName() {
        return language.getName();
    }
}

