package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.persistence.beans.AdProcessExec;
import com.vincomobile.fw.basic.persistence.beans.AdProcessLog;

import java.util.ArrayList;
import java.util.List;

public class ProcessLogger {

    List<AdProcessExec> logItems = new ArrayList<>();

    public synchronized long count() {
        return logItems.size();
    }

    public synchronized void addProcessExec(AdProcessExec item) {
        logItems.add(item);
    }

    public synchronized void addProcessLog(AdProcessExec processExec, AdProcessLog item) {
        processExec.getLogs().add(item);
    }

    public synchronized List<AdProcessExec> getItems() {
        List<AdProcessExec> items = new ArrayList<>();
        for (AdProcessExec processExec : logItems) {
            AdProcessExec newItem = processExec.duplicate();
            processExec.setSaved(true);
            processExec.getLogs().clear();
            items.add(newItem);
        }
        return items;
    }

    public synchronized AdProcessExec getProcessExec(String idProcessExec) {
        for (AdProcessExec processExec : logItems) {
            if (idProcessExec.equals(processExec.getIdProcessExec()))
                return processExec;
        }
        return null;
    }

    public synchronized AdProcessExec getProcess(String idProcess) {
        for (AdProcessExec processExec : logItems) {
            if (idProcess.equals(processExec.getIdProcess()))
                return processExec;
        }
        return null;
    }

    public synchronized void clearProcessExec() {
        int i = 0;
        while (i < logItems.size()) {
            AdProcessExec processExec = logItems.get(i);
            if (!ProcessDefinition.STATUS_RUNNING.equals(processExec.getStatus()) && processExec.getLogs().size() == 0) {
                logItems.remove(processExec);
            } else {
                i++;
            }
        }
    }
}
