package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdSampledataTable;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_sampledata_table
 */
public interface AdSampledataTableService extends BaseService<AdSampledataTable, String> {

    /**
     * List active tables for sample data
     *
     * @param idSampledata Sample data identifier
     * @return Active tables list
     */
    List<AdSampledataTable> listTables(String idSampledata);
}


