package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import com.vincomobile.fw.basic.persistence.model.AdProcess;
import com.vincomobile.fw.basic.persistence.model.AdWindow;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de AD_MENU
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_menu")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdMenu extends AdEntityBean {

    private String idMenu;
    private String value;
    private String name;
    private String action;
    private String parentMenu;
    private String icon;
    private Long seqno;
    private String idWindow;
    private String idProcess;
    private String manualCommand;

    private AdWindow window;
    private AdProcess process;
    private String submenuName;
    private List<AdMenu> subMenus = new ArrayList<>();

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idMenu;
    }

    @Override
    public void setId(String id) {
            this.idMenu = id;
    }

    @Id
    @Column(name = "id_menu")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    @Column(name = "value", length = 45, unique = true)
    @Size(min = 1, max = 45)
    @NotNull
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "action", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "parent_menu", length = 45)
    @Size(min = 1, max = 45)
    public String getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(String parentMenu) {
        this.parentMenu = parentMenu;
    }

    @Column(name = "icon", length = 100)
    @Size(min = 1, max = 100)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "id_window")
    public String getIdWindow() {
        return idWindow;
    }

    public void setIdWindow(String idWindow) {
        this.idWindow = idWindow;
    }

    @Column(name = "id_process")
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "manual_command", length = 60)
    @Size(min = 1, max = 60)
    public String getManualCommand() {
        return manualCommand;
    }

    public void setManualCommand(String manualCommand) {
        this.manualCommand = manualCommand;
    }

    @Transient
    public String getSubmenuName() {
        return submenuName;
    }

    public void setSubmenuName(String submenuName) {
        this.submenuName = submenuName;
    }

    @Transient
    public List<AdMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<AdMenu> subMenus) {
        this.subMenus = subMenus;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "id_window", referencedColumnName = "id_window", insertable = false, updatable = false)
    public AdWindow getWindow() {
        return window;
    }

    public void setWindow(AdWindow window) {
        this.window = window;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "id_process", referencedColumnName = "id_process", insertable = false, updatable = false)
    public AdProcess getProcess() {
        return process;
    }

    public void setProcess(AdProcess process) {
        this.process = process;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdMenu)) return false;

        final AdMenu that = (AdMenu) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idMenu == null) && (that.idMenu == null)) || (idMenu != null && idMenu.equals(that.idMenu)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((action == null) && (that.action == null)) || (action != null && action.equals(that.action)));
        result = result && (((parentMenu == null) && (that.parentMenu == null)) || (parentMenu != null && parentMenu.equals(that.parentMenu)));
        result = result && (((icon == null) && (that.icon == null)) || (icon != null && icon.equals(that.icon)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((idWindow == null) && (that.idWindow == null)) || (idWindow != null && idWindow.equals(that.idWindow)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((manualCommand == null) && (that.manualCommand == null)) || (manualCommand != null && manualCommand.equals(that.manualCommand)));
        return result;
    }

}

