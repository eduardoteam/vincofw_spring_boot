package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.persistence.beans.AdProcessLog;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;

import java.util.Date;

/**
 * Bean to store the event data to be sent to the client
 */
public class ProcessEvent {

    private AdProcessLog adProcessLog;
    private boolean finish;
    private boolean log;
    private String idProcess;
    private String outParamName;
    private Object outParamValue;
    private boolean success;
    private String info;
    private Date date;

    /**
     * Constructor
     *
     * @param adProcessLog Event log
     */
    public ProcessEvent(AdProcessLog adProcessLog) {
        this.adProcessLog = adProcessLog;
        this.finish = false;
        this.log = true;
    }

    /**
     * Constructor
     *
     * @param param Process out parameter
     */
    public ProcessEvent(AdProcessParam param) {
        outParamName = param.getName();
        outParamValue = param.getValue();
        this.finish = false;
        this.log = false;
    }

    /**
     * Constructor
     *
     * @param idProcess Process identifier
     * @param info Log information
     * @param success Success or error
     */
    public ProcessEvent(String idProcess, String info, boolean success) {
        this.idProcess = idProcess;
        this.info = info;
        this.success = success;
        this.date = new Date();
    }

    /**
     * Constructor
     *
     * @param info Log information
     * @param success Success or error
     */
    public ProcessEvent(String info, boolean success) {
        this.info = info;
        this.success = success;
        this.date = new Date();
    }

    /**
     * Default constructor
     */
    public ProcessEvent() {

    }

    public AdProcessLog getAdProcessLog() {
        return adProcessLog;
    }

    public void setAdProcessLog(AdProcessLog adProcessLog) {
        this.adProcessLog = adProcessLog;
    }

    public boolean getFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public boolean getLog() {
        return log;
    }

    public void setLog(boolean log) {
        this.log = log;
    }

    public String getOutParamName() {
        return outParamName;
    }

    public void setOutParamName(String outParamName) {
        this.outParamName = outParamName;
    }

    public Object getOutParamValue() {
        return outParamValue;
    }

    public void setOutParamValue(Object outParamValue) {
        this.outParamValue = outParamValue;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }
}
