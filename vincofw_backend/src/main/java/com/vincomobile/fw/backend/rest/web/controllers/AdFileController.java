package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.persistence.model.AdFile;
import com.vincomobile.fw.backend.persistence.services.AdFileService;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.services.AdPreferenceService;
import com.vincomobile.fw.basic.persistence.services.AdTableService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * Created by Devtools.
 * Controller for table ad_file
 *
 * Date: 23/01/2016
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_file/{idClient}")
public class AdFileController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdFileController.class);

    @Autowired
    AdFileService service;

    @Autowired
    AdTableService tableService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdFile> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        if (Converter.isEmpty(sort)) {
            sort = "[{\"property\":\"seqno\",\"direction\":\"ASC\"},{\"property\":\"created\",\"direction\":\"DESC\"},{\"property\":\"caption\",\"direction\":\"ASC\"}]";
        }
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdFile.class);
        Page<AdFile> result = service.findAll(pageReq);
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (apacheServerCacheUrl != null && result.getContent().size() > 0) {
            AdFile entity = result.getContent().get(0);
            AdTable table = tableService.findById(entity.getIdTable());
            for (AdFile item : result.getContent()) {
                item.setFileUrl(apacheServerCacheUrl + item.getUpdated().getTime() + "/files/ad_file/" + table.getName() + "/" + item.getIdRow() + "/" + item.getName());
            }
        }
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdFile get(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("GET get("+id+")");
        AdFile entity = RestPreconditions.checkNotNull(service.findById(id));
        String apacheServerCacheUrl = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE_URL);
        if (apacheServerCacheUrl != null) {
            AdTable table = tableService.findById(entity.getIdTable());
            entity.setFileUrl(apacheServerCacheUrl + entity.getUpdated().getTime() + "/files/ad_file/" + table.getName() + "/" + entity.getIdRow() + "/" + entity.getName());
        }
        return entity;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdFile entity,
            @RequestParam(value = "file_name") MultipartFile name,
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        ControllerResult valid = saveFile(idClient, entity, name);
        if (!valid.isSuccess()) {
            throwError(valid, bindingResults);
        }
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @PathVariable("idClient") String idClient,
            @RequestPart("entity") AdFile entity,
            @RequestParam(value = "file_name", required = false) MultipartFile name,
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        AdFile item = service.findById(entity.getId());
        RestPreconditions.checkNotNull(item);
        if (!bindingResults.hasErrors() && name != null) {
            entity.setName(item.getName());
            ControllerResult valid = saveFile(idClient, entity, name);
            if (!valid.isSuccess()) {
                throwError(valid, bindingResults);
            }
        }
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(
            @PathVariable("idClient") String idClient,
            @PathVariable("id") String id
    ) {
        logger.debug("DELETE delete(" + id + ")");
        AdFile entity = service.findById(id);
        RestPreconditions.checkRequestElementNotNull(entity);
        AdTable table = tableService.findById(entity.getIdTable());
        String baserDir = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            FileTool.deleteFile(baserDir + "/files/ad_file/" + table.getName() + "/" + entity.getIdRow() + "/" + entity.getName());
        }
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        for (String id: ids) {
            delete(idClient, id);
        }
        HookManager.executeHook(FWConfig.HOOK_TABLE_DELETE_BATCH, idClient, getAuthUser(), getService(), ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult upload(
            @PathVariable("idClient") String idClient,
            @RequestPart(value = "fileUpload") MultipartFile fileUpload,
            @RequestPart(value = "field") String field,
            @RequestPart(value = "table") String table
    ) {
        logger.debug(String.format("POST upload(%s, %s, %s)", table, field,fileUpload.getOriginalFilename()));
        String path = getUploadFilePath(idClient);
        String fileId = Converter.getUUID();
        String fileName = FileTool.cleanFileName(fileUpload.getOriginalFilename());
        String filePath = path + table + "/" + field + "/" + fileId + "/" + fileName;
        File file = FileTool.saveFile(fileUpload, filePath);
        ControllerResult result = new ControllerResult();
        if (file != null) {
            result.put("id", fileId);
            result.put("name", fileName);
            result.put("field", field);
        } else {
            result.setError(ERROR_FILE_UPLOAD, ERROR_UPLOAD_FILE);
        }
        return result;
    }

    /**
     * Save file to file system
     *
     * @param idClient Client id.
     * @param adFile File information
     * @param file File
     * @return Success or fail
     */
    private ControllerResult saveFile(String idClient, AdFile adFile, MultipartFile file) {
        AdTable table = tableService.findById(adFile.getIdTable());
        ControllerResult result = new ControllerResult();
        if (table != null) {
            adFile.setIdModule(table.getIdModule());
            Long prefMaxSize = preferenceService.getPreferenceLong(table.getCapitalizeStandardName() + "_FileMaxSize", idClient, 1024L);
            result = validFileUpload(file, null, prefMaxSize);
            if (result.isSuccess()) {
                String baserDir = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
                if (baserDir != null) {
                    String fileName = FileTool.cleanFileName(file.getOriginalFilename());
                    File fileFile = new File(baserDir + "/files/ad_file/" + table.getName() + "/" + adFile.getIdRow() + "/" + fileName);
                    boolean res = FileTool.saveFile(file, fileFile.getPath()) != null;
                    if (res) {
                        if (!Converter.isEmpty(adFile.getName()) && !adFile.getName().equals(adFile)) {
                            FileTool.deleteFile(baserDir + "/files/ad_file/" + table.getName() + "/" + adFile.getIdRow() + "/" + adFile.getName());
                        }
                        adFile.setName(fileName);
                        adFile.setMimetype(file.getContentType());
                        adFile.setFilesize(file.getSize());
                    } else {
                        logger.error("Can not save file: " + fileFile.getAbsolutePath());
                        result.setError(ERROR_FILE_UPLOAD, ERROR_UPLOAD_FILE);
                    }
                } else {
                    logger.error("Not found base directory (Preference: " + AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE + ")");
                    result.setError(ERROR_FILE_UPLOAD, ERROR_UPLOAD_FILE);
                }
            } else {
                logger.error("Invalid file: " + result.getMessage());
            }
        } else {
            logger.error("Not found table: " + adFile.getIdTable());
            result.setError("AD_msgErrorFileInvalidTable");
        }
        return result;
    }

}