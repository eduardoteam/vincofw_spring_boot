package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.persistence.model.AdProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.process.OutputParam;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ProcessDefinition {

    String STATUS_RUNNING           = "RUNNING";
    String STATUS_ERROR             = "ERROR";
    String STATUS_SUCCESS           = "SUCCESS";

    String LOG_TYPE_SUCCESS         = "SUCCESS";
    String LOG_TYPE_INFO            = "INFO";
    String LOG_TYPE_WARN            = "WARN";
    String LOG_TYPE_ERROR           = "ERROR";
    String LOG_TYPE_FINISH_SUCCESS  = "FINISH_SUCCESS";
    String LOG_TYPE_FINISH_ERROR    = "FINISH_ERROR";

    String OUTPUT_EMAIL             = "EMAIL";
    String OUTPUT_FILESYSTEM        = "FILESYSTEM";
    String OUTPUT_FTP               = "FTP";
    String OUTPUT_SFTP              = "SFTP";
    String OUTPUT_HTTP              = "HTTP";

    /**
     * Set user executor
     *
     * @param user User
     */
    void setUser(AdUser user);

    /**
     * Set client id
     *
     * @param idClient Client id
     */
    void setIdClient(String idClient);

    /**
     * Set language id
     *
     * @param idLanguage Language id
     */
    void setIdLanguage(String idLanguage);

    /**
     * Set process item
     *
     * @param process Process
     */
    void setProcess(AdProcess process);

    /**
     * Set process parameters
     *
     * @param params Parameter list
     */
    void setParams(List<AdProcessParam> params);

    /**
     * Set Http Servlet Response
     *
     * @param response Response
     */
    void setResponse(HttpServletResponse response);

    /**
     * Set Http Servlet Request
     *
     * @param request Request
     */
    void setRequest(HttpServletRequest request);

    /**
     * Set Http Servlet Context
     *
     * @param servletContext Servlet context
     */
    void setServletContext(ServletContext servletContext);

    /**
     * This method is called when process is activate/desactivate
     *
     * @param active Activate / Desactivate
     */
    void setActive(boolean active);

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    boolean isConcurrent();

    /**
     * Get if process notify error by email
     *
     * @return If concurrent
     */
    boolean notifyError();

    /**
     * Get template for email notification
     *
     * @return Path to email template
     */
    String getMailNotificationTemplate();

    /**
     * Set model for email notification template
     *
     * @param model Model for data
     * @param resources Resources
     */
    void setModelForMailNotificationTemplate(Map model, Map<String, String> resources);

    /**
     * Initialize proccess execution
     *
     * @return Process Execution Identifier
     */
    String initExec();

    /**
     * Finish process execution
     *
     * @param idProcessExec Process Execution Identifier
     * @param status Execution status
     * @param output Output file (Optional)
     * @param params List parameters (Optional)
     */
    void finishExec(String idProcessExec, String status, File output, List<OutputParam> params);
    void finishExec(String idProcessExec, String status, File output);
    void finishExec(String idProcessExec, String status);

    /**
     * Return start message for log
     *
     * @return Message
     */
    String getStartMsg();

    /**
     * Return finish message for log
     *
     * @param status Execution status
     * @return Message
     */
    String getFinishMsg(String status);

    /**
     * Get last error description
     *
     * @return Last error
     */
    String getLastError();

    /**
     * Return if process is in execution
     *
     * @return If executing
     */
    boolean isInExecution();

    /**
     * Return if the process have HTTP outputs
     *
     * @return If have HTTP outputs
     */
    boolean hasHttpOutputs();

    /**
     * Get start time
     *
     * @param idProcess Process Execution Identifier
     * @return Start time
     */
    Date getStarted(String idProcess);

    /**
     * Get finish time
     *
     * @param idProcess Process Execution Identifier
     * @return Finish time
     */
    Date getFinished(String idProcess);

    /**
     * Return process execution status
     *
     * @return If not have errors
     */
    boolean isSuccess();

    /**
     * Write success to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    void success(String idProcessExec, String log);

    /**
     * Write log to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    void info(String idProcessExec, String log);

    /**
     * Write warning to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    void warn(String idProcessExec, String log);

    /**
     * Write error to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    void error(String idProcessExec, String log);

}
