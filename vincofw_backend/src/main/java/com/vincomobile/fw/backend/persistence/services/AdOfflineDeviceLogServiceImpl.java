package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdDeviceSynchro;
import com.vincomobile.fw.backend.persistence.model.AdOfflineDeviceLog;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.beans.AdBaseTools;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.AdPreference;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.persistence.services.SqlSort;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.Datetool;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.*;

/**
 * Created by Devtools.
 * Service layer implementation for ad_offline_device_log
 *
 * Date: 06/03/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdOfflineDeviceLogServiceImpl extends BaseServiceImpl<AdOfflineDeviceLog, String> implements AdOfflineDeviceLogService {

    /**
     * Constructor.
     */
    public AdOfflineDeviceLogServiceImpl() {
        super(AdOfflineDeviceLog.class);
    }

    /**
     * Save log information
     *
     * @param idClient Client identifier
     * @param level Log level
     * @param idOfflineDevice Device identifier
     * @param ltype Log type
     * @param log Log information
     */
    @Override
    public void log(String idClient, String level, String idOfflineDevice, String ltype, String log) {
        AdPreference preference = CacheManager.getPreference(idClient, "OfflineDeviceLogLevel", DEVICE_LOG_INFO);
        if (level.compareTo(preference.getString()) >= 0) {
            AdOfflineDeviceLog deviceLog = new AdOfflineDeviceLog();
            deviceLog.setIdOfflineDevice(idOfflineDevice);
            deviceLog.setIdClient(idClient);
            deviceLog.setIdModule(FWConfig.FWCORE_ID_MODULE);
            deviceLog.setActive(true);
            deviceLog.setLevel(level);
            deviceLog.setLtype(ltype);
            deviceLog.setLog(log != null ? Converter.truncText(log, 1999) : "---");
            save(deviceLog);
        }
    }

    /**
     * Get last synchronization information
     *
     * @param pageReq Page information
     * @param idClient Client identifier
     * @return Last synchronization information
     */
    @Override
    public Page<AdDeviceSynchro> getLastSynchro(PageSearch pageReq, String idClient) {
        Calendar firstDate = Calendar.getInstance();
        firstDate.add(Calendar.DAY_OF_MONTH, -7);
        SqlSort sort = getSort(pageReq);
        String orderBy = sort != null ? sort.getSqlOrder() : "";
        if (orderBy.contains("userName")) {
            orderBy = orderBy.replace("userName", "OD.user_name");
        }
        String select =
                "SELECT OD.id_offline_device, OD.user_name, concat(OD.device_name, ' ', OD.device_model, ' ', OD.device_os, ' ', OD.device_version) as device, OD.version, date(ODL.created), count(1) \n" +
                "FROM ad_offline_device_log ODL JOIN ad_offline_device OD ON ODL.id_offline_device = OD.id_offline_device \n" +
                "WHERE ODL.ltype = 'LOGIN' and date(ODL.created) >= DATE_ADD(now(), INTERVAL -7 DAY) AND ODL.id_client = :idClient \n";
        Map<String, Object> searchs = pageReq.getSearchs();
        String where = buildWhere(searchs);
        if (!Converter.isEmpty(where)) {
            select += " AND " + where + "\n";
        }
        select += "GROUP BY 1, 2, 3, 4, 5 ORDER BY " + (orderBy.equals("") ? "OD.user_name" : orderBy);
        Query query = entityManager.createNativeQuery(select);
        query.setParameter("idClient", idClient);
        AdBaseTools.setQueryParams(query, searchs);
        int offset = (int) pageReq.getOffset();
        int limit = pageReq.getPageSize();
        List<Object> items = query.getResultList();
        List<AdDeviceSynchro> result = new ArrayList<AdDeviceSynchro>();
        for (Object item1 : items) {
            Object[] item = (Object[]) item1;
            int day = Datetool.getDaysBetween(firstDate.getTime(), (Date) item[4]) + 1;
            boolean found = false;
            for (AdDeviceSynchro deviceSynchro : result) {
                if (deviceSynchro.getIdOfflineDevice().equals(item[0])) {
                    deviceSynchro.setPropertyValue("d" + day, ((Number) item[5]).longValue());
                    found = true;
                    break;
                }
            }
            if (!found) {
                AdDeviceSynchro deviceSynchro = new AdDeviceSynchro();
                deviceSynchro.setIdOfflineDevice((String) item[0]);
                deviceSynchro.setUserName((String) item[1]);
                deviceSynchro.setDevice((String) item[2]);
                deviceSynchro.setVersion((String) item[3]);
                deviceSynchro.setPropertyValue("d" + day, ((Number) item[5]).longValue());
                result.add(deviceSynchro);
            }
        }
        List<AdDeviceSynchro> pageResult = new ArrayList<>();
        for (int i = offset; i < offset + limit && i < result.size(); i++) {
            pageResult.add(result.get(i));
        }
        return new PageImpl<>(pageResult, pageReq, result.size());
    }

}
