package com.vincomobile.fw.backend.business;

import com.vincomobile.fw.backend.process.ProcessDefinition;
import com.vincomobile.fw.basic.persistence.model.AdProcess;

import java.util.Date;

public class AdProcessExecuted {

    String idProcess;
    String name;
    Date started;
    Date finished;
    String status;

    public AdProcessExecuted(AdProcess process, ProcessDefinition definition) {
        idProcess = process.getIdProcess();
        name = process.getName();
        started = definition.getStarted(process.getIdProcess());
        finished = definition.getFinished(process.getIdProcess());
        status = definition.isInExecution() ? "EXECUTING" : (definition.isSuccess() ? "FINISH_OK" : "FINISH_ERROR");
    }

    public String getIdProcess() {
        return idProcess;
    }

    public String getName() {
        return name;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
