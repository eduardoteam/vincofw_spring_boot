package com.vincomobile.fw.backend.sync;

import com.vincomobile.fw.backend.persistence.model.AdColumnSync;
import com.vincomobile.fw.backend.persistence.model.AdTableSync;
import com.vincomobile.fw.basic.tools.Converter;

import java.util.ArrayList;
import java.util.List;

public class SyncSchemaTable {
    String idTable;
    String name;
    String standardName;
    Long version;
    Boolean exportable;
    Boolean importable;

    List<SyncSchemaColumn> columns;

    public void setTable(AdTableSync tableSync) {
        idTable = tableSync.getIdTable();
        name = Converter.isEmpty(tableSync.getNameRemote()) ? tableSync.getTable().getName() : tableSync.getNameRemote();
        standardName = Converter.removeUnderscores(this.name.toLowerCase());
        version = tableSync.getVersion();
        exportable = tableSync.getExportable();
        importable = tableSync.getImportable();
    }

    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Boolean isExportable() {
        return exportable;
    }

    public void setExportable(Boolean exportable) {
        this.exportable = exportable;
    }

    public Boolean isImportable() {
        return importable;
    }

    public void setImportable(Boolean importable) {
        this.importable = importable;
    }

    public List<SyncSchemaColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<AdColumnSync> columns) {
        this.columns = new ArrayList<SyncSchemaColumn>();
        for (AdColumnSync column :  columns) {
            this.columns.add(new SyncSchemaColumn(column));
        }
    }
}
