package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdAuxInput;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 17/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_aux_input
 */
@Repository
@Transactional(readOnly = true)
public class AdAuxInputServiceImpl extends BaseServiceImpl<AdAuxInput, String> implements AdAuxInputService {

    /**
     * Constructor.
     */
    public AdAuxInputServiceImpl() {
        super(AdAuxInput.class);
    }

    /**
     * Replace parameters with auxiliary inputs
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     * @return Replaced value
     */
    @Override
    public String replaceAuxInputForIn(String idClient, String value, String param_1, String param_2, String param_3, String param_4, String param_5) {
        int paramIndx = value.indexOf("@AUX_INPUT");
        if (paramIndx >= 0) {
            String param = value.substring(value.indexOf("$") + 1, value.lastIndexOf("$"));
            Map filter = new HashMap();
            filter.put("idClient", getClientFilter(idClient));
            filter.put("value", param);
            AdAuxInput auxInput = findFirst(filter);
            if (auxInput != null) {
                String replaced = "";
                Query query = entityManager.createNativeQuery(replaceParameters(idClient, auxInput.getQuery(), param_1, param_2, param_3, param_4, param_5));
                List auxResult = query.getResultList();
                for (Object item : auxResult) {
                    if (!"".equals(replaced)) {
                        replaced += ", ";
                    }
                    replaced += "'" + item + "'";
                }
                value = value.replaceAll("\\@AUX_INPUT\\$" + param + "\\$", replaced);
            }
        }
        return value;
    }

}
