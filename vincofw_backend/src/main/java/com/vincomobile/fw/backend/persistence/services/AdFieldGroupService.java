package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdFieldGroup;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Devtools.
 * Interface of service of AD_FIELD_GROUP
 *
 * Date: 19/02/2015
 */
public interface AdFieldGroupService extends BaseService<AdFieldGroup, String> {

}


