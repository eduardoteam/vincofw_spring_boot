package com.vincomobile.fw.backend.sync;

import com.vincomobile.fw.backend.persistence.model.AdColumnSync;

public class SyncSchemaColumn {
    private String name;
    private String ctype;
    private Boolean primaryKey;
    private Boolean exportable;
    private Boolean importable;
    private Long seqno;

    public SyncSchemaColumn(AdColumnSync columnSync) {
        name = columnSync.getName();
        ctype = columnSync.getCtype();
        primaryKey = columnSync.getPrimaryKey();
        exportable = columnSync.getExportable();
        importable = columnSync.getImportable();
        seqno = columnSync.getSeqno();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public Boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Boolean isExportable() {
        return exportable;
    }

    public void setExportable(Boolean exportable) {
        this.exportable = exportable;
    }

    public Boolean isImportable() {
        return importable;
    }

    public void setImportable(Boolean importable) {
        this.importable = importable;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }
}
