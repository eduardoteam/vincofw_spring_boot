package com.vincomobile.fw.backend.sync;

import java.util.List;

public class SyncRow {

    String op, sql;
    List values;

    public SyncRow(String op, List values) {
        this.op = op;
        this.values = values;
    }

    public SyncRow(String op) {
        this.op = op;
    }

    public SyncRow() {
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public List getValues() {
        return values;
    }

    public void setValues(List values) {
        this.values = values;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
