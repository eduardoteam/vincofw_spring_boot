package com.vincomobile.fw.backend.sync;

import java.util.List;

public class SyncExportDB {

    String idOfflineDevice;
    String idSeller;
    Boolean ignoreUndefined;

    List<SyncOfflineTable> tables;

    public String getIdOfflineDevice() {
        return idOfflineDevice;
    }

    public void setIdOfflineDevice(String idOfflineDevice) {
        this.idOfflineDevice = idOfflineDevice;
    }

    public String getIdSeller() {
        return idSeller;
    }

    public void setIdSeller(String idSeller) {
        this.idSeller = idSeller;
    }

    public Boolean isIgnoreUndefined() {
        return ignoreUndefined;
    }

    public void setIgnoreUndefined(Boolean ignoreUndefined) {
        this.ignoreUndefined = ignoreUndefined;
    }

    public List<SyncOfflineTable> getTables() {
        return tables;
    }

    public void setTables(List<SyncOfflineTable> tables) {
        this.tables = tables;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append("idOfflineDevice: ").append(idOfflineDevice).append("\n");
        for (SyncOfflineTable syncTable : tables) {
            out.append(syncTable);
        }
        return out.toString();
    }
}
