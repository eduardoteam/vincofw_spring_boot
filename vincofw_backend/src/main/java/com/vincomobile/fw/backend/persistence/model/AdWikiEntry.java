package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_wiki_entry
 */
@Entity
@Table(name = "ad_wiki_entry")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdWikiEntry extends AdEntityBean {

    private String idWikiEntry;
    private String idWikiTopic;
    private String name;
    private String title;
    private String content;
    private Long seqno;

    private AdWikiTopic wikiTopic;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idWikiEntry;
    }

    @Override
    public void setId(String id) {
            this.idWikiEntry = id;
    }

    @Id
    @Column(name = "id_wiki_entry")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdWikiEntry() {
        return idWikiEntry;
    }

    public void setIdWikiEntry(String idWikiEntry) {
        this.idWikiEntry = idWikiEntry;
    }

    @Column(name = "id_wiki_topic", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdWikiTopic() {
        return idWikiTopic;
    }

    public void setIdWikiTopic(String idWikiTopic) {
        this.idWikiTopic = idWikiTopic;
    }

    @Column(name = "name", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "title", length = 1000)
    @NotNull
    @Size(min = 1, max = 1000)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "content")
    @NotNull
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_wiki_topic", referencedColumnName = "id_wiki_topic", insertable = false, updatable = false)
    public AdWikiTopic getWikiTopic() {
        return wikiTopic;
    }

    public void setWikiTopic(AdWikiTopic wikiTopic) {
        this.wikiTopic = wikiTopic;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdWikiEntry)) return false;

        final AdWikiEntry that = (AdWikiEntry) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idWikiEntry == null) && (that.idWikiEntry == null)) || (idWikiEntry != null && idWikiEntry.equals(that.idWikiEntry)));
        result = result && (((idWikiTopic == null) && (that.idWikiTopic == null)) || (idWikiTopic != null && idWikiTopic.equals(that.idWikiTopic)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((title == null) && (that.title == null)) || (title != null && title.equals(that.title)));
        result = result && (((content == null) && (that.content == null)) || (content != null && content.equals(that.content)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }


}

