package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.AdPhoto;
import com.vincomobile.fw.backend.persistence.model.AdImage;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_image
 *
 * Date: 23/01/2016
 */
public interface AdImageService extends BaseService<AdImage, String> {

    String TYPE_STANDARD            = "STANDARD";

    /**
     * Get first image
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @param itype Image type
     * @return Image
     */
    AdImage getImage(String idTable, String idRow, String itype);
    AdImage getImage(String idTable, String idRow);

    /**
     * Get first photo
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @param itype Image type
     * @return Image
     */
    AdPhoto getPhoto(String idTable, String idRow, String itype);
    AdPhoto getPhoto(String idTable, String idRow);

    /**
     * Load all images
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @param itype Image type
     * @return Image list
     */
    List<AdPhoto> getAllPhotos(String idTable, String idRow, String itype);
    List<AdPhoto> getAllPhotos(String idTable, String idRow);
}


