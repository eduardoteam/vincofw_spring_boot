package com.vincomobile.fw.backend.business;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Describes the information needed to execute a native query
 */
public class SqlExecData {
    private String sqlType;
    private String sqlQuery;

    @NotNull
    @Size(min = 1)
    public String getSqlType() {
        return sqlType;
    }

    public void setSqlType(String sqlType) {
        this.sqlType = sqlType;
    }

    @NotNull
    @Size(min = 1)
    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }
}
