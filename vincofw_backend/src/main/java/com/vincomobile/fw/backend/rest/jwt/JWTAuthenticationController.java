package com.vincomobile.fw.backend.rest.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.hooks.HookResult;
import com.vincomobile.fw.basic.persistence.model.AdModule;
import com.vincomobile.fw.basic.persistence.model.AdUserRoles;
import com.vincomobile.fw.basic.persistence.services.AdClientModuleService;
import com.vincomobile.fw.basic.persistence.services.AdUserRolesService;
import com.vincomobile.fw.basic.session.ApplicationUser;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "vinco_core")
public class JWTAuthenticationController {

    private Logger logger = LoggerFactory.getLogger(JWTAuthenticationController.class);

    @Value("${app.jwt.tokenSecret}")
    String tokenSecret;

    @Value("${app.jwt.tokenExpirationMsec}")
    Long tokenExpiration;

    private final AuthenticationManager authenticationManager;
    private final AdUserRolesService userRolesService;
    private final AdClientModuleService clientModuleService;

    public JWTAuthenticationController(AuthenticationManager authenticationManager, AdUserRolesService userRolesService, AdClientModuleService clientModuleService) {
        this.authenticationManager = authenticationManager;
        this.userRolesService = userRolesService;
        this.clientModuleService = clientModuleService;
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JWTRequest authenticationRequest) throws Exception {
        logger.info("POST createAuthenticationToken(" + authenticationRequest.getUsername() + ")");
        String username = authenticationRequest.getUsername().trim();
        String password = Converter.decryptPassword(authenticationRequest.getPassword(), username);
        String idRole = null;
        Authentication authentication = authenticate(username, password);
        if (username.length() > 32 && username.indexOf("$") == 32) {
            username = username.substring(33);
        }
        HookResult result = HookManager.executeHook(FWConfig.HOOK_AUTHENTICATE_USER, username);
        if (result.hasKey(FWConfig.HOOK_AUTHENTICATE_USER_APPUSER)) {
            ApplicationUser user = (ApplicationUser) result.get(FWConfig.HOOK_AUTHENTICATE_USER_APPUSER);
            Collection authorities = authentication.getAuthorities();
            List<String> privileges = new ArrayList<>();
            authorities.forEach(auth -> privileges.add(auth.toString()));
            Date expireAt = new Date(System.currentTimeMillis() + tokenExpiration);
            logger.info("Expire: " + expireAt);
            AdUserRoles userRoles = userRolesService.findUserRole(user.getId(), idRole != null ? idRole : user.getDefaultIdRole());
            List<String> modules = new ArrayList<>();
            if (userRoles != null) {
                List<AdModule> clientModules = clientModuleService.listModules(userRoles.getIdClient());
                clientModules.forEach(m -> modules.add(m.getRestPath()));
            }
            String token = JWT.create()
                    .withSubject(user.getId())
                    .withClaim("name", user.getName())
                    .withClaim("email", user.getEmail())
                    .withArrayClaim("privileges", privileges.toArray(new String[privileges.size()]))
                    .withArrayClaim("modules", modules.toArray(new String[modules.size()]))
                    .withExpiresAt(expireAt)
                    .sign(Algorithm.HMAC512(tokenSecret.getBytes()));
            return ResponseEntity.ok(new JWTResponse(token));
        } else {
            throw new Exception("INVALID_CREDENTIALS");
        }
    }

    private Authentication authenticate(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
