package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.backend.persistence.services.AdRefSequenceService;
import com.vincomobile.fw.backend.sync.ActionTrigger;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import org.hibernate.annotations.GenericGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de ad_table_sync
 *
 * Date: 05/03/2016
 */
@Entity
@Table(name = "ad_table_sync")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdTableSync extends AdEntityBean {

    private Logger logger = LoggerFactory.getLogger(AdTableSync.class);

    private String idTableSync;
    private String idTable;
    private String idTableReal;
    private String deleteRemote;
    private Boolean exportable;
    private Boolean importable;
    private Boolean translatable;
    private String nameRemote;
    private Long seqno;
    private Boolean synchronize;
    private String triggers;
    private Long version;
    private String xfrom;
    private String xgroup;
    private String xprefix;
    private String xselect;
    private String xwhere;

    private AdTable table;
    private List<AdColumnSync> columns;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idTableSync;
    }

    @Override
    public void setId(String id) {
            this.idTableSync = id;
    }

    @Id
    @Column(name = "id_table_sync")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdTableSync() {
        return idTableSync;
    }

    public void setIdTableSync(String idTableSync) {
        this.idTableSync = idTableSync;
    }

    @Column(name = "delete_remote", length = 1000)
    @Size(min = 1, max = 1000)
    public String getDeleteRemote() {
        return deleteRemote;
    }

    public void setDeleteRemote(String deleteRemote) {
        this.deleteRemote = deleteRemote;
    }

    @Column(name = "exportable")
    @NotNull
    public Boolean getExportable() {
        return exportable;
    }

    public void setExportable(Boolean exportable) {
        this.exportable = exportable;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_table_real")
    @NotNull
    public String getIdTableReal() {
        return idTableReal;
    }

    public void setIdTableReal(String idTableReal) {
        this.idTableReal = idTableReal;
    }

    @Column(name = "importable")
    @NotNull
    public Boolean getImportable() {
        return importable;
    }

    public void setImportable(Boolean importable) {
        this.importable = importable;
    }

    @Column(name = "translatable")
    @NotNull
    public Boolean getTranslatable() {
        return translatable;
    }

    public void setTranslatable(Boolean translatable) {
        this.translatable = translatable;
    }

    @Column(name = "name_remote", length = 50)
    @Size(min = 1, max = 50)
    public String getNameRemote() {
        return nameRemote;
    }

    public void setNameRemote(String nameRemote) {
        this.nameRemote = nameRemote;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "synchronize")
    @NotNull
    public Boolean getSynchronize() {
        return synchronize;
    }

    public void setSynchronize(Boolean synchronize) {
        this.synchronize = synchronize;
    }

    @Column(name = "triggers", length = 150)
    @Size(min = 1, max = 150)
    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    @Column(name = "version")
    @NotNull
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Column(name = "xfrom", length = 1000)
    @Size(min = 1, max = 1000)
    public String getXfrom() {
        return xfrom;
    }

    public void setXfrom(String xfrom) {
        this.xfrom = xfrom;
    }

    @Column(name = "xgroup", length = 1000)
    @Size(min = 1, max = 1000)
    public String getXgroup() {
        return xgroup;
    }

    public void setXgroup(String xgroup) {
        this.xgroup = xgroup;
    }

    @Column(name = "xprefix", length = 45)
    @Size(min = 1, max = 45)
    public String getXprefix() {
        return xprefix;
    }

    public void setXprefix(String xprefix) {
        this.xprefix = xprefix;
    }

    @Column(name = "xselect", length = 1000)
    @Size(min = 1, max = 1000)
    public String getXselect() {
        return xselect;
    }

    public void setXselect(String xselect) {
        this.xselect = xselect;
    }

    @Column(name = "xwhere", length = 1000)
    @Size(min = 1, max = 1000)
    public String getXwhere() {
        return xwhere;
    }

    public void setXwhere(String xwhere) {
        this.xwhere = xwhere;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @OneToMany
    @JoinColumn(name = "id_table_sync", referencedColumnName = "id_table_sync", insertable = false, updatable = false)
    public List<AdColumnSync> getColumns() {
        return columns;
    }

    public void setColumns(List<AdColumnSync> columns) {
        this.columns = columns;
    }

    @Transient
    public String getTableName() {
        return table != null ? table.getStandardName() : "";
    }

    @Transient
    public String getName() {
        return table != null ? table.getName() : "";
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdTableSync)) return false;

        final AdTableSync that = (AdTableSync) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idTableSync == null) && (that.idTableSync == null)) || (idTableSync != null && idTableSync.equals(that.idTableSync)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idTableReal == null) && (that.idTableReal == null)) || (idTableReal != null && idTableReal.equals(that.idTableReal)));
        result = result && (((deleteRemote == null) && (that.deleteRemote == null)) || (deleteRemote != null && deleteRemote.equals(that.deleteRemote)));
        result = result && (((importable == null) && (that.importable == null)) || (importable != null && importable.equals(that.importable)));
        result = result && (((exportable == null) && (that.exportable == null)) || (exportable != null && exportable.equals(that.exportable)));
        result = result && (((translatable == null) && (that.translatable == null)) || (translatable != null && translatable.equals(that.translatable)));
        result = result && (((nameRemote == null) && (that.nameRemote == null)) || (nameRemote != null && nameRemote.equals(that.nameRemote)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((synchronize == null) && (that.synchronize == null)) || (synchronize != null && synchronize.equals(that.synchronize)));
        result = result && (((triggers == null) && (that.triggers == null)) || (triggers != null && triggers.equals(that.triggers)));
        result = result && (((version == null) && (that.version == null)) || (version != null && version.equals(that.version)));
        result = result && (((xfrom == null) && (that.xfrom == null)) || (xfrom != null && xfrom.equals(that.xfrom)));
        result = result && (((xgroup == null) && (that.xgroup == null)) || (xgroup != null && xgroup.equals(that.xgroup)));
        result = result && (((xprefix == null) && (that.xprefix == null)) || (xprefix != null && xprefix.equals(that.xprefix)));
        result = result && (((xselect == null) && (that.xselect == null)) || (xselect != null && xselect.equals(that.xselect)));
        result = result && (((xwhere == null) && (that.xwhere == null)) || (xwhere != null && xwhere.equals(that.xwhere)));
        return result;
    }

    /**
     * Get table name on remote system
     *
     * @return Remote table name
     */
    @Transient
    public String getTableRemote() {
        return com.vincomobile.fw.basic.tools.Converter.isEmpty(nameRemote) ? table.getName() : nameRemote;
    }

    /**
     * List Key Columns
     *
     * @return Columns list
     */
    @Transient
    public List<AdColumnSync> getKeyFields() {
        List<AdColumnSync> results = new ArrayList<AdColumnSync>();
        for (AdColumnSync column : columns) {
            if (column.getPrimaryKey()) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * Test if is multiple key (more than one filed) or not
     *
     * @return True if is multiple key
     */
    @Transient
    @JsonIgnore
    public boolean getMultipleKey() {
        return this.getKeyFields().size() > 1;
    }

    /**
     * List No Key Columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumnSync> getNoKeyFields() {
        List<AdColumnSync> results = new ArrayList<>();
        for (AdColumnSync column : getColumns()) {
            if (!column.getPrimaryKey()) {
                results.add(column);
            }
        }
        return results;
    }

    /**
     * List all columns
     *
     * @return Columns list
     */
    @Transient
    @JsonIgnore
    public List<AdColumnSync> getAllFields() {
        List<AdColumnSync> results = new ArrayList<AdColumnSync>();
        for (AdColumnSync column : getColumns()) {
            results.add(column);
        }
        return results;
    }

    @Transient
    ActionTrigger triggerClass = null;

    /**
     * Load a trigger class
     *
     * @param entityManager DB manager
     */
    public void loadTriggerClass(EntityManager entityManager, AdRefSequenceService sequenceService) {
        if (triggerClass == null && !com.vincomobile.fw.basic.tools.Converter.isEmpty(triggers)) {
            ClassLoader classLoader = AdTableSync.class.getClassLoader();
            try {
                Class trgClass = classLoader.loadClass(triggers);
                triggerClass = (ActionTrigger) trgClass.newInstance();
                triggerClass.setEntityManager(entityManager);
                triggerClass.setSequenceService(sequenceService);
            } catch (Exception e) {
                logger.error("Error loading trigger: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Transient
    @JsonIgnore
    public boolean hasTriggers() {
        return triggers != null;
    }

    /**
     * Call trigger for UPDATE operation
     *
     * @param columns Column list (value = new value, old_value = value stored in DB)
     */
    public boolean beforeUpdate(List<AdColumnSync> columns) {
        return !hasTriggers() || this.triggerClass.beforeUpdate(columns);
    }

    public void afterUpdate(List<AdColumnSync> fields) {
        if (hasTriggers()) {
            this.triggerClass.afterUpdate(fields);
        }
    }

    /**
     * Call trigger for INSERT operation
     *
     * @param columns Column list (value = new value, old_value = null)
     */
    public boolean beforeInsert(List<AdColumnSync> columns) {
        return !hasTriggers() || this.triggerClass.beforeInsert(columns);
    }

    public void afterInsert(List<AdColumnSync> fields) {
        if (hasTriggers()) {
            this.triggerClass.afterInsert(fields);
        }
    }

    /**
     * Call trigger for DELETE operation
     *
     * @param columns Column list (value = new value, old_value = null)
     */
    public boolean beforeDelete(List<AdColumnSync> columns) {
        return !hasTriggers() || this.triggerClass.beforeDelete(columns);
    }

    public void afterDelete(List<AdColumnSync> columns) {
        if (hasTriggers()) {
            this.triggerClass.afterDelete(columns);
        }
    }
}

