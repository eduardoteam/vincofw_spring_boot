package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdAuxInput;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Vincomobile FW on 17/10/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_aux_input
 */
public interface AdAuxInputService extends BaseService<AdAuxInput, String> {

    /**
     * Replace parameters with auxiliary inputs
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     * @return Replaced value
     */
    String replaceAuxInputForIn(String idClient, String value, String param_1, String param_2, String param_3, String param_4, String param_5);
}


