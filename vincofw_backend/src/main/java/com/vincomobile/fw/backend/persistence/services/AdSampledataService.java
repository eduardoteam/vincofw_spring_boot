package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdSampledata;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_sampledata
 */
public interface AdSampledataService extends BaseService<AdSampledata, String> {

}


