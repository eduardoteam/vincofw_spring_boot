package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.persistence.model.AdUserLog;
import com.vincomobile.fw.basic.session.UserLog;

import java.util.ArrayList;
import java.util.List;

public class SessionLog {

    List<UserLog> logDisconnect = new ArrayList<>();
    List<AdUserLog> logItems = new ArrayList<>();

    public synchronized long count() {
        return logItems.size();
    }

    public synchronized void addUserLog(AdUserLog item) {
        for (AdUserLog userLog : logItems) {
            if (userLog.getIdUser().equals(item.getIdUser()) && userLog.getSessionId().equals(item.getSessionId()))
                return;
        }
        logItems.add(item);
    }

    public synchronized void addUserDisconnect(UserLog item) {
        logDisconnect.add(item);
    }

    public synchronized List<AdUserLog> getItems() {
        return logItems;
    }

    public synchronized List<UserLog> getUserDisconnect() {
        return logDisconnect;
    }

    public synchronized UserLog getDisconnect(String idUser, String sessionId) {
        for (UserLog item : logDisconnect) {
            if (idUser.equals(item.getIdUser()) && sessionId.equals(item.getSessionId())) {
                return item;
            }
        }
        return null;
    }

    public synchronized void clearUserLog() {
        logItems.clear();
    }

    public synchronized void clearUserDisconnect() {
        logDisconnect.clear();
    }
}
