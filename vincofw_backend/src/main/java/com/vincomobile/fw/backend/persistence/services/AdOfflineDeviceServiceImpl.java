package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdOfflineDevice;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_offline_device
 *
 * Date: 06/03/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdOfflineDeviceServiceImpl extends BaseServiceImpl<AdOfflineDevice, String> implements AdOfflineDeviceService {

    private Logger logger = LoggerFactory.getLogger(AdOfflineDeviceServiceImpl.class);

    /**
     * Constructor.
     */
    public AdOfflineDeviceServiceImpl() {
        super(AdOfflineDevice.class);
    }

    /**
     * Update device information
     *
     * @param idClient Client identifier
     * @param idOfflineDevice Device identifier
     * @param version Version
     * @param idUser Login user identifier
     * @param userName User name
     * @param deviceName Device name
     * @param deviceModel Device model
     * @param deviceOS Device operation system
     * @param deviceVersion Device OS version
     * @return Offline device
     */
    @Override
    public AdOfflineDevice update(String idClient, String idOfflineDevice, String version, String idUser, String userName, String deviceName, String deviceModel, String deviceOS, String deviceVersion) {
        logger.info("Update device: " + idOfflineDevice);
        Map filter = new HashMap();
        filter.put("idOfflineDevice", idOfflineDevice);
        AdOfflineDevice device = findFirst(filter);
        boolean isNew = false;
        if (device == null) {
            logger.info("New device: " + deviceModel+ " - " + deviceOS);
            device = new AdOfflineDevice();
            device.setIdOfflineDevice(idOfflineDevice);
            device.setDstatus(AdOfflineDeviceService.STATUS_ACTIVE);
            device.setIdClient(idClient);
            device.setIdModule(FWConfig.FWCORE_ID_MODULE);
            isNew = true;
        }
        device.setUserName(userName);
        device.setIdUser(idUser);
        device.setVersion(version);
        device.setDeviceName(deviceName);
        device.setDeviceModel(deviceModel);
        device.setDeviceOs(deviceOS);
        device.setDeviceVersion(deviceVersion);
        device.setActive(true);
        if (isNew)
            save(device);
        else
            update(device);
        return device;
    }

}
