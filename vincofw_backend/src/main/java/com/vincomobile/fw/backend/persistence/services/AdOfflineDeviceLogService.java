package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdDeviceSynchro;
import com.vincomobile.fw.backend.persistence.model.AdOfflineDeviceLog;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import org.springframework.data.domain.Page;

/**
 * Created by Devtools.
 * Service layer interface for ad_offline_device_log
 *
 * Date: 06/03/2016
 */
public interface AdOfflineDeviceLogService extends BaseService<AdOfflineDeviceLog, String> {

    String DEVICE_LOG_ERROR    = "01_ERROR";
    String DEVICE_LOG_WARNING  = "02_WARNING";
    String DEVICE_LOG_INFO     = "03_INFO";
    String DEVICE_LOG_DEBUG    = "04_DEBUG";

    String DEVICE_LOG_TYPE_LOGIN        = "LOGIN";
    String DEVICE_LOG_TYPE_GET_SCHEMA   = "GET_SCHEMA";
    String DEVICE_LOG_TYPE_GET_TABLE    = "GET_TABLE";
    String DEVICE_LOG_TYPE_GET_UPDATE   = "GET_UPDATE";
    String DEVICE_LOG_TYPE_MAKE_UPDATE  = "MAKE_UPDATE";

    /**
     * Save log information
     *
     * @param idClient Client identifier
     * @param level Log level
     * @param idOfflineDevice Device identifier
     * @param ltype Log type
     * @param log Log information
     */
    void log(String idClient, String level, String idOfflineDevice, String ltype, String log);

    /**
     * Get last synchronization information
     *
     * @param pageReq Page information
     * @param idClient Client identifier
     * @return Last synchronization information
     */
    Page<AdDeviceSynchro> getLastSynchro(PageSearch pageReq, String idClient);
}


