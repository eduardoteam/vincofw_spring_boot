package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.persistence.dto.AdReferenceDto;
import com.vincomobile.fw.basic.persistence.model.AdReference;
import com.vincomobile.fw.basic.persistence.services.AdReferenceService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_REFERENCE
 *
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_reference/{idClient}")
public class AdReferenceController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdReferenceController.class);

    @Autowired
    AdReferenceService service;

    @Override
    public AdReferenceService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
//    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdReference> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdReference.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
//    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdReference get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdReference entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdReference entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/{idReference}/load", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdReferenceDto load(
            @PathVariable("idClient") String idClient,
            @PathVariable("idReference") String idReference,
            @RequestParam String idLanguage
    ) {
        logger.debug("GET load(" + idReference + ")");
        long time = System.currentTimeMillis();
        AdReferenceDto result = service.loadReference(idClient, idLanguage, idReference);
        logger.debug("Reference loaded in: " +(System.currentTimeMillis() - time) + " ms");
        return result;
    }

    @RequestMapping(value = "/load", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public List<AdReferenceDto> load(
            @PathVariable("idClient") String idClient,
            @RequestParam("references") String[] references,
            @RequestParam String idLanguage
    ) {
        logger.debug("GET load(" + Converter.printArray(references) + ")");
        long time = System.currentTimeMillis();
        List<AdReferenceDto> result = service.loadReferences(idClient, idLanguage, references);
        logger.debug("References loaded in: " +(System.currentTimeMillis() - time) + " ms");
        return result;
    }

}