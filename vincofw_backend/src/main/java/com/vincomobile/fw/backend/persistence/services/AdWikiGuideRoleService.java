package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdWikiGuideRole;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Vincomobile FW on 30/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_wiki_guide_role
 */
public interface AdWikiGuideRoleService extends BaseService<AdWikiGuideRole, String> {

}


