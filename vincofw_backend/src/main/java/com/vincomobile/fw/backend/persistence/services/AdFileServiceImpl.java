package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdFile;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_file
 *
 * Date: 23/01/2016
 */
@Repository
@Transactional(readOnly = true)
public class AdFileServiceImpl extends BaseServiceImpl<AdFile, String> implements AdFileService {

    /**
     * Constructor.
     */
    public AdFileServiceImpl() {
        super(AdFile.class);
    }

    /**
     * Get files for table row
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @return File list
     */
    @Override
    public List<AdFile> getFiles(String idTable, String idRow) {
        Map filter = new HashMap();
        filter.put("idTable", idTable);
        filter.put("idRow", idRow);
        return findAll(filter);
    }

}
