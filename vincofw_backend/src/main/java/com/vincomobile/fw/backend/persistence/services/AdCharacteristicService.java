package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.CharacteristicField;
import com.vincomobile.fw.backend.persistence.model.AdCharacteristic;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_characteristic
 * <p/>
 * Date: 23/01/2016
 */
public interface AdCharacteristicService extends BaseService<AdCharacteristic, String> {

    /**
     * Returns the characteristic fields associated with a given table
     *
     * @param idClient Client identifier
     * @param dtype   Table type
     * @param idTable Table ID
     * @param mode    Characteristic mode
     * @return List of fields
     */
    List<CharacteristicField> listFields(String idClient, String dtype, String idTable, String mode);
}


