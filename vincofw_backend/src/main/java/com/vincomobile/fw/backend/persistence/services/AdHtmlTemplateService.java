package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdHtmlTemplate;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Vincomobile FW on 03/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_html_template
 */
public interface AdHtmlTemplateService extends BaseService<AdHtmlTemplate, String> {

}


