package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.AdChartResult;
import com.vincomobile.fw.basic.persistence.model.AdChart;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.Map;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 * <p>
 * Service layer interface for ad_chart
 */
public interface AdChartService extends BaseService<AdChart, String> {

    int MONTH_MAX_DAYS                  = 62;
    int WEEK_MAX_DAYS                   = 21;

    String MODE_NORMAL                  = "NORMAL";
    String MODE_DATE                    = "DATE";
    String MODE_DATE_COMPARE            = "DATE_COMPARE";
    String MODE_DATE_COMPARE_WEEK       = "DATE_COMPARE_WEEK";
    String MODE_DATE_COMPARE_MONTH      = "DATE_COMPARE_MONTH";
    String MODE_DATE_COMPARE_QUARTER    = "DATE_COMPARE_QUARTER";
    String MODE_DATE_COMPARE_SEMESTER   = "DATE_COMPARE_SEMESTER";
    String MODE_DATE_COMPARE_YEAR       = "DATE_COMPARE_YEAR";
    String MODE_HOURS                   = "HOURS";
    String MODE_CUSTOM                  = "CUSTOM";

    String DATE_MODE_DAY                = "DAY";
    String DATE_MODE_WEEK               = "WEEK";
    String DATE_MODE_MONTH              = "MONTH";

    String TYPE_VERTICAL_BAR            = "VERTICAL_BAR";
    String TYPE_HORIZONTAL_BAR          = "HORIZONTAL_BAR";
    String TYPE_LINE                    = "LINE";
    String TYPE_PIE                     = "PIE";
    String TYPE_STACKED_LINE            = "STACKED_LINE";
    String TYPE_STACKED_BAR             = "STACKED_BAR";
    String TYPE_EASY_PIE                = "EASY_PIE";
    String TYPE_LIST                    = "LIST";

    String PREF_CHART_MAX_SIZE_X_LABEL  = "ChartMaxSizeXLabel";

    /**
     * Evaluate a chart
     *
     * @param idClient    Client identifier (executor)
     * @param idChart     Chart identifier
     * @param idLanguage  Language identifier
     * @param constraints Constraints
     * @return Chart values
     */
    AdChartResult evaluate(String idClient, String idChart, String idLanguage, Map<String, Object> constraints, Integer page, String sort);

    /**
     * Evaluate a chart
     *
     * @param idClient    Client identifier (executor)
     * @param idChart     Chart identifier
     * @param idLanguage  Language identifier
     * @param constraints Constraints
     * @return Chart values
     */
    AdChartResult evaluate(String idClient, String idChart, String idLanguage, Map<String, Object> constraints);
}


