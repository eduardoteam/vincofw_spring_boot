package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.business.AdProcessExecuted;
import com.vincomobile.fw.backend.persistence.services.AdProcessService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_CLIENT
 *
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_process_runtime/{idClient}")
public class AdProcessRuntimeController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdProcessRuntimeController.class);

    @Autowired
    AdProcessService service;

    @Override
    public BaseService getService() {
        return null;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Page<AdProcessExecuted> list(
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        List<AdProcessExecuted> result = service.getExecutedProcess(getSort(sort));
        return new PageImpl<>(result, pageReq, result.size());
    }

}