package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 02/08/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Model for table ad_scheduler
 */
@Entity
@Table(name = "ad_scheduler")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdScheduler extends AdEntityBean {

    private String idProcess;
    private String classmethod;
    private String classname;
    private String cronExpression;
    private String name;
    private String ptype;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcess;
    }

    @Override
    public void setId(String id) {
            this.idProcess = id;
    }

    @Id
    @Column(name = "id_process")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "classmethod", length = 60)
    @NotNull
    @Size(min = 1, max = 60)
    public String getClassmethod() {
        return classmethod;
    }

    public void setClassmethod(String classmethod) {
        this.classmethod = classmethod;
    }

    @Column(name = "classname", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Column(name = "cron_expression", length = 120)
    @Size(min = 1, max = 120)
    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "ptype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdScheduler)) return false;

        final AdScheduler that = (AdScheduler) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((classmethod == null) && (that.classmethod == null)) || (classmethod != null && classmethod.equals(that.classmethod)));
        result = result && (((classname == null) && (that.classname == null)) || (classname != null && classname.equals(that.classname)));
        result = result && (((cronExpression == null) && (that.cronExpression == null)) || (cronExpression != null && cronExpression.equals(that.cronExpression)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((ptype == null) && (that.ptype == null)) || (ptype != null && ptype.equals(that.ptype)));
        return result;
    }

}

