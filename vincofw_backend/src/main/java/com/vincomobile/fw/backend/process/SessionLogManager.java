package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.backend.FWCoreProcessConfig;
import com.vincomobile.fw.basic.persistence.model.AdUserLog;
import com.vincomobile.fw.basic.persistence.services.AdUserLogService;
import com.vincomobile.fw.basic.session.UserLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class SessionLogManager {

    private Logger logger = LoggerFactory.getLogger(SessionLogManager.class);

    @Autowired
    AdUserLogService userLogService;

    @Transactional(propagation = Propagation.REQUIRED)
    public void doSave() {
        long count = FWCoreProcessConfig.sessionLog.count();
        logger.info("Save user log: " + count);
        if (count > 0) {
            List<AdUserLog> logItems = FWCoreProcessConfig.sessionLog.getItems();
            for (AdUserLog item : logItems) {
                UserLog log = FWCoreProcessConfig.sessionLog.getDisconnect(item.getIdUser(), item.getSessionId());
                if (log != null) {
                    AdUserLog reloaded = item.getIdUserLog() != null ? userLogService.findById(item.getIdUserLog()) : item;
                    if (reloaded != null) {
                        reloaded.setLogoutTime(log.getLastUpdate());
                        if (reloaded.getIdUserLog() != null) {
                            userLogService.update(reloaded);
                        }
                    }
                    log.setProcessed(true);
                }
                if (item.getIdUserLog() == null) {
                    userLogService.save(item);
                }
            }
            FWCoreProcessConfig.sessionLog.clearUserLog();
        }
        List<UserLog> logDisconnect = FWCoreProcessConfig.sessionLog.getUserDisconnect();
        logger.info("Update logout: " + logDisconnect.size());
        for (UserLog log : logDisconnect) {
            if (!log.isProcessed() && log.getIdUser() != null) {
                userLogService.updateLogout(log);
            }
        }
        FWCoreProcessConfig.sessionLog.clearUserDisconnect();
        logger.info("Reset old logout: " + userLogService.resetLogout());
    }

}
