package com.vincomobile.fw.backend.business;

import java.util.ArrayList;
import java.util.List;

public class AdAllMenu {

    private String idMenu;
    private String idModule;
    private String value;
    private String name;
    private String icon;
    private String module;
    private Boolean selected;

    private List<AdAllMenu> subMenus = new ArrayList<>();

    public AdAllMenu(String idMenu, String value, String name, String icon, String module) {
        this.idMenu = idMenu;
        this.idModule = null;
        this.value = value;
        this.name = name;
        this.icon = icon;
        this.module = module;
        this.selected = false;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public List<AdAllMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<AdAllMenu> subMenus) {
        this.subMenus = subMenus;
    }
}
