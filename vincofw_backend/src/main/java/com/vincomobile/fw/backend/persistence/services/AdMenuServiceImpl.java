package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.AdAllMenu;
import com.vincomobile.fw.backend.persistence.model.AdMenu;
import com.vincomobile.fw.backend.persistence.model.AdMenuRoles;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_MENU
 * <p/>
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdMenuServiceImpl extends BaseServiceImpl<AdMenu, String> implements AdMenuService {

    private final AdMenuRolesService menuRolesService;

    /**
     * Constructor.
     */
    public AdMenuServiceImpl(AdMenuRolesService menuRolesService) {
        super(AdMenu.class);
        this.menuRolesService = menuRolesService;
    }

    /**
     * Loads the submenus recursively
     *
     * @param menu     Parent menu
     * @param clients  Clients ids
     * @param idRole   Role id
     * @return Menu list
     */
    public List<AdMenu> loadSubmenu(AdMenu menu, List<String> clients, String idRole) {
        if (menu == null) {
            List<AdMenu> mainMenu = findSubmenu(null, clients, idRole);
            for (AdMenu mnu : mainMenu) {
                mnu.setSubMenus(loadSubmenu(mnu, clients, idRole));
            }
            return mainMenu;
        } else {
            return findSubmenu(menu, clients, idRole);
        }
    }

    /**
     * Gets submenu list
     *
     * @param parentMenu Parent menu
     * @param clients    Clients ids
     * @param idRole     Role id
     * @return Submenu list
     */
    public List<AdMenu> findSubmenu(AdMenu parentMenu, List<String> clients, String idRole) {
        String sql = "SELECT M FROM AdMenu M, AdMenuRoles MR \n" +
                "WHERE MR.idMenu = M.idMenu AND M.idMenu = MR.idMenu \n";
        if (clients != null)
            sql += "AND MR.idClient IN (:clients) AND M.idClient IN (:clients) \n";
        if (idRole != null)
            sql += "AND MR.idRole = :idRole \n";
        sql += (parentMenu == null ? "AND M.parentMenu IS NULL \n" : "AND M.parentMenu = :parentMenu \n") +
                "ORDER BY M.seqno";
        Query query = entityManager.createQuery(sql, AdMenu.class);
        if (clients != null)
            query.setParameter("clients", clients);
        if (idRole != null)
            query.setParameter("idRole", idRole);
        if (parentMenu != null)
            query.setParameter("parentMenu", parentMenu.getValue());
        List<AdMenu> menuList = query.getResultList();
        for (AdMenu menu: menuList) {
            menu.setSubMenus(loadSubmenu(menu, clients, idRole));
        }
        return menuList;
    }

    /**
     * Expands a menu
     *
     * @param result   Result list
     * @param mainMenu Main menu (menu to expand)
     */
    @Override
    public void expandMenu(List<AdMenu> result, List<AdMenu> mainMenu) {
        for (AdMenu subMenu : mainMenu) {
            for (int j = 0; j < subMenu.getSubMenus().size(); j++) {
                AdMenu menu = subMenu.getSubMenus().get(j);
                menu.setSubmenuName(subMenu.getName());
                result.add(menu);
            }
        }
    }

    /**
     * Expands a menu, includes the menus with no children
     *
     * @param result   Result list
     * @param mainMenu Main menu (menu to expand)
     */
    @Override
    public void expandMenuFull(List<AdMenu> result, List<AdMenu> mainMenu) {
        for (AdMenu subMenu : mainMenu) {
            if (subMenu.getSubMenus().size() == 0)
                result.add(subMenu);
            for (int j = 0; j < subMenu.getSubMenus().size(); j++) {
                AdMenu menu = subMenu.getSubMenus().get(j);
                menu.setSubmenuName(subMenu.getName());
                result.add(menu);
            }
        }
    }

    /**
     * Build full menu tree for a role
     *
     * @param idRole Role identifier
     * @return Full menu tree
     */
    @Override
    public List<AdAllMenu> buildRoleMenu(String idRole) {
        List<AdMenu> menu = loadMenuTree(null);
        List<AdAllMenu> result = buildRoleSubMenu(menu);
        Map filter = new HashMap();
        filter.put("idClient", FWConfig.ALL_CLIENT_ID);
        filter.put("idRole", idRole);
        List<AdMenuRoles> menuRole = menuRolesService.findAll(filter);
        for (AdMenuRoles item : menuRole) {
            AdAllMenu menuItem = getMenuItem(result, item.getIdMenu());
            if (menuItem != null) {
                menuItem.setSelected(true);
                menuItem.setIdModule(item.getIdModule());
            }
        }
        return result;
    }

    /**
     * Build recursively the menu tree
     *
     * @param menu Menu list
     * @return Menu tree
     */
    private List<AdAllMenu> buildRoleSubMenu(List<AdMenu> menu) {
        List<AdAllMenu> result = new ArrayList<>();
        for (AdMenu item: menu) {
            AdAllMenu menuItem = new AdAllMenu(item.getIdMenu(), item.getValue(), item.getName(), item.getIcon(), item.getModule().getName());
            result.add(menuItem);
            menuItem.setSubMenus(buildRoleSubMenu(item.getSubMenus()));
        }
        return result;
    }

    /**
     * Find a menu item
     *
     * @param menu Menu tree
     * @param idMenu Menu identifier
     * @return Menu item
     */
    private AdAllMenu getMenuItem(List<AdAllMenu> menu, String idMenu) {
        for (AdAllMenu item: menu) {
            if (item.getIdMenu().equals(idMenu)) {
                return item;
            }
            AdAllMenu result = getMenuItem(item.getSubMenus(), idMenu);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    /**
     * Load a full menu tree from DB
     *
     * @param parentMenu Parent menu
     * @return Menu tree
     */
    private List<AdMenu> loadMenuTree(AdMenu parentMenu) {
        String sql = "SELECT M FROM AdMenu M \n";
        sql += "WHERE " + (parentMenu == null ? "M.parentMenu IS NULL \n" : "M.parentMenu = :parentMenu \n") +
               "ORDER BY M.seqno";
        Query query = entityManager.createQuery(sql, AdMenu.class);
        if (parentMenu != null)
            query.setParameter("parentMenu", parentMenu.getValue());
        List<AdMenu> menuList = query.getResultList();
        for (AdMenu menu: menuList) {
            menu.setSubMenus(loadMenuTree(menu));
        }
        return menuList;
    }
}
