package com.vincomobile.fw.backend.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.basic.process.ProcessMessage;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class DispatchMessageProcessImpl implements DispatchMessageProcess {

    private static ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(DispatchMessageProcessImpl.class);

    private CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void receiveMessage(Object message) {
        // logger.info(message.toString());
        try {
            ProcessMessage msg = mapper.readValue(message.toString(), ProcessMessage.class);
            ProcessDBLogger.logDB(msg.getIdProcessExec(), msg.getType(), Converter.truncText(msg.getLog(), 65500));
        } catch (Exception e) {
            logger.error(e.getMessage());
            logger.error(message.toString());
        }
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
