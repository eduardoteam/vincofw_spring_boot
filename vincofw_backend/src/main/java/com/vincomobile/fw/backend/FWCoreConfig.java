package com.vincomobile.fw.backend;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.services.ExtendedFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class FWCoreConfig extends FWConfig {

    @Autowired
    protected WebApplicationContext applicationContext;

    private void loadExtendedFilters() {
        for (String filterClassName : CacheManager.extendedFilters) {
            ExtendedFilter extendedFilter = (ExtendedFilter) applicationContext.getBean(filterClassName);
            List<ExtendedFilter> filters = CacheManager.cacheExtendedFilters.get(extendedFilter.getQualifier());
            if (filters == null) {
                filters = new ArrayList<>();
                CacheManager.cacheExtendedFilters.put(extendedFilter.getQualifier(), filters);
            }
            filters.add(extendedFilter);
        }
    }

    @PostConstruct
    public void init() {
        super.init();
        loadExtendedFilters();
        logger.info("Webapps directory: " + applicationContext.getServletContext().getRealPath("/"));
    }

}

