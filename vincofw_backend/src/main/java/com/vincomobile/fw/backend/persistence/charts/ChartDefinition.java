package com.vincomobile.fw.backend.persistence.charts;

import com.vincomobile.fw.backend.business.AdChartInfo;
import com.vincomobile.fw.backend.business.AdChartResult;
import com.vincomobile.fw.backend.persistence.model.AdChartLinked;
import com.vincomobile.fw.basic.persistence.model.AdChart;
import com.vincomobile.fw.basic.persistence.model.AdChartFilter;

import java.util.List;
import java.util.Map;

public interface ChartDefinition {

    /**
     * Set client id
     *
     * @param idClient Client id
     */
    void setIdClient(String idClient);

    /**
     * Set language id
     *
     * @param idLanguage Language id
     */
    void setIdLanguage(String idLanguage);

    /**
     * Set chart result
     *
     * @param result Chart result
     */
    void setChartResult(AdChartResult result);

    /**
     * Set chart
     *
     * @param chart Chart
     */
    void setChart(AdChart chart);

    /**
     * Set chart filters
     *
     * @param filters Filters
     */
    void setFilters(List<AdChartFilter> filters);

    /**
     * Set linked charts
     *
     * @param chartLinkeds linked chart list
     */
    void setChartLinkeds(List<AdChartLinked> chartLinkeds);

    /**
     * Chart constraints
     *
     * @param constraints Constraints
     */
    void setConstraints(Map<String, Object> constraints);

    /**
     * Set chart information
     *
     * @param chartInfo Chart information
     */
    void setChartInfo(AdChartInfo chartInfo);

}
