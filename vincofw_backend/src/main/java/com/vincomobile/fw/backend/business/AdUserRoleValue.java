package com.vincomobile.fw.backend.business;

import com.vincomobile.fw.basic.persistence.model.AdRole;

public class AdUserRoleValue {

    String idClient;
    String idRole;
    String roleType;
    String name;
    String description;

    public AdUserRoleValue(String idClient, AdRole role) {
        this.idClient = idClient;
        this.idRole = role.getIdRole();
        this.roleType = role.getRtype();
        this.name = role.getName();
        this.description = role.getDescription();
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
