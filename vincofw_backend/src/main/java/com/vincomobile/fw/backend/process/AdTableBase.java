package com.vincomobile.fw.backend.process;


import com.vincomobile.fw.basic.persistence.services.AdReferenceService;
import com.vincomobile.fw.basic.tools.plugins.CodeGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;

@Component
public class AdTableBase extends ProcessDefinitionImpl {

    public static final String ERROR                = "Error";

    @Autowired
    protected AdReferenceService adReferenceService;

    @Autowired
    protected ServletContext servletContext;

    @Autowired
    CodeGenerator codeGenerator;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

}
