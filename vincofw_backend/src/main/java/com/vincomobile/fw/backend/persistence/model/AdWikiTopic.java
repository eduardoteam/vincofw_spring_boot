package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_wiki_topic
 */
@Entity
@Table(name = "ad_wiki_topic")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdWikiTopic extends AdEntityBean {

    private String idWikiTopic;
    private String idWikiGuide;
    private String name;
    private String title;
    private String parentTopic;
    private Long seqno;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idWikiTopic;
    }

    @Override
    public void setId(String id) {
            this.idWikiTopic = id;
    }

    @Id
    @Column(name = "id_wiki_topic")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdWikiTopic() {
        return idWikiTopic;
    }

    public void setIdWikiTopic(String idWikiTopic) {
        this.idWikiTopic = idWikiTopic;
    }

    @Column(name = "id_wiki_guide", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdWikiGuide() {
        return idWikiGuide;
    }

    public void setIdWikiGuide(String idWikiGuide) {
        this.idWikiGuide = idWikiGuide;
    }

    @Column(name = "name", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "title", length = 1000)
    @NotNull
    @Size(min = 1, max = 1000)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "parent_topic", length = 150)
    @Size(min = 1, max = 150)
    public String getParentTopic() {
        return parentTopic;
    }

    public void setParentTopic(String parentTopic) {
        this.parentTopic = parentTopic;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdWikiTopic)) return false;

        final AdWikiTopic that = (AdWikiTopic) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idWikiTopic == null) && (that.idWikiTopic == null)) || (idWikiTopic != null && idWikiTopic.equals(that.idWikiTopic)));
        result = result && (((idWikiGuide == null) && (that.idWikiGuide == null)) || (idWikiGuide != null && idWikiGuide.equals(that.idWikiGuide)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((title == null) && (that.title == null)) || (title != null && title.equals(that.title)));
        result = result && (((parentTopic == null) && (that.parentTopic == null)) || (parentTopic != null && parentTopic.equals(that.parentTopic)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

}

