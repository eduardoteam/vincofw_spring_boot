package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.persistence.services.AdWindowService;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.persistence.beans.AdDesignerInfo;
import com.vincomobile.fw.basic.persistence.dto.AdTabMainDto;
import com.vincomobile.fw.basic.persistence.model.*;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_TAB
 *
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_tab/{idClient}")
public class AdTabController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdTabController.class);

    private final AdTabService service;
    private final AdFieldGridService fieldGridService;
    private final AdWindowService windowService;
    private final AdModuleService moduleService;

    public AdTabController(AdTabService service, AdFieldGridService fieldGridService, AdWindowService windowService, AdModuleService moduleService) {
        this.service = service;
        this.fieldGridService = fieldGridService;
        this.windowService = windowService;
        this.moduleService = moduleService;
    }

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdTab> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "idUser", required = false) String idUser,
        @RequestParam(value = "idLanguage", required = false) String idLanguage,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdTab.class);
        Page<AdTab> result = service.findAll(pageReq);
        if (!Converter.isEmpty(idLanguage)) {
            loadTranslations(result.getContent(), idClient, idLanguage);
        }
        if (!Converter.isEmpty(idUser)) {
            for (AdTab tab : result.getContent()) {
                List<AdFieldGrid> fields = fieldGridService.getUserColumns(idClient, tab.getIdTab(), idUser);
                if (fields.size() > 0) {
                    for (AdField field : tab.getFields()) {
                        AdFieldGrid fieldGrid = null;
                        for (AdFieldGrid fg : fields) {
                            if (fg.getIdField().equals(field.getIdField())) {
                                fieldGrid = fg;
                                break;
                            }
                        }
                        if (fieldGrid == null) {
                            field.setGridSeqno(0L);
                            field.setShowingrid(false);
                        } else {
                            field.setGridSeqno(fieldGrid.getSeqno());
                            field.setShowingrid(true);
                        }
                    }
                }
            }
        }
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdTab get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdTab entity) {
        logger.debug("POST create(" + entity + ")");
        if (entity.getViewDefault()) {
            service.clearViewDefault(entity.getIdWindow(), entity.getIdTable(), entity.getTablevel());
        }
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdTab entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        entity.setTable(null);
        entity.setFields(null);
        if (entity.getViewDefault()) {
            service.clearViewDefault(entity.getIdWindow(), entity.getIdTable(), entity.getTablevel());
        }
        updateEntity(entity);
        if (!entity.getViewDefault()) {
            service.checkViewDefault(entity.getIdWindow(), entity.getIdTable(), entity.getTablevel());
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "/editable_tab", method = RequestMethod.GET)
    @ResponseBody
    public ControllerResult editableTab(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "idUser") String idUser,
            @RequestParam(value = "idRole") String idRole,
            @RequestParam(value = "idTable") String idTable
    ) {
        logger.debug("GET editableTab(" + idTable + ")");
        ControllerResult result = new ControllerResult();
        result.put("tab", service.getUserEditableTab(idClient, idUser, idRole, idTable));
        return result;
    }

    @RequestMapping(value = "/{idTab}/load", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdTabMainDto load(
            @PathVariable("idClient") String idClient,
            @PathVariable("idTab") String idTab,
            @RequestParam String idWindow,
            @RequestParam String idLanguage
    ) {
        logger.debug("GET load(" + idWindow + ", " + idTab + ")");
        long time = System.currentTimeMillis();
        AdUser user = RestPreconditions.checkNotNull(getAuthUser());
        AdTabMainDto result = service.loadMainTab(idClient, user.getIdUser(), idWindow, idTab, idLanguage, windowService.getWindowGUI(idClient, idWindow));
        logger.debug("Tab loaded in: " +(System.currentTimeMillis() - time) + " ms");
        return result;
    }

    @RequestMapping(value = "/{idTab}/save", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public void save(
            @PathVariable("idClient") String idClient,
            @PathVariable("idTab") String idTab,
            @RequestBody AdDesignerInfo designer
    ) {
        logger.debug("POST save(" + idTab + ")");
        service.saveTabDesign(idTab, designer);
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdTab bean = (AdTab) entity;
        List<ValidationError> result = new ArrayList<>();
        String type = bean.getTtype();
        if (!AdTabService.TYPE_CHART.equals(type) && !AdTabService.TYPE_USERDEFINED.equals(type)) {
            if (Converter.isEmpty(bean.getIdTable())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "idTable", ValidationError.CODE_NOT_NULL, ""));
            }
        } else {
            bean.setIdTable(null);
        }
        if (AdTabService.TYPE_USERDEFINED.equals(type)) {
            if (Converter.isEmpty(bean.getCommand())) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "command", ValidationError.CODE_NOT_NULL, ""));
            } else {
                String[] command = bean.getCommand().split(":");
                if (command.length != 2) {
                    result.add(new ValidationError(ValidationError.TYPE_GLOBAL, "", ValidationError.CODE_MESSAGE, "AD_ValidationErrorTabCommand"));
                } else {
                    AdModule module = moduleService.findById(bean.getIdModule());
                    if (!command[0].trim().equals(module.getRestPath())) {
                        result.add(new ValidationError(ValidationError.TYPE_GLOBAL, "", ValidationError.CODE_MESSAGE, "AD_ValidationErrorTabCommandModule"));
                    }
                }
            }
        } else {
            bean.setCommand(null);
        }
        if (AdTabService.UIPATTERN_SORTABLE.equals(bean.getUipattern()) || AdTabService.UIPATTERN_MULTISELECT.equals(bean.getUipattern())) {
            bean.setViewDefault(false);
        }
        Long columns = bean.getColumns();
        if (columns == 5 || (columns > 6 && columns < 12)) {
            result.add(new ValidationError(ValidationError.TYPE_GLOBAL, "", ValidationError.CODE_MESSAGE, "AD_ValidationErrorTabColumns"));
        }
        return result;
    }

}