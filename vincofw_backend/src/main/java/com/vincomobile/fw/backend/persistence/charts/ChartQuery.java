package com.vincomobile.fw.backend.persistence.charts;

public class ChartQuery {

    private String select;
    private String groupby;
    private String orderby;
    private String from;
    private String where;
    private String limit;
    private String compareWhere;

    public ChartQuery(String select, String from, ChartWhere where, String groupby, String orderby, String limit) {
        this.select = select;
        this.from = from;
        this.where = where.getWhere();
        this.compareWhere = where.getCompareWhere();
        this.groupby = groupby;
        this.orderby = orderby;
        this.limit = limit;
    }


    public String getStandardQuery() {
        return "SELECT " + select + "\nFROM " + from + "\nWHERE " + where + "\n" + groupby + "\n" + orderby + limit;
    }

    public String getCompareQuery() {
        return "SELECT " + select + "\nFROM " + from + "\nWHERE " + compareWhere + "\n" + groupby + "\n" + orderby + limit;
    }
}
