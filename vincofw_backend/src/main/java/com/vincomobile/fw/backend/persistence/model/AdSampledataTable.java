package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Model for table ad_sampledata_table
 */
@Entity
@Table(name = "ad_sampledata_table")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdSampledataTable extends AdEntityBean {

    private String idSampledataTable;
    private String idSampledata;
    private String idTable;

    private AdSampledata sampledata;
    private AdTable table;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idSampledataTable;
    }

    @Override
    public void setId(String id) {
            this.idSampledataTable = id;
    }

    @Id
    @Column(name = "id_sampledata_table")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdSampledataTable() {
        return idSampledataTable;
    }

    public void setIdSampledataTable(String idSampledataTable) {
        this.idSampledataTable = idSampledataTable;
    }

    @Column(name = "id_sampledata", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdSampledata() {
        return idSampledata;
    }

    public void setIdSampledata(String idSampledata) {
        this.idSampledata = idSampledata;
    }

    @Column(name = "id_table", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sampledata", referencedColumnName = "id_sampledata", insertable = false, updatable = false)
    public AdSampledata getSampledata() {
        return sampledata;
    }

    public void setSampledata(AdSampledata sampledata) {
        this.sampledata = sampledata;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdSampledataTable)) return false;

        final AdSampledataTable that = (AdSampledataTable) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idSampledataTable == null) && (that.idSampledataTable == null)) || (idSampledataTable != null && idSampledataTable.equals(that.idSampledataTable)));
        result = result && (((idSampledata == null) && (that.idSampledata == null)) || (idSampledata != null && idSampledata.equals(that.idSampledata)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        return result;
    }


}

