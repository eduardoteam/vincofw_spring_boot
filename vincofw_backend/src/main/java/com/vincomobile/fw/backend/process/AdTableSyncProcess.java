package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.backend.persistence.model.AdColumnSync;
import com.vincomobile.fw.backend.persistence.model.AdTableSync;
import com.vincomobile.fw.backend.persistence.services.AdColumnSyncService;
import com.vincomobile.fw.backend.persistence.services.AdTableSyncService;
import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.process.OutputParam;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.ZipManager;
import com.vincomobile.fw.basic.tools.plugins.CodeGenerator;
import com.vincomobile.fw.basic.tools.plugins.CodeItem;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
@FWProcess
public class AdTableSyncProcess extends AdTableBase {
    public static final String ERROR = "Error";
    private Logger logger = LoggerFactory.getLogger(AdTableSyncProcess.class);

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdTableSyncProcess service = new AdTableSyncProcess();

    @Autowired
    AdTableSyncService tableSyncService;

    @Autowired
    AdColumnSyncService adColumnSyncService;

    /**
     * Read table information from DB and insert columns in AdColumnSync
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genColumns(String idProcessExec) {
        String idTable = getParamString(idProcessExec, "idTable");
        if (idTable == null) {
            return;
        }
        AdTableSync table = tableSyncService.findById(idTable);
        if (table == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idTablaSync"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        for (AdColumn column : table.getTable().getColumns()) {
            if ("DATABASE".equals(column.getCsource())) {
                AdColumnSync columnSync = adColumnSyncService.findByName(table.getIdTableSync(), column.getName());
                if (columnSync == null) {
                    columnSync = new AdColumnSync();
                    columnSync.setIdTableSync(table.getIdTableSync());
                    columnSync.setIdClient(table.getIdClient());
                    columnSync.setIdModule(table.getIdModule());
                    columnSync.setActive(true);
                    columnSync.setName(column.getName());
                    columnSync.setCtype(column.getCtype());
                    columnSync.setPrimaryKey(column.getPrimaryKey());
                    columnSync.setSeqno(column.getSeqno() == null ? 10 : column.getSeqno());
                    columnSync.setCsource(column.getCsource());
                    columnSync.setExportable(false);
                    columnSync.setImportable(true);
                    adColumnSyncService.save(columnSync);
                    info(idProcessExec, getMessage("AD_ProcessTableSyncSaveField", column.getName()));
                } else {
                    warn(idProcessExec, getMessage("AD_ProcessTableSyncSaveExists", column.getName()));
                }
            }
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Copy a table syncro from one client to another
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void copyTable(String idProcessExec) {
        String idTableSync = getParamString(idProcessExec, "idTableSync");
        String idClient = getParamString(idProcessExec, "idClient");
        if (idTableSync == null || idClient == null) {
            return;
        }
        AdTableSync table = tableSyncService.findById(idTableSync);
        if (table == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", "idTablaSync"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        if (table.getIdClient().equals(idClient)) {
            error(idProcessExec, getMessage("AD_ProcessSyncTableCopyEqualsClient"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        warn(idProcessExec, "TODO: Make table copy. Not implemented");
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Generate Swift code for CRUD operations (Model, Services, Controller)
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genSwift(String idProcessExec) {
        String idTableSync = getParamString(idProcessExec, "idTableSync");
        if (idTableSync == null) {
            return;
        }
        try {
            AdTableSync table = tableSyncService.findById(idTableSync);
            logger.debug("Table: " + table.getTable().getCapitalizeStandardName());

            byte[] fileResult = generateSwift(idProcessExec, table);
            if (fileResult.length==0)
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            File tmpFile = File.createTempFile("process_" + process.getIdProcess() + "_", ".zip");
            FileUtils.writeByteArrayToFile(tmpFile, fileResult);
            List<OutputParam> params = new ArrayList<>();
            params.add(new OutputParam("tableName", table.getTable().getCapitalizeStandardName()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS, tmpFile, params);
        } catch (IOException e) {
            logger.error(ERROR, e);
            error(idProcessExec, getMessage("AD_ProcessTableErr"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        } catch (TemplateException e) {
            logger.error(ERROR, e);
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
    }

    /**
     * Generates Swift classes
     *
     * @param idProcessExec Process Execution Identifier
     * @param table Table to generate
     * @return ZIP file with java classes
     * @throws IOException       Exception generating ZIP file
     * @throws TemplateException Exception generating template file
     */
    public byte[] generateSwift(String idProcessExec, AdTableSync table) throws IOException, TemplateException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipManager zip = new ZipManager(outputStream);
        HashMap<String, Object> options = new HashMap<>();
        options.put("table", table);
        options.put("date", Converter.formatDate(new Date()));
        options.put("year", "" + Calendar.getInstance().get(Calendar.YEAR));
        List<CodeItem> codeItems = codeGenerator.generate(CodeGenerator.MODE_SWIFT, CodeGenerator.GEN_TYPE_STANDARD, options);
        for (CodeItem code : codeItems) {
            String codeStr = code.getCode();
            if (!Converter.isEmpty(codeStr)) {
                String fileName = tableSyncService.getFileName(table, code.getCodegenFile().getPath());
                info(idProcessExec, getMessage("AD_ProcessTableCrudInfo", fileName));
                zip.addEntry(fileName);
                zip.writeToOS(codeStr.getBytes());
                zip.closeEntry();
            }
        }
        zip.getZipOS().close();
        IOUtils.closeQuietly(outputStream);
        return outputStream.toByteArray();
    }

}
