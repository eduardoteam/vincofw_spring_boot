package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdHtmlTemplate;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 03/05/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_html_template
 */
@Repository
@Transactional(readOnly = true)
public class AdHtmlTemplateServiceImpl extends BaseServiceImpl<AdHtmlTemplate, String> implements AdHtmlTemplateService {

    /**
     * Constructor.
     */
    public AdHtmlTemplateServiceImpl() {
        super(AdHtmlTemplate.class);
    }

}
