package com.vincomobile.fw.backend.business;

import java.util.List;

public class MenuRoleParam {

    String idRole;
    List<MenuRoleItem> menu;

    public MenuRoleParam() {
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    public List<MenuRoleItem> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuRoleItem> menu) {
        this.menu = menu;
    }
}
