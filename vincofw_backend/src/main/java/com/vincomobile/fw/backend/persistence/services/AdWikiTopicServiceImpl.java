package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.business.AdGuideTopic;
import com.vincomobile.fw.backend.persistence.model.AdWikiTopic;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.services.AdTranslationService;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.persistence.services.SqlSort;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 29/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_wiki_topic
 */
@Repository
@Transactional(readOnly = true)
public class AdWikiTopicServiceImpl extends BaseServiceImpl<AdWikiTopic, String> implements AdWikiTopicService {

    private final AdTranslationService translationService;

    /**
     * Constructor.
     */
    public AdWikiTopicServiceImpl(AdTranslationService translationService) {
        super(AdWikiTopic.class);
        this.translationService = translationService;
    }

    /**
     * Load a topic tree
     *
     * @param parent Parent topic
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idWikiGuide Guide identifier
     * @return Topic tree
     */
    @Override
    public List<AdGuideTopic> loadTopicTree(String parent, String idClient, String idLanguage, String idWikiGuide) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idWikiGuide", idWikiGuide);
        filter.put("parentTopic", parent);
        filter.put("active", true);
        List<AdGuideTopic> result = new ArrayList<>();
        List<AdWikiTopic> topics = findAll(new SqlSort("seqno", "asc"), filter);
        for (AdWikiTopic topic : topics) {
            String title = !Converter.isEmpty(idLanguage) ?
                translationService.getTranslation(idClient, FWConfig.TABLE_ID_WIKI_TOPIC, FWConfig.COLUMN_ID_WIKI_TOPIC_TITLE, idLanguage, topic.getIdWikiTopic(), topic.getTitle()) :
                topic.getTitle();
            AdGuideTopic guideTopic = new AdGuideTopic(topic.getIdWikiTopic(), topic.getName(), title, topic.getParentTopic(), topic.getSeqno());
            guideTopic.setTopics(loadTopicTree(topic.getName(), idClient, idLanguage, idWikiGuide));
            result.add(guideTopic);
        }
        return result;
    }

}
