package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdProcessParam;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.persistence.services.SqlSort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Servicio de ad_process_param
 *
 * Date: 06/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessParamServiceImpl extends BaseServiceImpl<AdProcessParam, String> implements AdProcessParamService {

    /**
     * Constructor.
     *
     */
    public AdProcessParamServiceImpl() {
        super(AdProcessParam.class);
    }

    /**
     * Load input params for a process
     *
     * @param idProcess Process identifier
     * @return Input params
     */
    @Override
    public List<AdProcessParam> getInputParams(String idProcess) {
        Map filter = new HashMap();
        filter.put("idProcess", idProcess);
        filter.put("ptype", AdProcessParamService.TYPE_IN);
        return findAll(new SqlSort("seqno", "asc"), filter);
    }

}
