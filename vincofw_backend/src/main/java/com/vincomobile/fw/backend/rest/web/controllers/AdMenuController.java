package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.persistence.model.AdMenu;
import com.vincomobile.fw.backend.persistence.services.AdMenuRolesService;
import com.vincomobile.fw.backend.persistence.services.AdMenuService;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.persistence.model.EntityBean;
import com.vincomobile.fw.basic.persistence.services.AdTranslationService;
import com.vincomobile.fw.basic.persistence.services.AdUserService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Controller para la tabla AD_MENU
 * <p/>
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_menu/{idClient}")
public class AdMenuController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdMenuController.class);

    @Autowired
    AdMenuService service;

    @Autowired
    AdUserService usuariosService;

    @Autowired
    AdMenuRolesService adMenuRolesService;

    @Autowired
    AdTranslationService translationService;

    @Override
    public BaseService getService() {
        return service;
    }

    /**
     * Return application menu
     *
     * @param page        Page number
     * @param limit       Page limit
     * @param sort        Sort criteria
     * @param fullExpand  Expand all menu. False for return first level
     * @param notExpand   Not expand
     * @param roleId      Role filter
     * @param idLanguage  Language filter
     * @param constraints Filters
     * @return Application menu
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdMenu> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "fullExpand", required = false, defaultValue = "false") Boolean fullExpand,
            @RequestParam(value = "notExpand", required = false, defaultValue = "false") Boolean notExpand,
            @RequestParam(value = "roleId", required = false) String roleId,
            @RequestParam(value = "idLanguage", required = false) String idLanguage,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));

        if (Converter.isEmpty(roleId)) {
            pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdMenu.class);
            return service.findAll(pageReq);
        }

        List<AdMenu> menuList = new ArrayList<>();
        pageReq.parseConstraints(constraints + ",idClient=[0;" + idClient + "]", AdMenu.class);
        List<AdMenu> mainMenu = service.loadSubmenu(null, (List<String>) pageReq.getSearchs().get("idClient"), roleId);
        if (notExpand == null || !notExpand) {
            if (fullExpand != null && fullExpand)
                service.expandMenuFull(menuList, mainMenu);
            else
                service.expandMenu(menuList, mainMenu);
        }
        if (menuList.isEmpty()) {
            menuList = mainMenu;
        }
        Page<AdMenu> result = new PageImpl<>(menuList, pageReq, menuList.size());
        translate(idClient, idLanguage, result.getContent());
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdMenu get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        AdMenu item = service.findById(id);
        return RestPreconditions.checkNotNull(item);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdMenu entity) {
        logger.debug("POST create(" + entity + ")");
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdMenu entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        entity.setWindow(null);
        entity.setProcess(null);
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        Map<String, Object> filter = new HashMap<>();
        filter.put("idMenu", id);
        adMenuRolesService.deleteAll(filter);
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    @RequestMapping(value = "all_menu", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public ControllerResult allMenu(@RequestParam(value = "idRole") String idRole) {
        logger.debug("GET allMenu()");
        ControllerResult result = new ControllerResult();
        result.put("menu", service.buildRoleMenu(idRole));
        return result;
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdMenu bean = (AdMenu) entity;
        List<ValidationError> result = new ArrayList<>();
        if (!AdMenuService.ACTION_WINDOW.equals(bean.getAction())) {
            bean.setIdWindow(null);
        }
        if (!AdMenuService.ACTION_PROCESS.equals(bean.getAction())) {
            bean.setIdProcess(null);
        }
        if (AdMenuService.ACTION_WINDOW.equals(bean.getAction()) && Converter.isEmpty(bean.getIdWindow())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "idWindow", ValidationError.CODE_NOT_NULL, ""));
        }
        if (AdMenuService.ACTION_PROCESS.equals(bean.getAction()) && Converter.isEmpty(bean.getIdProcess())) {
            result.add(new ValidationError(ValidationError.TYPE_FIELD, "idProcess", ValidationError.CODE_NOT_NULL, ""));
        }
        return result;
    }

    /**
     * Translate submenu options
     *
     * @param idClient Client id
     * @param idLanguage Language id
     * @param subMenus Submenu items
     */
    private void translate(String idClient, String idLanguage, List<AdMenu> subMenus) {
        for (AdMenu submenu : subMenus) {
            submenu.setName(translationService.getTranslation(idClient, "ff808081513458260151345c19980000", "ff808081513458260151345c2a780008", idLanguage, submenu.getIdMenu(), submenu.getName()));
            translate(idClient, idLanguage, submenu.getSubMenus());
        }
    }
}