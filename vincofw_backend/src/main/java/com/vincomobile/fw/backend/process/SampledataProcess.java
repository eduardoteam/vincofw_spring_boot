package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.backend.business.DatabaseUtils;
import com.vincomobile.fw.backend.persistence.model.AdSampledata;
import com.vincomobile.fw.backend.persistence.model.AdSampledataTable;
import com.vincomobile.fw.backend.persistence.services.AdImportDeleteService;
import com.vincomobile.fw.backend.persistence.services.AdSampledataService;
import com.vincomobile.fw.backend.persistence.services.AdSampledataTableService;
import com.vincomobile.fw.basic.JSONTable;
import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.services.AdColumnService;
import com.vincomobile.fw.basic.persistence.services.AdTableService;
import com.vincomobile.fw.basic.persistence.services.SqlSort;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype")
@Transactional(readOnly = true)
@FWProcess
public class SampledataProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static SampledataProcess service = new SampledataProcess();

    @Autowired
    AdSampledataService sampledataService;

    @Autowired
    AdSampledataTableService sampledataTableService;

    @Autowired
    AdTableService tableService;

    @Autowired
    AdImportDeleteService importDeleteService;

    @Autowired
    AdColumnService columnService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Export sample data to json files
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void exportSampledata(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBExportSampledata"));
        String idClientData = getParamString(idProcessExec, "idClientData");
        String idSampledata = getParamString(idProcessExec, "idSampledata");
        if (idClientData == null || idSampledata == null) {
            return;
        }
        AdSampledata sampledata = sampledataService.findById(idSampledata);
        if (sampledata == null) {
            error(idProcessExec, getMessage("AD_GlobalErrParam", idSampledata));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }
        if (!sampledata.getActive()) {
            error(idProcessExec, getMessage("AD_ProcessDBExportSampledataInactive", idSampledata));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        File dir = DatabaseUtils.getWorkDirectory(idClient, "/export/sampledata/" + idClientData, true);

        boolean hasError = false;
        List<AdSampledataTable> tables = sampledataTableService.listTables(idSampledata);
        for (AdSampledataTable table : tables) {
            // Call the method to persist the table data
            try {
                persistTableData(idProcessExec, dir, table.getTable(), idClientData);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ProcessDBExportTableErrData", table.getTable().getName()));
                logger.error("Error", e);
                hasError = true;
            }
        }
        finishExec(idProcessExec, hasError ? ProcessDefinition.STATUS_ERROR : ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Import sample data to json files
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void importSampledata(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBImportSampledata"));
        String idClientData = getParamString(idProcessExec, "idClientData");
        String idSampledata = getParamString(idProcessExec, "idSampledata");
        if (idClientData == null || idSampledata == null) {
            return;
        }

        File dir = DatabaseUtils.getWorkDirectory(idClient, "/import/sampledata/" + idClientData, false);
        if (!dir.exists()) {
            error(idProcessExec, getMessage("AD_ProcessDBImportSampledataNotExist", dir.getAbsolutePath()));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        // Import sample data
        boolean hasError = false;
        long threadId = Thread.currentThread().getId();
        List<AdSampledataTable> tables = sampledataTableService.listTables(idSampledata);
        for (AdSampledataTable table : tables) {
            // Call the method to persist the table data
            try {
                updateTableData(idProcessExec, dir, table.getTable(), threadId);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ProcessDBImportErrData", table.getTable().getName()));
                logger.error("Error", e);
                hasError = true;
            }
        }

        // Delete not updated rows
        info(idProcessExec, getMessage("AD_ProcessDBImportDelete"));
        for (int i = tables.size() - 1; i >= 0; i--) {
            AdSampledataTable table = tables.get(i);
            String sqlDeleteQuery = "", where = "";
            try {
                // Deleting the rows not present in the json file
                sqlDeleteQuery = "DELETE FROM " + table.getTable().getName();
                where = DatabaseUtils.getWHEREDelete(threadId, table.getTable(), idClientData);

                int deleted = tableService.applyNativeUpdate(sqlDeleteQuery + where);
                info(idProcessExec, getMessage("AD_ProcessDBImportDeleteInfo", table.getTable().getName(), deleted));
            } catch (Throwable ex) {
                DatabaseUtils.logError(logger, sqlDeleteQuery, ex, null);
                DatabaseUtils.logError(logger, tableService, table.getTable(), where);
                error(idProcessExec, getMessage("AD_ProcessDBImportDeleteErr", table.getTable().getName()));
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
        }
        importDeleteService.deleteProcessEntries(threadId);
        CacheManager.clearAll();

        finishExec(idProcessExec, hasError ? ProcessDefinition.STATUS_ERROR : ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Persist data of tables into JSON files
     *
     * @param idProcessExec Process Execution Identifier
     * @param directory Output directory
     * @param table Table to persist
     * @param idClientData Client selector
     * @throws Exception
     */
    private void persistTableData(String idProcessExec, File directory, AdTable table, String idClientData) throws Exception {
        try {
            SqlSort sort = new SqlSort();
            sort.addSorter("seqno", "asc");
            String sqlQuery = "SELECT ";
            Map<String, Object> filterColumns = new HashMap();
            filterColumns.put("csource", "DATABASE");
            filterColumns.put("id_table", table.getIdTable());
            List<AdColumn> columns = columnService.findAll(sort, filterColumns);
            for (AdColumn column : columns) {
                sqlQuery += "T." + (column.getName() + ", ");
            }
            sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 2);
            sqlQuery += "\nFROM " + table.getName() + " T";
            sqlQuery += "\nWHERE T.id_client = " + Converter.getSQLString(idClientData);
            JSONTable jsonTable = new JSONTable(table.getName());
            int exported = DatabaseUtils.exportDataToJSON(tableService.nativeQuery(sqlQuery), columns, jsonTable);
            DatabaseUtils.writeToJSON(directory, table.getName(), jsonTable);
            info(idProcessExec, getMessage("AD_ProcessDBExportSampledataTable", table.getName(), exported));
        } catch (IOException e) {
            logger.error("Error", e);
            throw e;
        }
    }

    /**
     * Update table from JSON files
     *
     * @param idProcessExec Process Execution Identifier
     * @param directory Output directory
     * @param table Table to persist
     * @param threadId Current thread identifier
     * @throws Exception
     */
    private void updateTableData(String idProcessExec, File directory, AdTable table, long threadId) throws Exception {
        // Update table
        HashMap<String, Object> valueCol = null;
        String sqlUpdateQuery = "";
        try {
            JSONTable tableJSON = tableService.getJSONTableByFile(directory + "/" + table.getName() + ".json");
            if (tableJSON != null) {
                String primaryKey = table.getKeyFields().get(0).getName();
                List<AdColumn> columns = table.getAllDBFields();
                // Iterating json rows
                int updated = 0, inserted = 0;
                for (int i = 0; i < tableJSON.getRows().size(); i++) {
                    valueCol = tableJSON.getRows().get(i);
                    String valueKey = valueCol.get(primaryKey).toString();
                    List<Object> row = tableService.nativeQuery(DatabaseUtils.getTableSelectByPk(tableJSON.getTableName(), primaryKey, valueKey));
                    if (row.size() > 0) {
                        // Update the row
                        sqlUpdateQuery = "UPDATE " + tableJSON.getTableName() + " SET ";
                        for (AdColumn currentCol : columns) {
                            if (!currentCol.getPrimaryKey()) {
                                String columName = currentCol.getName();
                                sqlUpdateQuery += (columName + " = " + DatabaseUtils.getSQLValue(columName, valueCol.get(columName), user.getIdUser()) + ", ");
                            }
                        }
                        sqlUpdateQuery = sqlUpdateQuery.substring(0, sqlUpdateQuery.length() - 2);
                        sqlUpdateQuery += " WHERE " + primaryKey + " = " + Converter.getSQLString(valueKey, true);
                        updated++;
                    } else {
                        // Insert the row
                        sqlUpdateQuery = "INSERT INTO " + tableJSON.getTableName() + " (";
                        String sqlValuesIn = " VALUES (";
                        for (AdColumn currentCol : columns) {
                            String columName = currentCol.getName();
                            sqlUpdateQuery += columName + ", ";
                            sqlValuesIn += (columName + " = " + DatabaseUtils.getSQLValue(columName, valueCol.get(columName), user.getIdUser()) + ", ");
                        }
                        sqlUpdateQuery = DatabaseUtils.getSQLInsert(sqlUpdateQuery, sqlValuesIn);
                        inserted++;
                    }
                    if (sqlUpdateQuery != null)
                        tableService.updateRow(sqlUpdateQuery);
                    // Insert into AdImportDelete
                    importDeleteService.addRow(threadId, tableJSON.getTableName(), valueKey);
                }
                info(idProcessExec, getMessage("AD_ProcessDBImportSampledataUpdate", table.getName(), updated, inserted));
            } else {
                error(idProcessExec, getMessage("AD_ProcessDBImportSampledataNotExistTable", table.getName()));
            }
        } catch (Throwable e) {
            DatabaseUtils.logError(logger, sqlUpdateQuery, e, valueCol);
            throw e;
        }
    }

}