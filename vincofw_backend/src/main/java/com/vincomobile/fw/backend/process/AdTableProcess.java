package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.persistence.beans.CodeGenColumn;
import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import com.vincomobile.fw.basic.persistence.model.AdReference;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.services.AdColumnService;
import com.vincomobile.fw.basic.persistence.services.AdRefTableService;
import com.vincomobile.fw.basic.persistence.services.AdReferenceService;
import com.vincomobile.fw.basic.persistence.services.AdTableService;
import com.vincomobile.fw.basic.process.OutputParam;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FieldsInfo;
import com.vincomobile.fw.basic.tools.ZipManager;
import com.vincomobile.fw.basic.tools.plugins.CodeGenerator;
import com.vincomobile.fw.basic.tools.plugins.CodeItem;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.hibernate.engine.spi.SessionImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Date;
import java.util.*;

@Component
@FWProcess
public class AdTableProcess extends AdTableBase {
    private Logger logger = LoggerFactory.getLogger(AdTableProcess.class);

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static AdTableProcess service = new AdTableProcess();

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    AdTableService tableService;

    @Autowired
    AdColumnService columnService;

    @Autowired
    AdReferenceService referenceService;

    @Autowired
    AdRefTableService refTableService;

    /**
     * Read table information from DB and insert columns en ADColumn
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genColumns(String idProcessExec) {
        String idTable = getParamString(idProcessExec, "idTable");
        if (idTable == null) {
            return;
        }
        AdTable table = tableService.findById(idTable);
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        try {
            if (table == null) {
                error(idProcessExec, getMessage("AD_ProcessTableErrId", idTable));
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
            doTableFields(idProcessExec, connection, table, true);
        } catch (SQLException e) {
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            logger.error(ERROR, e);
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Generate Java code for CRUD operations (Model, Services, Controller)
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genCrud(String idProcessExec) {
        String idTable = getParamString(idProcessExec, "idTable");
        String genType = getParamString(idProcessExec, "genType");
        if (idTable == null || genType == null) {
            return;
        }
        try {
            AdTable table = tableService.findById(idTable);
            if (validateTable(idProcessExec, table)) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ZipManager zip = new ZipManager(outputStream);
                if (!generateCRUD(idProcessExec, zip, table, genType)) {
                    finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                } else {
                    closeZipAndSend(idProcessExec, outputStream, zip, table.getCapitalizeStandardName());
                }
            } else {
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            }
        } catch (IOException e) {
            logger.error(ERROR, e);
            error(idProcessExec, getMessage("AD_ProcessTableErr"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        } catch (TemplateException e) {
            logger.error(ERROR, e);
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
    }

    /**
     * Validate table definition (Checking columns references)
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void doValidation(String idProcessExec) {
        String idTable = getParamString(idProcessExec, "idTable");
        if (idTable == null) {
            return;
        }
        AdTable table = tableService.findById(idTable);
        finishExec(idProcessExec, validateTable(idProcessExec, table) ? ProcessDefinition.STATUS_SUCCESS : ProcessDefinition.STATUS_ERROR);
    }

    /**
     * Generate Java code for CRUD operations (Model, Services, Controller)
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void genCrudModule(String idProcessExec) {
        String idModule = getParamString(idProcessExec, "idModule");
        String genType = getParamString(idProcessExec, "genType");
        if (idModule == null || genType == null) {
            return;
        }
        try {
            List<AdTable> tables = tableService.getModuleTable(idModule);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ZipManager zip = new ZipManager(outputStream);
            for (AdTable table : tables) {
                if (validateTable(idProcessExec, table)) {
                    if (!generateCRUD(idProcessExec, zip, table, genType)) {
                        finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                        return;
                    }
                }
            }
            closeZipAndSend(idProcessExec, outputStream, zip, "module");
        } catch (IOException e) {
            logger.error(ERROR, e);
            error(idProcessExec, getMessage("AD_ProcessTableErr"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        } catch (TemplateException e) {
            logger.error(ERROR, e);
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }
    }

    private void closeZipAndSend(String idProcessExec, ByteArrayOutputStream outputStream, ZipManager zip, String name) throws IOException {
        zip.getZipOS().close();
        IOUtils.closeQuietly(outputStream);
        byte[] fileResult = outputStream.toByteArray();
        File tmpFile = File.createTempFile("process_" + process.getIdProcess() + "_", ".zip");
        FileUtils.writeByteArrayToFile(tmpFile, fileResult);
        List<OutputParam> params = new ArrayList<>();
        params.add(new OutputParam("tableName", name));
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS, tmpFile, params);
    }

    private boolean validateTable(String idProcessExec, AdTable table) {
        info(idProcessExec, getMessage("AD_ProcessTableValidate", table.getName()));
        String result = tableService.validateTable(idClient, idLanguage, table);
        if (result == null) {
            info(idProcessExec, getMessage("AD_ProcessTableValidateOk", table.getName()));
        } else {
            error(idProcessExec, result);
        }
        return result == null;
    }

    /**
     * Process the fields of a database table
     *
     * @param idProcessExec Process Execution Identifier
     * @param connection Connection
     * @param table      Table to analyze
     * @param addOnly    True to add new columns only. False to check all columns
     * @throws SQLException SQL Exception thrown if problem occurs while accesing database
     */
    private void doTableFields(String idProcessExec, Connection connection, AdTable table, boolean addOnly) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        String catalog = connection.getCatalog();
        String dataBaseName = connection.getCatalog();
        String tableName = table.getName();
        List<String> tableKeys = new ArrayList<>();
        List<FieldsInfo> tableColumns = new ArrayList<>();
        if (AdTableService.TABLESOURCE_TABLE.equals(table.getDataOrigin())) {
            logger.debug("PrimaryKeys");
            ResultSet primaryKeysRs = meta.getPrimaryKeys(catalog, null, tableName);
            while (primaryKeysRs.next()) {
                String keyColumn = primaryKeysRs.getString("COLUMN_NAME");
                tableKeys.add(keyColumn);
                logger.debug("\t" + keyColumn);
            }
            ResultSet columnsRs = meta.getColumns(dataBaseName, null, tableName, null);
            while (columnsRs.next()) {
                tableColumns.add(new FieldsInfo(columnsRs.getString("COLUMN_NAME"), columnsRs.getString("REMARKS"), columnsRs.getString("NULLABLE").equals("0"), columnsRs.getInt("DATA_TYPE"), columnsRs.getInt("COLUMN_SIZE"), columnsRs.getInt("DECIMAL_DIGITS")));
            }
        }
        if (AdTableService.TABLESOURCE_SQL.equals(table.getDataOrigin())) {
            PreparedStatement ps = connection.prepareStatement(table.getSqlQuery());
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                tableColumns.add(new FieldsInfo(rsmd.getColumnName(i), null, rsmd.isNullable(i) == ResultSetMetaData.columnNullable, rsmd.getColumnType(i), rsmd.getColumnDisplaySize(i), rsmd.getPrecision(i)));
            }
        }

        Map<String, Object> filters = new HashMap<>();
        long seqNo = 0;
        for (FieldsInfo fieldInfo : tableColumns) {
            filters.clear();
            filters.put("name", fieldInfo.getName());
            filters.put("id_table", table.getId());
            AdColumn processedField = columnService.findFirst(filters);
            if (processedField != null && !Converter.isEmpty(processedField.getSeqno())) {
                if (seqNo < processedField.getSeqno() && processedField.getSeqno() < 1000) {
                    seqNo = processedField.getSeqno();
                }
            }
        }
        for (FieldsInfo fieldInfo : tableColumns) {
            String columnName = fieldInfo.getName();
            boolean isKey = tableKeys.contains(columnName);
            logger.debug("\t" + columnName + ", " + fieldInfo.getType());

            filters.clear();
            filters.put("name", columnName);
            filters.put("id_table", table.getId());
            AdColumn processedField = columnService.findFirst(filters);
            if (processedField == null) {
                processedField = new AdColumn();
            } else {
                warn(idProcessExec, getMessage("AD_ProcessTableColumnExist", processedField.getName()));
                if (Converter.isEmpty(processedField.getCodeDoc()) && !Converter.isEmpty(fieldInfo.getComment())) {
                    processedField.setCodeDoc(fieldInfo.getComment());
                    columnService.update(processedField);
                }
                if (addOnly)
                    continue;
            }
            String rtype = fieldInfo.getType().equals("BOOLEAN") ? "YESNO" : fieldInfo.getType().equals("FLOAT") ? "NUMBER" : fieldInfo.getType().equals("BLOB") ? "FILE" : fieldInfo.getType();
            filters.clear();
            filters.put("base", true);
            filters.put("rtype", rtype);
            AdReference reference = adReferenceService.findFirst(filters);
            if (reference == null) {
                error(idProcessExec, getMessage("AD_ProcessTableErrColumn", processedField.getName(), fieldInfo.getType()));
            } else {
                String id_reference = reference.getIdReference();
                processedField.setIdClient(table.getIdClient());
                processedField.setIdModule(table.getIdModule());
                processedField.setActive(true);
                processedField.setIdTable(table.getIdTable());
                processedField.setIdReference(id_reference);
                processedField.setName(columnName.toLowerCase());
                processedField.setCtype(fieldInfo.getType());
                processedField.setCformat(null);
                processedField.setValuedefault(null);
                processedField.setCodeDoc(fieldInfo.getComment());
                if (fieldInfo.getType().equals("STRING")) {
                    processedField.setLengthMin(1L);
                    processedField.setLengthMax((long) fieldInfo.getSize());
                }
                if (isKey) {
                    processedField.setSeqno(1000L);
                } else if ("created".equals(columnName)) {
                    processedField.setSeqno(1010L);
                } else if ("updated".equals(columnName)) {
                    processedField.setSeqno(1020L);
                } else if ("created_by".equals(columnName)) {
                    processedField.setSeqno(1030L);
                } else if ("updated_by".equals(columnName)) {
                    processedField.setSeqno(1040L);
                } else {
                    seqNo += 10;
                    processedField.setSeqno(seqNo);
                }
                if ("active".equals(columnName)) {
                    processedField.setValuedefault("@true@");
                }
                if ("id_client".equals(columnName)) {
                    processedField.setIdReference("A05ACA9B2E1A435CA2829CD738279503");
                    processedField.setIdReferenceValue("A05ACA9B2E1A435CA2829CD738279512");
                    processedField.setValuedefault("@idClient@");
                }
                processedField.setPrimaryKey(isKey);
                processedField.setMandatory(fieldInfo.isNullable());
                processedField.setCsource("DATABASE");
                processedField.setCsourceValue(null);
                processedField.setValuemin(null);
                processedField.setValuemax(null);
                processedField.setReadonlylogic(null);
                processedField.setShowinsearch(false);
                processedField.setTranslatable(false);
                processedField.setLinkParent(false);
                columnService.save(processedField);
                info(idProcessExec, getMessage("AD_ProcessTableColumn", processedField.getName()));
            }
        }
    }

    /**
     * Generates JPA java classes
     *
     * @param idProcessExec Process Execution Identifier
     * @param table Table to generate
     * @param genType Generation type
     * @return ZIP file with java classes
     * @throws IOException       Exception generating ZIP file
     * @throws TemplateException Exception generating template file
     */
    public boolean generateCRUD(String idProcessExec, ZipManager zip, AdTable table, String genType) throws IOException, TemplateException {
        // Prepare AdTable information
        for (AdColumn column : table.getColumns()) {
            if (!column.getName().equalsIgnoreCase("id_client") && AdReferenceService.BASE_ID_TABLE.equals(column.getIdReference()) && !Converter.isEmpty(column.getIdReferenceValue())) {
                String name = column.getName();
                if (name.startsWith("id_")) {
                    AdRefTable refTable = refTableService.getRefTableByReference(column.getIdReferenceValue());
                    CodeGenColumn codeGenColumn = new CodeGenColumn(
                            Converter.removeUnderscores(name.substring(3).toLowerCase()),
                            refTable.getTable().getCapitalizeStandardName(), column.getName(), refTable.getKey().getName()
                    );
                    column.setCodeGen(codeGenColumn);
                }
            }
        }
        // Generate CRUD
        HashMap<String, Object> options = new HashMap<>();
        options.put("ManyToOne", false);
        options.put("OneToMany", false);
        options.put("table", table);
        if (table.getModule() == null) {
            error(idProcessExec, getMessage("AD_ProcessTableErrJavaPackage"));
            return false;
        }
        options.put("package", table.getModule().getJavaPackage());
        options.put("codeDocumentation", table.getModule().getCodeDocumentation());
        options.put("date", Converter.formatDate(new Date()));
        options.put("year", "" + Calendar.getInstance().get(Calendar.YEAR));
        List<CodeItem> codeItems = codeGenerator.generate(CodeGenerator.MODE_JAVA, genType, options);
        for (CodeItem code : codeItems) {
            if ("modelPK".equals(code.getCodegenFile().getProperty()) && !table.getMultipleKey())
                continue;
            String codeStr = code.getCode();
            if (!Converter.isEmpty(codeStr)) {
                String fileName = tableService.getFileName(table, code.getCodegenFile().getPath());
                info(idProcessExec, getMessage("AD_ProcessTableCrudInfo", fileName));
                zip.addEntry(fileName);
                zip.writeToOS(codeStr.getBytes());
                zip.closeEntry();
            }
        }
        return true;
    }
}
