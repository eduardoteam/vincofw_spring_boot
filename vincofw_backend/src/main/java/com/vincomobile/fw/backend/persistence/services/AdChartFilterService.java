package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdChartFilter;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_chart_filter
 */
public interface AdChartFilterService extends BaseService<AdChartFilter, String> {

    /**
     * Get Tab chart filters
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @return Chart Filters
     */
    List<AdChartFilter> getChartFilters(String idClient, String idTab);

}


