package com.vincomobile.fw.backend.business;

import com.vincomobile.fw.backend.persistence.model.AdImage;

import java.util.Date;

public class AdPhoto {

    private String idImage;
    private String idTable;
    private String idRow;
    private Date updated;
    private String name;
    private String description;
    private String itype;
    private Long seqno;
    private String url;
    private String tableName;

    public AdPhoto(AdImage image) {
        this.idImage = image.getIdImage();
        this.idTable = image.getIdTable();
        this.idRow = image.getIdRow();
        this.updated = image.getUpdated();
        this.name = image.getName();
        this.description = image.getDescription();
        this.itype = image.getItype();
        this.seqno = image.getSeqno();
        this.url = image.getUrl();
        this.tableName = image.getTable().getName();
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    public String getIdRow() {
        return idRow;
    }

    public void setIdRow(String idRow) {
        this.idRow = idRow;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItype() {
        return itype;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}
