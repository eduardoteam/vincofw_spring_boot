package com.vincomobile.fw.backend.process;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

public interface QuartzSchedulerWrapper {
    org.quartz.Scheduler getScheduler();

    void scheduleAJob(Trigger trigger, JobDetail job) throws SchedulerException;
}
