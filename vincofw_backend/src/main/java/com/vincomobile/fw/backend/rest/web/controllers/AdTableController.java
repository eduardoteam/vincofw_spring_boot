package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.dto.AdTableDto;
import com.vincomobile.fw.basic.persistence.exception.FWBadRequestException;
import com.vincomobile.fw.basic.persistence.model.AdModule;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.services.AdModuleService;
import com.vincomobile.fw.basic.persistence.services.AdTableService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller para la tabla AD_TABLE
 * <p/>
 * Date: 19/02/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_table/{idClient}")
public class AdTableController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdTableController.class);

    @Autowired
    AdTableService service;

    @Autowired
    AdModuleService moduleService;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdTable> list(
            @PathVariable("idClient") String idClient,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdTable.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdTable get(@PathVariable("id") String id) {
        logger.debug("GET get(" + id + ")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(
            @RequestBody AdTable entity,
            BindingResult bindingResults
    ) {
        logger.debug("POST create(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        checkPreconditions(entity, bindingResults);
        CacheManager.removeTable(entity.getName());
        return (String) createEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(
            @RequestBody @Valid AdTable entity,
            BindingResult bindingResults
    ) {
        logger.debug("PUT update(" + entity + ")");
        RestPreconditions.checkRequestElementNotNull(entity);
        checkPreconditions(entity, bindingResults);
        AdTable stored = service.findById(entity.getId());
        RestPreconditions.checkNotNull(stored);
        entity.setCreated(stored.getCreated());
        entity.setClient(null);
        updateEntity(entity);
        CacheManager.removeTable(entity.getName());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        AdTable entity = service.findById(id);
        RestPreconditions.checkNotNull(entity);
        CacheManager.removeTable(entity.getName());
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Gets the list of tables not in AD_*
     */
    @RequestMapping(value = "listTables", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public List<AdTable> listTables() {
        logger.debug("GET listTables()");
        try {
            return service.readTablesInformation();
        } catch (SQLException e) {
            logger.error("Error obtaining information of the tables of ID:");
            return new ArrayList<AdTable>();
        }
    }

    @RequestMapping(value = "/{idTable}/load", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdTableDto load(@PathVariable("idTable") String idTable) {
        logger.debug("GET load(" + idTable + ")");
        long time = System.currentTimeMillis();
        AdTableDto result = service.loadTable(idTable);
        logger.debug("Table loaded in: " +(System.currentTimeMillis() - time) + " ms");
        return result;
    }

    /**
     * Check table name restrictions
     *
     * @param entity Entity values
     * @param bindingResults Binding errors
     */
    private void checkPreconditions(AdTable entity, BindingResult bindingResults) {
        // Check preconditions
        AdModule module = moduleService.findById(entity.getIdModule());
        if (module != null) {
            if (entity.getName().toLowerCase().startsWith(module.getDbprefix().toLowerCase()+"_")) {
                try {
                    if (!service.isValidTable(entity.getName(), entity.getIsview(), entity.getDataOrigin())) {
                        bindingResults.rejectValue("name", "InvalidValue", "AD_ErrValidationTableNotExist");
                    }
                } catch (SQLException e) {
                    logger.error("checkPreconditions: " + e.getMessage());
                    bindingResults.rejectValue("name", "InvalidValue", "AD_ErrValidationTableSQL");
                }
            } else {
                bindingResults.rejectValue("name", "InvalidValue", "AD_ErrValidationTableModuleDbprefix" + "." + module.getDbprefix() + "_");
            }
        } else {
            bindingResults.rejectValue("idModule", "InvalidValue", "AD_ErrValidationTableModule");
        }
        if (bindingResults.hasErrors()) {
            throw new FWBadRequestException(bindingResults);
        }
    }
}