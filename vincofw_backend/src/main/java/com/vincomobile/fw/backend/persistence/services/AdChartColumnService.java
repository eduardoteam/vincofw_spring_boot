package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdChartColumn;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Vincomobile FW on 09/11/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_chart_column
 */
public interface AdChartColumnService extends BaseService<AdChartColumn, String> {

    /**
     * Retrieves all the columns of the given chart
     *
     * @param idChart Chart Id
     * @return Chart columns
     */
    List<AdChartColumn> findChartColumns(String idChart);
}


