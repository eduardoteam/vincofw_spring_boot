package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_dbscript_sql
 */
@Entity
@Table(name = "ad_dbscript_sql")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdDbscriptSql extends AdEntityBean {

    private String idDbscriptSql;
    private String idDbscript;
    private String sqlUpdate;
    private Long seqno;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idDbscriptSql;
    }

    @Override
    public void setId(String id) {
            this.idDbscriptSql = id;
    }

    @Id
    @Column(name = "id_dbscript_sql")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdDbscriptSql() {
        return idDbscriptSql;
    }

    public void setIdDbscriptSql(String idDbscriptSql) {
        this.idDbscriptSql = idDbscriptSql;
    }

    @Column(name = "id_dbscript", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdDbscript() {
        return idDbscript;
    }

    public void setIdDbscript(String idDbscript) {
        this.idDbscript = idDbscript;
    }

    @Column(name = "sql_update", length = 2000)
    @NotNull
    @Size(min = 1, max = 2000)
    public String getSqlUpdate() {
        return sqlUpdate;
    }

    public void setSqlUpdate(String sqlUpdate) {
        this.sqlUpdate = sqlUpdate;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdDbscriptSql)) return false;

        final AdDbscriptSql that = (AdDbscriptSql) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idDbscriptSql == null) && (that.idDbscriptSql == null)) || (idDbscriptSql != null && idDbscriptSql.equals(that.idDbscriptSql)));
        result = result && (((idDbscript == null) && (that.idDbscript == null)) || (idDbscript != null && idDbscript.equals(that.idDbscript)));
        result = result && (((sqlUpdate == null) && (that.sqlUpdate == null)) || (sqlUpdate != null && sqlUpdate.equals(that.sqlUpdate)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

}

