package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdProcessOutput;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service layer implementation for ad_process_output
 *
 * Date: 29/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessOutputServiceImpl extends BaseServiceImpl<AdProcessOutput, String> implements AdProcessOutputService {

    /**
     * Constructor.
     */
    public AdProcessOutputServiceImpl() {
        super(AdProcessOutput.class);
    }

    /**
     * Load process output for a process
     *
     * @param idProcess Process identifier
     * @return Outputs
     */
    @Override
    public List<AdProcessOutput> getOutputs(String idProcess) {
        Map filter = new HashMap();
        filter.put("idProcess", idProcess);
        filter.put("active", true);
        return findAll(filter);
    }

}
