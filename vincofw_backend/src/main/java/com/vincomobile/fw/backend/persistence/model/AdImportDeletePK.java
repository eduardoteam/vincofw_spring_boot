package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Created by Vincomobile FW on 23/06/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Interface for PK of ad_import_delete
 */
@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdImportDeletePK implements Cloneable, Serializable {

    private Long processId;
    private String tableKey;
    private String tableName;

    public AdImportDeletePK() {
    }

    public AdImportDeletePK(Long processId, String tableKey, String tableName) {
        this.processId = processId;
        this.tableKey = tableKey;
        this.tableName = tableName;
    }

    @Transient
    @JsonIgnore
    public Class<?> getClassType() {
        return AdImportDeletePK.class;
    }

    @Transient
    public String getKey() {
        return processId + "_" + tableKey + "_" + tableName;
    }

    @Column(name = "process_id", nullable = false)
    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    @Column(name = "table_key", nullable = false)
    public String getTableKey() {
        return tableKey;
    }

    public void setTableKey(String tableKey) {
        this.tableKey = tableKey;
    }

    @Column(name = "table_name", nullable = false)
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    /**
     * Deep copy.
     * @return cloned object
     * @throws CloneNotSupportedException on error
     */
    @Override
    public AdImportDeletePK clone() throws CloneNotSupportedException {
        final AdImportDeletePK copy = (AdImportDeletePK) super.clone();
        copy.setProcessId(this.processId);
        copy.setTableKey(this.tableKey);
        copy.setTableName(this.tableName);
        return copy;
    }

    /**
     * toString implementation
     *
     * @see Object#toString()
     * @return String representation of this class.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("processId: ").append(this.processId).append(", ");
        sb.append("tableKey: ").append(this.tableKey).append(", ");
        sb.append("tableName: ").append(this.tableName).append(", ");
        return sb.toString();
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        Object proxyThat = aThat;

        if ( this == aThat ) {
            return true;
        }

        if (aThat == null)  {
            return false;
        }

        final AdImportDeletePK that;
        try {
            that = (AdImportDeletePK) proxyThat;
            if ( !(that.getClassType().equals(this.getClassType()))){
                return false;
            }
        } catch (org.hibernate.ObjectNotFoundException e) {
            return false;
        } catch (ClassCastException e) {
            return false;
        }


        boolean result = true;
        result = result && (((getProcessId() == null) && (that.getProcessId() == null)) || (getProcessId() != null && getProcessId().equals(that.getProcessId())));
        result = result && (((getTableKey() == null) && (that.getTableKey() == null)) || (getTableKey() != null && getTableKey().equals(that.getTableKey())));
        result = result && (((getTableName() == null) && (that.getTableName() == null)) || (getTableName() != null && getTableName().equals(that.getTableName())));
        return result;
    }

    /**
     * Calculate hash code
     *
     * @see Object#hashCode()
     * @return a calculated number
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += getProcessId().hashCode();
        hash += getTableKey().hashCode();
        hash += getTableName().hashCode();
        return hash;
    }

}