package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdFile;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_file
 *
 * Date: 23/01/2016
 */
public interface AdFileService extends BaseService<AdFile, String> {

    /**
     * Get files for table row
     *
     * @param idTable Table identifier
     * @param idRow Row identifier
     * @return File list
     */
    List<AdFile> getFiles(String idTable, String idRow);
}


