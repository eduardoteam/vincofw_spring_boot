package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdImportDelete;
import com.vincomobile.fw.backend.persistence.model.AdImportDeletePK;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 23/06/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_import_delete
 */
@Repository
@Transactional(readOnly = true)
public class AdImportDeleteServiceImpl extends BaseServiceImpl<AdImportDelete, AdImportDeletePK> implements AdImportDeleteService {

    /**
     * Constructor.
     */
    public AdImportDeleteServiceImpl() {
        super(AdImportDelete.class);
    }

    /**
     * Remove all entries for process
     *
     * @param processId Process identifier
     * @return Number of delete rows
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteProcessEntries(Long processId) {
        return applyUpdate("DELETE FROM AdImportDelete " + (processId == null ? "" : "WHERE id.processId = " + processId));
    }

    /**
     * Remove all entries for process/table
     *
     * @param processId Process identifier
     * @param tableName Table name
     * @return Number of delete rows
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int deleteProcessTableEntries(Long processId, String tableName) {
        return applyUpdate("DELETE FROM AdImportDelete WHERE id.processId = " + processId + " AND id.tableName = " + Converter.getSQLString(tableName));
    }

    /**
     * Add a row
     *
     * @param processId Process identifier
     * @param tableName Table name
     * @param tableKey Table key
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void addRow(Long processId, String tableName, String tableKey) {
        applyNativeUpdate("INSERT INTO ad_import_delete (process_id, table_name, table_key) VALUES (" + processId + ", '" + tableName + "', '" + tableKey + "')");
    }

}
