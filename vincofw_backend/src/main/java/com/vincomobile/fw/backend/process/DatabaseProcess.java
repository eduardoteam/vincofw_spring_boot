package com.vincomobile.fw.backend.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.backend.business.DatabaseUtils;
import com.vincomobile.fw.backend.persistence.model.AdDbscript;
import com.vincomobile.fw.backend.persistence.model.AdDbscriptSql;
import com.vincomobile.fw.backend.persistence.services.AdDbscriptService;
import com.vincomobile.fw.backend.persistence.services.AdDbscriptSqlService;
import com.vincomobile.fw.backend.persistence.services.AdImportDeleteService;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.JSONColumn;
import com.vincomobile.fw.basic.JSONColumnStructure;
import com.vincomobile.fw.basic.JSONTable;
import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.hooks.HookResult;
import com.vincomobile.fw.basic.persistence.beans.AdBaseTools;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.*;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.tools.Converter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope(value = "prototype")
@Transactional(readOnly = true)
@FWProcess
public class DatabaseProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static DatabaseProcess service = new DatabaseProcess();

    private static String coreModuleId = "D351EB083EC7E8C4891CD7B25B1E274E";

    @Autowired
    ServletContext servletContext;

    @Autowired
    AdModuleService moduleService;

    @Autowired
    AdTableService tableService;

    @Autowired
    AdImportDeleteService importDeleteService;

    @Autowired
    AdColumnService columnService;

    @Autowired
    AdLanguageService languageService;

    @Autowired
    AdDbscriptService dbscriptService;

    @Autowired
    AdDbscriptSqlService dbscriptSqlService;

    @PersistenceContext
    EntityManager entityManager;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Export data to json files
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void exportDB(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBExport"));
        Map<String, Object> filter = new HashMap<String, Object>();
        filter.put("dataOrigin", "TABLE");
        filter.put("name", "ad_%");
        SqlSort sort = new SqlSort();
        sort.addSorter("seqno_update", "DESC");
        List<AdTable> tables = tableService.findAll(sort, filter);
        List<AdModule> modules = moduleService.findAll();
        logger.debug("Reading tables");
        boolean hasError = false;
        String path = getJSONPath();
        for (AdTable table : tables) {
            info(idProcessExec, getMessage("AD_ProcessDBExportTable", table.getName()));
            // Call the method to persist the table structure
            try {
                persistTableColumnInformation(idProcessExec, table, modules, path);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ProcessDBExportTableErrStruct", table.getName()));
                logger.error("Error", e);
                hasError = true;
                continue;
            }
            // Call the method to persist the table data
            try {
                persistTableData(idProcessExec, table, modules, path);
            } catch (Exception e) {
                error(idProcessExec, getMessage("AD_ProcessDBExportTableErrData", table.getName()));
                logger.error("Error", e);
                hasError = true;
            }
        }
        if (hasError)
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        else
            finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Persist columns of tables into JSON files
     *
     * @param idProcessExec Process Execution Identifier
     * @param table         Table to persist
     * @param modules       Module list
     * @param path          Base directory to write files
     * @throws IOException
     */
    private void persistTableColumnInformation(String idProcessExec, AdTable table, List<AdModule> modules, String path) throws IOException {
        try {
            for (AdModule module : modules) {
                if (table.getIdModule().equals(module.getIdModule())) {
                    Map<String, Object> filterColumns = new HashMap<String, Object>();
                    filterColumns.put("csource", "DATABASE");
                    filterColumns.put("id_table", table.getId());
                    List<AdColumn> columns = columnService.findAll(filterColumns);
                    JSONColumn objColumn = new JSONColumn(table.getName());
                    for (AdColumn column : columns) {
                        JSONColumnStructure cStructure = new JSONColumnStructure();
                        cStructure.setName(column.getName());
                        cStructure.setcType(column.getCtype());
                        cStructure.setPrimaryKey(column.getPrimaryKey());
                        objColumn.addColumnStructure(cStructure);
                    }
                    // Persisting data
                    File dir = new File(path + module.getRestPath() + "/model");
                    if (!dir.exists())
                        dir.mkdirs();
                    String fullPath = dir.getPath() + "/" + table.getName() + ".json";
                    ObjectMapper mapper = new ObjectMapper();
                    // Object to JSON in file
                    mapper.writerWithDefaultPrettyPrinter().writeValue(new File(fullPath), objColumn);
                    info(idProcessExec, getMessage("AD_ProcessDBExportTableInfo", module.getName()));
                }
            }
        } catch (IOException e) {
            logger.error("Error", e);
            throw e;
        }
    }

    /**
     * Persist data of tables into JSON files
     *
     * @param idProcessExec Process Execution Identifier
     * @param table         Table to persist
     * @param modules       Module list
     * @param path          Base directory to write files
     * @throws IOException
     */
    private void persistTableData(String idProcessExec, AdTable table, List<AdModule> modules, String path) throws Exception {
        try {
            SqlSort sort = new SqlSort();
            sort.addSorter("seqno", "asc");
            for (AdModule module : modules) {
                JSONTable jsonTable = new JSONTable(table.getName());
                int exported = 0;
                if (table.getExportData()) {
                    String sqlQuery = "SELECT ";
                    Map<String, Object> filterColumns = new HashMap<String, Object>();
                    filterColumns.put("csource", "DATABASE");
                    filterColumns.put("id_table", table.getId());
                    List<AdColumn> columns = columnService.findAll(sort, filterColumns);
                    for (AdColumn column : columns) {
                        sqlQuery += "t." + (column.getName() + ", ");
                    }
                    sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 2);
                    sqlQuery += "\nFROM " + table.getName() + " t";
                    if ("ad_translation".equals(table.getName())) {
                        sqlQuery += "\nJOIN ad_table tb ON t.id_table = tb.id_table AND tb.id_module = " + Converter.getSQLString(coreModuleId);
                    }
                    sqlQuery += "\nWHERE t.id_module = " + Converter.getSQLString(module.getIdModule());
                    exported = DatabaseUtils.exportDataToJSON(tableService.nativeQuery(sqlQuery), columns, jsonTable);
                }
                File dir = new File(path + module.getRestPath() + "/data");
                if (!dir.exists())
                    dir.mkdirs();
                DatabaseUtils.writeToJSON(dir, table.getName(), jsonTable);
                info(idProcessExec, getMessage("AD_ProcessDBExportTableData", module.getName(), exported));
            }
        } catch (IOException e) {
            logger.error("Error", e);
            throw e;
        }
    }

    /**
     * Updates data from json files
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void updateDB(String idProcessExec) {
        info(idProcessExec, getMessage("AD_ProcessDBImport"));
        AdProcessParam defaultValuesParam = getParam("defaultValues");
        String defaultValues = defaultValuesParam != null ? defaultValuesParam.getString() : null;

        // Reading tables AD from DB
        Map<String, Object> filter = new HashMap<>();
        filter.put("dataOrigin", "TABLE");
        filter.put("name", "ad_%");
        List<AdTable> tables = tableService.findAll(new SqlSort("seqno_update", "ASC"), filter);
        List<AdModule> modules = moduleService.findAll();

        String path = getJSONPath();
        long threadId = Thread.currentThread().getId();
        importDeleteService.deleteProcessEntries(null);
        for (AdTable table : tables) {
            try {
                if (!table.getExportData()) {
                    continue;
                }
                updateTableData(idProcessExec, table, modules, path, threadId, defaultValues);
            } catch (Throwable ex) {
                logger.error("Error", ex);
                error(idProcessExec, getMessage("AD_ProcessDBImportErrData", table.getName()));
                error(idProcessExec, "Error: " + ex.getMessage());
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
        }
        // Delete not updated rows
        tables = tableService.findAll(new SqlSort("seqno_update", "DESC"), filter);
        info(idProcessExec, getMessage("AD_ProcessDBImportDelete"));
        for (AdTable table : tables) {
            if (!table.getExportData() || "ad_client".equals(table.getName())) {
                continue;
            }
            String sqlDeleteQuery = "", where = "";
            try {
                // Deleting the rows not present in the json file
                if ("ad_translation".equals(table.getName())) {
                    sqlDeleteQuery = "DELETE ad_translation FROM ad_translation JOIN ad_table ON ad_translation.id_table = ad_table.id_table AND ad_table.id_module = " + Converter.getSQLString(coreModuleId);
                } else {
                    sqlDeleteQuery = "DELETE FROM " + table.getName();
                }
                where = DatabaseUtils.getWHEREDelete(threadId, table, null);

                int deleted = tableService.applyNativeUpdate(sqlDeleteQuery + where);
                info(idProcessExec, getMessage("AD_ProcessDBImportDeleteInfo", table.getName(), deleted));
            } catch (Throwable ex) {
                logger.error("Error", ex);
                logger.error("SQL: " + sqlDeleteQuery);
                if (!"ad_translation".equals(table.getName())) {
                    DatabaseUtils.logError(logger, tableService, table, where);
                }
                error(idProcessExec, getMessage("AD_ProcessDBImportDeleteErr", table.getName()));
                finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
                return;
            }
        }
        importDeleteService.deleteProcessEntries(threadId);
        CacheManager.clearAll();
        // Exceute DB Scripts
        List<AdDbscript> dbscripts = dbscriptService.listActives();
        info(idProcessExec, getMessage("AD_ProcessDBImportApplyScripts", dbscripts.size()));
        for (AdDbscript dbscript : dbscripts) {
            info(idProcessExec, getMessage("AD_ProcessDBImportApplyScriptDB", dbscript.getName()));
            for (AdDbscriptSql sql : dbscript.getSql()) {
                if (sql.getActive()) {
                    entityManager.createNativeQuery(sql.getSqlUpdate()).executeUpdate();
                }
            }
        }
        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Update table from JSON files
     *
     * @param idProcessExec Process Execution Identifier
     * @param table         Table to update
     * @param modules       Module list
     * @param path          Base directory to read files
     * @param threadId      Current thread identifier
     * @param defaultValues Default values for new columns
     * @throws Exception
     */
    private void updateTableData(String idProcessExec, AdTable table, List<AdModule> modules, String path, long threadId, String defaultValues) throws Exception {
        // Parse default columns values
        HashMap<String, Object> defaultColumnValues = new HashMap<>();
        if (!Converter.isEmpty(defaultValues)) {
            String[] values = defaultValues.split(",");
            for (String val : values) {
                String[] tokens = val.split("=");
                if (tokens.length == 2) {
                    defaultColumnValues.put(tokens[0].trim(), tokens[1].trim());
                }
            }
        }
        // Update table
        HashMap<String, Object> valueCol = null;
        String sqlUpdateQuery = "";
        try {
            info(idProcessExec, getMessage("AD_ProcessDBImportUpdate", table.getName()));
            JSONColumn columStructure = null;
            // Loading json file model
            for (AdModule module : modules) {
                String fileColumnPath = path + module.getRestPath() + "/model/" + table.getName() + ".json";
                columStructure = tableService.getJSONColumsByFile(fileColumnPath);
                if (columStructure != null) {
                    break;
                }
            }
            if (columStructure != null) {
                String contextPath = servletContext.getContextPath();
                Map htmlColumns = new HashMap();
                for (AdColumn column : table.getColumns()) {
                    if (AdReferenceService.RTYPE_HTML.equals(column.getReference().getRtype())) {
                        htmlColumns.put(column.getName(), "YES");
                    }
                }
                for (AdModule module : modules) {
                    String filePath = path + module.getRestPath() + "/data/" + table.getName() + ".json";
                    JSONTable tableJSON = tableService.getJSONTableByFile(filePath);
                    if (tableJSON != null) {
                        String primaryKey = table.getKeyFields().get(0).getName();
                        // Iterating json rows
                        int updated = 0, inserted = 0;
                        for (int i = 0; i < tableJSON.getRows().size(); i++) {
                            valueCol = tableJSON.getRows().get(i);
                            String valueKey = valueCol.get(primaryKey).toString();
                            List<Object> row = tableService.nativeQuery(DatabaseUtils.getTableSelectByPk(tableJSON.getTableName(), primaryKey, valueKey));
                            if (row.size() > 0) {
                                if ("ad_preference".equals(table.getName()) && "false".equals(valueCol.get("isimportable").toString())) {
                                    sqlUpdateQuery = null;
                                } else {
                                    // Update the row
                                    sqlUpdateQuery = "UPDATE " + tableJSON.getTableName() + " SET ";
                                    boolean userTable = "ad_user".equals(tableJSON.getTableName());
                                    for (JSONColumnStructure currentCol : columStructure.getColumns()) {
                                        String columName = currentCol.getName();
                                        if (userTable && ("password".equals(columName) || "photo".equals(columName))) {
                                            continue;
                                        }
                                        Object value = valueCol.containsKey(columName) ? valueCol.get(columName) : defaultColumnValues.get(columName);
                                        if (htmlColumns.containsKey(columName)) {
                                            value = updateCKEditorImages(contextPath, (String) value);
                                        }
                                        sqlUpdateQuery += columName + " = ";
                                        if ("ad_table".equals(table.getName()) && "id_sort_column".equals(columName)) {
                                            if (value != null) {
                                                AdColumn sortColumn = columnService.findById(value.toString());
                                                if (sortColumn == null) {
                                                    value = null;
                                                    warn(idProcessExec, getMessage("AD_ProcessDBImportTableSortColumn", module.getName()));
                                                }
                                            }
                                        }
                                        sqlUpdateQuery += DatabaseUtils.getSQLValue(columName, value, user.getIdUser()) + ", ";
                                    }
                                    sqlUpdateQuery = sqlUpdateQuery.substring(0, sqlUpdateQuery.length() - 2);
                                    sqlUpdateQuery += " WHERE " + primaryKey + " = " + Converter.getSQLString(valueKey, true);
                                }
                                updated++;
                            } else {
                                // Insert the row
                                if ("ad_translation".equals(table.getName())) {
                                    AdLanguage lang = languageService.findById((String) valueCol.get("id_language"));
                                    if (lang == null)
                                        continue;
                                }
                                sqlUpdateQuery = "INSERT INTO " + tableJSON.getTableName() + " (";
                                String sqlValuesIn = " VALUES (";
                                for (JSONColumnStructure currentCol : columStructure.getColumns()) {
                                    String columName = currentCol.getName();
                                    sqlUpdateQuery += columName + ", ";
                                    Object value = valueCol.containsKey(columName) ? valueCol.get(columName) : defaultColumnValues.get(columName);
                                    if (htmlColumns.containsKey(columName)) {
                                        value = updateCKEditorImages(contextPath, (String) value);
                                    }
                                    if ("ad_table".equals(table.getName()) && "id_sort_column".equals(columName)) {
                                        if (value != null) {
                                            value = null;
                                            warn(idProcessExec, getMessage("AD_ProcessDBImportTableSortColumn", module.getName()));
                                        }
                                    }
                                    sqlValuesIn += (DatabaseUtils.getSQLValue(columName, value, user.getIdUser()) + ", ");
                                }
                                sqlUpdateQuery = DatabaseUtils.getSQLInsert(sqlUpdateQuery, sqlValuesIn);
                                inserted++;
                            }
                            if (sqlUpdateQuery != null)
                                tableService.updateRow(sqlUpdateQuery);
                            // Insert into AdImportDelete
                            importDeleteService.addRow(threadId, tableJSON.getTableName(), valueKey);
                        }
                        info(idProcessExec, getMessage("AD_ProcessDBImportUpdateInfo", module.getName(), updated, inserted));
                    } else {
                        error(idProcessExec, getMessage("AD_ProcessDBImportErrTableJSON", module.getName()));
                    }
                }
            } else {
                throw new Exception("Not found table model");
            }
        } catch (Throwable e) {
            DatabaseUtils.logError(logger, sqlUpdateQuery, e, valueCol);
            throw e;
        }
    }

    /**
     * Check module for AD tables
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void checkDB(String idProcessExec) {
        // Characteristics
        checkTableErrors(
                idProcessExec, "AdCharacteristicDef",
                "SELECT characteristic.name, module.name as moduleName FROM AdCharacteristicDef WHERE idModule <> characteristic.idModule"
        );

        // Charts
        checkTableErrors(
                idProcessExec, "AdChart",
                "SELECT concat(W.name, ' - ', T.name, ' - ', C.name) as name, C.module.name as moduleName " +
                "FROM AdChart C, AdTab T, AdWindow W " +
                "WHERE C.idTab = T.idTab AND W.idWindow = T.idWindow AND C.idModule <> T.idModule"
        );

        // Charts
        checkTableErrors(
                idProcessExec, "AdChartFilter",
                "SELECT concat(W.name, ' - ', T.name, ' - ', CF.name) as name, CF.module.name as moduleName " +
                "FROM AdChartFilter CF, AdTab T, AdWindow W " +
                "WHERE CF.idTab = T.idTab AND W.idWindow = T.idWindow AND CF.idModule <> T.idModule"
        );

        checkTableErrors(
                idProcessExec, "AdChartColumn",
                "SELECT concat(W.name, ' - ', T.name, ' - ', C.name, ' - ', CC.name) as name, CC.module.name as moduleName " +
                "FROM AdChartColumn CC, AdChart C, AdTab T, AdWindow W " +
                "WHERE CC.idChart = C.idChart AND C.idTab = T.idTab AND W.idWindow = T.idWindow AND CC.idModule <> C.idModule"
        );

        checkTableErrors(
                idProcessExec, "AdChartLinked",
                "SELECT concat(W.name, ' - ', T.name, ' - ', CL.chart.name, ' - ', CL.linked.name) as name, CL.module.name as moduleName " +
                "FROM AdChartLinked CL, AdTab T, AdWindow W " +
                "WHERE CL.chart.idTab = T.idTab AND W.idWindow = T.idWindow AND CL.idModule <> CL.chart.idModule"
        );

        // Columns
        checkTableErrors(
                idProcessExec, "AdColumn",
                "SELECT concat(C.table.name, ' - ', C.name) as name, C.module.name as moduleName " +
                "FROM AdColumn C WHERE C.idModule <> C.table.idModule"
        );

        checkTableErrors(
                idProcessExec, "AdColumnSync",
                "SELECT concat(T.name, ' - ', C.name) as name, C.module.name as moduleName " +
                "FROM AdColumnSync C, AdTable T " +
                "WHERE C.tableSync.idTable = T.idTable AND C.idModule <> C.tableSync.idModule"
        );

        // Fields
        checkTableErrors(
                idProcessExec, "AdField",
                "SELECT concat(W.name, ' - ', T.name, ' - ', F.name) as name, F.module.name as moduleName " +
                "FROM AdField F, AdTab T, AdWindow W " +
                "WHERE F.idTab = T.idTab AND W.idWindow = T.idWindow AND F.idModule <> T.idModule"
        );

        // References
        checkTableErrors(
                idProcessExec, "AdRefTable",
                "SELECT concat(RT.reference.name, ' - ', RT.table.name) as name, RT.module.name as moduleName " +
                "FROM AdRefTable RT " +
                "WHERE RT.idModule <> RT.table.idModule"
        );

        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Show errors for checked table
     *
     * @param idProcessExec Process Execution Identifier
     * @param table Table name
     * @param select Check query
     */
    private void checkTableErrors(String idProcessExec, String table, String select) {
        info(idProcessExec, getMessage("AD_ProcessCheckDBCheckingTable", table));
        Query query = entityManager.createQuery(select);
        List<Object[]> errors = query.getResultList();
        for (Object[] item: errors) {
            warn(idProcessExec, getMessage("AD_ProcessCheckDBDiffModule", AdBaseTools.getString(item, 0), AdBaseTools.getString(item, 1)));
        }
    }

    /**
     * Change context path for CKEditor uploaded images
     *
     * @param contextPath Web application context path
     * @param html HTML content
     * @return New HTML content
     */
    private String updateCKEditorImages(String contextPath, String html) {
        if (Converter.isEmpty(html))
            return html;
        Document doc = Jsoup.parse(html);
        boolean replaceImg = false;
        for (Element el : doc.getElementsByTag("img")) {
            String src = el.attr("src");
            int index = src.indexOf("/restful/api/vinco_core/ckeditor");
            if (index >= 0) {
                replaceImg = true;
                src = contextPath + src.substring(index);
                el.attr("src", src);
            }
        }
        return replaceImg ? doc.toString() : html;
    }

    /**
     * Get JSON path to read/write files
     *
     * @return JSON Path
     */
    private String getJSONPath() {
        String path;
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_BASE_DIRECTORY, idClient, user);
        if (hookResult.success && hookResult.hasKey(FWConfig.HOOK_GET_BASE_DIRECTORY_DATABASE)) {
            path = hookResult.get(FWConfig.HOOK_GET_BASE_DIRECTORY_DATABASE) + "/database/";
        } else {
            path = servletContext.getRealPath("WEB-INF/database") + File.pathSeparatorChar;
        }
        return path;
    }
}