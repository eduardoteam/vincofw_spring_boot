package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdWikiGuideRole;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vincomobile FW on 30/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_wiki_guide_role
 */
@Repository
@Transactional(readOnly = true)
public class AdWikiGuideRoleServiceImpl extends BaseServiceImpl<AdWikiGuideRole, String> implements AdWikiGuideRoleService {

    /**
     * Constructor.
     */
    public AdWikiGuideRoleServiceImpl() {
        super(AdWikiGuideRole.class);
    }

}
