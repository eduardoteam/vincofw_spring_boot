package com.vincomobile.fw.backend.process;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.backend.persistence.services.AdProcessOutputService;
import com.vincomobile.fw.backend.persistence.services.ReportService;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.persistence.beans.AdProcessLog;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.*;
import com.vincomobile.fw.basic.persistence.services.AdClientService;
import com.vincomobile.fw.basic.persistence.services.AdMessageService;
import com.vincomobile.fw.basic.persistence.services.AdPreferenceService;
import com.vincomobile.fw.basic.persistence.services.AdTranslationService;
import com.vincomobile.fw.basic.process.OutputParam;
import com.vincomobile.fw.basic.process.ProcessMessage;
import com.vincomobile.fw.basic.process.TinyProcess;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import com.vincomobile.fw.basic.tools.Mailer;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional(readOnly = true)
public abstract class ProcessDefinitionImpl implements ProcessDefinition {

    private static final String ERROR = "ERROR";

    protected Logger logger = LoggerFactory.getLogger(ProcessDefinitionImpl.class);

    @Autowired
    protected AdProcessOutputService outputService;

    @Autowired
    protected AdMessageService messageService;

    @Autowired
    protected AdClientService clientService;

    @Autowired
    protected AdTranslationService translationService;

    @Autowired
    protected AdPreferenceService preferenceService;

    @Autowired
    DispatchMessageProcess dispatchMessageProcess;

    @Autowired
    public Mailer mailer;

    @Autowired
    ConnectionFactory connectionFactory;

    @Autowired
    RabbitTemplate rabbitTemplate;

    protected String idClient;
    protected String idLanguage;
    protected String lastError;
    protected String finishStatus;
    protected AdUser user;
    protected AdProcess process;
    protected List<AdProcessParam> params;
    protected List<AdProcessOutput> outputs;
    protected HttpServletResponse response;
    protected HttpServletRequest request;
    protected ServletContext servletContext;
    protected boolean inExecution = false;
    private Long startTime;
    private ObjectMapper mapper = new ObjectMapper();
    private SimpleMessageListenerContainer listenerContainer;
    private Binding queueLogBinding;
    private Binding queueWebBinding;

    /**
     * Set user executor
     *
     * @param user User
     */
    @Override
    public void setUser(AdUser user) {
        this.user = user;
    }

    /**
     * Set client id
     *
     * @param idClient Client id
     */
    @Override
    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    /**
     * Set language id
     *
     * @param idLanguage Language id
     */
    @Override
    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    /**
     * Set process item
     *
     * @param process Process
     */
    @Override
    public void setProcess(AdProcess process) {
        this.process = process;
        Map filter = new HashMap();
        filter.put("idProcess", process.getIdProcess());
        outputs = outputService.findAll(filter);
    }

    /**
     * Set process parameters
     *
     * @param params Parameter list
     */
    @Override
    public void setParams(List<AdProcessParam> params) {
        this.params = params;
    }

    /**
     * Set Http Servlet Response
     *
     * @param response Response
     */
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    /**
     * Set Http Servlet Request
     *
     * @param request Request
     */
    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Set Http Servlet Context
     *
     * @param servletContext Servlet context
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * This method is called when process is activate/desactivate
     *
     * @param active Activate / Desactivate
     */
    @Override
    public void setActive(boolean active) {

    }

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return true;
    }

    /**
     * Get if process notify error by email
     *
     * @return If concurrent
     */
    @Override
    public boolean notifyError() {
        return false;
    }

    /**
     * Get template for email notification
     *
     * @return Path to email template
     */
    @Override
    public String getMailNotificationTemplate() {
        return null;
    }

    /**
     * Set model for email notification template
     *
     * @param model     Model for data
     * @param resources Resources
     */
    @Override
    public void setModelForMailNotificationTemplate(Map model, Map<String, String> resources) {
        model.put("phone", CacheManager.getPreferenceString(idClient, ReportService.PREF_HEADER_PHONE));
        model.put("email", CacheManager.getPreferenceString(idClient, ReportService.PREF_HEADER_EMAIL));
        model.put("company", Converter.getHTMLString(CacheManager.getPreferenceString(idClient, ReportService.PREF_HEADER_COMPANY)));

        model.put("processId", process.getIdProcess());
        model.put("processName", Converter.getHTMLString(process.getName()));
        model.put("processClass", process.getClassname());
        model.put("processMethod", process.getClassmethod());
        model.put("lastError", Converter.getHTMLString(lastError));
    }

    /**
     * Initialize proccess execution
     *
     * @return Process Execution Identifier
     */
    @Override
    public String initExec() {
        inExecution = true;
        lastError = "";
        finishStatus = STATUS_RUNNING;
        startTime = System.currentTimeMillis();
        logger.debug("InitExec: " + process.getName());
        bindToRabbitExchange(dispatchMessageProcess);
        String idProcessExec = ProcessDBLogger.initExec(process, user.getIdUser());
        if (FWConfig.runtimeExcludeProcess.contains(process.getIdProcess())) {
            error(idProcessExec, getMessage("AD_ErrProcessExecuteNotAllow"));
            return null;
        } else {
            success(idProcessExec, getStartMsg());
            return idProcessExec;
        }
    }

    /**
     * Finish process execution
     *
     * @param idProcessExec Process Execution Identifier
     * @param status Execution status
     * @param output Output file (Optional)
     * @param params List parameters (Optional)
     */
    @Override
    public void finishExec(String idProcessExec, String status, File output, List<OutputParam> params) {
        long endTime = System.currentTimeMillis();
        long seconds = (endTime - startTime) / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        String time = Converter.leftFill(hours, '0', 2) + ":" + Converter.leftFill(minutes % 60, '0', 2) + ":" + Converter.leftFill(seconds % 60, '0', 2);
        info(idProcessExec, getMessage("AD_ProcessExecutionTime", time));

        logger.debug("FinishExec: " + process.getName() + ", " + status + " (" + time + ")");
        // Update status
        inExecution = false;
        finishStatus = status;
        ProcessDBLogger.finishExec(idProcessExec, status);
        if (STATUS_SUCCESS.equals(status)) {
            if (output != null)
                sendOutput(idProcessExec, output, params);
            success(idProcessExec, getFinishMsg(status));
        } else {
            error(idProcessExec, getFinishMsg(status));
            if (notifyError()) {
                AdClient client = clientService.findById(idClient);
                if (client != null && Converter.validEmail(client.getEmail())) {
                    String template = getMailNotificationTemplate();
                    if (template != null) {
                        Map model = new HashMap();
                        Map<String, String> resources = new HashMap();
                        setModelForMailNotificationTemplate(model, resources);
                        info(idProcessExec, getMessage("AD_ProcessNotifyError", client.getEmail(), "Error - " + process.getName()));
                        mailer.sendTemplateEmail(mailer.getFromUser(), client.getEmail(), "Error - " + process.getName(), template, model, resources);
                    }
                }
            }
        }
        sendFinishLog(idProcessExec, STATUS_SUCCESS.equals(status));
        HookManager.executeHook(FWConfig.HOOK_PROCESS_FINISH, idClient, user, process, status);
        unbindToRabbitExchange();
    }

    @Override
    public void finishExec(String idProcessExec, String status, File output) {
        finishExec(idProcessExec, status, output, null);
    }

    @Override
    public void finishExec(String idProcessExec, String status) {
        finishExec(idProcessExec, status, null, null);
    }

    /**
     * Return start message for log
     *
     * @return Message
     */
    @Override
    public String getStartMsg() {
        return getMessage("AD_ProcessStart");
    }

    /**
     * Return finish message for log
     *
     * @param status Execution status
     * @return Message
     */
    @Override
    public String getFinishMsg(String status) {
        return STATUS_SUCCESS.equals(status) ? getMessage("AD_ProcessFinished") : getMessage("AD_ProcessFinishedError");
    }

    /**
     * Get last error description
     *
     * @return Last error
     */
    @Override
    public String getLastError() {
        return lastError;
    }

    /**
     * Return if process is in execution
     *
     * @return If executing
     */
    @Override
    public boolean isInExecution() {
        return inExecution;
    }

    /**
     * Return if the process have HTTP outputs
     *
     * @return If have HTTP outputs
     */
    @Override
    public boolean hasHttpOutputs() {
        for (AdProcessOutput output : outputs) {
            if (OUTPUT_HTTP.equals(output.getOtype())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get start time
     *
     * @param idProcess Process Execution Identifier
     * @return Start time
     */
    @Override
    public Date getStarted(String idProcess) {
        return ProcessDBLogger.getStarted(idProcess);
    }

    /**
     * Get finish time
     *
     * @param idProcess Process Execution Identifier
     * @return Finish time
     */
    @Override
    public Date getFinished(String idProcess) {
        return ProcessDBLogger.getFinished(idProcess);
    }

    /**
     * Return process execution status
     *
     * @return If not have errors
     */
    @Override
    public boolean isSuccess() {
        return STATUS_SUCCESS.equals(finishStatus);
    }

    /**
     * Send a message to RabbitMQ Exchange
     *
     * @param message Message
     */
    private void sendToRabbitMQ(ProcessMessage message) {
        try {
            rabbitTemplate.convertAndSend(
                    TinyProcess.RABBIT_EXCHANGE_NAME, TinyProcess.RABBIT_ROUTING_KEY + process.getIdProcess(),
                    mapper.writeValueAsString(message)
            );
        } catch (JsonProcessingException e) {
            logger.error("Error send message: " + e.getMessage());
        }
    }

    /**
     * Write success to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    @Override
    public void success(String idProcessExec, String log) {
        logger.info(log);
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_SUCCESS, log));
    }

    /**
     * Write log to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    @Override
    public void info(String idProcessExec, String log) {
        logger.info(log);
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_INFO, log));
    }

    /**
     * Write warning to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    @Override
    public void warn(String idProcessExec, String log) {
        logger.warn(log);
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_WARN, log));
    }

    /**
     * Write error to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    @Override
    public void error(String idProcessExec, String log) {
        logger.error(log);
        lastError = log;
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_ERROR, log));
    }

    /**
     * Send output parameter to WebSocket
     *
     * @param idProcessExec Process Execution Identifier
     * @param param Output param
     */
    protected void sendOutParam(String idProcessExec, AdProcessParam param) {
        logger.debug("\tsendOutParam /topic/process.event (" + param + ")");
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_INFO, "Output parameter: " + param.getName()));
    }

    /**
     * Send output log to WebSocket
     *
     * @param idProcessExec Process Execution Identifier
     * @param log       Output param
     */
    protected void sendOutLog(String idProcessExec, AdProcessLog log) {
        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(), LOG_TYPE_INFO, "Output log: " + log.getLog()));
    }

    /**
     * Send finish log to WebSocket
     *
     * @param idProcessExec Process Execution Identifier
     * @param success Defines if the process finished OK
     */
    protected void sendFinishLog(String idProcessExec, boolean success) {
        sendToRabbitMQ(new ProcessMessage(
                process.getIdProcess(), idProcessExec, idClient, process.getIdModule(), user.getIdUser(),
                success ? LOG_TYPE_FINISH_SUCCESS : LOG_TYPE_FINISH_ERROR, success ? LOG_TYPE_SUCCESS : LOG_TYPE_ERROR)
        );
    }

    public String getMessage(String value, Object... params) {
        return messageService.getMessage(idClient, idLanguage, value, params);
    }

    /**
     * Get parameter of process
     *
     * @param name Parameter name
     * @return Parameter
     */
    protected AdProcessParam getParam(String name) {
        for (AdProcessParam param : params) {
            if (param.getName().equals(name)) {
                return param;
            }
        }
        return null;
    }

    /**
     * Get parameter string value of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter value
     */
    protected String getParamString(String idProcessExec, String name) {
        AdProcessParam param = getParam(name);
        return param == null ? (String) paramError(idProcessExec, name) : param.getString();
    }

    /**
     * Get parameter file of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return File
     */
    protected MultipartFile getParamFile(String idProcessExec, String name) {
        AdProcessParam param = getParam(name);
        return param == null ? (MultipartFile) paramError(idProcessExec, name) : param.getFile();
    }

    /**
     * Get parameter boolean value of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter value
     */
    protected Boolean getParamBoolean(String idProcessExec, String name) {
        AdProcessParam param = getParam(name);
        return param == null ? (Boolean) paramError(idProcessExec, name) : param.getBoolean();
    }

    /**
     * Get parameter of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter
     */
    protected AdProcessParam getParamMandatory(String idProcessExec, String name) {
        AdProcessParam param = getParam(name);
        return param == null ? (AdProcessParam) paramError(idProcessExec, name) : param;
    }


    protected Object paramError(String idProcessExec, String name) {
        error(idProcessExec, getMessage("AD_GlobalErrParam", name));
        finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        return null;
    }

    /**
     * Get parameter string value of process (Optional parameter)
     *
     * @param name Parameter name
     * @param defaultValue Default parameter value
     * @return Parameter value
     */
    protected String getOptionalParamString(String name, String defaultValue) {
        AdProcessParam param = getParam(name);
        String result = param != null ? param.getString() : defaultValue;
        return "null".equals(result) ? null : result;
    }

    protected String getOptionalParamString(String name) {
        return getOptionalParamString(name, null);
    }

    /**
     * Set output parameter value
     *
     * @param name  Parameter name
     * @param value Value
     */
    protected void setOutputParam(String name, Object value) {
        AdProcessParam param = getParam(name);
        if (param != null) {
            param.setValue(value);
            ProcessEvent event = new ProcessEvent(param);
            //this.brokerMessagingTemplate.convertAndSend("/topic/process.event", event);
        }
    }

    /**
     * Send output file to configured targets
     *
     * @param idProcessExec Process Execution Identifier
     * @param outFile Output file
     * @param params  List parameters (Optional)
     */
    protected void sendOutput(String idProcessExec, File outFile, List<OutputParam> params) {
        for (AdProcessOutput output : outputs) {
            if (OUTPUT_FILESYSTEM.equals(output.getOtype())) {
                String outFilename = getFileName(output, params);
                info(idProcessExec, getMessage("AD_ProcessSaveFile", outFilename));
                if (!Converter.isEmpty(output.getFilePath())) {
                    try {
                        FileUtils.forceMkdir(new File(output.getFilePath()));
                    } catch (IOException e) {
                        error(idProcessExec, getMessage("AD_ProcessErrorOutputDir", output.getFilePath()));
                        error(idProcessExec, e.toString());
                        continue;
                    }
                }
                try {
                    FileUtils.copyFile(outFile, new File(outFilename));
                } catch (IOException e) {
                    error(idProcessExec, getMessage("AD_ProcessErrorCopyFile", outFile.getPath(), outFilename));
                    error(idProcessExec, e.toString());
                }
            } else if (OUTPUT_EMAIL.equals(output.getOtype())) {
                String email = output.getEmailTo();
                String name = output.getEmailName();
                if (!Converter.isEmpty(email)) {
                    email = email.trim();
                    if (email.startsWith("{")) {
                        if (params == null) {
                            String error = getMessage("AD_ProcessErrorNotParams");
                            error(idProcessExec, error);
                            logger.error(error);
                        }
                        email = getParamValue(params, "email");
                        name = getParamValue(params, "name");
                    }
                    info(idProcessExec, getMessage("AD_ProcessSendEmail", name, email));
                    String template = servletContext.getRealPath("/") + "WEB-INF/template/" + process.getModule().getRestPath() + "/report_email.vm";
                    String processName = translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS, FWConfig.COLUMN_ID_PROCESS_NAME, idLanguage, process.getIdProcess(), process.getName());
                    Map model = new HashMap();
                    Map<String, String> resources = new HashMap();
                    resources.put("id_logo", servletContext.getRealPath("/") + CacheManager.getPreferenceString(idClient, ReportService.PREF_HEADER_LOGO));
                    Map<String, String> attachments = new HashMap<>();
                    // Send email
                    model.put("company", CacheManager.getPreference(idClient, ReportService.PREF_HEADER_COMPANY).getString());
                    model.put("email", CacheManager.getPreference(idClient, ReportService.PREF_HEADER_EMAIL).getString());
                    model.put("phone", CacheManager.getPreference(idClient, ReportService.PREF_HEADER_PHONE).getString());
                    model.put("name", Converter.getHTMLString(name != null ? Converter.capitalizePhrase(name) : ""));
                    model.put("reportName", Converter.getHTMLString(processName));
                    model.put("reportDate", Converter.formatDate(new Date(), "dd/MM/yyyy HH:mm:ss"));
                    attachments.put(outFile.getName(), outFile.getPath());
                    mailer.sendTemplateEmail(mailer.getFromUser(), email, processName, template, model, resources, attachments);
                } else {
                    String error = getMessage("AD_ProcessErrorNotEmail");
                    error(idProcessExec, error);
                    logger.error(error);
                }
            } else if (OUTPUT_FTP.equals(output.getOtype())) {

            } else if (OUTPUT_SFTP.equals(output.getOtype())) {

            } else if (OUTPUT_HTTP.equals(output.getOtype())) {
                if (response == null)
                    return;
                String outFilename = getFileName(output, params);
                info(idProcessExec, getMessage("AD_ProcessSendFile", outFilename));
                try {
                    byte[] bytes = FileUtils.readFileToByteArray(outFile);
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + outFilename + "\"");
                    Path path = FileSystems.getDefault().getPath(outFile.getAbsolutePath());
                    String mimeType = Files.probeContentType(path);
                    response.setContentType(mimeType);
                    response.setContentLength(bytes.length);
                    ServletOutputStream outputStr = response.getOutputStream();
                    outputStr.write(bytes);
                    outputStr.flush();
                } catch (IOException e) {
                    logger.error(ERROR, e);
                }
            }
        }
    }

    /**
     * Get file name for process output
     *
     * @param output Process output information
     * @param params Parameters list
     * @return File name
     */
    protected String getFileName(AdProcessOutput output, List<OutputParam> params) {
        String prefix = Converter.isEmpty(output.getFilePrefix()) ? "" : output.getFilePrefix();
        if (!Converter.isEmpty(params) && !Converter.isEmpty(prefix)) {
            for (OutputParam param : params) {
                prefix = prefix.replaceAll("\\{" + param.getParam() + "\\}", param.getValue());
            }
        }
        return
                (Converter.isEmpty(output.getFilePath()) ? "" : output.getFilePath() + "/") + prefix + "_" +
                        (Converter.formatDate(new Date(), Converter.isEmpty(output.getFileTimeFmt()) ? "yyyyMMdd" : output.getFileTimeFmt())) +
                        (Converter.isEmpty(output.getFileExt()) ? "" : (output.getFileExt().startsWith(".") ? "" : ".") + output.getFileExt());
    }

    /**
     * Return parameter value
     *
     * @param params Parameter list
     * @param param  Parameter name
     * @return Parameter value
     */
    protected String getParamValue(List<OutputParam> params, String param) {
        for (OutputParam outputParam : params) {
            if (outputParam.getParam().equals(param)) {
                return outputParam.getValue();
            }
        }
        return null;
    }

    /**
     * Upload and validate a file
     *
     * @param idProcessExec Process Execution Identifier
     * @param file Multipart file
     * @param contentType Content type
     * @param fileExt File extention
     * @param maxSize Max file size
     * @param directory Store directory
     * @param autoUnzip Auto unzip file
     * @param deleteZip Remove zip file
     * @return File upload
     */
    protected File uploadFile(String idProcessExec, MultipartFile file, String contentType, String fileExt, Long maxSize, String directory, boolean autoUnzip, boolean deleteZip) {
        if (!FileTool.validContentType(file, contentType)) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadFileType", file.getOriginalFilename(), fileExt));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return null;
        }
        if (!FileTool.validSize(file, maxSize)) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadFileSize", file.getOriginalFilename(), ""+maxSize));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return null;
        }
        String path = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        File uploadFile = new File(path + directory + file.getOriginalFilename());
        return FileTool.saveFile(file, uploadFile.getPath(), autoUnzip, deleteZip);
    }

    protected File uploadFile(String idProcessExec, MultipartFile file, String contentType, String fileExt, Long maxSize, String directory) {
        return uploadFile(idProcessExec, file, contentType, fileExt, maxSize, directory, true, false);
    }

    /**
     * Bind to RabitMQ exchange
     *
     * @param receiver Message receiver
     */
    protected void bindToRabbitExchange(DispatchMessageProcess receiver) {
        String idProcess = process.getIdProcess();

        String queueLogName = TinyProcess.RABBIT_QUEUE_NAME + "_" + idProcess;
        String queueWebName = TinyProcess.RABBIT_QUEUE_WEB_NAME + "_" + idProcess;

        TopicExchange exchange = new TopicExchange(TinyProcess.RABBIT_EXCHANGE_NAME);
        Queue queueLog = new Queue(queueLogName, false);
        Queue queueWeb = new Queue(queueWebName, true);
        queueLogBinding = BindingBuilder.bind(queueLog).to(exchange).with(TinyProcess.RABBIT_ROUTING_KEY + idProcess);
        queueWebBinding = BindingBuilder.bind(queueWeb).to(exchange).with(TinyProcess.RABBIT_ROUTING_KEY + idProcess);

        AmqpAdmin amqpAdmin = new RabbitAdmin(connectionFactory);
        amqpAdmin.declareExchange(exchange);
        amqpAdmin.declareQueue(queueLog);
        amqpAdmin.declareBinding(queueLogBinding);
        amqpAdmin.declareQueue(queueWeb);
        amqpAdmin.declareBinding(queueWebBinding);

        MessageListenerAdapter msgListenerAdapter = new MessageListenerAdapter(receiver, "receiveMessage");
        listenerContainer = new SimpleMessageListenerContainer();
        listenerContainer.setConnectionFactory(connectionFactory);
        listenerContainer.setQueues(queueLog);
        listenerContainer.setMessageListener(msgListenerAdapter);
        listenerContainer.start();
    }

    /**
     * Unbind to RabitMQ exchange
     */
    protected void unbindToRabbitExchange() {
        listenerContainer.shutdown();
    }
}