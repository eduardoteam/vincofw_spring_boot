package com.vincomobile.fw.backend.process.importing;

public class BaseError {

    public final static String LEVEL_ERROR      = "ERROR";
    public final static String LEVEL_WARNING    = "WARNING";
    public final static String LEVEL_INFO       = "INFO";

    String level;
    String error;

    public BaseError(String level, String error) {
        this.level = level;
        this.error = error;
    }

    public String getError() {
        return "[" +  level + "] " + error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getLevel() {
        return level;
    }
}
