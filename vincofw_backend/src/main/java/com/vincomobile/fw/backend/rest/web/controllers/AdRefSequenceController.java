package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.persistence.model.AdRefSequence;
import com.vincomobile.fw.backend.persistence.services.AdRefSequenceService;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.persistence.model.EntityBean;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Datetool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Controller for table ad_sequence
 *
 * Date: 17/12/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_ref_sequence/{idClient}")
public class AdRefSequenceController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdRefSequenceController.class);

    @Autowired
    AdRefSequenceService service;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public Page<AdRefSequence> list(
        @PathVariable("idClient") String idClient,
        @RequestParam(value = "page", required = false, defaultValue = "1") int page,
        @RequestParam(value = "limit", required = false, defaultValue = "1000") int limit,
        @RequestParam(value = "sort", required = false) String sort,
        @RequestParam(value = "q", required = false) String constraints
    ) {
        logger.debug("GET list(" + constraints + ")");
        PageSearch pageReq = new PageSearch(page, limit, getSort(sort));
        pageReq.parseConstraints(constraints + getClientConstraint(idClient), AdRefSequence.class);
        return service.findAll(pageReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public AdRefSequence get(@PathVariable("id") String id) {
        logger.debug("GET get("+id+")");
        return RestPreconditions.checkNotNull(service.findById(id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public String create(@RequestBody @Valid AdRefSequence entity) {
        logger.debug("POST create(" + entity + ")");
        String result = (String) createEntity(entity);
        service.getNextValue(result);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void update(@RequestBody @Valid AdRefSequence entity) {
        logger.debug("PUT update(" + entity + ")");
        entity.setClient(null);
        updateEntity(entity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void delete(@PathVariable("id") String id) {
        logger.debug("DELETE delete(" + id + ")");
        deleteEntity(id);
    }

    @RequestMapping(value = "/delete_batch", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public void deleteBatch(
            @PathVariable("idClient") String idClient,
            @RequestParam String[] ids
    ) {
        logger.debug("DELETE deleteBatch()");
        deleteItems(idClient, ids);
    }

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_AD_WRITE)
    public ControllerResult sort(
            @PathVariable("idClient") String idClient,
            @RequestBody SortParam entity
    ) {
        logger.debug("POST sort(" + entity + ")");
        return sortItems(idClient, entity);
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    @Override
    protected List<ValidationError> validate(EntityBean entity) {
        AdRefSequence bean = (AdRefSequence) entity;
        List<ValidationError> result = new ArrayList<>();
        if (AdRefSequenceService.TYPE_STANDARD.equals(bean.getStype())) {
            bean.setSyear(null);
            bean.setSmonth(null);
            bean.setSday(null);
        } else {
            if (bean.getSyear() == null) {
                bean.setSyear((long) Datetool.getCurrentYear());
            }
            if (AdRefSequenceService.TYPE_YEAR_MONTH.equals(bean.getStype()) || AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(bean.getStype())) {
                if (bean.getSmonth() == null) {
                    bean.setSmonth((long) Datetool.getCurrentMonth());
                }
                if (bean.getYearOrder() == null) {
                    bean.setYearOrder(AdRefSequenceService.YEAR_ORDER_YEAR_MONTH_DAY);
                }
            } else {
                bean.setSmonth(null);
                bean.setSday(null);
            }
            if (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(bean.getStype())) {
                if (bean.getSday() == null) {
                    bean.setSday((long) Datetool.getCurrentDay());
                }
            } else {
                bean.setSday(null);
            }
        }
        return result;
    }

}