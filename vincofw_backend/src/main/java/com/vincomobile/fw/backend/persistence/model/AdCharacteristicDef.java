package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_characteristic_def
 *
 * Date: 23/01/2016
 */
@Entity
@Table(name = "ad_characteristic_def")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdCharacteristicDef extends AdEntityBean {

    private String idCharacteristicDef;
    private String idCharacteristic;
    private String dtype;
    private String defValue;
    private Boolean mandatory;
    private Long seqno;

    private AdCharacteristic characteristic;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idCharacteristicDef;
    }

    @Override
    public void setId(String id) {
            this.idCharacteristicDef = id;
    }

    @Id
    @Column(name = "id_characteristic_def")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdCharacteristicDef() {
        return idCharacteristicDef;
    }

    public void setIdCharacteristicDef(String idCharacteristicDef) {
        this.idCharacteristicDef = idCharacteristicDef;
    }

    @Column(name = "id_characteristic")
    @NotNull
    public String getIdCharacteristic() {
        return idCharacteristic;
    }

    public void setIdCharacteristic(String idCharacteristic) {
        this.idCharacteristic = idCharacteristic;
    }

    @Column(name = "dtype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    @Column(name = "def_value", length = 250)
    @Size(min = 1, max = 250)
    public String getDefValue() {
        return defValue;
    }

    public void setDefValue(String defValue) {
        this.defValue = defValue;
    }

    @Column(name = "mandatory")
    @NotNull
    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_characteristic", referencedColumnName = "id_characteristic", insertable = false, updatable = false)
    public AdCharacteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(AdCharacteristic characteristic) {
        this.characteristic = characteristic;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdCharacteristicDef)) return false;

        final AdCharacteristicDef that = (AdCharacteristicDef) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idCharacteristicDef == null) && (that.idCharacteristicDef == null)) || (idCharacteristicDef != null && idCharacteristicDef.equals(that.idCharacteristicDef)));
        result = result && (((idCharacteristic == null) && (that.idCharacteristic == null)) || (idCharacteristic != null && idCharacteristic.equals(that.idCharacteristic)));
        result = result && (((dtype == null) && (that.dtype == null)) || (dtype != null && dtype.equals(that.dtype)));
        result = result && (((defValue == null) && (that.defValue == null)) || (defValue != null && defValue.equals(that.defValue)));
        result = result && (((mandatory == null) && (that.mandatory == null)) || (mandatory != null && mandatory.equals(that.mandatory)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

}

