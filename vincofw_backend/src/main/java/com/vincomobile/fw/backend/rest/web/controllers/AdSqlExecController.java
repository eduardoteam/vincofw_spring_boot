package com.vincomobile.fw.backend.rest.web.controllers;

import com.vincomobile.fw.backend.business.SqlExecData;
import com.vincomobile.fw.backend.persistence.services.AdProcessExecService;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.controllers.BaseController;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Controller para la tabla ad_process_exec
 * <p>
 * Date: 28/11/2015
 */
@Controller
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
@RequestMapping(value = "vinco_core/ad_sql_exec/{idClient}")
public class AdSqlExecController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(AdSqlExecController.class);

    @Autowired
    AdProcessExecService service;

    @Override
    public BaseService getService() {
        return service;
    }

    @RequestMapping(value = "/exec", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    @Transactional
    @Secured(FWSecurityConstants.Privileges.CAN_SUPERADMIN)
    public ControllerResult exec(
            @PathVariable("idClient") String idClient,
            @RequestBody @Valid SqlExecData entity,
            BindingResult bindingResults
    ) {
        ControllerResult result = new ControllerResult();
        logger.debug("GET exec(" + entity.getSqlType() + ", " + entity.getSqlQuery() + ")");
        List<Map<String, Object>> hints;
        if ("sql".equals(entity.getSqlType().toLowerCase())) {
            try {
                hints = service.executeNativeSelectSql(entity.getSqlQuery(), 100);
                result.put("resultData", hints);
            } catch (InvalidDataAccessResourceUsageException ex) {
                result.setErrCode((long) -1);
                result.setMessage("AD_errSqlQueryException");
                logger.error(ex.getMessage());
                throwError(result, bindingResults);
            } catch (SQLException e) {
                result.setErrCode((long) -1);
                result.setMessage("AD_errSqlQueryException");
                logger.error(e.getMessage());
                throwError(result, bindingResults);
            }
        } else if ("hsql".equals(entity.getSqlType().toLowerCase())) {
            try {
                List queryResult = service.query(entity.getSqlQuery(), new HashMap(), 100);
                result.put("resultData", queryResult);
            } catch (InvalidDataAccessApiUsageException ex) {
                result.setErrCode((long) -1);
                result.setMessage("AD_errSqlQueryException");
                logger.error(ex.getMessage());
                throwError(result, bindingResults);
            }
        }
        result.setSuccess(true);
        return result;
    }

}