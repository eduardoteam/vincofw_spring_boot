package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.backend.persistence.services.AdRefSequenceService;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_sequence
 *
 * Date: 17/12/2015
 */
@Entity
@Table(name = "ad_ref_sequence")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRefSequence extends AdEntityBean {

    private String idRefSequence;
    private String idReference;
    private String name;
    private String description;
    private Long incrementno;
    private Long startno;
    private Long currentnext;
    private String prefix;
    private String suffix;
    private String stype;
    private String sorder;
    private String fieldSeparator;
    private Long leftFill;
    private Long syear;
    private Long smonth;
    private Long sday;
    private Boolean yearTwodigit;
    private String yearSeparator;
    private String yearOrder;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRefSequence;
    }

    @Override
    public void setId(String id) {
            this.idRefSequence = id;
    }

    @Id
    @Column(name = "id_ref_sequence")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRefSequence() {
        return idRefSequence;
    }

    public void setIdRefSequence(String idRefSequence) {
        this.idRefSequence = idRefSequence;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "incrementno")
    @NotNull
    public Long getIncrementno() {
        return incrementno;
    }

    public void setIncrementno(Long incrementno) {
        this.incrementno = incrementno;
    }

    @Column(name = "startno")
    @NotNull
    public Long getStartno() {
        return startno;
    }

    public void setStartno(Long startno) {
        this.startno = startno;
    }

    @Column(name = "currentnext")
    @NotNull
    public Long getCurrentnext() {
        return currentnext;
    }

    public void setCurrentnext(Long currentnext) {
        this.currentnext = currentnext;
    }

    @Column(name = "prefix", length = 10)
    @Size(min = 1, max = 10)
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Column(name = "suffix", length = 10)
    @Size(min = 1, max = 10)
    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Column(name = "stype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    @Column(name = "sorder", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getSorder() {
        return sorder;
    }

    public void setSorder(String sorder) {
        this.sorder = sorder;
    }

    @Column(name = "field_separator", length = 5)
    @Size(min = 1, max = 5)
    public String getFieldSeparator() {
        return fieldSeparator;
    }

    public void setFieldSeparator(String fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
    }

    @Column(name = "left_fill", length = 5)
    public Long getLeftFill() {
        return leftFill;
    }

    public void setLeftFill(Long leftFill) {
        this.leftFill = leftFill;
    }

    @Column(name = "syear")
    public Long getSyear() {
        return syear;
    }

    public void setSyear(Long syear) {
        this.syear = syear;
    }

    @Column(name = "smonth")
    public Long getSmonth() {
        return smonth;
    }

    public void setSmonth(Long smonth) {
        this.smonth = smonth;
    }

    @Column(name = "sday")
    public Long getSday() {
        return sday;
    }

    public void setSday(Long sday) {
        this.sday = sday;
    }

    @Column(name = "year_twodigit")
    public Boolean getYearTwodigit() {
        return yearTwodigit;
    }

    public void setYearTwodigit(Boolean yearTwodigit) {
        this.yearTwodigit = yearTwodigit;
    }

    @Column(name = "year_separator", length = 5)
    @Size(min = 1, max = 5)
    public String getYearSeparator() {
        return yearSeparator;
    }

    public void setYearSeparator(String yearSeparator) {
        this.yearSeparator = yearSeparator;
    }

    @Column(name = "year_order", length = 50)
    @Size(min = 1, max = 50)
    public String getYearOrder() {
        return yearOrder;
    }

    public void setYearOrder(String yearOrder) {
        this.yearOrder = yearOrder;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRefSequence)) return false;

        final AdRefSequence that = (AdRefSequence) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRefSequence == null) && (that.idRefSequence == null)) || (idRefSequence != null && idRefSequence.equals(that.idRefSequence)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((incrementno == null) && (that.incrementno == null)) || (incrementno != null && incrementno.equals(that.incrementno)));
        result = result && (((startno == null) && (that.startno == null)) || (startno != null && startno.equals(that.startno)));
        result = result && (((currentnext == null) && (that.currentnext == null)) || (currentnext != null && currentnext.equals(that.currentnext)));
        result = result && (((prefix == null) && (that.prefix == null)) || (prefix != null && prefix.equals(that.prefix)));
        result = result && (((suffix == null) && (that.suffix == null)) || (suffix != null && suffix.equals(that.suffix)));
        result = result && (((stype == null) && (that.stype == null)) || (stype != null && stype.equals(that.stype)));
        result = result && (((sorder == null) && (that.sorder == null)) || (sorder != null && sorder.equals(that.sorder)));
        result = result && (((fieldSeparator == null) && (that.fieldSeparator == null)) || (fieldSeparator != null && fieldSeparator.equals(that.fieldSeparator)));
        result = result && (((leftFill == null) && (that.leftFill == null)) || (leftFill != null && leftFill.equals(that.leftFill)));
        result = result && (((syear == null) && (that.syear == null)) || (syear != null && syear.equals(that.syear)));
        result = result && (((smonth == null) && (that.smonth == null)) || (smonth != null && syear.equals(that.smonth)));
        result = result && (((sday == null) && (that.sday == null)) || (sday != null && sday.equals(that.sday)));
        result = result && (((yearTwodigit == null) && (that.yearTwodigit == null)) || (yearTwodigit != null && yearTwodigit.equals(that.yearTwodigit)));
        result = result && (((yearSeparator == null) && (that.yearSeparator == null)) || (yearSeparator != null && yearSeparator.equals(that.yearSeparator)));
        result = result && (((yearOrder == null) && (that.yearOrder == null)) || (yearOrder != null && yearOrder.equals(that.yearOrder)));
        return result;
    }

    @Transient
    public String getCurrentValue() {
        String currentValue, date = "";
        String realPrefix = prefix != null ? prefix : "";
        String separator = fieldSeparator != null ? fieldSeparator : "";
        String yearSep = yearSeparator != null ? yearSeparator : "";
        if (!AdRefSequenceService.TYPE_STANDARD.equals(stype)) {
            String year = syear != null ? syear.toString() : "2000";
            if (yearTwodigit != null && yearTwodigit) {
                year = year.substring(2);
            }
            if (AdRefSequenceService.TYPE_YEAR_MONTH.equals(stype) || AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype)) {
                String month = com.vincomobile.fw.basic.tools.Converter.leftFill(smonth, '0', 2), day = "";
                if (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype)) {
                    day = com.vincomobile.fw.basic.tools.Converter.leftFill(sday, '0', 2);
                }
                if (AdRefSequenceService.YEAR_ORDER_YEAR_MONTH_DAY.equals(yearOrder)) {
                    date = year + yearSep + month + (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype) ? yearSep + day : "");
                } else if (AdRefSequenceService.YEAR_ORDER_YEAR_DAY_MONTH.equals(yearOrder)) {
                    date = year + yearSep + (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype) ? day + yearSep : "") + month;
                } else if (AdRefSequenceService.YEAR_ORDER_MONTH_DAY_YEAR.equals(yearOrder)) {
                    date = month + yearSep + (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype) ? day + yearSep : "") + year;
                } else {
                    date = (AdRefSequenceService.TYPE_YEAR_MONTH_DAY.equals(stype) ? day + yearSep : "") + month + yearSep + year;
                }
            }
            date += separator;
        }
        if (AdRefSequenceService.ORDER_YEAR_PREFIX_SEQ_SUFIX.equals(sorder)) {
            currentValue = date + realPrefix + separator;
        } else {
            currentValue = realPrefix;
            if (prefix != null) {
                currentValue += separator;
            }
            currentValue += date;
        }
        currentValue += leftFill > 0 ? com.vincomobile.fw.basic.tools.Converter.leftFill(currentnext, '0', leftFill.intValue()) : currentnext;
        if (suffix != null) {
            currentValue += separator + suffix;
        }
        return currentValue;
    }

}

