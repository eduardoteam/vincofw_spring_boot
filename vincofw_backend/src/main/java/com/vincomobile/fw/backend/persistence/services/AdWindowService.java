package com.vincomobile.fw.backend.persistence.services;


import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.dto.AdWindowDto;
import com.vincomobile.fw.basic.persistence.model.AdWindow;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Devtools.
 * Interface of service of AD_WINDOW
 *
 * Date: 19/02/2015
 */
public interface AdWindowService extends BaseService<AdWindow, String> {

    /**
     * Load meta data for a window (load first tab)
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idWindow Window identifier
     * @param idLanguage Language identifier
     * @return Window meta data
     */
    AdWindowDto load(String idClient, String idUser, String idWindow, String idLanguage);

    /**
     * Get Window GUI
     *
     * @param idClient Client identifier
     * @param idWindow Window identifier
     * @return GUI
     */
    AdGUIDto getWindowGUI(String idClient, String idWindow);
}


