package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.basic.persistence.services.AdMessageService;
import org.springframework.stereotype.Component;

@Component
public class ReportUtils {

    public static AdMessageService messageService;

    /**
     * Get message from DB
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param value Message search key
     * @param params Message parameters
     * @return Message
     */
    public static String getMessage(String idClient, String idLanguage, String value, Object... params) {
        return messageService.getMessage(idClient, idLanguage, value, params);
    }

    /**
     * Get message from DB (first characters)
     *
     * @param length Max char to return
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param value Message search key
     * @param params Message parameters
     * @return Message
     */
    public static String getMessage(int length, String idClient, String idLanguage, String value, Object... params) {
        String msg = messageService.getMessage(idClient, idLanguage, value, params);
        return msg.length() < length ? msg : msg.substring(0, length);
    }

}
