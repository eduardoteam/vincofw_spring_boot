package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_file
 *
 * Date: 23/01/2016
 */
@Entity
@Table(name = "ad_file")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdFile extends AdEntityBean {

    private String idFile;
    private String idTable;
    private String idRow;
    private String name;
    private String caption;
    private String description;
    private String category;
    private Long filesize;
    private String mimetype;
    private Long seqno;

    private String fileUrl;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idFile;
    }

    @Override
    public void setId(String id) {
            this.idFile = id;
    }

    @Id
    @Column(name = "id_file")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdFile() {
        return idFile;
    }

    public void setIdFile(String idFile) {
        this.idFile = idFile;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_row")
    @NotNull
    public String getIdRow() {
        return idRow;
    }

    public void setIdRow(String idRow) {
        this.idRow = idRow;
    }

    @Column(name = "name", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "caption", length = 500)
    @Size(min = 1, max = 500)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "description", length = 4000)
    @Size(min = 1, max = 4000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "category", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "filesize")
    @NotNull
    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    @Column(name = "mimetype", length = 255)
    @Size(min = 1, max = 255)
    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdFile)) return false;

        final AdFile that = (AdFile) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idFile == null) && (that.idFile == null)) || (idFile != null && idFile.equals(that.idFile)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idRow == null) && (that.idRow == null)) || (idRow != null && idRow.equals(that.idRow)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((caption == null) && (that.caption == null)) || (caption != null && caption.equals(that.caption)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((category == null) && (that.category == null)) || (category != null && category.equals(that.category)));
        result = result && (((filesize == null) && (that.filesize == null)) || (filesize != null && filesize.equals(that.filesize)));
        result = result && (((mimetype == null) && (that.mimetype == null)) || (mimetype != null && mimetype.equals(that.mimetype)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

    @Transient
    public String getFile() {
        return name;
    }

    @Transient
    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

}

