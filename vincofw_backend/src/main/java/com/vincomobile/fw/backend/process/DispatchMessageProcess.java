package com.vincomobile.fw.backend.process;

public interface DispatchMessageProcess {

    void receiveMessage(Object message);
}
