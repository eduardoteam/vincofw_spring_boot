package com.vincomobile.fw.backend.business;

import java.util.List;

public class AdGuide {

    private String idWikiGuide;
    private String name;
    private String title;
    private String content;
    private Long seqno;

    List<AdGuideTopic> topics;

    public AdGuide(String idWikiGuide, String name, String title, String content, Long seqno) {
        this.idWikiGuide = idWikiGuide;
        this.name = name;
        this.title = title;
        this.content = content;
        this.seqno = seqno;
    }

    public String getIdWikiGuide() {
        return idWikiGuide;
    }

    public void setIdWikiGuide(String idWikiGuide) {
        this.idWikiGuide = idWikiGuide;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public List<AdGuideTopic> getTopics() {
        return topics;
    }

    public void setTopics(List<AdGuideTopic> topics) {
        this.topics = topics;
    }
}
