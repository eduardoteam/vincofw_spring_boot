package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdChartFilter;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 28/09/2016.
 * Copyright © 2016 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_chart_filter
 */
@Repository
@Transactional(readOnly = true)
public class AdChartFilterServiceImpl extends BaseServiceImpl<AdChartFilter, String> implements AdChartFilterService {

    /**
     * Constructor.
     */
    public AdChartFilterServiceImpl() {
        super(AdChartFilter.class);
    }

    /**
     * Get Tab chart filters
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @return Chart Filters
     */
    @Override
    public List<AdChartFilter> getChartFilters(String idClient, String idTab) {
        List<String> clients = new ArrayList<>();
        clients.add("0");
        clients.add(idClient);
        Map filter = new HashMap();
        filter.put("idClient", clients);
        filter.put("idTab", idTab);
        return findAll(filter);
    }

}
