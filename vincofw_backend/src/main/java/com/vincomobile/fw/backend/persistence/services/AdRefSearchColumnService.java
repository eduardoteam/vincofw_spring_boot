package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdRefSearchColumn;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Devtools.
 * Interface del servicio de ad_ref_search_column
 *
 * Date: 07/11/2015
 */
public interface AdRefSearchColumnService extends BaseService<AdRefSearchColumn, String> {

}


