package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdDbscript;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Vincomobile FW on 03/10/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_dbscript
 */
public interface AdDbscriptService extends BaseService<AdDbscript, String> {

    /**
     * List all active DB Scripts
     * @return DB Scripts
     */
    List<AdDbscript> listActives();
}


