package com.vincomobile.fw.backend.process;

import com.vincomobile.fw.backend.persistence.model.AdMenu;
import com.vincomobile.fw.backend.persistence.model.AdMenuRoles;
import com.vincomobile.fw.backend.persistence.services.*;
import com.vincomobile.fw.basic.annotations.FWProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcessOutput;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@FWProcess
public class CopyProcess extends ProcessDefinitionImpl {

    /**
     * Factory method to instance object (Mandatory)
     */
    public static ProcessDefinition getService() {
        return service;
    }

    private static CopyProcess service = new CopyProcess();

    @Autowired
    AdMenuRolesService menuRolesService;

    @Autowired
    AdMenuService menuService;

    @Autowired
    AdProcessService processService;

    @Autowired
    AdProcessParamService processParamService;

    @Autowired
    AdProcessOutputService processOutputService;

    /**
     * Get if process can execute concurrently or not
     *
     * @return If concurrent
     */
    @Override
    public boolean isConcurrent() {
        return false;
    }

    /**
     * Copy menu roles
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void copyMenuRole(String idProcessExec) {
        String idModuleFrom = getParamString(idProcessExec, "idModuleFrom");
        String idModuleTo = getParamString(idProcessExec, "idModuleTo");
        String idRole = getParamString(idProcessExec, "idRole");
        if (idModuleFrom == null || idModuleTo == null || idRole == null) {
            return;
        }

        if (idModuleFrom.equals(idModuleTo)) {
            error(idProcessExec, getMessage("AD_GlobalErrModuleEquals"));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            return;
        }

        List<AdMenuRoles> menuRoles = menuRolesService.listByModule(idModuleFrom);
        for (AdMenuRoles mr : menuRoles) {
            AdMenuRoles item = menuRolesService.getItem("0", mr.getIdMenu(), idRole);
            AdMenu menu = menuService.findById(mr.getIdMenu());
            if (item == null) {
                info(idProcessExec, getMessage("AD_ProcessCopyMenuAdd", menu.getName()));
                item = new AdMenuRoles();
                item.setIdClient("0");
                item.setIdModule(idModuleTo);
                item.setIdMenu(mr.getIdMenu());
                item.setIdRole(idRole);
                item.setActive(true);
                menuRolesService.save(item);
            } else {
                warn(idProcessExec, getMessage("AD_ProcessCopyMenuExist", menu.getName()));
            }
        }

        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }

    /**
     * Copy process
     *
     * @param idProcessExec Process Execution Identifier
     */
    public void copyProcess(String idProcessExec) {
        String idProcess = getParamString(idProcessExec, "idProcess");
        if (idProcess == null) {
            return;
        }

        AdProcess process = processService.findById(idProcess);
        if (process != null) {
            // Copy process
            AdProcess item = new AdProcess();
            item.init(user.getIdUser());
            item.setIdClient(idClient);
            item.setIdModule(process.getIdModule());
            item.setName("Copy of: " + process.getName());
            item.setDescription(process.getDescription());
            item.setPtype(process.getPtype());
            item.setClassname(process.getClassname());
            item.setClassmethod("copyOf"+process.getClassmethod());
            item.setBackground(process.getBackground());
            item.setUipattern(process.getUipattern());
            item.setJrxml(process.getJrxml());
            item.setJrxmlExcel(process.getJrxmlExcel());
            item.setJrxmlWord(process.getJrxmlWord());
            item.setPdf(process.getPdf());
            item.setExcel(process.getExcel());
            item.setHtml(process.getHtml());
            item.setWord(process.getWord());
            item.setShowinparams(process.getShowinparams());
            item.setMultiple(process.getMultiple());
            item.setEvalSql(process.getEvalSql());
            item.setSeqno(process.getSeqno());
            item.setPrivilege(process.getPrivilege());
            item.setPrivilegeDesc(process.getPrivilegeDesc());
            item.setShowConfirm(process.getShowConfirm());
            item.setConfirmMsg(process.getConfirmMsg());
            processService.save(item);

            // Copy process input params
            List<AdProcessParam> params = processParamService.getInputParams(process.getIdProcess());
            for (AdProcessParam param : params) {
                AdProcessParam p = new AdProcessParam();
                p.init(user.getIdUser());
                p.setIdClient(idClient);
                p.setIdModule(process.getIdModule());
                p.setIdProcess(item.getIdProcess());
                p.setName(param.getName());
                p.setDescription(param.getDescription());
                p.setIdReference(param.getIdReference());
                p.setIdReferenceValue(param.getIdReferenceValue());
                p.setValuemin(param.getValuemin());
                p.setValuemax(param.getValuemax());
                p.setValuedefault(param.getValuedefault());
                p.setMandatory(param.getMandatory());
                p.setRanged(param.getRanged());
                p.setDisplayed(param.getDisplayed());
                p.setSaveType(param.getSaveType());
                p.setPtype(param.getPtype());
                p.setCaption(param.getCaption());
                p.setDisplaylogic(param.getDisplaylogic());
                p.setSeqno(param.getSeqno());
                processParamService.save(p);
            }

            // Copy process output
            List<AdProcessOutput> outputs = processOutputService.getOutputs(process.getIdProcess());
            for (AdProcessOutput output : outputs) {
                AdProcessOutput op = new AdProcessOutput();
                op.init(user.getIdUser());
                op.setIdClient(idClient);
                op.setIdModule(process.getIdModule());
                op.setIdProcess(item.getIdProcess());
                op.setOtype(output.getOtype());
                op.setServer(output.getServer());
                op.setUsername(output.getUsername());
                op.setPassword(output.getPassword());
                op.setPort(output.getPort());
                op.setEmailTo(output.getEmailTo());
                op.setEmailName(output.getEmailName());
                op.setFilePath(output.getFilePath());
                op.setFilePrefix(output.getFilePrefix());
                op.setFileExt(output.getFileExt());
                op.setFileTimeFmt(output.getFileTimeFmt());
                processOutputService.save(op);
            }
        } else {
            error(idProcessExec, getMessage("AD_GlobalErrParam", idProcess));
            finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
        }

        finishExec(idProcessExec, ProcessDefinition.STATUS_SUCCESS);
    }
}