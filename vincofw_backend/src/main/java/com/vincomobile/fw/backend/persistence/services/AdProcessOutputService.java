package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdProcessOutput;
import com.vincomobile.fw.basic.persistence.services.BaseService;

import java.util.List;

/**
 * Created by Devtools.
 * Service layer interface for ad_process_output
 *
 * Date: 29/11/2015
 */
public interface AdProcessOutputService extends BaseService<AdProcessOutput, String> {

    /**
     * Load process output for a process
     *
     * @param idProcess Process identifier
     * @return Outputs
     */
    List<AdProcessOutput> getOutputs(String idProcess);

}


