package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdFieldGroup;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_FIELD_GROUP
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdFieldGroupServiceImpl extends BaseServiceImpl<AdFieldGroup, String> implements AdFieldGroupService {

    /**
     * Constructor.
     *
     */
    public AdFieldGroupServiceImpl() {
        super(AdFieldGroup.class);
    }

}
