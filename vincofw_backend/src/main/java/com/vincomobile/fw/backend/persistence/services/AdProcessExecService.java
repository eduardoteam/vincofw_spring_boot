package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdProcessExec;
import com.vincomobile.fw.basic.persistence.services.BaseService;

/**
 * Created by Devtools.
 * Interface del servicio de ad_process_exec
 *
 * Date: 28/11/2015
 */
public interface AdProcessExecService extends BaseService<AdProcessExec, String> {

    /**
     * Clear process execution table
     *
     * @param days Number of days to preserve
     * @return Number of remove rows
     */
    int clearProcessExec(int days);

}


