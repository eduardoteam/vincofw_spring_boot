package com.vincomobile.fw.backend.persistence.services;

import com.vincomobile.fw.backend.persistence.model.AdSampledataTable;
import com.vincomobile.fw.basic.persistence.services.BaseServiceImpl;
import com.vincomobile.fw.basic.persistence.services.SqlSort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 22/03/2019.
 * Copyright © 2019 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_sampledata_table
 */
@Repository
@Transactional(readOnly = true)
public class AdSampledataTableServiceImpl extends BaseServiceImpl<AdSampledataTable, String> implements AdSampledataTableService {

    /**
     * Constructor.
     */
    public AdSampledataTableServiceImpl() {
        super(AdSampledataTable.class);
    }

    /**
     * List active tables for sample data
     *
     * @param idSampledata Sample data identifier
     * @return Active tables list
     */
    @Override
    public List<AdSampledataTable> listTables(String idSampledata) {
        Map filter = new HashMap();
        filter.put("idSampledata", idSampledata);
        filter.put("active", true);
        return findAll(new SqlSort("table.seqnoUpdate", "asc"), filter);
    }

}
