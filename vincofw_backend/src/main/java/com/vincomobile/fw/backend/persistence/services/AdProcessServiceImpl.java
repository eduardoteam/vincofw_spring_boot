package com.vincomobile.fw.backend.persistence.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.backend.business.AdProcessExecuted;
import com.vincomobile.fw.backend.business.ProcessMethod;
import com.vincomobile.fw.backend.process.BackgroundProcess;
import com.vincomobile.fw.backend.process.ProcessDefinition;
import com.vincomobile.fw.backend.process.QuartzSchedulerWrapper;
import com.vincomobile.fw.backend.process.ReportProcess;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.dto.AdProcessDto;
import com.vincomobile.fw.basic.persistence.dto.AdProcessParamDto;
import com.vincomobile.fw.basic.persistence.exception.FWProcessPlanningException;
import com.vincomobile.fw.basic.persistence.model.AdProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.process.ProcessExecError;
import com.vincomobile.fw.basic.process.ProcessMessage;
import com.vincomobile.fw.basic.process.TinyProcess;
import com.vincomobile.fw.basic.tools.Constants;
import com.vincomobile.fw.basic.tools.ProcessPlanningErrorCode;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.aop.TargetSource;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.*;

/**
 * Created by Devtools.
 * Servicio de ad_process
 * <p/>
 * Date: 06/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdProcessServiceImpl extends BaseServiceImpl<AdProcess, String> implements AdProcessService {

    private Logger logger = LoggerFactory.getLogger(AdProcessServiceImpl.class);

    private final WebApplicationContext applicationContext;
    private final ServletContext servletContext;
    private final ApplicationContext context;
    private final ReportProcess reportProcess;
    protected final AdProcessLogService logService;
    protected final AdTranslationService translationService;
    private final AdProcessParamService processParamService;
    private final QuartzSchedulerWrapper scheduler;
    private final RabbitTemplate rabbitTemplate;

    private Map<String, AdProcessExecuted> executedProcess = new HashMap<>();
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Constructor.
     */
    public AdProcessServiceImpl(
            WebApplicationContext applicationContext, ServletContext servletContext, ApplicationContext context, ReportProcess reportProcess,
            AdProcessLogService logService, AdTranslationService translationService, AdProcessParamService processParamService,
            QuartzSchedulerWrapper scheduler, RabbitTemplate rabbitTemplate
    ) {
        super(AdProcess.class);
        this.applicationContext = applicationContext;
        this.servletContext = servletContext;
        this.context = context;
        this.reportProcess = reportProcess;
        this.logService = logService;
        this.translationService = translationService;
        this.processParamService = processParamService;
        this.scheduler = scheduler;
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * Execute process
     *
     * @param idProcessExec Process Execution Identifier
     * @param process Process information
     * @param processClass Process class
     * @param method Method to execute
     */
    private void executeProcess(String idProcessExec, AdProcess process, ProcessDefinition processClass, ProcessMethod method) {
        if (!executedProcess.containsKey(process.getIdProcess())) {
            executedProcess.put(process.getIdProcess(), new AdProcessExecuted(process, processClass));
        } else {
            AdProcessExecuted executed = executedProcess.get(process.getIdProcess());
            executed.setStarted(new Date());
            executed.setFinished(null);
            executed.setStatus("EXECUTING");
        }
        try {
            method.getMethod().setAccessible(true);
            if (method.isProxy()) {
                method.getMethod().invoke(method.getTarget(), idProcessExec);
            } else {
                method.getMethod().invoke(processClass, idProcessExec);
            }
            if (processClass.isInExecution()) {
                processClass.error(idProcessExec, "Process not call 'finishExec' method");
                processClass.finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            }
            AdProcessExecuted executed = executedProcess.get(process.getIdProcess());
            executed.setStatus(processClass.isSuccess() ? "FINISH_OK" : "FINISH_ERROR");
            executed.setFinished(processClass.getFinished(process.getIdProcess()));
        } catch (Exception e) {
            Throwable cause = e.getCause();
            logger.error("Error calling: " + process.getClassname() + "." + process.getClassmethod());
            logger.error("Error: " + cause.getMessage());
            processClass.error(idProcessExec, "Error calling: " + process.getClassname() + "." + process.getClassmethod());
            processClass.error(idProcessExec, cause.getMessage());
            processClass.error(idProcessExec, e.getMessage());
            processClass.finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            e.printStackTrace();
        }
    }

    private ProcessMethod getMethod(String methodName, Object classInstance) {
        Class _class = classInstance.getClass();
        try {
            if (Proxy.isProxyClass(_class)) {
                Method m = _class.getMethod("getTargetSource");
                TargetSource targetSource = (TargetSource) m.invoke(classInstance);
                m = targetSource.getTarget().getClass().getMethod(methodName, String.class);
                return new ProcessMethod(m, targetSource.getTarget());
            } else {
                return new ProcessMethod(_class.getMethod(methodName, String.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Execute a process
     *
     * @param process    Process to execute
     * @param user       User
     * @param idClient   Client id.
     * @param idLanguage Language id.
     * @param params     Parameter list
     * @param async      Asynchrone execution
     * @param request    Http request (Optional)
     * @param response   Http response (Optional)
     * @return Process execution result
     */
    @Override
    @Transactional
    public ProcessExecError exec(final AdProcess process, AdUser user, String idClient, String idLanguage, List<AdProcessParam> params, boolean async, HttpServletRequest request, HttpServletResponse response) {
        logger.debug("exec(" + process.getName() + ", " + user.getName() + ", " + idClient + ")");
        // Load process class
        ProcessExecError result = new ProcessExecError();
        if ("PROCESS".equals(process.getPtype())) {
            final ProcessDefinition processClass;
            try {
                // Instantiate a process class
                processClass = (ProcessDefinition) applicationContext.getBean(process.getClassname());
            } catch (Exception e) {
                String error = "Error loading class: " + e.getMessage();
                result.setError(error);
                logger.error(error);
                sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_ERROR, error));
                return result;
            }
            try {
                // Instantiate a process class
                processClass.setProcess(process);
                processClass.setUser(user);
                processClass.setIdClient(idClient);
                processClass.setIdLanguage(idLanguage);
                processClass.setServletContext(servletContext);
                processClass.setParams(params);
                processClass.setRequest(request);
                processClass.setResponse(response);

                ProcessMethod method = getMethod(process.getClassmethod(), processClass);
                if (method != null) {
                    if (processClass.isConcurrent() || !processClass.isInExecution()) {
                        if (async && !processClass.hasHttpOutputs()) {
                            try {
                                String paramsStr = "";
                                for (int i = 0; i < params.size(); i++) {
                                    paramsStr += (params.get(i).getName() + "=" + params.get(i).getValue());
                                    if (i + 1 < params.size())
                                        paramsStr += (",");
                                }
                                schedule(process, user, idClient, idLanguage, paramsStr);
                            }
                            catch(FWProcessPlanningException ex){
                                String error = "Error planning process: " + ex.getMessage();
                                result.setError(error);
                                logger.error(error);
                                sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_ERROR, error));
                            }
                        } else {
                            String idProcessExec = processClass.initExec();
                            if (idProcessExec != null) {
                                executeProcess(idProcessExec, process, processClass, method);
                                if (!processClass.isSuccess()) {
                                    result.setError(processClass.getLastError());
                                }
                            } else {
                                String error = "The process not allow execution in this enviroment: " + process.getName();
                                result.setError(error);
                                logger.error(error);
                                sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_ERROR, error));
                            }
                        }
                    } else {
                        String error = "Process: " + process.getClassname() + "." + process.getClassmethod() + " already executing";
                        logger.warn(error);
                        sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_WARN, error));
                    }
                } else {
                    String error = "Not found method: " + process.getClassmethod() + " in class " + process.getClassname();
                    result.setError(error);
                    logger.error(error);
                    sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_ERROR, error));
                }
            } catch (Exception e) {
                String error = "Error process execution: " + e.getMessage();
                result.setError(error);
                logger.error(error);
                sendToRabbitMQ(new ProcessMessage(process.getIdProcess(), null, idClient, process.getIdModule(), user.getIdUser(), ProcessDefinition.LOG_TYPE_ERROR, error));
                e.printStackTrace();
            }
        } else {
            execReport(process, idClient, idLanguage, user, params, request, response);
        }
        return result;
    }

    /**
     * Search by Id.
     *
     * @param id Process identifier
     * @return Process
     */
    @Override
    public AdProcess findById(String id){
        AdProcess entity = super.findById(id);
        if (entity != null && entity.getBackground()) {
            TriggerKey triggerKey = new TriggerKey(entity.getId(), entity.getModule().getId());
            try {
                if (scheduler.getScheduler() != null && scheduler.getScheduler().getTrigger(triggerKey) instanceof CronTrigger) {
                    String cronExpr = ((CronTrigger) scheduler.getScheduler().getTrigger(triggerKey)).getCronExpression();
                    entity.setCronExpression(cronExpr);
                    String idUser = ((JobDetail)((scheduler.getScheduler().getTrigger(triggerKey)).getJobDataMap()).get("jobDetail")).getJobDataMap().getString("idUser");
                    String idClient = ((JobDetail)((scheduler.getScheduler().getTrigger(triggerKey)).getJobDataMap()).get("jobDetail")).getJobDataMap().getString("idClient");
                    entity.setExecutingIdClient(idClient);
                    entity.setExecutingIdUser(idUser);
                }
            } catch (SchedulerException e) {
                logger.error("Error getting scheduling data for process " + entity.getId(), e);
            }
        }
        return entity;
    }

    /**
     * Schedules a process
     *
     * @param process    Process
     * @param authUser   Authenticated User
     * @param idClient   Client ID
     * @param idLanguage Language ID
     * @param paramList  Parameters
     */
    @Override
    public void schedule(AdProcess process, AdUser authUser, String idClient, String idLanguage, String paramList) throws FWProcessPlanningException {
        schedule(process, null, authUser, idClient, idLanguage, paramList);
    }

    /**
     * Schedules a process
     *
     * @param process    Process
     * @param cron       Cron expression
     * @param authUser   Authenticated User
     * @param idClient   Client ID
     * @param idLanguage Language ID
     * @param paramList  Parameters
     */
    @Override
    public void schedule(AdProcess process, String cron, AdUser authUser, String idClient, String idLanguage, String paramList) throws FWProcessPlanningException {
        try {
            Class<?> classData = BackgroundProcess.class;
            TriggerKey triggerKey = new TriggerKey(process.getId(), process.getModule().getId());
            Trigger trigger = scheduler.getScheduler().getTrigger(triggerKey);
            if (trigger != null)
                scheduler.getScheduler().unscheduleJob(triggerKey);

            CronTriggerFactoryBean triggerFactoryBean = new CronTriggerFactoryBean();
            JobDetailFactoryBean jobFactory = new JobDetailFactoryBean();
            // TODO: Errores de migración
            //jobFactory.setJobClass(classData);
            jobFactory.setName(process.getId());
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("idUser", authUser.getIdUser());
            mapData.put("idClient", (idClient!=null && !idClient.equals("")) ? idClient : process.getIdClient());
            mapData.put("idLanguage", idLanguage);
            mapData.put("paramList", paramList);
            mapData.put("manual", cron == null);
            jobFactory.setJobDataAsMap(mapData);
            jobFactory.setGroup(process.getModule().getId());
            jobFactory.setApplicationContext(context);
            jobFactory.afterPropertiesSet();
            try {
                if (cron != null) {
                    triggerFactoryBean.setCronExpression(cron);
                    triggerFactoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                    triggerFactoryBean.setStartDelay(0);
                    triggerFactoryBean.setGroup(process.getModule().getId());
                    triggerFactoryBean.setName(process.getId());
                    triggerFactoryBean.setJobDetail(jobFactory.getObject());
                    triggerFactoryBean.afterPropertiesSet();
                    trigger = triggerFactoryBean.getObject();
                } else {
                    SimpleTriggerFactoryBean simpleTriggerFactoryBean = new SimpleTriggerFactoryBean();
                    simpleTriggerFactoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
                    simpleTriggerFactoryBean.setStartDelay(0);
                    simpleTriggerFactoryBean.setRepeatInterval(1);
                    simpleTriggerFactoryBean.setRepeatCount(0);
                    simpleTriggerFactoryBean.setPriority(0);
                    simpleTriggerFactoryBean.setGroup(process.getModule().getId());
                    simpleTriggerFactoryBean.setName(process.getId());
                    simpleTriggerFactoryBean.setJobDetail(jobFactory.getObject());
                    simpleTriggerFactoryBean.afterPropertiesSet();
                    trigger = simpleTriggerFactoryBean.getObject();
                }
            } catch (Exception e) {
                logger.error("Error scheduling the job", e);
                throw new FWProcessPlanningException(e.getMessage(), ProcessPlanningErrorCode.UNKNOWN_REASON);
            }
            try {
                scheduler.scheduleAJob(trigger, jobFactory.getObject());
            } catch (SchedulerException e) {
                logger.error("Error scheduling the job", e);
                if (e.getMessage().contains("will never fire")){
                    throw new FWProcessPlanningException(e.getMessage(), ProcessPlanningErrorCode.PROCESS_WILL_NEVER_FIRE);
                }
                throw new FWProcessPlanningException(e.getMessage(), ProcessPlanningErrorCode.UNKNOWN_REASON);
            }
        } catch (SchedulerException e) {
            logger.error("Error checking trigger", e);
            if (e.getMessage().contains("will never fire")){
                throw new FWProcessPlanningException(e.getMessage(), ProcessPlanningErrorCode.PROCESS_WILL_NEVER_FIRE);
            }
            throw new FWProcessPlanningException(e.getMessage(), ProcessPlanningErrorCode.UNKNOWN_REASON);
        }
    }

    /**
     * Unschedules a process
     *
     * @param process  Process to unschedule
     */
    @Override
    public void unschedule(AdProcess process) {
        try {
            TriggerKey triggerKey = new TriggerKey(process.getId(), process.getModule().getId());
            Trigger trigger = scheduler.getScheduler().getTrigger(triggerKey);
            if (trigger != null)
                scheduler.getScheduler().unscheduleJob(triggerKey);
        } catch (SchedulerException e) {
            logger.error("Error checking trigger", e);
        }
    }

    /**
     * Execute a report process
     *
     * @param process  Process to execute
     * @param idClient   Client id.
     * @param idLanguage Language id.
     * @param user     User
     * @param params   Parameter list
     * @param request Http request (Optional)
     * @param response Http response (Optional)
     */
    private void execReport(AdProcess process, String idClient, String idLanguage, AdUser user, List<AdProcessParam> params, HttpServletRequest request, HttpServletResponse response) {
        // Instantiate process
        reportProcess.setProcess(process);
        reportProcess.setUser(user);
        reportProcess.setIdClient(idClient);
        reportProcess.setIdLanguage(idLanguage);
        reportProcess.setParams(params);
        reportProcess.setRequest(request);
        reportProcess.setResponse(response);
        String idProcessExec = null;
        try {
            // Execute process
            idProcessExec = reportProcess.initExec();
            reportProcess.executeReport(idProcessExec);
        } catch (Throwable e) {
            if (idProcessExec != null) {
                reportProcess.error(idProcessExec, "Exception: " + e.getMessage());
                reportProcess.error(idProcessExec, e.toString());
                reportProcess.finishExec(idProcessExec, ProcessDefinition.STATUS_ERROR);
            }
            logger.error("Error calling: " + process.getClassname() + "." + process.getClassmethod(), e);
        }
    }

    /**
     * Build a param list from arguments
     *
     * @param process Process
     * @param params  Params (idParam1=value1,idParam2=value2,idParam3=value3, ....)
     * @return Param list
     */
    @Override
    public List<AdProcessParam> buildParamsList(AdProcess process, String params) {
        List<AdProcessParam> result = new ArrayList<>();
        // Add input params
        PageSearch pageReq = new PageSearch();
        pageReq.parseConstraints(params, AdProcess.class);
        for (String key : pageReq.getSearchs().keySet()) {
            if ("REPORT".equals(process.getPtype()) && Constants.OUTPUT_REPORT_TYPE.equals(key)) {
                AdProcessParam outputParam = new AdProcessParam();
                outputParam.setName(key);
                outputParam.setValue(pageReq.getSearchs().get(key));
                result.add(outputParam);
                continue;
            }
            Map<String, Object> filter = new HashMap<>();
            filter.put("idProcess", process.getIdProcess());
            filter.put("name", key);
            filter.put("ptype", "IN");
            AdProcessParam param = processParamService.findFirst(filter);
            if (param != null) {
                param.setValue(pageReq.getSearchs().get(key));
                result.add(param);
            } else {
                AdProcessParam newParam = new AdProcessParam();
                newParam.setIdProcess(process.getIdProcess());
                newParam.setPtype("IN");
                newParam.setName(key);
                newParam.setValue(pageReq.getSearchs().get(key));
                result.add(newParam);
            }
        }
        // Add output params
        Map<String, Object> filter = new HashMap<>();
        filter.put("idProcess", process.getIdProcess());
        filter.put("ptype", "OUT");
        List<AdProcessParam> outParams = processParamService.findAll(filter);
        result.addAll(outParams);
        return result;
    }

    /**
     * Called when process change active flag
     *
     * @param process Process
     * @param user User
     * @param idClient Client identifier
     * @param active Active / Inactive
     */
    @Override
    public void onChangeActive(AdProcess process, AdUser user, String idClient, boolean active) {
        try {
            // Instantiate a process class
            final ProcessDefinition processClass = (ProcessDefinition) applicationContext.getBean(process.getClassname());
            processClass.setProcess(process);
            processClass.setUser(user);
            processClass.setIdClient(idClient);
            processClass.setServletContext(servletContext);
            // Execute process
            ProcessMethod method = getMethod("setActive", processClass);
            if (method != null) {
                // TODO: Set Active process
/*
                executeProcess(process, processClass, method, active);
*/
            } else {
                logger.warn("Not found method: setActive in class " + process.getClassname());
            }
        } catch (Exception e) {
            logger.error("Error loading class: " + e.getMessage());
        }
    }

    /**
     * Get executed process (runtime only)
     *
     * @param sort Sort field
     * @return Process list
     */
    @Override
    public List<AdProcessExecuted> getExecutedProcess(Sort sort) {
        List<AdProcessExecuted> result = new ArrayList<>(executedProcess.values());
        if (sort != null) {
            SqlSort order = getSort(sort);
            final SqlSortItem item = order.getField(0);
            if (item != null) {
                Collections.sort(result, new Comparator<AdProcessExecuted>() {
                    @Override
                    public int compare(AdProcessExecuted o1, AdProcessExecuted o2) {
                        if (item.getSort().equals("name")) {
                            return "asc".equals(item.getDir()) ? o1.getName().compareTo(o2.getName()) : o2.getName().compareTo(o1.getName());
                        } else if (item.getSort().equals("started")) {
                            return "asc".equals(item.getDir()) ? o1.getStarted().compareTo(o2.getStarted()) : o2.getStarted().compareTo(o1.getStarted());
                        } else if (item.getSort().equals("finished")) {
                            Date d1 = o1.getFinished(), d2 = o2.getFinished();
                            return "asc".equals(item.getDir()) ?
                                    (d1 == null ? -1 : (d2 == null ? 1 : d1.compareTo(d2))) :
                                    (d2 == null ? -1 : (d1 == null ? 1 : d2.compareTo(d1)));
                        } else if (item.getSort().equals("status")) {
                            return "asc".equals(item.getDir()) ? o1.getStatus().compareTo(o2.getStatus()) : o2.getStatus().compareTo(o1.getStatus());
                        }
                        return 0;
                    }
                });
            }
        }
        return result;
    }

    /**
     * Load process information
     *
     * @param idClient Client identifier
     * @param idProcess Window identifier
     * @param idLanguage Language identifier
     * @param gui GUI inherit properties
     * @return AdTabMainDto
     */
    @Override
    public AdProcessDto loadProcess(String idClient, String idProcess, String idLanguage, AdGUIDto gui) {
        AdProcessDto result = new AdProcessDto(findById(idProcess), gui);
        result.setName(translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS, FWConfig.COLUMN_ID_PROCESS_NAME, idLanguage, result.getIdProcess(), result.getName()));
        result.setDescription(translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS, FWConfig.COLUMN_ID_PROCESS_DESCRIPTION, idLanguage, result.getIdProcess(), result.getDescription()));
        result.setPrivilegeDesc(translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS, FWConfig.COLUMN_ID_PROCESS_PRIVILEGE_DESC, idLanguage, result.getIdProcess(), result.getPrivilegeDesc()));
        result.setConfirmMsg(translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS, FWConfig.COLUMN_ID_PROCESS_CONFIRM_MSG, idLanguage, result.getIdProcess(), result.getConfirmMsg()));
        for (AdProcessParamDto param : result.getParams()) {
            param.setCaption(translationService.getTranslation(idClient, FWConfig.TABLE_ID_PROCESS_PARAM, FWConfig.COLUMN_ID_PROCESS_PARAM_CAPTION, idLanguage, param.getIdProcessParam(), param.getCaption()));
        }
        return result;
    }

    /**
     * Send a message to RabbitMQ Exchange
     *
     * @param message Message
     */
    private void sendToRabbitMQ(ProcessMessage message) {
        try {
            rabbitTemplate.convertAndSend(
                    TinyProcess.RABBIT_EXCHANGE_NAME, TinyProcess.RABBIT_ROUTING_KEY + message.getIdProcess(),
                    mapper.writeValueAsString(message)
            );
        } catch (JsonProcessingException e) {
            logger.error("Error send message: " + e.getMessage());
        }
    }
}
