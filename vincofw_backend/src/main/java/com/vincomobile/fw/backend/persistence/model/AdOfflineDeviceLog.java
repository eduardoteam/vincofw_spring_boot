package com.vincomobile.fw.backend.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_offline_device_log
 *
 * Date: 06/03/2016
 */
@Entity
@Table(name = "ad_offline_device_log")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdOfflineDeviceLog extends AdEntityBean {

    private String idOfflineDeviceLog;
    private String idOfflineDevice;
    private String level;
    private String log;
    private String ltype;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idOfflineDeviceLog;
    }

    @Override
    public void setId(String id) {
            this.idOfflineDeviceLog = id;
    }

    @Id
    @Column(name = "id_offline_device_log")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdOfflineDeviceLog() {
        return idOfflineDeviceLog;
    }

    public void setIdOfflineDeviceLog(String idOfflineDeviceLog) {
        this.idOfflineDeviceLog = idOfflineDeviceLog;
    }

    @Column(name = "id_offline_device")
    public String getIdOfflineDevice() {
        return idOfflineDevice;
    }

    public void setIdOfflineDevice(String idOfflineDevice) {
        this.idOfflineDevice = idOfflineDevice;
    }

    @Column(name = "level", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Column(name = "log", length = 2000)
    @NotNull
    @Size(min = 1, max = 2000)
    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    @Column(name = "ltype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getLtype() {
        return ltype;
    }

    public void setLtype(String ltype) {
        this.ltype = ltype;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdOfflineDeviceLog)) return false;

        final AdOfflineDeviceLog that = (AdOfflineDeviceLog) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idOfflineDeviceLog == null) && (that.idOfflineDeviceLog == null)) || (idOfflineDeviceLog != null && idOfflineDeviceLog.equals(that.idOfflineDeviceLog)));
        result = result && (((idOfflineDevice == null) && (that.idOfflineDevice == null)) || (idOfflineDevice != null && idOfflineDevice.equals(that.idOfflineDevice)));
        result = result && (((level == null) && (that.level == null)) || (level != null && level.equals(that.level)));
        result = result && (((log == null) && (that.log == null)) || (log != null && log.equals(that.log)));
        result = result && (((ltype == null) && (that.ltype == null)) || (ltype != null && ltype.equals(that.ltype)));
        return result;
    }

}

