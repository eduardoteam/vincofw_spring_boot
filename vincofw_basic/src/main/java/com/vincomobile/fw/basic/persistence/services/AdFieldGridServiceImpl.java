package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdField;
import com.vincomobile.fw.basic.persistence.model.AdFieldGrid;
import com.vincomobile.fw.basic.persistence.model.AdTab;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 02/06/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_field_grid
 */
@Repository
@Transactional(readOnly = true)
public class AdFieldGridServiceImpl extends BaseServiceImpl<AdFieldGrid, String> implements AdFieldGridService {

    private final AdTabService tabService;

    /**
     * Constructor.
     */
    public AdFieldGridServiceImpl(AdTabService tabService) {
        super(AdFieldGrid.class);
        this.tabService = tabService;
    }

    /**
     * List columns for User and Tab
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @return Columns
     */
    @Override
    public List<AdFieldGrid> getUserColumns(String idClient, String idTab, String idUser) {
        // List selected fields
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idTab", idTab);
        filter.put("idUser", idUser);
        filter.put("active", true);
        return findAll(new SqlSort("seqno", "asc"), filter);
    }

    /**
     * List all columns for User and Tab
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @return Columns
     */
    @Override
    public Page<AdFieldGrid> getColumns(String idClient, String idTab, String idUser, String idLanguage) {
        // List selected fields
        List<AdFieldGrid> result = getUserColumns(idClient, idTab, idUser);
        Long seqno = 10L;
        for (AdFieldGrid fieldGrid : result) {
            fieldGrid.setChecked(true);
            fieldGrid.setSeqno(seqno);
            seqno += 10;
        }

        // List the rest fields
        Query query = entityManager.createQuery(
                "SELECT F FROM AdField F " +
                "WHERE F.idClient IN :idClient AND F.idTab = :idTab " +
                " AND not exists (SELECT 1 FROM AdFieldGrid FG WHERE F.idTab = FG.idTab AND F.idField = FG.idField AND FG.idUser = :idUser) " +
                "ORDER BY F.gridSeqno",
                AdField.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idTab", idTab);
        query.setParameter("idUser", idUser);
        List<AdField> dbFields = query.getResultList();
        // Put first the showingrid fields
        List<AdField> otherFields = new ArrayList<>();
        for (AdField field : dbFields) {
            if (field.getGridSeqno() != null && field.getGridSeqno() > 0) {
                otherFields.add(field);
            }
        }
        for (AdField field : dbFields) {
            if (field.getGridSeqno() == null || field.getGridSeqno() <= 0) {
                otherFields.add(field);
            }
        }
        boolean userDefined = result.size() > 0;
        for (AdField field : otherFields) {
            AdFieldGrid fieldGrid = new AdFieldGrid();
            fieldGrid.setIdClient(field.getIdClient());
            fieldGrid.setIdModule(field.getIdModule());
            fieldGrid.setIdTab(idTab);
            fieldGrid.setIdUser(idUser);
            fieldGrid.setField(field);
            fieldGrid.setSeqno(seqno);
            fieldGrid.setChecked(!userDefined && field.getShowingrid());
            fieldGrid.setActive(true);
            result.add(fieldGrid);
            seqno += 10;
        }

        // TODO: Obtener traduccion del nombre del campo
        // if (!Converter.isEmpty(idLanguage)) {  }

        return new PageImpl<>(result, new PageSearch(), result.size());
    }

    /**
     * Save columns
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @param fields Field list
     */
    @Override
    public void saveColumns(String idClient, String idTab, String idUser, String[] fields) {
        // Remove previous columns
        deleteColumns(idTab, idUser);
        // Insert new columns
        AdTab tab = tabService.findById(idTab);
        Long seqno = 10L;
        for (String field: fields) {
            if (!Converter.isEmpty(field)) {
                AdFieldGrid fieldGrid = new AdFieldGrid();
                fieldGrid.setIdClient(idClient);
                fieldGrid.setIdModule(tab.getIdModule());
                fieldGrid.setIdTab(idTab);
                fieldGrid.setIdUser(idUser);
                fieldGrid.setIdField(field);
                fieldGrid.setSeqno(seqno);
                fieldGrid.setActive(true);
                save(fieldGrid);
                seqno += 10;
            }
        }
    }

    /**
     * Delete columns
     *
     * @param idTab Tab identifier
     * @param idUser User identifier
     */
    @Override
    public void deleteColumns(String idTab, String idUser) {
        Map filter = new HashMap();
        filter.put("idTab", idTab);
        filter.put("idUser", idUser);
        applyUpdate("DELETE FROM AdFieldGrid WHERE idTab = :idTab AND idUser = :idUser", filter);
    }

}
