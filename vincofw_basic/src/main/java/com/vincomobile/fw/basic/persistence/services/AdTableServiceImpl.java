package com.vincomobile.fw.basic.persistence.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.JSONColumn;
import com.vincomobile.fw.basic.JSONTable;
import com.vincomobile.fw.basic.persistence.dto.AdTableDto;
import com.vincomobile.fw.basic.persistence.model.*;
import com.vincomobile.fw.basic.tools.Converter;
import org.hibernate.engine.spi.SessionImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_TABLE
 * <p/>
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdTableServiceImpl extends BaseServiceImpl<AdTable, String> implements AdTableService {
    private Logger logger = LoggerFactory.getLogger(AdTableServiceImpl.class);

    @Autowired
    AdReferenceService referenceService;

    @Autowired
    AdRefTableService refTableService;

    @Autowired
    AdRefButtonService refButtonService;

    @Autowired
    AdMessageService messageService;

    /**
     * Constructor.
     */
    public AdTableServiceImpl() {
        super(AdTable.class);
    }

    /**
     * Checks if the name belongs to a table or view
     *
     * @param tableName Table or view name
     * @param view      True to check if it is a view. False to check if it is a table
     * @param dataOrigin Origen de los datos
     * @return True if the table of view exists, false otherwise
     * @throws SQLException SQL Exception thrown if problem occurs while accesing database
     */
    @Override
    public boolean isValidTable(String tableName, boolean view, String dataOrigin) throws SQLException {
        if (!"TABLE".equals(dataOrigin)) {
            return true;
        }
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        DatabaseMetaData meta = connection.getMetaData();
        String catalog = connection.getCatalog();
        String serverType = meta.getDatabaseProductName();
        String[] VIEW_TYPES = {"VIEW"};
        if (!view)
            VIEW_TYPES = null;
        logger.debug("Reading tables from DB");
        if (FWConfig.DATABASE_TYPE_MYSQL.equalsIgnoreCase(serverType)) {
            return validTable(meta, catalog, catalog, VIEW_TYPES, tableName);
        } else {
            ResultSet schemas = meta.getSchemas();
            while (schemas.next()) {
                String schemaName = schemas.getString("TABLE_SCHEM");
                if (validTable(meta, catalog, schemaName, VIEW_TYPES, tableName))
                    return true;
            }
        }

        return false;
    }

    /**
     * Validate table structure
     *
     * @param meta Database meta data
     * @param catalog Database catalog
     * @param schemaName Database schema
     * @param viewTypes View types
     * @param tableName Table name
     * @return Table exist
     * @throws SQLException
     */
    private boolean validTable(DatabaseMetaData meta, String catalog, String schemaName, String[] viewTypes, String tableName) throws SQLException {
        ResultSet tables = meta.getTables(catalog, schemaName, null, viewTypes);
        while (tables.next()) {
            String table = tables.getString("TABLE_NAME");
            if (table.equals(tableName))
                return true;
        }
        return false;
    }

    /**
     * Gets the name of a file replacing parameters @[name]
     *
     * @param table Table information
     * @param name  File name
     * @return Real name
     */
    @Override
    public String getFileName(AdTable table, String name) {
        name = name.replace("@package", table.getModule().getJavaPackage().replace('.','/'));
        name = name.replace("@CapitalizeStandardName", table.getCapitalizeStandardName());
        name = name.replace("@CapitalizeName", table.getCapitalizeName());
        name = name.replace("@UpperCaseName", table.getUpperCaseName());
        name = name.replace("@LowerName", table.getLowerName());
        return name;
    }

    /**
     * Returns information about tables not in AD_*
     *
     * @throws SQLException SQL Exception thrown if problem occurs while accesing database
     */
    @Override
    public List<AdTable> readTablesInformation() throws SQLException {
        Connection connection = entityManager.unwrap(SessionImplementor.class).connection();
        List<AdTable> allTables = new ArrayList<AdTable>();
        DatabaseMetaData meta = connection.getMetaData();
        String catalog = connection.getCatalog();
        String dataBaseName = connection.getCatalog();

        logger.debug("Reading tables from DB");
        ResultSet tables = meta.getTables(catalog, dataBaseName, null, null);
        while (tables.next()) {
            String tableName = tables.getString("TABLE_NAME");
            if (!tableName.toUpperCase().startsWith("AD_")) {
                Map<String, Object> filters = new HashMap<String, Object>();
                filters.put("name", tableName);
                AdTable newTable = findFirst(filters);
                if (newTable == null) {
                    newTable = new AdTable();
                    newTable.setName(tableName);
                }
                logger.debug("Reading columns from table " + tableName);
                ResultSet c1 = meta.getColumns(dataBaseName, null, tableName, null);
                int counter = 0;
                while (c1.next()) {
                    counter++;
                }
                newTable.setTotalColumns(counter);
                allTables.add(newTable);
            }
        }
        return allTables;
    }

    /**
     * Get JSONTable structure by file path
     *
     * @param filePath Directory to store files into
     * @return JSONTable structure
     */
    @Override
    public JSONTable getJSONTableByFile(String filePath) {
        final File f = new File(filePath);
        if (f.exists()) {
            //JSON from file to Object
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readValue(f, JSONTable.class);
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Get JSONColumn structure by file path
     *
     * @param filePath Directory to store files into
     * @return JSONColumn structure
     */
    @Override
    public JSONColumn getJSONColumsByFile(String filePath) {
        final File f = new File(filePath);
        if (f.exists()) {
            // JSON from file to Object
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readValue(f, JSONColumn.class);
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Get table information
     *
     * @param name Table name
     * @return AdTable
     */
    @Override
    public AdTable getTable(String name) {
        Map filter = new HashMap();
        filter.put("name", name);
        return findFirst(filter);
    }

    /**
     * Load table (Meta Data)
     *
     * @param idTable Table identifier
     * @return AdTableDto
     */
    @Override
    public AdTableDto loadTable(String idTable) {
        return new AdTableDto(findById(idTable));
    }

    /**
     * Get module tables information
     *
     * @param idModule Module identifier
     * @return Tables for module
     */
    @Override
    public List<AdTable> getModuleTable(String idModule) {
        Map filter = new HashMap();
        filter.put("idModule", idModule);
        filter.put("active", true);
        return findAll(filter);
    }

    /**
     * Update a row in new transaction
     *
     * @param sql Update sentence
     * @return Update rows count
     */
    @Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Transactional(propagation = Propagation.REQUIRED)
    public int updateRow(String sql) {
        // logger.debug(sql);
        return applyNativeUpdate(sql);
    }

    /**
     * Validate table structure and references
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param table Table information
     * @return Null or error
     */
    @Override
    public String validateTable(String idClient, String idLanguage, AdTable table) {
        if (table.getKeyFields().size() == 0) {
            return messageService.getMessage(idClient, idLanguage, "AD_ProcessTableErrCRUDNoPK");
        }
        for (AdColumn column : table.getColumns()) {
            if (
                    AdReferenceService.BASE_ID_TABLE.equals(column.getIdReference()) || AdReferenceService.BASE_ID_TABLEDIR.equals(column.getIdReference()) ||
                    AdReferenceService.BASE_ID_SEARCH.equals(column.getIdReference()) || AdReferenceService.BASE_ID_BUTTON.equals(column.getIdReference()) ||
                    AdReferenceService.BASE_ID_LIST.equals(column.getIdReference()) || AdReferenceService.BASE_ID_LISTMULTIPLE.equals(column.getIdReference())
            ) {
                if (Converter.isEmpty(column.getIdReferenceValue())) {
                    return messageService.getMessage(idClient, idLanguage, "AD_ProcessTableErrValidateRefValue", column.getName());
                } else {
                    if (AdReferenceService.BASE_ID_TABLE.equals(column.getIdReference()) || AdReferenceService.BASE_ID_TABLEDIR.equals(column.getIdReference()) || AdReferenceService.BASE_ID_SEARCH.equals(column.getIdReference())) {
                        AdRefTable refTable = refTableService.getRefTableByReference(column.getIdReferenceValue());
                        if (refTable == null) {
                            AdReference tableRef = referenceService.findById(column.getIdReferenceValue());
                            return messageService.getMessage(idClient, idLanguage, "AD_ProcessTableErrValidateRefValueTable", column.getName(), tableRef.getName());
                        }
                    } else if (AdReferenceService.BASE_ID_BUTTON.equals(column.getIdReference())) {
                        AdRefButton refButton = refButtonService.getRefButtonByReference(column.getIdReferenceValue());
                        if (refButton == null) {
                            AdReference buttonRef = referenceService.findById(column.getIdReferenceValue());
                            return messageService.getMessage(idClient, idLanguage, "AD_ProcessTableErrValidateRefValueButton", column.getName(), buttonRef.getName());
                        }
                    }
                }
            }
        }
        return null;
    }

}
