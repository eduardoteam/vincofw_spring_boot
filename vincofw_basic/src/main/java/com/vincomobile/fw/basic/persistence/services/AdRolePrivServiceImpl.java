package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdRolePriv;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_ROLE_PRIV
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRolePrivServiceImpl extends BaseServiceImpl<AdRolePriv, String> implements AdRolePrivService {

    /**
     * Constructor.
     *
     */
    public AdRolePrivServiceImpl() {
        super(AdRolePriv.class);
    }

    /**
     * Delete all privilege of role
     *
     * @param idRole Role identifier
     * @return Delete row count
     */
    @Override
    public int deletePrivilege(String idRole) {
        Map filter = new HashMap();
        filter.put("idRole", idRole);
        return applyUpdate("DELETE FROM AdRolePriv WHERE idRole = :idRole", filter);
    }

}
