package com.vincomobile.fw.basic.persistence.services;

import java.util.ArrayList;
import java.util.List;

public class SqlSort {
    List<SqlSortItem> sorters = new ArrayList<>();

    public SqlSort() {
    }

    public SqlSort(String sort, String dir) {
        addSorter(sort, dir);
    }

    public void clear() {
        sorters.clear();
    }

    public void addSorter(String sort, String dir) {
        sorters.add(new SqlSortItem(sort, dir));
    }

    public String getSqlOrder() {
        String result = "";
        for (int i = 0; i < sorters.size(); i++) {
            SqlSortItem item = sorters.get(i);
            if (i > 0)
                result += ", ";
            result += item.getSort() + " " + item.getDir();
        }
        return result;
    }

    public String getSqlOrderBy() {
        String result = getSqlOrder();
        return "".equals(result) ? result :  " order by " + result;
    }

    public boolean hasField(String name) {
        for (SqlSortItem item: sorters) {
            if (item.getSort().toLowerCase().contains(name.toLowerCase()))
                return true;
        }
        return false;
    }

    public SqlSortItem getField(String name) {
        for (SqlSortItem item: sorters) {
            if (item.getSort().toLowerCase().contains(name.toLowerCase()))
                return item;
        }
        return null;
    }

    public SqlSortItem getField(int index) {
        return index >= 0 && index < sorters.size() ? sorters.get(index) : null;
    }

}
