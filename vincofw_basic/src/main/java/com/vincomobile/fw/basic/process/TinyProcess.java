package com.vincomobile.fw.basic.process;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.services.AdMessageService;
import com.vincomobile.fw.basic.persistence.services.AdPreferenceService;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

public class TinyProcess {

    public static final String STATUS_RUNNING           = "RUNNING";
    public static final String STATUS_ERROR             = "ERROR";
    public static final String STATUS_SUCCESS           = "SUCCESS";
    public static final String RABBIT_EXCHANGE_NAME     = "ProcessExec";
    public static final String RABBIT_ROUTING_KEY       = "Process_";
    public static final String RABBIT_QUEUE_NAME        = "ProcessQueue";
    public static final String RABBIT_QUEUE_WEB_NAME    = "ProcessQueueWeb";

    @Autowired
    protected AdMessageService messageService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    protected Logger logger = LoggerFactory.getLogger(TinyProcess.class);

    protected String idClient;
    protected String idModule;
    protected String idUser;
    protected String idLanguage;
    protected String idProcess;
    protected String lastError;
    protected String finishStatus;
    protected List<ProcessParam> params;

    private ObjectMapper mapper = new ObjectMapper();
    private Long startTime;

    /**
     * Init the process with external parameters
     *
     * @param parameters Process Parameters
     */
    protected void initProcess(TinyProcessParam parameters) {
        this.idClient = parameters.idClient;
        this.idUser = parameters.idUser;
        this.idLanguage = parameters.idLanguage;
        this.idProcess = parameters.idProcess;
        this.params = parameters.params;
        lastError = "";
        finishStatus = STATUS_RUNNING;
        startTime = System.currentTimeMillis();
    }

    /**
     * Get message
     *
     * @param value Message search key
     * @param params Parameters
     * @return Message
     */
    protected String getMessage(String value, Object... params) {
        return messageService.getMessage(idClient, idLanguage, value, params);
    }

    /**
     * Send a message to RabbitMQ Exchange
     *
     * @param message Message
     */
    private void sendToRabbitMQ(ProcessMessage message) {
        try {
            rabbitTemplate.convertAndSend(
                    TinyProcess.RABBIT_EXCHANGE_NAME, TinyProcess.RABBIT_ROUTING_KEY + idProcess,
                    mapper.writeValueAsString(message)
            );
        } catch (JsonProcessingException e) {
            logger.error("Error send message: " + e.getMessage());
        }
    }

    /**
     * Write success to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    public void success(String idProcessExec, String log) {
        logger.info(log);
        sendToRabbitMQ(new ProcessMessage(idProcess, idProcessExec, idClient, idModule, idUser, "SUCCESS", log));
    }

    /**
     * Write log to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    public void info(String idProcessExec, String log) {
        logger.info(log);
        sendToRabbitMQ(new ProcessMessage(idProcess, idProcessExec, idClient, idModule, idUser, "INFO", log));
    }

    /**
     * Write warning to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    public void warn(String idProcessExec, String log) {
        logger.warn(log);
        sendToRabbitMQ(new ProcessMessage(idProcess, idProcessExec, idClient, idModule, idUser, "WARN", log));
    }

    /**
     * Write error to DB
     *
     * @param idProcessExec Process Execution Identifier
     * @param log Log info
     */
    public void error(String idProcessExec, String log) {
        logger.error(log);
        sendToRabbitMQ(new ProcessMessage(idProcess, idProcessExec, idClient, idModule, idUser, "ERROR", log));
    }

    /**
     * Finish process execution
     *
     * @param idProcessExec Process Execution Identifier
     * @param status Execution status
     * @param output Output file (Optional)
     * @param params List parameters (Optional)
     */
    public void finishExec(String idProcessExec, String status, File output, List<OutputParam> params) {
        long endTime = System.currentTimeMillis();
        long seconds = (endTime - startTime) / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        String time = Converter.leftFill(hours, '0', 2) + ":" + Converter.leftFill(minutes % 60, '0', 2) + ":" + Converter.leftFill(seconds % 60, '0', 2);
        info(idProcessExec, getMessage("AD_ProcessExecutionTime", time));
    }

    public void finishExec(String idProcessExec, String status, File output) {
        finishExec(idProcessExec, status, output, null);
    }

    public void finishExec(String idProcessExec, String status) {
        finishExec(idProcessExec, status, null, null);
    }


    /**
     * Get parameter of process
     *
     * @param name Parameter name
     * @return Parameter
     */
    protected ProcessParam getParam(String name) {
        for (ProcessParam param : params) {
            if (param.getName().equals(name)) {
                return param;
            }
        }
        return null;
    }

    /**
     * Get parameter string value of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter value
     */
    protected String getParamString(String idProcessExec, String name) {
        ProcessParam param = getParam(name);
        return param == null ? (String) paramError(idProcessExec, name) : param.getString();
    }

    /**
     * Get parameter file of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return File
     */
    protected MultipartFile getParamFile(String idProcessExec, String name) {
        ProcessParam param = getParam(name);
        return param == null ? (MultipartFile) paramError(idProcessExec, name) : param.getFile();
    }

    /**
     * Get parameter boolean value of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter value
     */
    protected Boolean getParamBoolean(String idProcessExec, String name) {
        ProcessParam param = getParam(name);
        return param == null ? (Boolean) paramError(idProcessExec, name) : param.getBoolean();
    }

    /**
     * Get parameter of process (Mandatory parameter)
     *
     * @param idProcessExec Process identifier
     * @param name Parameter name
     * @return Parameter
     */
    protected ProcessParam getParamMandatory(String idProcessExec, String name) {
        ProcessParam param = getParam(name);
        return param == null ? (ProcessParam) paramError(idProcessExec, name) : param;
    }


    protected Object paramError(String idProcessExec, String name) {
        error(idProcessExec, getMessage("AD_GlobalErrParam", name));
        finishExec(idProcessExec, STATUS_ERROR);
        return null;
    }

    /**
     * Get parameter string value of process (Optional parameter)
     *
     * @param name Parameter name
     * @param defaultValue Default parameter value
     * @return Parameter value
     */
    protected String getOptionalParamString(String name, String defaultValue) {
        ProcessParam param = getParam(name);
        String result = param != null ? param.getString() : defaultValue;
        return "null".equals(result) ? null : result;
    }

    protected String getOptionalParamString(String name) {
        return getOptionalParamString(name, null);
    }

    /**
     * Upload and validate a file
     *
     * @param idProcessExec Process Execution Identifier
     * @param file Multipart file
     * @param contentType Content type
     * @param fileExt File extention
     * @param maxSize Max file size
     * @param directory Store directory
     * @return File upload
     */
    protected File uploadFile(String idProcessExec, MultipartFile file, String contentType, String fileExt, Long maxSize, String directory) {
        if (!FileTool.validContentType(file, contentType)) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadFileType", file.getOriginalFilename(), fileExt));
            finishExec(idProcessExec, STATUS_ERROR);
            return null;
        }
        if (!FileTool.validSize(file, maxSize)) {
            error(idProcessExec, getMessage("AD_ErrValidationUploadFileSize", file.getOriginalFilename(), ""+maxSize));
            finishExec(idProcessExec, STATUS_ERROR);
            return null;
        }
        String path = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        File uploadFile = new File(path + directory + file.getOriginalFilename());
        return FileTool.saveFile(file, uploadFile.getPath(), true, false);
    }

}
