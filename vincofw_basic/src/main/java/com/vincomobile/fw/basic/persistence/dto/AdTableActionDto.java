package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdTableAction;

public class AdTableActionDto {

    private String idTableAction;
    private String idProcess;
    private String atype;
    private String displaylogic;
    private String name;
    private String restCommand;
    private String url;
    private String target;
    private String icon;
    private String uipattern;
    private Long seqno;
    private String privilege;

    public AdTableActionDto(AdTableAction item) {
        this.idTableAction = item.getIdTableAction();
        this.idProcess = item.getIdProcess();
        this.atype = item.getAtype();
        this.displaylogic = item.getDisplaylogic();
        this.name = item.getName();
        this.restCommand = item.getRestCommand();
        this.url = item.getUrl();
        this.target = item.getTarget();
        this.icon = item.getIcon();
        this.uipattern = item.getUipattern();
        this.seqno = item.getSeqno();
        this.privilege = item.getPrivilege();
    }

    /*
     * Set/Get Methods
     */

    public String getIdTableAction() {
        return idTableAction;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public String getAtype() {
        return atype;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public String getName() {
        return name;
    }

    public String getRestCommand() {
        return restCommand;
    }

    public String getUrl() {
        return url;
    }

    public String getTarget() {
        return target;
    }

    public String getIcon() {
        return icon;
    }

    public String getUipattern() {
        return uipattern;
    }

    public Long getSeqno() {
        return seqno;
    }

    public String getPrivilege() {
        return privilege;
    }
}

