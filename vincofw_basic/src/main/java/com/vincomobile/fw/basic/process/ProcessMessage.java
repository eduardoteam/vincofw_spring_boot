package com.vincomobile.fw.basic.process;

import java.util.Date;

public class ProcessMessage {
    String idProcess;
    String idProcessExec;
    String idClient;
    String idModule;
    String idUser;
    String type;
    Date timestamp;
    String log;

    public ProcessMessage() {
    }

    public ProcessMessage(String idProcess, String idProcessExec, String idClient, String idModule, String idUser, String type, String log) {
        this.idProcess = idProcess;
        this.idProcessExec = idProcessExec;
        this.idClient = idClient;
        this.idModule = idModule;
        this.idUser = idUser;
        this.timestamp = new Date();
        this.type = type;
        this.log = log;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    public String getIdProcessExec() {
        return idProcessExec;
    }

    public void setIdProcessExec(String idProcessExec) {
        this.idProcessExec = idProcessExec;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
