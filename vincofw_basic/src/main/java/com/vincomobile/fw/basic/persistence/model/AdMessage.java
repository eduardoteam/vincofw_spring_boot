package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_MESSAGE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_message")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdMessage extends AdEntityBean {

    private String idMessage;
    private String value;
    private String msgtext;
    private Boolean admin;
    private Boolean web;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idMessage;
    }

    @Override
    public void setId(String id) {
            this.idMessage = id;
    }

    @Id
    @Column(name = "id_message")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    @Column(name = "value", length = 60)
    @NotNull
    @Size(min = 1, max = 60)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "msgtext", length = 1500)
    @NotNull
    @Size(min = 1, max = 1500)
    public String getMsgtext() {
        return msgtext;
    }

    public void setMsgtext(String msgtext) {
        this.msgtext = msgtext;
    }

    @Column(name = "admin", columnDefinition = "BIT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    @NotNull
    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    @Column(name = "web", columnDefinition = "BIT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    @NotNull
    public Boolean getWeb() {
        return web;
    }

    public void setWeb(Boolean web) {
        this.web = web;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdMessage)) return false;

        final AdMessage that = (AdMessage) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idMessage == null) && (that.idMessage == null)) || (idMessage != null && idMessage.equals(that.idMessage)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        result = result && (((msgtext == null) && (that.msgtext == null)) || (msgtext != null && msgtext.equals(that.msgtext)));
        result = result && (((admin == null) && (that.admin == null)) || (admin != null && admin.equals(that.admin)));
        result = result && (((web == null) && (that.web == null)) || (web != null && web.equals(that.web)));
        return result;
    }

}

