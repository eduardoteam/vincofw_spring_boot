package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 02/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_audit
 */
@Entity
@Table(name = "ad_audit")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdAudit extends EntityBean<String> {

    private String idClient;
    private String idAudit;
    private String idTable;
    private String idRecord;
    private String idUser;
    private String operation;
    private String oldValue;
    private String newValue;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idAudit;
    }

    @Override
    public void setId(String id) {
            this.idAudit = id;
    }

    @Id
    @Column(name = "id_audit")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(String idAudit) {
        this.idAudit = idAudit;
    }

    @Column(name = "id_client")
    @NotNull
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_table", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_record", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    @Column(name = "id_user", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "operation", length = 1)
    @NotNull
    @Size(min = 1, max = 1)
    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Column(name = "old_value")
    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Column(name = "new_value")
    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }


    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdAudit)) return false;

        final AdAudit that = (AdAudit) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idAudit == null) && (that.idAudit == null)) || (idAudit != null && idAudit.equals(that.idAudit)));
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idRecord == null) && (that.idRecord == null)) || (idRecord != null && idRecord.equals(that.idRecord)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((operation == null) && (that.operation == null)) || (operation != null && operation.equals(that.operation)));
        result = result && (((oldValue == null) && (that.oldValue == null)) || (oldValue != null && oldValue.equals(that.oldValue)));
        result = result && (((newValue == null) && (that.newValue == null)) || (newValue != null && newValue.equals(that.newValue)));
        return result;
    }

}

