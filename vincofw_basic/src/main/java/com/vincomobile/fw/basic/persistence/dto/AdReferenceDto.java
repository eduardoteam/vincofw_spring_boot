package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.beans.AdRefListValue;
import com.vincomobile.fw.basic.persistence.model.AdReference;

import java.util.ArrayList;
import java.util.List;

public class AdReferenceDto {

    private String idReference;
    private String rtype;
    private AdRefTableDto refTable;
    private List<AdRefListDto> refList;
    private AdRefButtonDto refButton;

    public AdReferenceDto(AdReference reference, AdReference referenceValue) {
        this.idReference = referenceValue == null ? reference.getIdReference() : referenceValue.getIdReference();
        this.rtype = reference.getRtype();
    }

    public AdReferenceDto(AdReference reference, String idReferenceValue) {
        this.idReference = idReferenceValue == null ? reference.getIdReference() : idReferenceValue;
        this.rtype = reference.getRtype();
    }

    public AdReferenceDto(AdReference reference) {
        this.idReference = reference.getIdReference();
        this.rtype = reference.getRtype();
    }

    public void addRefList(List<AdRefListValue> items) {
        this.refList = new ArrayList<>();
        for (AdRefListValue item : items) {
            this.refList.add(new AdRefListDto(item));
        }
    }

    /*
     * Set/Get Methods
     */

    public String getIdReference() {
        return idReference;
    }

    public String getRtype() {
        return rtype;
    }

    public AdRefTableDto getRefTable() {
        return refTable;
    }

    public void setRefTable(AdRefTableDto refTable) {
        this.refTable = refTable;
    }

    public List<AdRefListDto> getRefList() {
        return refList;
    }

    public AdRefButtonDto getRefButton() {
        return refButton;
    }

    public void setRefButton(AdRefButtonDto refButton) {
        this.refButton = refButton;
    }
}

