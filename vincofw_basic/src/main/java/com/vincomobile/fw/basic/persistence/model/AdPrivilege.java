package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_PRIVILEGE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_privilege")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdPrivilege extends AdEntityBean {

    private String idPrivilege;
    private String name;
    private String description;

    /*
     * Set/Get Methods
     */
    @Override
    @Transient
    public String getId() {
        return idPrivilege;
    }

    @Override
    public void setId(String id) {
            this.idPrivilege = id;
    }

    @Id
    @Column(name = "id_privilege")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdPrivilege() {
        return idPrivilege;
    }

    public void setIdPrivilege(String idPrivilege) {
        this.idPrivilege = idPrivilege;
    }

    @Column(name = "name", length = 100, unique = true)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdPrivilege)) return false;

        final AdPrivilege that = (AdPrivilege) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idPrivilege == null) && (that.idPrivilege == null)) || (idPrivilege != null && idPrivilege.equals(that.idPrivilege)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        return result;
    }

}

