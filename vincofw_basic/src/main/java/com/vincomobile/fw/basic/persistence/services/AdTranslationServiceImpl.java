package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.beans.AdMessageTranslation;
import com.vincomobile.fw.basic.persistence.beans.AdTranslations;
import com.vincomobile.fw.basic.persistence.beans.AdTranslationsData;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.cache.CacheTable;
import com.vincomobile.fw.basic.persistence.model.AdPreference;
import com.vincomobile.fw.basic.persistence.model.AdTranslation;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.*;

/**
 * Created by Devtools.
 * Service of AD_TRANSLATION
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdTranslationServiceImpl extends BaseServiceImpl<AdTranslation, String> implements AdTranslationService {

    /**
     * Constructor.
     */
    public AdTranslationServiceImpl() {
        super(AdTranslation.class);
    }

    /**
     * Get a translation item
     *
     * @param idClient   Client identifier
     * @param idTable    Table identifier
     * @param idColumn   Column identifier
     * @param idLanguage Language identifier
     * @param rowkey     Row key
     * @return Translation
     */
    @Override
    public AdTranslation findTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey) {
        if (Converter.isEmpty(idLanguage))
            return null;
        return findFirst(getFilter(idClient, null, idTable, idColumn, rowkey, idLanguage, false));
    }

    /**
     * Get all item translations
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param idLanguage Language identifier
     * @return Translations
     */
    @Override
    public List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey, String idLanguage, String idModule) {
        return findAll(getFilter(idClient, idModule, idTable, idColumn, rowkey, idLanguage, false));
    }

    public List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey, String idLanguage) {
        return getTranslations(idClient, idTable, idColumn, rowkey, idLanguage, null);
    }

    @Override
    public List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey) {
        return getTranslations(idClient, idTable, idColumn, rowkey, null, null);
    }

    /**
     * Get translation of a field in a language
     *
     * @param idClient   Client identifier
     * @param idTable    Table identifier
     * @param idColumn   Column identifier
     * @param idLanguage Language identifier
     * @param rowkey     Row key
     * @param defValue   Default value
     * @return Translation
     */
    @Override
    public String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey, String defValue) {
        AdTranslation result = findTranslation(idClient, idTable, idColumn, idLanguage, rowkey);
        return result == null ? defValue : result.getTranslation();
    }

    @Override
    public String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, Long rowkey, String defValue) {
        return getTranslation(idClient, idTable, idColumn, idLanguage, "" + rowkey, defValue);
    }

    @Override
    public String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey) {
        return getTranslation(idClient, idTable, idColumn, idLanguage, rowkey, null);
    }

    @Override
    public String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, Long rowkey) {
        return getTranslation(idClient, idTable, idColumn, idLanguage, "" + rowkey, null);
    }

    /**
     * Delete all rows for column translation
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param idLanguage Language identifier
     */
    @Override
    public void deleteTranslation(String idClient, String idTable, String idColumn, String rowkey, String idLanguage) {
        List<AdTranslation> result = findAll(getFilter(idClient, null, idTable, idColumn, rowkey, idLanguage, false));
        for (AdTranslation translation : result) {
            deleteTranslationFile(translation);
            delete(translation);
        }
    }

    @Override
    public void deleteTranslation(String idClient, String idTable, String idColumn, String rowkey) {
        deleteTranslation(idClient, idTable, idColumn, rowkey, null);
    }

    @Override
    public void deleteTranslation(String idClient, String idTable, String idColumn, Long rowkey) {
        deleteTranslation(idClient, idTable, idColumn, "" + rowkey, null);
    }

    /**
     * Remove files of a translation item
     *
     * @param item Translation
     */
    @Override
    public void deleteTranslationFile(AdTranslation item) {
        if (item != null && item.getPath() != null) {
            CacheTable table = getCacheTableById(item.getIdTable());
            AdPreference baserDir = CacheManager.getPreference(item.getIdClient(), AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
            if (baserDir != null) {
                FileTool.deleteFile(baserDir.getString() + "/files/" + table.getTable().getName() + "/" + item.getPath());
            }
        }
    }

    /**
     * List all translate fields caption
     *
     * @param idClient   Client identifier
     * @param idLanguage Language identifier
     * @param idTable Table identifier
     * @return List of translation { key: tableName$columnName, value: translation }
     */
    @Override
    public Map<String, String> listFieldCaptions(String idClient, String idLanguage, String idTable) {
        String sql =
                "SELECT t.name AS tableName, c.name AS columnName, f.id_field, COALESCE(tr.translation, f.caption) \n" +
                "FROM ad_field f JOIN ad_column c ON f.id_column = c.id_column \n" +
                "JOIN ad_table t ON t.id_table = c.id_table \n" +
                "LEFT JOIN ad_translation tr ON tr.id_language = :idLanguage \n" +
                "  AND tr.id_table = '" + FWConfig.TABLE_ID_FIELD + "' AND tr.id_column = '" + FWConfig.COLUMN_ID_FIELD_CAPTION + "'\n" +
                "  AND tr.id_client IN :idClient AND tr.rowkey = f.id_field \n";
        if (!Converter.isEmpty(idTable)) {
            sql += "WHERE t.id_table = :idTable \n";
        }
        sql += "ORDER BY t.name, c.name, tr.id_client DESC";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idLanguage", idLanguage);
        if (!Converter.isEmpty(idTable)) {
            query.setParameter("idTable", idTable);
        }
        Map<String, String> result = new HashMap<>();
        List<Object[]> values = query.getResultList();
        for (Object[] item : values) {
            String keyGlobal = item[0] + "$" + item[1];
            String keyWithId = keyGlobal + "$" + item[2];
            if (!result.containsKey(keyGlobal)) {
                result.put(keyGlobal, (String) item[3]);
            }
            if (!result.containsKey(keyWithId)) {
                result.put(keyWithId, (String) item[3]);
            }
        }
        return result;
    }

    public Map<String, String> listFieldCaptions(String idClient, String idLanguage) {
        return listFieldCaptions(idClient, idLanguage, null);
    }

    /**
     * List all translate fields descriptions
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translations { key: tableName$columnName, value: translation }
     */
    @Override
    public Map<String, String> listFieldDescriptions(String idClient, String idLanguage) {
        String sql =
                "SELECT * FROM (\n" +
                "SELECT t.name AS tableName, c.name AS columnName, COALESCE(tr.translation, c.description) AS description \n" +
                "FROM ad_column c JOIN ad_table t ON t.id_table = c.id_table \n" +
                "LEFT JOIN ad_translation tr ON tr.id_language = :idLanguage \n" +
                "  AND tr.id_table = '" + FWConfig.TABLE_ID_COLUMN + "' AND tr.id_column = '" + FWConfig.COLUMN_ID_COLUMN_DESCRIPTION + "'\n" +
                "  AND tr.id_client IN :idClient AND tr.rowkey = c.id_column \n" +
                "ORDER BY t.name, c.name, tr.id_client DESC) AS T \n" +
                "WHERE description IS NOT NULL";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idLanguage", idLanguage);
        Map<String, String> result = new HashMap<>();
        List<Object[]> values = query.getResultList();
        for (Object[] item : values) {
            String description = (String) item[2];
            String keyGlobal = item[0] + "$" + item[1];
            if (!result.containsKey(keyGlobal)) {
                result.put(keyGlobal, description);
            }
        }
        return result;
    }

    /**
     * List all translation of messages
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idModule Module identifier
     * @param admin Admin messages
     * @param web Web messages
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public List<AdMessageTranslation> listMessages(String idClient, String idLanguage, String idModule, Boolean admin, Boolean web) {
        Query query = getTranslationsQuery(idModule, admin, web);
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idLanguage", idLanguage);
        if (!Converter.isEmpty(idModule)) {
            query.setParameter("idModule", idModule);
        }
        List<AdMessageTranslation> result = new ArrayList<>();
        List<Object[]> values = query.getResultList();
        for (Object[] item : values) {
            result.add(new AdMessageTranslation(item));
        }
        return result;
    }

    /**
     * List all translation of messages
     *
     * @param idClient   Client identifier
     * @param idLanguage Language identifier
     * @param admin Admin messages
     * @param web Web messages
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listMessages(String idClient, String idLanguage, Boolean admin, Boolean web) {
        Query query = getTranslationsQuery(null, admin, web);
        return getTranslations(query, idClient, idLanguage, null);
    }

    /**
     * List all translation of process params
     *
     * @param idClient   Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listProccessParams(String idClient, String idLanguage) {
        Query query = entityManager.createNativeQuery(
                "SELECT pp.name AS value, pp.caption AS name, t.translation \n" +
                "FROM ad_process_param pp LEFT JOIN ad_translation t ON t.id_language = :idLanguage \n" +
                "  AND t.id_table = '" + FWConfig.TABLE_ID_PROCESS_PARAM + "' AND t.id_column = '" + FWConfig.COLUMN_ID_PROCESS_PARAM_CAPTION + "' \n" +
                "  AND t.id_client IN :idClient AND t.rowkey = pp.id_process_param \n" +
                "ORDER BY pp.name, t.id_client DESC"
        );
        return getTranslations(query, idClient, idLanguage, null);
    }

    /**
     * List all translation of chart params
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listChartFilterParams(String idClient, String idLanguage) {
        Query query = entityManager.createNativeQuery(
                "SELECT cf.name AS value, cf.caption AS name, t.translation \n" +
                "FROM ad_chart_filter cf LEFT JOIN ad_translation t ON t.id_language = :idLanguage \n" +
                "  AND t.id_table = '" + FWConfig.TABLE_ID_CHART_FILTER + "' AND t.id_column = '" + FWConfig.COLUMN_ID_CHART_FILTER_CAPTION +"' \n" +
                "  AND t.id_client IN :idClient AND t.rowkey = cf.id_chart_filter \n" +
                "ORDER BY cf.name, t.id_client DESC"
        );
        return getTranslations(query, idClient, idLanguage, null);
    }


    /**
     * List all translation of chart columns
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    @Override
    public Map<String, String> listChartColumns(String idClient, String idLanguage) {
        Query query = entityManager.createNativeQuery(
                "SELECT c.name as chartName, cc.name AS value, cc.caption AS name, t.translation \n" +
                "FROM ad_chart_column cc LEFT JOIN ad_translation t ON t.id_language = :idLanguage \n" +
                "  AND t.id_table = '" + FWConfig.TABLE_ID_CHART_COLUMN + "' AND t.id_column = '" + FWConfig.COLUMN_ID_CHART_COLUMN_CAPTION +"' \n" +
                "  AND t.id_client IN :idClient AND t.rowkey = cc.id_chart_column \n" +
                "  JOIN ad_chart c ON c.id_chart = cc.id_chart  \n" +
                "ORDER BY cc.name, t.id_client DESC"
        );
        query.setParameter("idClient", Arrays.asList(new String[]{"0", idClient}));
        query.setParameter("idLanguage", idLanguage);
        Map<String, String> result = new HashMap<>();
        List<Object[]> values = query.getResultList();
        for (Object[] item : values) {
            String keyGlobal = item[0] + "$" + item[1];
            if (!result.containsKey(keyGlobal)) {
                String translation = (String) item[3];
                result.put(keyGlobal, translation != null ? translation : (String) item[2]);
            }
        }
        return result;
    }

    /**
     * Update / Create a translation
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idModule Module identifier
     * @param idLanguage Language identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param translation Translation
     * @return "true" if added and "false" is updated
     */
    @Override
    public boolean addTranslation(String idClient, String idUser, String idModule, String idLanguage, String idTable, String idColumn, String rowkey, String translation) {
        AdTranslation item = findTranslation(idClient, idTable, idColumn, idLanguage, rowkey);
        boolean result = item == null;
        if (result) {
            item = new AdTranslation();
            item.setIdClient(idClient);
            item.setCreatedBy(idUser);
            item.setActive(true);
            item.setIdLanguage(idLanguage);
            item.setIdTable(idTable);
            item.setIdColumn(idColumn);
            item.setRowkey(rowkey);
        }
        item.setIdModule(idModule);
        item.setTranslation(translation);
        item.setUpdatedBy(idUser);
        if (result)
            save(item);
        else
            update(item);
        return result;
    }

    /**
     * Save translations for a row key
     *
     * @param translations Translation information
     * @param idModule Module identifier
     */
    @Override
    public void saveTranslations(AdTranslations translations, String idModule) {
        List<AdTranslation> dbTranslations = findAll(getFilter(translations.getIdClient(), null, translations.getIdTable(), translations.getIdColumn(), translations.getRowkey(), null, true));
        for (AdTranslationsData data : translations.getTranslations()) {
            AdTranslation dbTranslation = null;
            String translation = data.getTranslation();
            translation = translation != null ? translation.trim() : null;
            if (Converter.isEmpty(translation)) {
                translation = null;
            }
            for (AdTranslation item: dbTranslations) {
                if (item.getIdLanguage().equals(data.getIdLanguage())) {
                    dbTranslation = item;
                    break;
                }
            }
            if (dbTranslation == null) {
                if (translation != null) {
                    dbTranslation = new AdTranslation();
                    dbTranslation.setIdClient(translations.getIdClient());
                    dbTranslation.setIdModule(idModule);
                    dbTranslation.init(translations.getIdUser());
                    dbTranslation.setIdTable(translations.getIdTable());
                    dbTranslation.setIdColumn(translations.getIdColumn());
                    dbTranslation.setRowkey(translations.getRowkey());
                    dbTranslation.setIdLanguage(data.getIdLanguage());
                    dbTranslation.setTranslation(translation);
                    save(dbTranslation);
                }
            } else {
                if (translation == null) {
                    delete(dbTranslation);
                } else {
                    dbTranslation.setTranslation(translation);
                    dbTranslation.setUpdatedBy(translations.getIdUser());
                    update(dbTranslation);
                }
            }
        }
    }

    /**
     * Get translations query
     *
     * @param idModule Module identifier
     * @param admin Admin messages
     * @param web Web messages
     * @return Query
     */
    private Query getTranslationsQuery(String idModule, Boolean admin, Boolean web) {
        String sql =
                "SELECT m.value, m.msgtext, t.translation, coalesce(t.id_client, m.id_client) as id_client, coalesce(t.id_module, m.id_module) as id_module \n" +
                        "FROM ad_message m LEFT JOIN ad_translation t ON t.id_language = :idLanguage \n" +
                        "  AND t.id_table = '" + FWConfig.TABLE_ID_MESSAGE + "' AND t.id_column = '" + FWConfig.COLUMN_ID_MESSAGE_MSGTEXT + "' \n" +
                        "  AND t.rowkey = m.id_message AND t.id_client IN :idClient \n";
        if (admin != null || web != null || idModule != null) {
            sql += "WHERE ";
            if (idModule != null) {
                sql += "m.id_module = :idModule AND ";
            }
            if (admin != null) {
                sql += "m.admin = " + (admin ? 1 : 0) + " AND ";
            }
            if (web != null) {
                sql += "m.web = " + (web ? 1 : 0) + " AND ";
            }
            sql = sql.substring(0, sql.length() - 5) + "\n";
        }
        sql += "ORDER BY m.value, t.id_client DESC";

        return entityManager.createNativeQuery(sql);
    }

    /**
     * Get translations
     *
     * @param query Query
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idModule Module identifier
     * @return Translations
     */
    private Map<String, String> getTranslations(Query query, String idClient, String idLanguage, String idModule) {
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idLanguage", idLanguage);
        if (!Converter.isEmpty(idModule)) {
            query.setParameter("idModule", idModule);
        }
        Map<String, String> result = new HashMap<>();
        List<Object[]> values = query.getResultList();
        for (Object[] item : values) {
            String key = (String) item[0];
            if (!result.containsKey(key)) {
                String translation = (String) item[2];
                result.put(key, translation != null ? translation : (String) item[1]);
            }
        }
        return result;
    }

    /**
     * Get translation filter
     *
     * @param idClient Client identifier
     * @param idModule Module identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param idLanguage Language identifier
     * @param clientStrict Strict client filter (not include *)
     * @return Filter
     */
    private Map getFilter(String idClient, String idModule, String idTable, String idColumn, String rowkey, String idLanguage, boolean clientStrict) {
        Map filter = new HashMap();
        filter.put("idClient", clientStrict ? idClient : getClientFilter(idClient));
        filter.put("idTable", idTable);
        filter.put("idColumn", idColumn);
        filter.put("language.active", true);
        filter.put("active", true);
        if (!Converter.isEmpty(rowkey)) {
            filter.put("rowkey", rowkey);
        }
        if (!Converter.isEmpty(idLanguage)) {
            filter.put("idLanguage", idLanguage);
        }
        if (!Converter.isEmpty(idModule)) {
            filter.put("idModule", idModule);
        }
        return filter;
    }

}
