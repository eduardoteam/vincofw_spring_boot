package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdRole;

/**
 * Created by Devtools.
 * Interface of service of AD_ROLE
 *
 * Date: 19/02/2015
 */
public interface AdRoleService extends BaseService<AdRole, String> {

}


