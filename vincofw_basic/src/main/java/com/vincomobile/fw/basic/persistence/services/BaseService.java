package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.cache.CacheTable;
import com.vincomobile.fw.basic.persistence.model.EntityBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import javax.persistence.Query;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface BaseService<T extends EntityBean, PK extends Serializable> {

    String FILTER_GROUPEDSEARCH     = "search";
    String TABLE_ENTITY_ALIAS       = "_entity_";
    String QUERY_BETWEEN_OP = "~";
    String QUERY_LIKE_OP = "%";

    int PAGE_SIZE_1000              = 1000;

    /**
     * Get generic type
     *
     * @return Type
     */
    Class<T> getType();

    /**
     * Get table name for service
     *
     * @return Table name
     */
    String getTypeTableName();

    /**
     * Search by Id.
     *
     * @param id Bean ID
     * @return Bean
     */
    T findById(PK id);

    /**
     * Search by reference
     *
     * @param field Field name
     * @param idClient Client identifier
     * @param value Value
     * @return Bean
     */
    T findByField(String field, String idClient, String value);
    T findByField(String field, String value);

    /**
     * Search first item by filter
     *
     * @param filter filter
     * @return First item found
     */
    T findFirst(Map filter);

    /**
     * Search first item by filter
     *
     * @param filter Filter
     * @param sort   Sort field
     * @param dir    Sort direction
     * @return First item found
     */
    T findFirst(Map filter, String sort, String dir);

    /**
     * Find all beans
     *
     * @param offset      Start index
     * @param limit       List size
     * @param sort        Sorting fields
     * @param filter      Filter to apply
     * @param extraQuery  Extra query
     * @param extraFilter Extra filter
     * @return Bean list
     */
    List<T> findAll(long offset, int limit, SqlSort sort, Map filter, String extraQuery, Map extraFilter);

    List<T> findAll(long offset, int limit, SqlSort sort, Map filter);

    List<T> findAll(SqlSort sort, Map filter);

    List<T> findAll(SqlSort sort);

    List<T> findAll(long offset, int limit);

    List<T> findAll(Map filter);

    List<T> findAll();

    /**
     * Paged list of sorted beans.
     *
     * @param request     Paging request
     * @param extraQuery  Extra query
     * @param extraFilter Extra filter
     * @return List of beans
     */
    Page<T> findAll(PageSearch request, String extraQuery, Map extraFilter);

    Page<T> findAll(PageSearch request, boolean useNamedQuery);
    Page<T> findAll(PageSearch request);

    /**
     * Updates the bean
     *
     * @param bean Entity
     */
    void update(T bean);

    /**
     * Deletes the bean
     *
     * @param bean Entity
     */
    void delete(T bean);

    /**
     * Deletes the bean
     *
     * @param id Bean ID
     */
    void delete(PK id);

    /**
     * Deletes all beans that comply with filter
     *
     * @param filter Filter
     */
    void deleteAll(Map filter);

    /**
     * Saves the bean
     *
     * @param bean Entity
     */
    PK save(T bean);

    /**
     * Creates the bean
     *
     * @param bean Entity
     */
    T create(T bean);


    /**
     * Saves the bean (if exists, otherwise it creates it)
     *
     * @param bean Entity
     */
    PK merge(T bean);

    /**
     * Clears the cache
     */
    void flush();


    /**
     * Counts the rows
     *
     * @param filter Filter to apply
     * @return Total elements
     */
    long count(Map filter);

    /**
     * Performs a sum of the fields according to a filter
     *
     * @param filter Filter
     * @param field  Field to analyze
     * @return Sum result
     */
    double sum(String field, Map filter);

    /**
     * Performs a sum of the fields according to a filter
     *
     * @param filter      Filter
     * @param field       Field to analyze
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Sum result
     */
    long sum(String field, Map filter, String extraWhere, Map extraFilter);

    /**
     * Counts the rows
     *
     * @param filter      Filter to apply
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Total elements
     */
    long count(Map filter, String extraWhere, Map extraFilter);


    /**
     * Executes a custom query
     *
     * @param customQuery SELECT section of the query
     * @param filter      Filter
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Query result
     */
    List customQuery(String customQuery, Map filter, String extraWhere, Map extraFilter);

    /**
     * Executes a custom query
     *
     * @param customQuery Custom query
     * @param filter      Filter
     * @param offset      List start
     * @param limit       List size
     * @return Query result
     */
    List query(String customQuery, Map filter, int offset, int limit);
    List query(String customQuery, Map filter, int limit);
    List query(String customQuery, Map filter);
    List query(String customQuery);

    /**
     * Executes a custom native query
     *
     * @param customQuery SQL to execute
     * @param filter      Filter
     * @param offset      First element
     * @param limit       List size
     * @return Rows
     */
    List nativeQuery(String customQuery, Map filter, int offset, int limit);
    List nativeQuery(String customQuery, Map filter, int limit);
    List nativeQuery(String customQuery, Map filter);
    List nativeQuery(String customQuery);

    /**
     *  Get the result and column names of the executed query
     *
     * @param customQuery SQL to execute
     * @param limit       List size
     * @return Rows
     */
    List<Map<String, Object>> executeNativeSelectSql(String customQuery, int limit) throws SQLException;

    /**
     * Create a custom query
     *
     * @param customQuery Update sentence
     * @return Query
     */
    Query getUpdateQuery(String customQuery);
    Query getNativeUpdateQuery(String customQuery);

    /**
     * Executes a UPDATE
     *
     * @param params Parameters for update
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    int applyUpdate(String customQuery, Map params);

    /**
     * Executes a UPDATE
     *
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    int applyUpdate(String customQuery);

    /**
     * Executes a native UPDATE
     *
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    int applyNativeUpdate(String customQuery);

    /**
     * Executes a UPDATE
     *
     * @param params Parameters for update
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    int applyNativeUpdate(String customQuery, Map params);

    /**
     * Gets a single item page
     *
     * @param bean Item
     * @return Page with item
     */
    Page<T> getItem(T bean);

    /**
     * Gets the sort information
     *
     * @param request Page request
     * @return Sort information to add to the ORDER BY clause
     */
    SqlSort getSort(PageSearch request);

    SqlSort getSort(Sort sort);

    SqlSort getSort(String sort, String dir);

    /**
     * Obtains the filter conditions
     *
     * @param filters Filters
     * @return Filter condition (WHERE)
     */
    String buildWhere(Map filters);

    /**
     * Gets the associated runtime type used in generics
     *
     * @return Associated type class
     */
    Class getAssociatedType();

    /**
     * Replace the parameters in the SQL
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     * @return Values replaced
     */
    String replaceParameters(String idClient, String value, String param_1, String param_2, String param_3, String param_4, String param_5);

    /**
     * Get table information from cache. If not found info is loaded
     *
     * @param tableName Table name
     * @return Cache information
     */
    CacheTable getCacheTable(String tableName);
    CacheTable getCacheTable();

    /**
     * Get table information from cache. If not found info is loaded
     *
     * @param idTable Table identifier
     * @return Cache information
     */
    CacheTable getCacheTableById(String idTable);

    /**
     * Get search criteria for table
     *
     * @param filters Search criteria
     * @param key Key field
     * @return Search criteria
     */
    String getSearchCriteria(Map filters, String key);

    /**
     * Get client filter
     *
     * @param idClient Client identifier
     * @return ['0', idClient]
     */
    List<String> getClientFilter(String idClient);

}
