package com.vincomobile.fw.basic.tools.plugins;

public class CodegenFiles {
    private String path;
    private String template;
    private String property;

    public CodegenFiles(String path, String template, String property) {
        this.path = path;
        this.template = template;
        this.property = property;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * Create code generator manager and add standard generation files
     *
     * @param mode Generation mode
     * @param genType Generation type
     * @return CodegenManager
     */
    public static CodegenManager getCodegenManager(String mode, String genType) {
        CodegenManager codegenManager = new CodegenManager();
        if (CodeGenerator.MODE_JAVA.equals(mode)) {
            String templateDir = "vinco_core/" + (CodeGenerator.GEN_TYPE_STANDARD.equals(genType) ? "spring" : "spring_boot");
            // Controller
            codegenManager.addFile(new CodegenFiles(
                    "src/main/java/@package/rest/web/controllers/@CapitalizeStandardNameController.java",
                    templateDir + "/controller.java",
                    "controller"
            ));
            // Model
            codegenManager.addFile(new CodegenFiles(
                    "src/main/java/@package/persistence/model/@CapitalizeStandardName.java",
                    templateDir + "/model.java",
                    "model"
            ));
            if (CodeGenerator.GEN_TYPE_STANDARD.equals(genType)) {
                codegenManager.addFile(new CodegenFiles(
                        "src/main/java/@package/persistence/model/@CapitalizeStandardNamePK.java",
                        "vinco_core/spring/modelPK.java",
                        "modelPK"
                ));
            }
            // Services
            codegenManager.addFile(new CodegenFiles(
                    "src/main/java/@package/persistence/services/@CapitalizeStandardNameService.java",
                    "vinco_core/spring/service_inter.java",
                    "serv_inter"
            ));
            codegenManager.addFile(new CodegenFiles(
                    "src/main/java/@package/persistence/services/@CapitalizeStandardNameServiceImpl.java",
                    "vinco_core/spring/service_impl.java",
                    "serv_impl"
            ));
            if (CodeGenerator.GEN_TYPE_SPRING_BOOT.equals(genType)) {
                // Business
                codegenManager.addFile(new CodegenFiles(
                        "src/main/java/@package/persistence/business/@CapitalizeStandardNameBusiness.java",
                        "vinco_core/spring_boot/business_inter.java",
                        "business_inter"
                ));
                codegenManager.addFile(new CodegenFiles(
                        "src/main/java/@package/persistence/business/@CapitalizeStandardNameBusinessImpl.java",
                        "vinco_core/spring_boot/business_impl.java",
                        "business_impl"
                ));
                // DTO
                codegenManager.addFile(new CodegenFiles(
                        "src/main/java/@package/rest/web/dto/@CapitalizeStandardNameDTO.java",
                        "vinco_core/spring_boot/dto.java",
                        "dto"
                ));
                codegenManager.addFile(new CodegenFiles(
                        "src/main/java/@package/rest/web/mapper/@CapitalizeStandardNameMapper.java",
                        "vinco_core/spring_boot/mapper.java",
                        "mapper"
                ));
            }
        } else if (CodeGenerator.MODE_SWIFT.equals(mode)) {
            codegenManager.addFile(new CodegenFiles(
                    "Model/Bean/DB/@CapitalizeStandardName.swift",
                    "vinco_core/swift/bean.swift",
                    "bean"
            ));
            codegenManager.addFile(new CodegenFiles(
                    "Model/Bean/Rest/@CapitalizeStandardName.swift",
                    "vinco_core/swift/rest.swift",
                    "rest"
            ));
        }
        return codegenManager;
    }
}
