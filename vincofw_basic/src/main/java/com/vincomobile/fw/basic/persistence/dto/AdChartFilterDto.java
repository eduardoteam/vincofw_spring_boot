package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdChartFilter;

public class AdChartFilterDto {
    private String idChartFilter;
    private String caption;
    private String name;
    private String description;
    private String displaylogic;
    private Boolean displayed;
    private Boolean mandatory;
    private Boolean ranged;
    private String saveType;
    private Long seqno;
    private String valuedefault;
    private String valuedefault2;
    private String valuemax;
    private String valuemin;

    public AdChartFilterDto(AdChartFilter item) {
        this.idChartFilter = item.getIdChartFilter();
        this.caption = item.getCaption();
        this.name = item.getName();
        this.description = item.getDescription();
        this.displaylogic = item.getDisplaylogic();
        this.displayed = item.getDisplayed();
        this.mandatory = item.getMandatory();
        this.ranged = item.getRanged();
        this.saveType = item.getSaveType();
        this.seqno = item.getSeqno();
        this.valuedefault = item.getValuedefault();
        this.valuedefault2 = item.getValuedefault2();
        this.valuemax = item.getValuemax();
        this.valuemin = item.getValuemin();
    }

    public String getIdChartFilter() {
        return idChartFilter;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public Boolean getRanged() {
        return ranged;
    }

    public String getSaveType() {
        return saveType;
    }

    public Long getSeqno() {
        return seqno;
    }

    public String getValuedefault() {
        return valuedefault;
    }

    public String getValuedefault2() {
        return valuedefault2;
    }

    public String getValuemax() {
        return valuemax;
    }

    public String getValuemin() {
        return valuemin;
    }
}
