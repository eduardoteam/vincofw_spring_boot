package com.vincomobile.fw.basic.tools;

public class ImageInfo {
    Long width;
    Long height;
    boolean proportionOk;
    boolean error;
    String msg;

    public ImageInfo() {
        proportionOk = true;
        error = false;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public boolean isProportionOk() {
        return proportionOk;
    }

    public void setProportionOk(boolean proportionOk) {
        this.proportionOk = proportionOk;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void checkProportion(Double proportion) {
        proportionOk = width == height * proportion;
    }
}
