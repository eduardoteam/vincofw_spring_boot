package com.vincomobile.fw.basic.rest.web.controllers;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.persistence.business.BaseBusiness;
import com.vincomobile.fw.basic.persistence.dto.EntityDTO;
import com.vincomobile.fw.basic.persistence.model.EntityBean;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.rest.web.mapper.BaseMapper;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public abstract class BaseBusinessController<T extends EntityBean, PK extends Serializable, DTO extends EntityDTO<PK>, M extends BaseMapper<T, DTO>> extends BaseController<T, PK> {

    /**
     * Get main service for controller
     *
     * @return Service
     */
    public abstract BaseBusiness getBusiness();

    /**
     * Get main service for controller
     *
     * @return Service
     */
    @Override
    public BaseService getService() {
        return getBusiness().getService();
    }

    /**
     * Get main mapper for Bean to DTO
     *
     * @return Mapper
     */
    public abstract M getMapper();

    /**
     * Apply update to entity
     *
     * @param entity Entity bean
     */
    protected void applyUpdateEntity(EntityBean entity) {
        getBusiness().update(entity);
    }

    /**
     * Update entity and check errors
     *
     * @param dto Entity DTO
     */
    protected void updateEntity(DTO dto) {
        RestPreconditions.checkRequestElementNotNull(dto);
        T entity = getMapper().toBean(dto);
        updateEntity(entity);
    }

    /**
     * Update entity and check errors
     *
     * @param dto Entity DTO
     * @param passwordColumn Name for password column
     */
    protected void updateEntityWithPassword(DTO dto, String passwordColumn) {
        RestPreconditions.checkRequestElementNotNull(dto);
        T entity = getMapper().toBean(dto);
        updateEntityWithPassword(entity, passwordColumn);
    }

    /**
     * Create entity and check errors
     *
     * @param dto Entity bean
     * @return PrimaryKey
     */
    protected PK createEntity(DTO dto) {
        RestPreconditions.checkRequestElementNotNull(dto);
        T entity = getMapper().toBean(dto);
        return createEntity(entity);
    }

    /**
     * Delete entity and check errors
     *
     * @param id Bean primary key
     */
    @Override
    protected void deleteEntity(PK id) {
        EntityBean entity = getBusiness().findById(id);
        RestPreconditions.checkNotNull(entity);
        super.deleteEntity(id);
    }

    /**
     * Deletes selected items
     *
     * @param idClient Client identifier
     * @param ids Items IDs to delete
     */
    @Override
    protected void deleteItems(String idClient, String[] ids) {
        for (String id: ids) {
            EntityBean entity = getBusiness().findById(id);
            deleteEntity(entity);
        }
        HookManager.executeHook(FWConfig.HOOK_TABLE_DELETE_BATCH, idClient, getAuthUser(), getBusiness().getService(), ids);
    }

}
