package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.model.AdClient;
import com.vincomobile.fw.basic.tools.Mailer;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_CLIENT
 *
 * Date: 19/02/2015
 */
public interface AdClientService extends BaseService<AdClient, String> {

    /**
     * Change mailer configuration
     *
     * @param idClient Client identifier
     * @param mailer Mailer
     * @param customSender Standard mail sender
     */
    void setMailerConfig(String idClient, Mailer mailer, JavaMailSenderImpl customSender);

    /**
     * Get all active clients (exclude client: *)
     *
     * @return Client list
     */
    List<AdClient> getActiveClients();

    /**
     * Get client GUI
     *
     * @param idClient Client identifier
     * @return GUI
     */
    AdGUIDto getClientGUI(String idClient);

}


