package com.vincomobile.fw.basic.persistence.business;

import com.vincomobile.fw.basic.persistence.model.EntityBean;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import org.springframework.data.domain.Page;

import java.io.Serializable;

public abstract class BaseBusinessImpl<T extends EntityBean<PK>, PK extends Serializable, S extends BaseService<T,PK>> implements BaseBusiness<T, PK, S> {

    protected final S service;

    public BaseBusinessImpl(S service) {
        this.service = service;
    }

    /**
     * Search by Id.
     *
     * @param id Bean ID
     * @return Bean
     */
    @Override
    public T findById(PK id) {
        return service.findById(id);
    }

    /**
     * Paged list of sorted beans.
     *
     * @param pageReq     Paging request
     * @return Page of beans
     */
    @Override
    public Page<T> findAll(PageSearch pageReq) {
        return service.findAll(pageReq);
    }

    /**
     * Saves the bean
     *
     * @param bean Entity
     */
    @Override
    public PK save(T bean) {
        return service.save(bean);
    }

    /**
     * Creates the bean
     *
     * @param bean Entity
     */
    @Override
    public T create(T bean) {
        return service.create(bean);
    }

    /**
     * Updates the bean
     *
     * @param bean Entity
     */
    @Override
    public void update(T bean) {
        service.update(bean);
    }

    /**
     * Deletes the bean
     *
     * @param bean Entity
     */
    @Override
    public void delete(T bean) {
        service.delete(bean);
    }

    /**
     * Deletes the bean
     *
     * @param id Bean ID
     */
    @Override
    public void delete(PK id) {
        service.delete(id);
    }

    public S getService() {
        return service;
    }
}
