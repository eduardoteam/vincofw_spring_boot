package com.vincomobile.fw.basic.persistence.beans;

/**
 * Created by eduardo on 22/10/17.
 */
public class AdPreferenceValue {

    String property;
    String value;

    public AdPreferenceValue(String property, String value) {
        this.property = property;
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public String getValue() {
        return value;
    }
}
