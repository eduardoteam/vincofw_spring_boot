package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 16/02/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_table_action
 */
@Entity
@Table(name = "ad_table_action")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdTableAction extends EntityBean<String> {

    private String idTableAction;
    private String atype;
    private String displaylogic;
    private String idClient;
    private String idModule;
    private String idProcess;
    private String idTable;
    private String idWindow;
    private String name;
    private String restCommand;
    private String url;
    private String target;
    private String icon;
    private String uipattern;
    private Long seqno;
    private String privilege;
    private String privilegeDesc;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idTableAction;
    }

    @Override
    public void setId(String id) {
            this.idTableAction = id;
    }

    @Id
    @Column(name = "id_table_action")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdTableAction() {
        return idTableAction;
    }

    public void setIdTableAction(String idTableAction) {
        this.idTableAction = idTableAction;
    }

    @Column(name = "atype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getAtype() {
        return atype;
    }

    public void setAtype(String atype) {
        this.atype = atype;
    }

    @Column(name = "displaylogic", length = 250)
    @Size(min = 1, max = 250)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "id_client", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_module", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    @Column(name = "id_process", length = 32)
    @Size(min = 1, max = 32)
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "id_table", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_window", length = 32)
    @Size(min = 1, max = 32)
    public String getIdWindow() {
        return idWindow;
    }

    public void setIdWindow(String idWindow) {
        this.idWindow = idWindow;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "rest_command", length = 150)
    @Size(min = 1, max = 150)
    public String getRestCommand() {
        return restCommand;
    }

    public void setRestCommand(String restCommand) {
        this.restCommand = restCommand;
    }

    @Column(name = "url", length = 500)
    @Size(min = 1, max = 500)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "target", length = 50)
    @Size(min = 1, max = 50)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Column(name = "icon", length = 45)
    @NotNull
    @Size(min = 1, max = 45)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "uipattern", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getUipattern() {
        return uipattern;
    }

    public void setUipattern(String uipattern) {
        this.uipattern = uipattern;
    }

    @Column(name = "seqno")
    @NotNull
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "privilege", length = 50)
    @Size(min = 1, max = 50)
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Column(name = "privilege_desc")
    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdTableAction)) return false;

        final AdTableAction that = (AdTableAction) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idTableAction == null) && (that.idTableAction == null)) || (idTableAction != null && idTableAction.equals(that.idTableAction)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idWindow == null) && (that.idWindow == null)) || (idWindow != null && idWindow.equals(that.idWindow)));
        result = result && (((atype == null) && (that.atype == null)) || (atype != null && atype.equals(that.atype)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((restCommand == null) && (that.restCommand == null)) || (restCommand != null && restCommand.equals(that.restCommand)));
        result = result && (((url == null) && (that.url == null)) || (url != null && url.equals(that.url)));
        result = result && (((target == null) && (that.target == null)) || (target != null && target.equals(that.target)));
        result = result && (((icon == null) && (that.icon == null)) || (icon != null && icon.equals(that.icon)));
        result = result && (((uipattern == null) && (that.uipattern == null)) || (uipattern != null && uipattern.equals(that.uipattern)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((privilege == null) && (that.privilege == null)) || (privilege != null && privilege.equals(that.privilege)));
        result = result && (((privilegeDesc == null) && (that.privilegeDesc == null)) || (privilegeDesc != null && privilegeDesc.equals(that.privilegeDesc)));
        return result;
    }

}

