package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.model.AdMessage;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_MESSAGE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdMessageServiceImpl extends BaseServiceImpl<AdMessage, String> implements AdMessageService {

    private final AdTranslationService translationService;

    /**
     * Constructor.
     *
     */
    public AdMessageServiceImpl(AdTranslationService translationService) {
        super(AdMessage.class);
        this.translationService = translationService;
    }

    /**
     * Get translate message
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @param value Message search key
     * @param params Message parameters
     * @return Translate message
     */
    @Override
    public String getMessage(String idClient, String idLanguage, String value, Object... params) {
        Map filter = new HashMap();
        if (idClient != null)
            filter.put("idClient", getClientFilter(idClient));
        filter.put("value", value);
        AdMessage msg = findFirst(filter, "idClient", "DESC");
        if (msg != null) {
            String result = Converter.isEmpty(idLanguage) ?
                    msg.getMsgtext() :
                    translationService.getTranslation(idClient, FWConfig.TABLE_ID_MESSAGE, FWConfig.COLUMN_ID_MESSAGE_MSGTEXT, idLanguage, msg.getIdMessage(), msg.getMsgtext());
            return String.format(result, params);
        } else {
            return value;
        }
    }

    @Override
    public String getMessage(String idLanguage, String value) {
        return getMessage(null, idLanguage, value);
    }

    @Override
    public String getMessage(String value) {
        return getMessage(null, null, value);
    }

    /**
     * Get translate message with default value
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @param value Message search key
     * @param defValue Default value
     * @param params Message parameters
     * @return Translate message
     */
    @Override
    public String getMessageDefault(String idClient, String idLanguage, String value, String defValue, Object... params) {
        String msg = getMessage(idClient, idLanguage, value, defValue, params);
        return msg.equals(value) ? defValue : msg;
    }

    @Override
    public String getMessageDefault(String idLanguage, String value, String defValue) {
        return getMessageDefault(null, idLanguage, value, defValue);
    }

    @Override
    public String getMessageDefault(String value, String defValue) {
        return getMessageDefault(null, null, value, defValue);
    }

    /**
     * Get message
     *
     * @param idClient Client identifier
     * @param value Message search key
     * @return AdMessage
     */
    @Override
    public AdMessage findMessage(String idClient, String value) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("value", value);
        return findFirst(filter);
    }

}
