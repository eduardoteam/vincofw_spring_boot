package com.vincomobile.fw.basic.tools.plugins;

public class CodeItem {
    private String key;
    private String code;
    private CodegenFiles codegenFile;

    public CodeItem(String key, String code, CodegenFiles codegenFile) {
        this.key = key;
        this.code = code;
        this.codegenFile = codegenFile;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CodegenFiles getCodegenFile() {
        return codegenFile;
    }

    public void setCodegenFile(CodegenFiles codegenFile) {
        this.codegenFile = codegenFile;
    }
}
