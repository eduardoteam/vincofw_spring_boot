package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdApproval;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Vincomobile FW on 23/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_approval
 */
@Repository
@Transactional(readOnly = true)
public class AdApprovalServiceImpl extends BaseServiceImpl<AdApproval, String> implements AdApprovalService {

    /**
     * Constructor.
     */
    public AdApprovalServiceImpl() {
        super(AdApproval.class);
    }

    /**
     * Save approvals
     *
     * @param user Login user
     * @param approvals Approvals list
     */
    @Override
    public void saveApprovals(AdUser user, List<AdApproval> approvals) {
        if (approvals != null) {
            for (AdApproval approval : approvals) {
                AdApproval item = findById(approval.getIdApproval());
                boolean isNew = item == null;
                if (isNew) {
                    item = new AdApproval();
                    item.setIdApproval(approval.getIdApproval());
                    item.setCreatedBy(user != null ? user.getIdUser() : null);
                }
                item.setIdClient(approval.getIdClient());
                item.setUpdatedBy(user != null ? user.getIdUser() : null);
                item.setAction(approval.getAction());
                item.setPrivilege(approval.getPrivilege());
                item.setIdTable(approval.getIdTable());
                item.setIdProcess(approval.getIdProcess());
                item.setIdTableAction(approval.getIdTableAction());
                item.setRowkey(approval.getRowkey());
                item.setIdUser(approval.getIdUser());
                item.setActive(true);
                if (isNew) {
                    save(item);
                } else {
                    update(item);
                }
            }
        }
    }

}
