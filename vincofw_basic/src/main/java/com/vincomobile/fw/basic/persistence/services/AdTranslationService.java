package com.vincomobile.fw.basic.persistence.services;


import com.vincomobile.fw.basic.persistence.beans.AdMessageTranslation;
import com.vincomobile.fw.basic.persistence.beans.AdTranslations;
import com.vincomobile.fw.basic.persistence.model.AdTranslation;

import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Interface of service of AD_TRANSLATION
 *
 * Date: 19/02/2015
 */
public interface AdTranslationService extends BaseService<AdTranslation, String> {

    /**
     * Get a translation item
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param idLanguage Language identifier
     * @param rowkey Row key
     * @return Translation
     */
    AdTranslation findTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey);

    /**
     * Get all item translations
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param idLanguage Language identifier
     * @param idModule Module identifier
     * @return Translations
     */
    List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey, String idLanguage, String idModule);
    List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey, String idLanguage);
    List<AdTranslation> getTranslations(String idClient, String idTable, String idColumn, String rowkey);

    /**
     * Get translation of a field in a language
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param idLanguage Language identifier
     * @param rowkey Row key
     * @param defValue Default value
     * @return Translation
     */
    String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey, String defValue);
    String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, Long rowkey, String defValue);
    String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, String rowkey);
    String getTranslation(String idClient, String idTable, String idColumn, String idLanguage, Long rowkey);

    /**
     * Delete all rows for column translation
     *
     * @param idClient Client identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param idLanguage Language identifier
     */
    void deleteTranslation(String idClient, String idTable, String idColumn, String rowkey, String idLanguage);
    void deleteTranslation(String idClient, String idTable, String idColumn, String rowkey);
    void deleteTranslation(String idClient, String idTable, String idColumn, Long rowkey);

    /**
     * Remove files of a translation item
     *
     * @param item Translation
     */
    void deleteTranslationFile(AdTranslation item);

    /**
     * List all translate fields caption
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idTable Table identifier
     * @return List of translation { key: tableName$columnName, value: translation }
     */
    Map<String, String> listFieldCaptions(String idClient, String idLanguage, String idTable);
    Map<String, String> listFieldCaptions(String idClient, String idLanguage);

    /**
     * List all translate fields descriptions
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translations { key: tableName$columnName, value: translation }
     */
    Map<String, String> listFieldDescriptions(String idClient, String idLanguage);

    /**
     * List all translation of messages
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idModule Module identifier
     * @param admin Admin messages
     * @param web Web messages
     * @return List of translation { key: value, value: translation }
     */
    List<AdMessageTranslation> listMessages(String idClient, String idLanguage, String idModule, Boolean admin, Boolean web);

    /**
     * List all translation of messages
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param admin Admin messages
     * @param web Web messages
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listMessages(String idClient, String idLanguage, Boolean admin, Boolean web);

    /**
     * List all translation of process params
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listProccessParams(String idClient, String idLanguage);

    /**
     * List all translation of chart params
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listChartFilterParams(String idClient, String idLanguage);

    /**
     * List all translation of chart columns
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @return List of translation { key: value, value: translation }
     */
    Map<String, String> listChartColumns(String idClient, String idLanguage);

    /**
     * Update / Create a translation
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idModule Module identifier
     * @param idLanguage Language identifier
     * @param idTable Table identifier
     * @param idColumn Column identifier
     * @param rowkey Row key
     * @param translation Translation
     * @return "true" if added and "false" is updated
     */
    boolean addTranslation(String idClient, String idUser, String idModule, String idLanguage, String idTable, String idColumn, String rowkey, String translation);

    /**
     * Save translations for a row key
     *
     * @param translations Translation information
     * @param idModule Module identifier
     */
    void saveTranslations(AdTranslations translations, String idModule);
}


