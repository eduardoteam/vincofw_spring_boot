package com.vincomobile.fw.basic.tools;

public class ImageResizeItem {

    int width;
    String prefix;

    public ImageResizeItem(int width, String prefix) {
        this.width = width;
        this.prefix = prefix;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
