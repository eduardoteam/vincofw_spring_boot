package com.vincomobile.fw.basic.tools.plugins;

import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CodeGenerator {

    String GEN_TYPE_STANDARD    = "STANDARD";
    String GEN_TYPE_SPRING_BOOT = "SPRING_BOOT";

    String MODE_JAVA            = "JAVA";
    String MODE_SWIFT           = "SWIFT";

    /**
     * Code generation
     *
     * @param mode Generation mode
     * @param genType Generation type
     * @param options Opciones de generacion
     * @return Lista con el código generado
     */
    List<CodeItem> generate(String mode, String genType, Map options) throws IOException, TemplateException;

    /**
     * Adiciona opciones especificas del generador
     *
     * @param id_tipo Tipo de generacion
     * @param options Opciones de generacion
     */
    void addOptions(String id_tipo, Map options);

}
