package com.vincomobile.fw.basic.rest.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by tomascouso on 19/08/14.
 */
public interface AuthenticationFacade {
    Authentication getAuthentication();
    UserDetails getMyUser();
}
