package com.vincomobile.fw.basic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yokiro on 09/11/2015.
 */
public class JSONColumn{

    private String tableName;
    private List<JSONColumnStructure> columns;

    public JSONColumn(){
        this.columns = new ArrayList<>();
    }

    public JSONColumn(String tableName){
        this.tableName = tableName;
        this.columns = new ArrayList<>();
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<JSONColumnStructure> getColumns() {
        return columns;
    }

    public void setColumns(List<JSONColumnStructure> columns) {
        this.columns = columns;
    }

    public void addColumnStructure(JSONColumnStructure columnStructure){
        this.columns.add(columnStructure);
    }

    public JSONColumnStructure getColumn(String name) {
        for (JSONColumnStructure column : columns) {
            if (column.getName().equals(name));
            return column;
        }
        return null;
    }

}
