package com.vincomobile.fw.basic.persistence.exception;

import com.vincomobile.fw.basic.tools.ProcessPlanningErrorCode;

/**
 * Exception to be thrown when a planning error occurs
 */
public class FWProcessPlanningException extends Exception {

    public static final int UNKNOWN_REASON = 0;
    public static final int PROCESS_WILL_NEVER_FIRE = 1;

    private String reason = "";
    private ProcessPlanningErrorCode reasonCode = ProcessPlanningErrorCode.UNKNOWN_REASON;

    /**
     * Constructor
     *
     * @param reason Fail reason
     */
    public FWProcessPlanningException(String reason, ProcessPlanningErrorCode reasonCode) {
        this.reason = reason;
        this.reasonCode = reasonCode;
    }

    /**
     * Gets the Fail reason
     *
     * @return Fail reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the fail reason
     *
     * @param reason Fail reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Reason code
     *
     * @return Returns the reason code
     */
    public ProcessPlanningErrorCode getReasonCode() {
        return reasonCode;
    }

    /**
     * Gets the reason code
     *
     * @param reasonCode Reason code
     */
    public void setReasonCode(ProcessPlanningErrorCode reasonCode) {
        this.reasonCode = reasonCode;
    }
}
