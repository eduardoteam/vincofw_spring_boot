package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Devtools.
 * Modelo de ad_process_param
 *
 * Date: 06/11/2015
 */
@Entity
@Table(name = "ad_process_param")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdProcessParam extends AdEntityBean implements Serializable {

    private String idProcessParam;
    private String idProcess;
    private String name;
    private String description;
    private String idReference;
    private String idReferenceValue;
    private String valuemin;
    private String valuemax;
    private String valuedefault;
    private Boolean mandatory;
    private Boolean ranged;
    private Boolean displayed;
    private String saveType;
    private String ptype;
    private String caption;
    private String displaylogic;
    private Long seqno;
    private Long span;

    private Object value;
    private MultipartFile file;
    private AdReference reference;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcessParam;
    }

    @Override
    public void setId(String id) {
            this.idProcessParam = id;
    }

    @Id
    @Column(name = "id_process_param")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdProcessParam() {
        return idProcessParam;
    }

    public void setIdProcessParam(String idProcessParam) {
        this.idProcessParam = idProcessParam;
    }

    @Column(name = "id_process")
    @NotNull
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_reference_value")
    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    @Column(name = "valuemin", length = 60)
    @Size(min = 1, max = 60)
    public String getValuemin() {
        return valuemin;
    }

    public void setValuemin(String valuemin) {
        this.valuemin = valuemin;
    }

    @Column(name = "valuemax", length = 60)
    @Size(min = 1, max = 60)
    public String getValuemax() {
        return valuemax;
    }

    public void setValuemax(String valuemax) {
        this.valuemax = valuemax;
    }

    @Column(name = "valuedefault", length = 60)
    @Size(min = 1, max = 60)
    public String getValuedefault() {
        return valuedefault;
    }

    public void setValuedefault(String valuedefault) {
        this.valuedefault = valuedefault;
    }

    @Column(name = "mandatory")
    @NotNull
    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Column(name = "ranged")
    @NotNull
    public Boolean getRanged() {
        return ranged;
    }

    public void setRanged(Boolean range) {
        this.ranged = range;
    }

    @Column(name = "displayed")
    @NotNull
    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    @Column(name = "save_type", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getSaveType() {
        return saveType;
    }

    public void setSaveType(String saveType) {
        this.saveType = saveType;
    }

    @Column(name = "ptype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    @Column(name = "caption", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Column(name = "displaylogic", length = 250)
    @Size(min = 1, max = 250)
    public String getDisplaylogic() {
        return displaylogic;
    }

    public void setDisplaylogic(String displaylogic) {
        this.displaylogic = displaylogic;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "span")
    @NotNull
    public Long getSpan() {
        return span;
    }

    public void setSpan(Long span) {
        this.span = span;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdProcessParam)) return false;

        final AdProcessParam that = (AdProcessParam) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcessParam == null) && (that.idProcessParam == null)) || (idProcessParam != null && idProcessParam.equals(that.idProcessParam)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idReferenceValue == null) && (that.idReferenceValue == null)) || (idReferenceValue != null && idReferenceValue.equals(that.idReferenceValue)));
        result = result && (((valuemin == null) && (that.valuemin == null)) || (valuemin != null && valuemin.equals(that.valuemin)));
        result = result && (((valuemax == null) && (that.valuemax == null)) || (valuemax != null && valuemax.equals(that.valuemax)));
        result = result && (((valuedefault == null) && (that.valuedefault == null)) || (valuedefault != null && valuedefault.equals(that.valuedefault)));
        result = result && (((mandatory == null) && (that.mandatory == null)) || (mandatory != null && mandatory.equals(that.mandatory)));
        result = result && (((ranged == null) && (that.ranged == null)) || (ranged != null && ranged.equals(that.ranged)));
        result = result && (((saveType == null) && (that.saveType == null)) || (saveType != null && saveType.equals(that.saveType)));
        result = result && (((ptype == null) && (that.ptype == null)) || (ptype != null && ptype.equals(that.ptype)));
        result = result && (((caption == null) && (that.caption == null)) || (caption != null && caption.equals(that.caption)));
        result = result && (((displaylogic == null) && (that.displaylogic == null)) || (displaylogic != null && displaylogic.equals(that.displaylogic)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((span == null) && (that.span == null)) || (span != null && span.equals(that.span)));
        return result;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Transient
    public Object getValue() {
        return value;
    }

    @Transient
    public String getString() {
        return (String) value;
    }

    @Transient
    public Boolean getBoolean() {
        return com.vincomobile.fw.basic.tools.Converter.getBoolean((String) value);
    }

    @Transient
    public Long getLong() {
        return com.vincomobile.fw.basic.tools.Converter.getLong((String) value);
    }

    @Transient
    public Date getDate() {
        return com.vincomobile.fw.basic.tools.Converter.getDate((String) value);
    }

    @Transient
    public Date getDate(String format) {
        return com.vincomobile.fw.basic.tools.Converter.getDate((String) value, format);
    }

    @Transient
    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}

