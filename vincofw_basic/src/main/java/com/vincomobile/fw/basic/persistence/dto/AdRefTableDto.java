package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import com.vincomobile.fw.basic.tools.Converter;

public class AdRefTableDto {

    private String idTable;
    private String tableName;
    private String tableStdName;
    private String tableFldName;
    private String restPath;

    private String idKey;
    private String keyName;

    private String idDisplay;
    private String displayName;

    private String sqlwhere;
    private String sqlorderby;

    public AdRefTableDto(AdRefTable item) {
        this.idTable = item.getIdTable();
        this.tableName = item.getTable().getName();
        this.tableStdName = item.getTable().getStandardName();
        int indx = this.tableName.indexOf("_");
        this.tableFldName = indx >= 0 ? Converter.removeUnderscores(this.tableName.substring(indx + 1).toLowerCase()) : this.tableStdName;
        this.restPath = item.getTable().getModule().getRestPath();

        this.idKey = item.getIdKey();
        this.keyName = item.getKey().getStandardName();

        this.idDisplay = item.getIdDisplay();
        this.displayName = item.getDisplay().getStandardName();

        this.sqlwhere = item.getSqlwhere();
        this.sqlorderby = item.getSqlorderby();
    }

    /*
     * Set/Get Methods
     */

    public String getIdTable() {
        return idTable;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableStdName() {
        return tableStdName;
    }

    public String getTableFldName() {
        return tableFldName;
    }

    public String getRestPath() {
        return restPath;
    }

    public String getIdKey() {
        return idKey;
    }

    public String getKeyName() {
        return keyName;
    }

    public String getIdDisplay() {
        return idDisplay;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getSqlwhere() {
        return sqlwhere;
    }

    public String getSqlorderby() {
        return sqlorderby;
    }
}

