package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdClient;
import com.vincomobile.fw.basic.persistence.model.AdClientModule;
import com.vincomobile.fw.basic.persistence.model.AdModule;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Servicio de ad_client_module
 *
 * Date: 28/11/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdClientModuleServiceImpl extends BaseServiceImpl<AdClientModule, String> implements AdClientModuleService {

    /**
     * Constructor.
     */
    public AdClientModuleServiceImpl() {
        super(AdClientModule.class);
    }

    /**
     * List all clients for module
     *
     * @param idModule Module identifier
     * @return Clienta list
     */
    @Override
    public List<AdClient> listClients(String idModule) {
        Map filter = new HashMap();
        filter.put("moduleId", idModule);
        filter.put("active", true);
        filter.put("client.active", true);
        List<AdClientModule> qResult = findAll(filter);
        List<AdClient> result = new ArrayList<>();
        for (AdClientModule clientModule : qResult) {
            result.add(clientModule.getClient());
        }
        return result;
    }

    /**
     * List all modules for client
     *
     * @param idClient Client identifier
     * @return Modules list
     */
    @Override
    public List<AdModule> listModules(String idClient) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        filter.put("active", true);
        filter.put("module.active", true);
        List<AdClientModule> qResult = findAll(filter);
        List<AdModule> result = new ArrayList<>();
        for (AdClientModule clientModule : qResult) {
            result.add(clientModule.getAssigedModule());
        }
        return result;
    }

}
