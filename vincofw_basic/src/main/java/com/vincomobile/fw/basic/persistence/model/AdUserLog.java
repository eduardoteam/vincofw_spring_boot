package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Vincomobile FW on 05/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Model for table ad_user_log
 */
@Entity
@Table(name = "ad_user_log")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdUserLog extends EntityBean<String> {

    private String idUserLog;
    private String idClient;
    private String idUser;
    private String sessionId;
    private String userType;
    private String userAgent;
    private String remoteHost;
    private String remoteIp;
    private Date logoutTime;

    private AdUser user;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idUserLog;
    }

    @Override
    public void setId(String id) {
            this.idUserLog = id;
    }

    @Id
    @Column(name = "id_user_log")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdUserLog() {
        return idUserLog;
    }

    public void setIdUserLog(String idUserLog) {
        this.idUserLog = idUserLog;
    }

    @Column(name = "id_client", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_user", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "session_id", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Column(name = "user_type", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Column(name = "user_agent", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Column(name = "remote_host", length = 150)
    @Size(min = 1, max = 150)
    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    @Column(name = "remote_ip", length = 16)
    @NotNull
    @Size(min = 1, max = 16)
    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    @Column(name = "logout_time")
    public Date getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    public AdUser getUser() {
        return user;
    }

    public void setUser(AdUser user) {
        this.user = user;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdUserLog)) return false;

        final AdUserLog that = (AdUserLog) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idUserLog == null) && (that.idUserLog == null)) || (idUserLog != null && idUserLog.equals(that.idUserLog)));
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((userType == null) && (that.userType == null)) || (userType != null && userType.equals(that.userType)));
        result = result && (((userAgent == null) && (that.userAgent == null)) || (userAgent != null && userAgent.equals(that.userAgent)));
        result = result && (((remoteHost == null) && (that.remoteHost == null)) || (remoteHost != null && remoteHost.equals(that.remoteHost)));
        result = result && (((remoteIp == null) && (that.remoteIp == null)) || (remoteIp != null && remoteIp.equals(that.remoteIp)));
        result = result && (((logoutTime == null) && (that.logoutTime == null)) || (logoutTime != null && logoutTime.equals(that.logoutTime)));
        result = result && (((sessionId == null) && (that.sessionId == null)) || (sessionId != null && sessionId.equals(that.sessionId)));
        return result;
    }

}

