package com.vincomobile.fw.basic.persistence.business;

import com.vincomobile.fw.basic.persistence.model.EntityBean;
import com.vincomobile.fw.basic.persistence.services.BaseService;
import com.vincomobile.fw.basic.persistence.services.PageSearch;
import org.springframework.data.domain.Page;

import java.io.Serializable;

public interface BaseBusiness<T extends EntityBean, PK extends Serializable, S extends BaseService<T,PK>> {
    S getService();

    /**
     * Search by Id.
     *
     * @param id Bean ID
     * @return Bean
     */
    T findById(PK id);

    /**
     * Paged list of sorted beans.
     *
     * @param pageReq     Paging request
     * @return Page of beans
     */
    Page<T> findAll(PageSearch pageReq);

    /**
     * Saves the bean
     *
     * @param bean Entity
     */
    PK save(T bean);

    /**
     * Creates the bean
     *
     * @param bean Entity
     */
    T create(T bean);

    /**
     * Updates the bean
     *
     * @param bean Entity
     */
    void update(T bean);

    /**
     * Deletes the bean
     *
     * @param bean Entity
     */
    void delete(T bean);

    /**
     * Deletes the bean
     *
     * @param id Bean ID
     */
    void delete(PK id);

}
