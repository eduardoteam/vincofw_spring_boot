package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Devtools.
 * Modelo de ad_client_module
 *
 * Date: 28/11/2015
 */
@Entity
@Table(name = "ad_client_module")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdClientModule extends AdEntityBean {

    private String idClientModule;
    private String moduleId;

    protected AdModule assigedModule;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idClientModule;
    }

    @Override
    public void setId(String id) {
            this.idClientModule = id;
    }

    @Id
    @Column(name = "id_client_module")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdClientModule() {
        return idClientModule;
    }

    public void setIdClientModule(String idClientModule) {
        this.idClientModule = idClientModule;
    }

    @Column(name = "module_id")
    @NotNull
    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "module_id", referencedColumnName = "id_module", insertable = false, updatable = false)
    public AdModule getAssigedModule() {
        return assigedModule;
    }

    public void setAssigedModule(AdModule assigedModule) {
        this.assigedModule = assigedModule;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdClientModule)) return false;

        final AdClientModule that = (AdClientModule) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idClientModule == null) && (that.idClientModule == null)) || (idClientModule != null && idClientModule.equals(that.idClientModule)));
        result = result && (((moduleId == null) && (that.moduleId == null)) || (moduleId != null && moduleId.equals(that.moduleId)));
        return result;
    }

    @Transient
    public String getModuleName() {
        return assigedModule == null ? null : assigedModule.getName();
    }
}

