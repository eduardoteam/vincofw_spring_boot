package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdColumn;

public class AdColumnDto {

    private String idTable;
    private String idColumn;
    private String name;
    private String description;
    private String ctype;
    private String cformat;
    private Long seqno;
    private Boolean primaryKey;
    private Boolean mandatory;
    private String valuemin;
    private String valuemax;
    private String valuedefault;
    private String readonlylogic;
    private Boolean showinsearch;
    private Boolean groupedsearch;
    private Boolean linkParent;
    private String linkColumn;
    private Boolean translatable;
    private Long lengthMin;
    private Long lengthMax;

    private AdReferenceDto reference;

    public AdColumnDto(AdColumn item) {
        this.idTable = item.getIdTable();
        this.idColumn = item.getIdColumn();
        this.name = item.getStandardName();
        this.description = item.getDescription();
        this.cformat = item.getCformat();
        this.ctype = item.getCtype();
        this.seqno = item.getSeqno();
        this.primaryKey = item.getPrimaryKey();
        this.mandatory = item.getMandatory();
        this.valuemax = item.getValuemax();
        this.valuemin = item.getValuemin();
        this.valuedefault = item.getValuedefault();
        this.readonlylogic = item.getReadonlylogic();
        this.showinsearch = item.getShowinsearch();
        this.groupedsearch = item.getGroupedsearch();
        this.linkParent = item.getLinkParent();
        this.linkColumn = item.getLinkColumn();
        this.translatable = item.getTranslatable();
        this.lengthMin = item.getLengthMin();
        this.lengthMax = item.getLengthMax();
        this.reference = new AdReferenceDto(item.getReference(), item.getReferenceValue());
    }

    /*
     * Set/Get Methods
     */

    public String getIdTable() {
        return idTable;
    }

    public String getIdColumn() {
        return idColumn;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCformat() {
        return cformat;
    }

    public String getCtype() {
        return ctype;
    }

    public Long getSeqno() {
        return seqno;
    }

    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public String getValuemin() {
        return valuemin;
    }

    public String getValuemax() {
        return valuemax;
    }

    public String getValuedefault() {
        return valuedefault;
    }

    public String getReadonlylogic() {
        return readonlylogic;
    }

    public Boolean getShowinsearch() {
        return showinsearch;
    }

    public Boolean getGroupedsearch() {
        return groupedsearch;
    }

    public Boolean getLinkParent() {
        return linkParent;
    }

    public String getLinkColumn() {
        return linkColumn;
    }

    public Boolean getTranslatable() {
        return translatable;
    }

    public Long getLengthMin() {
        return lengthMin;
    }

    public Long getLengthMax() {
        return lengthMax;
    }

    public AdReferenceDto getReference() {
        return reference;
    }
}

