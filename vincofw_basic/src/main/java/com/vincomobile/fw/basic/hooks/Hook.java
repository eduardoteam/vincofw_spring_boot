package com.vincomobile.fw.basic.hooks;

import com.vincomobile.fw.basic.FWConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public abstract class Hook {

    protected Logger logger = LoggerFactory.getLogger(Hook.class);

    protected String name;
    protected String packageName = "";
    protected String lastError = null;

    @PostConstruct
    public void init() {
        logger.info(packageName + " Register hook: " + name);
        if (name.equals(FWConfig.HOOK_APP_STARTED)) {
            HookResult result = new HookResult();
            execute(result);
        } else {
            HookManager.registerHook(this);
        }
    }

    /**
     * Execute a hook
     *
     * @param result Result execution
     * @param arguments Arguments
     */
    public abstract void execute(HookResult result, Object... arguments);

    /**
     * Get last execution error
     *
     * @return Last execution error
     */
    public String getError() {
        return lastError;
    }

}
