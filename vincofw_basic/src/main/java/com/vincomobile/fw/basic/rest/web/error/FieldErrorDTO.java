package com.vincomobile.fw.basic.rest.web.error;

/**
 * This class contains the information of a single field error.
 *
 * @author Petri Kainulainen
 */
final class FieldErrorDTO {

    private final String field;

    private final String message;

    FieldErrorDTO(String field, String message) {
        PreCondition.notNull(field, "Field cannot be null.");
        PreCondition.notEmpty(field, "Field cannot be empty");

        PreCondition.notNull(message, "Message cannot be null.");
        PreCondition.notEmpty(message, "Message cannot be empty.");

        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
