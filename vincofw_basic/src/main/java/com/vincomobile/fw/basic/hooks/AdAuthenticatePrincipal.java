package com.vincomobile.fw.basic.hooks;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.beans.AdPrincipal;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.persistence.services.AdUserLogService;
import com.vincomobile.fw.basic.persistence.services.AdUserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("AdAuthenticatePrincipalFW")
@Qualifier("hookBase")
public class AdAuthenticatePrincipal extends Hook {

    private final AdUserService userService;

    public AdAuthenticatePrincipal(AdUserService userService) {
        this.userService = userService;
        name = FWConfig.HOOK_AUTHENTICATE_PRINCIPAL;
        packageName = "[VincoFW]";
    }

    /**
     * Execute a hook
     *
     * @param result    Result execution
     * @param arguments Arguments (username)
     */
    @Override
    public void execute(HookResult result, Object... arguments) {
        logger.info(packageName + " Execute hook: " + name);
        if (result.hasKey(FWConfig.HOOK_AUTHENTICATE_PRINCIPAL_USER))
            return;
        String username = (String) arguments[0];
        String idRole = null;
        if (username != null && username.length() > 32 && username.indexOf("$") == 32) {
            idRole = username.substring(0, 32);
            username = username.substring(33);
        }

        AdUser user = userService.findByLogin(username);
        if (user != null) {
            AdPrincipal principal = new AdPrincipal();
            principal.setRoleId(idRole == null ? user.getDefaultIdRole() : idRole);
            principal.setPassword(user.getPassword());
            principal.setName(username);
            principal.setUserId(user.getIdUser());
            principal.setUserName(user.getName());
            principal.setUserType(AdUserLogService.USER_TYPE_AD_USER);
            result.put(FWConfig.HOOK_AUTHENTICATE_PRINCIPAL_USER, principal);
        }
    }

}
