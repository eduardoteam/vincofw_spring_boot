package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_MODULE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_module")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdModule extends EntityBean<String> {

    private String idModule;
    private String idClient;
    private String name;
    private String description;
    private String restPath;
    private String javaPackage;
    private String version;
    private Boolean indevelopment;
    private String dbprefix;
    private String codeDoc;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idModule;
    }

    @Override
    public void setId(String id) {
            this.idModule = id;
    }

    @Id
    @Column(name = "id_module")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }


    @Column(name = "id_client")
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "java_package", length = 150)
    @NotNull
    @Size(min = 1, max = 150)
    public String getJavaPackage() {
        return javaPackage;
    }

    public void setJavaPackage(String javaPackage) {
        this.javaPackage = javaPackage;
    }

    @Column(name = "rest_path", length = 30)
    @NotNull
    @Size(min = 1, max = 30)
    public String getRestPath() {
        return restPath;
    }

    public void setRestPath(String restPath) {
        this.restPath = restPath;
    }

    @Column(name = "version", length = 10)
    @NotNull
    @Size(min = 1, max = 10)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Column(name = "indevelopment")
    public Boolean getIndevelopment() {
        return indevelopment;
    }

    public void setIndevelopment(Boolean indevelopment) {
        this.indevelopment = indevelopment;
    }

    @Column(name = "dbprefix", length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    public String getDbprefix() {
        return dbprefix;
    }

    public void setDbprefix(String dbprefix) {
        this.dbprefix = dbprefix;
    }

    @Column(name = "code_doc", length = 250)
    @Size(min = 1, max = 250)
    public String getCodeDoc() {
        return codeDoc;
    }

    public void setCodeDoc(String codeDoc) {
        this.codeDoc = codeDoc;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdModule)) return false;

        final AdModule that = (AdModule) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idModule == null) && (that.idModule == null)) || (idModule != null && idModule.equals(that.idModule)));
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((javaPackage == null) && (that.javaPackage == null)) || (javaPackage != null && javaPackage.equals(that.javaPackage)));
        result = result && (((restPath == null) && (that.restPath == null)) || (restPath != null && restPath.equals(that.restPath)));
        result = result && (((version == null) && (that.version == null)) || (version != null && version.equals(that.version)));
        result = result && (((indevelopment == null) && (that.indevelopment == null)) || (indevelopment != null && indevelopment.equals(that.indevelopment)));
        result = result && (((dbprefix == null) && (that.dbprefix == null)) || (dbprefix != null && dbprefix.equals(that.dbprefix)));
        result = result && (((codeDoc == null) && (that.codeDoc == null)) || (codeDoc != null && codeDoc.equals(that.codeDoc)));
        return result;
    }

    /**
     * Get table documentation code
     *
     * @return Documentation
     */
    @Transient
    @JsonIgnore
    public String getCodeDocumentation() {
        return com.vincomobile.fw.basic.tools.Converter.isEmpty(codeDoc) ? name : codeDoc;
    }

}

