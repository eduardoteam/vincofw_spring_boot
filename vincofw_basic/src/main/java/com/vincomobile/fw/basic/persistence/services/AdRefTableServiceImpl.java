package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_REF_TABLE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdRefTableServiceImpl extends BaseServiceImpl<AdRefTable, String> implements AdRefTableService {

    /**
     * Constructor.
     *
     */
    public AdRefTableServiceImpl() {
        super(AdRefTable.class);
    }

    /**
     * Get reference table by reference
     *
     * @param idReference Reference identifier
     * @return Reference Table
     */
    @Override
    public AdRefTable getRefTableByReference(String idReference) {
        Map filter = new HashMap();
        filter.put("idReference", idReference);
        return findFirst(filter);
    }

}
