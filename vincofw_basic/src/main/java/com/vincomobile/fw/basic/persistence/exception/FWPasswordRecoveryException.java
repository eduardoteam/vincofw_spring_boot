package com.vincomobile.fw.basic.persistence.exception;

/**
 * Thrown when there is a problem during the password recovery process
 */
public class FWPasswordRecoveryException extends Exception {
    private String reason = "";

    /**
     * Constructor
     *
     * @param reason Fail reason
     */
    public FWPasswordRecoveryException(String reason) {

        this.reason = reason;
    }

    /**
     * Gets the Fail reason
     *
     * @return Fail reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the fail reason
     *
     * @param reason Fail reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }
}
