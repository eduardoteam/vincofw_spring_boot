package com.vincomobile.fw.basic.process;

import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * Created by Devtools.
 * Modelo de ad_process_param
 *
 * Date: 06/11/2015
 */
public class ProcessParam  {

    private String name;
    private Object value;
    private MultipartFile file;

    public ProcessParam() {
    }

    public ProcessParam(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public ProcessParam(String name, MultipartFile file) {
        this.name = name;
        this.file = file;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getString() {
        return (String) value;
    }

    public Boolean getBoolean() {
        return Converter.getBoolean((String) value);
    }

    public Long getLong() {
        return Converter.getLong((String) value);
    }

    public Date getDate() {
        return Converter.getDate((String) value);
    }

    public Date getDate(String format) {
        return Converter.getDate((String) value, format);
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}

