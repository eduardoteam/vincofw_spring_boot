package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdPrivilege;
import com.vincomobile.fw.basic.persistence.model.AdUser;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_PRIVILEGE
 *
 * Date: 19/02/2015
 */
public interface AdPrivilegeService extends BaseService<AdPrivilege, String> {


    /**
     * Obtains all the users that have a selected privilege
     *
     * @param privilegeName Name of the privilege
     * @return List of users that have the selected privilege
     */
    List<AdUser> obtainUsers(String privilegeName);
}


