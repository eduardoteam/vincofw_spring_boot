package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdUserLog;
import com.vincomobile.fw.basic.session.UserLog;

/**
 * Created by Vincomobile FW on 05/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_user_log
 */
public interface AdUserLogService extends BaseService<AdUserLog, String> {

    String USER_TYPE_AD_USER = "AD_USER";

    /**
     * Update user log
     *
     * @param log Log information
     */
    void updateLogout(UserLog log);

    /**
     * Reset all logoutTime with null value
     */
    int resetLogout();
}


