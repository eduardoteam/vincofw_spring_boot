package com.vincomobile.fw.basic.hooks;

import com.vincomobile.fw.basic.tools.Converter;

import java.util.HashMap;
import java.util.Map;

public class HookManager {

    private static Map<String, HookList> hooks = new HashMap<>();

    /**
     * Register a Hook
     *
     * @param hook Hook
     */
    public static void registerHook(Hook hook) {
        if (!Converter.isEmpty(hook.name)) {
            HookList list = hooks.get(hook.name);
            if (list == null) {
                list = new HookList();
                hooks.put(hook.name, list);
            }
            list.add(hook);
        }
    }

    /**
     * Execute all registered hooks
     *
     * @param name Hook name
     * @param arguments Hook arguments
     * @return Execution result
     */
    public static HookResult executeHook(String name, Object... arguments) {
        HookList list = hooks.get(name);
        HookResult result = new HookResult();
        if (list != null) {
            for (Hook hook: list.hooks) {
                hook.execute(result, arguments);
                if (!result.success)
                    break;
            }
        }
        return result;
    }

}
