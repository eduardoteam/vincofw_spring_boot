package com.vincomobile.fw.basic.persistence.beans;

public class AdTranslationsData {
    String idLanguage;
    String translation;

    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }
}
