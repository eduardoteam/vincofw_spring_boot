package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.beans.CodeGenColumn;
import com.vincomobile.fw.basic.persistence.services.AdColumnService;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_COLUMN
 * <p/>
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_column")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdColumn extends AdEntityBean {

    private String idColumn;
    private String idTable;
    private String idReference;
    private String idReferenceValue;
    private String idRefSequence;
    private String name;
    private String description;
    private String ctype;
    private String cformat;
    private Long seqno;
    private Boolean primaryKey;
    private Boolean mandatory;
    private String csource;
    private String csourceValue;
    private String valuemin;
    private String valuemax;
    private String valuedefault;
    private String readonlylogic;
    private Boolean showinsearch;
    private Boolean groupedsearch;
    private Boolean linkParent;
    private String linkColumn;
    private Boolean translatable;
    private Long lengthMin;
    private Long lengthMax;
    private String codeDoc;

    private AdTable table;
    private AdReference reference;
    private AdReference referenceValue;
    private CodeGenColumn codeGen;

    private Query importRefQuery;
    private AdColumn importRefColumn;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idColumn;
    }

    @Override
    public void setId(String id) {
        this.idColumn = id;
    }

    @Id
    @Column(name = "id_column")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(String idColumn) {
        this.idColumn = idColumn;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "id_reference_value")
    public String getIdReferenceValue() {
        return idReferenceValue;
    }

    public void setIdReferenceValue(String idReferenceValue) {
        this.idReferenceValue = idReferenceValue;
    }

    @Column(name = "id_ref_sequence")
    public String getIdRefSequence() {
        return idRefSequence;
    }

    public void setIdRefSequence(String idRefSequence) {
        this.idRefSequence = idRefSequence;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 2000)
    @Size(min = 1, max = 2000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "ctype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    @Column(name = "cformat", length = 50)
    @Size(min = 1, max = 50)
    public String getCformat() {
        return cformat;
    }

    public void setCformat(String cformat) {
        this.cformat = cformat;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "primary_key")
    @NotNull
    public Boolean getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(Boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Column(name = "mandatory")
    @NotNull
    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Column(name = "csource", length = 100)
    @Size(min = 1, max = 100)
    public String getCsource() {
        return csource;
    }

    public void setCsource(String csource) {
        this.csource = csource;
    }

    @Column(name = "csource_value", length = 250)
    @Size(min = 1, max = 250)
    public String getCsourceValue() {
        return csourceValue;
    }

    public void setCsourceValue(String csourceValue) {
        this.csourceValue = csourceValue;
    }

    @Column(name = "valuemin", length = 45)
    @Size(min = 1, max = 45)
    public String getValuemin() {
        return valuemin;
    }

    public void setValuemin(String valuemin) {
        this.valuemin = valuemin;
    }

    @Column(name = "valuemax", length = 45)
    @Size(min = 1, max = 45)
    public String getValuemax() {
        return valuemax;
    }

    public void setValuemax(String valuemax) {
        this.valuemax = valuemax;
    }

    @Column(name = "valuedefault", length = 45)
    @Size(min = 1, max = 45)
    public String getValuedefault() {
        return valuedefault;
    }

    public void setValuedefault(String valuedefault) {
        this.valuedefault = valuedefault;
    }

    @Column(name = "readonlylogic", length = 2000)
    @Size(min = 1, max = 2000)
    public String getReadonlylogic() {
        return readonlylogic;
    }

    public void setReadonlylogic(String readonlylogic) {
        this.readonlylogic = readonlylogic;
    }

    @Column(name = "showinsearch")
    public Boolean getShowinsearch() {
        return showinsearch;
    }

    public void setShowinsearch(Boolean showinsearch) {
        this.showinsearch = showinsearch;
    }

    @Column(name = "groupedsearch")
    public Boolean getGroupedsearch() {
        return groupedsearch != null ? groupedsearch : false;
    }

    public void setGroupedsearch(Boolean groupedsearch) {
        this.groupedsearch = groupedsearch;
    }

    @Column(name = "link_parent")
    public Boolean getLinkParent() {
        return linkParent;
    }

    public void setLinkParent(Boolean linkParent) {
        this.linkParent = linkParent;
    }

    @Column(name = "link_column", length = 100)
    @Size(min = 1, max = 100)
    public String getLinkColumn() {
        return linkColumn;
    }

    public void setLinkColumn(String linkColumn) {
        this.linkColumn = linkColumn;
    }

    @Column(name = "translatable")
    @NotNull
    public Boolean getTranslatable() {
        return translatable;
    }

    public void setTranslatable(Boolean translatable) {
        this.translatable = translatable;
    }

    @Column(name = "length_min")
    public Long getLengthMin() {
        return lengthMin;
    }

    public void setLengthMin(Long lengthMin) {
        this.lengthMin = lengthMin;
    }

    @Column(name = "length_max")
    public Long getLengthMax() {
        return lengthMax;
    }

    public void setLengthMax(Long lengthMax) {
        this.lengthMax = lengthMax;
    }

    @Column(name = "code_doc", length = 250)
    @Size(min = 1, max = 250)
    public String getCodeDoc() {
        return codeDoc;
    }

    public void setCodeDoc(String codeDoc) {
        this.codeDoc = codeDoc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdReference getReference() {
        return reference;
    }

    public void setReference(AdReference reference) {
        this.reference = reference;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_reference_value", referencedColumnName = "id_reference", insertable = false, updatable = false)
    @JsonIgnore
    public AdReference getReferenceValue() {
        return referenceValue;
    }

    public void setReferenceValue(AdReference referenceValue) {
        this.referenceValue = referenceValue;
    }

    /**
     * Implementa el equals
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdColumn)) return false;

        final AdColumn that = (AdColumn) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idColumn == null) && (that.idColumn == null)) || (idColumn != null && idColumn.equals(that.idColumn)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((idReferenceValue == null) && (that.idReferenceValue == null)) || (idReferenceValue != null && idReferenceValue.equals(that.idReferenceValue)));
        result = result && (((idRefSequence == null) && (that.idRefSequence == null)) || (idRefSequence != null && idRefSequence.equals(that.idRefSequence)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((ctype == null) && (that.ctype == null)) || (ctype != null && ctype.equals(that.ctype)));
        result = result && (((cformat == null) && (that.cformat == null)) || (cformat != null && cformat.equals(that.cformat)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((primaryKey == null) && (that.primaryKey == null)) || (primaryKey != null && primaryKey.equals(that.primaryKey)));
        result = result && (((mandatory == null) && (that.mandatory == null)) || (mandatory != null && mandatory.equals(that.mandatory)));
        result = result && (((csource == null) && (that.csource == null)) || (csource != null && csource.equals(that.csource)));
        result = result && (((csourceValue == null) && (that.csourceValue == null)) || (csourceValue != null && csourceValue.equals(that.csourceValue)));
        result = result && (((valuemin == null) && (that.valuemin == null)) || (valuemin != null && valuemin.equals(that.valuemin)));
        result = result && (((valuemax == null) && (that.valuemax == null)) || (valuemax != null && valuemax.equals(that.valuemax)));
        result = result && (((valuedefault == null) && (that.valuedefault == null)) || (valuedefault != null && valuedefault.equals(that.valuedefault)));
        result = result && (((readonlylogic == null) && (that.readonlylogic == null)) || (readonlylogic != null && readonlylogic.equals(that.readonlylogic)));
        result = result && (((showinsearch == null) && (that.showinsearch == null)) || (showinsearch != null && showinsearch.equals(that.showinsearch)));
        result = result && (((groupedsearch == null) && (that.groupedsearch == null)) || (groupedsearch != null && groupedsearch.equals(that.groupedsearch)));
        result = result && (((linkParent == null) && (that.linkParent == null)) || (linkParent != null && linkParent.equals(that.linkParent)));
        result = result && (((linkColumn == null) && (that.linkColumn == null)) || (linkColumn != null && linkColumn.equals(that.linkColumn)));
        result = result && (((translatable == null) && (that.translatable == null)) || (translatable != null && translatable.equals(that.translatable)));
        result = result && (((lengthMin == null) && (that.lengthMin == null)) || (lengthMin != null && lengthMin.equals(that.lengthMin)));
        result = result && (((lengthMax == null) && (that.lengthMax == null)) || (lengthMax != null && lengthMax.equals(that.lengthMax)));
        result = result && (((codeDoc == null) && (that.codeDoc == null)) || (codeDoc != null && codeDoc.equals(that.codeDoc)));
        return result;
    }

    @Transient
    @JsonIgnore
    public String getCapitalizeStandardName() {
        return com.vincomobile.fw.basic.tools.Converter.removeUnderscores(com.vincomobile.fw.basic.tools.Converter.capitalize(this.name.toLowerCase()));
    }

    @Transient
    @JsonIgnore
    public String getLowerName() {
        return this.name.toLowerCase();
    }

    @Transient
    @JsonIgnore
    public String getUpperCaseName() {
        return this.name.toUpperCase();
    }

    @Transient
    @JsonIgnore
    public String getCapitalizeName() {
        return com.vincomobile.fw.basic.tools.Converter.capitalize(this.name.toLowerCase());
    }

    @Transient
    @JsonIgnore
    public String getStandardName() {
        return com.vincomobile.fw.basic.tools.Converter.removeUnderscores(this.name.toLowerCase());
    }

    @Transient
    @JsonIgnore
    public String getImageStandardName() {
        if (this.name.endsWith("_url")) {
            String image = this.name.substring(0, this.name.length() - 4);
            return com.vincomobile.fw.basic.tools.Converter.removeUnderscores(image.toLowerCase());
        }
        return "";
    }

    /**
     * Obtiene el tipo Java
     *
     * @return Nombre del tipo
     */
    @Transient
    @JsonIgnore
    public String getJavaType() {
        if (this.ctype.equals(AdColumnService.TYPE_STRING) || this.ctype.equals(AdColumnService.TYPE_TEXT)) {
            return "String";
        } else if (this.ctype.equals(AdColumnService.TYPE_INTEGER)) {
            return "Long";
        } else if (this.ctype.equals(AdColumnService.TYPE_NUMBER)) {
            return "Double";
        } else if (this.ctype.equals(AdColumnService.TYPE_DATE) || this.ctype.equals(AdColumnService.TYPE_DATETIME)) {
            return "Date";
        } else if (this.ctype.equals(AdColumnService.TYPE_BOOLEAN)) {
            return "Boolean";
        } else if (this.ctype.equals("IMAGE") || this.ctype.equals("FILE")) {
            return "byte[]";
        }
        return "ERROR";
    }

    @Transient
    @JsonIgnore
    public boolean isDateField() {
        return AdColumnService.TYPE_DATE.equals(ctype) || AdColumnService.TYPE_DATETIME.equals(ctype);
    }

    @Transient
    @JsonIgnore
    public boolean isCheckSize() {
        return AdColumnService.TYPE_STRING.equals(ctype) && !com.vincomobile.fw.basic.tools.Converter.isEmpty(lengthMin)
                && !com.vincomobile.fw.basic.tools.Converter.isEmpty(lengthMax);
    }

    @Transient
    @JsonIgnore
    public boolean isBooleanField() {
        return AdColumnService.TYPE_BOOLEAN.equals(ctype);
    }

    @Transient
    @JsonIgnore
    public CodeGenColumn getCodeGen() {
        return codeGen;
    }

    public void setCodeGen(CodeGenColumn codeGen) {
        this.codeGen = codeGen;
    }

    @Transient
    @JsonIgnore
    public AdColumn getImportRefColumn() {
        return importRefColumn;
    }

    public void setImportRefColumn(AdColumn importRefColumn) {
        this.importRefColumn = importRefColumn;
    }

    @Transient
    @JsonIgnore
    public Query getImportRefQuery() {
        return importRefQuery;
    }

    public void setImportRefQuery(Query importRefQuery) {
        this.importRefQuery = importRefQuery;
    }

    /**
     * Get table documentation code
     *
     * @return Documentation
     */
    @Transient
    @JsonIgnore
    public String getCodeDocumentation() {
        return com.vincomobile.fw.basic.tools.Converter.isEmpty(codeDoc) ? getCapitalizeStandardName() : codeDoc;
    }

    @Transient
    @JsonIgnore
    public String getRandomValue() {
        if (this.ctype.equals(AdColumnService.TYPE_INTEGER)) {
            return "" + FWConfig.random.nextLong() + "L";
        } else if (this.ctype.equals(AdColumnService.TYPE_NUMBER)) {
            return "" + FWConfig.random.nextDouble();
        } else if (this.ctype.equals(AdColumnService.TYPE_DATE) || this.ctype.equals(AdColumnService.TYPE_DATETIME)) {
            return "DateUtils.addDays(new Date(), " + FWConfig.random.nextInt() + ")";
        } else if (this.ctype.equals(AdColumnService.TYPE_BOOLEAN)) {
            return FWConfig.random.nextLong() % 2 == 0 ? "true" : "false";
        } else {
            if (this.primaryKey)
                return "\"" + com.vincomobile.fw.basic.tools.Converter.getUUID() + "\"";
            else
                return "\"" + RandomStringUtils.randomAlphanumeric(this.lengthMax != null ? this.lengthMax.intValue() : 5) + "\"";
        }
    }

}

