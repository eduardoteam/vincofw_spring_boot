package com.vincomobile.fw.basic.tools;

/**
 * Created by alfredo on 31/08/2016.
 */
public enum ProcessPlanningErrorCode {

    UNKNOWN_REASON(0),
    PROCESS_WILL_NEVER_FIRE(1);
    private final int value;

    ProcessPlanningErrorCode(int value){
        this.value = value;
    }
}
