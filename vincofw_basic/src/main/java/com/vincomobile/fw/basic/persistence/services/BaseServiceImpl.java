package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdBaseTools;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.cache.CacheTable;
import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.model.EntityBean;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public abstract class BaseServiceImpl<T extends EntityBean<PK>, PK extends Serializable> implements BaseService<T, PK> {

    private static Pattern patternAt = Pattern.compile("\\#(.*?)\\#");
    private static Pattern patternParenthesis = Pattern.compile("\\((.*?)\\)");

    @PersistenceContext
    protected EntityManager entityManager;

    // Type
    protected Class<T> type;
    // Alias to filters in the query.
    private Map<String, String> queryAlias = new HashMap<>();
    // Alias to orderBy in the query.
    private Map<String, String> sortAlias = new HashMap<>();

    /**
     * Default bean constructor for spring.
     */
    public BaseServiceImpl() {
        // default constructor for spring
        initQueryAlias();
        initSortAlias();
    }

    /**
     * Constructor.
     *
     * @param type class type
     */
    public BaseServiceImpl(Class<T> type) {
        this();
        this.type = type;
    }

    /**
     * Method that initializes the alias that can be used in the queries of this service
     */
    protected void initQueryAlias() {
    }

    /**
     * Method that initializes the alias that can be used in the sorts of this service
     */
    protected void initSortAlias() {
    }

    /**
     * Search by Id.
     *
     * @param id Bean ID
     * @return Bean
     */
    @Override
    public T findById(PK id) {
        return id == null ? null : entityManager.find(type, id);
    }

    /**
     * Search by reference
     *
     * @param field Field name
     * @param idClient Client identifier
     * @param value Value
     * @return Bean
     */
    @Override
    public T findByField(String field, String idClient, String value) {
        if (value == null)
            return null;
        Map filter = new HashMap();
        filter.put(field, value);
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(idClient))
            filter.put("idClient", getClientFilter(idClient));
        return findFirst(filter);
    }

    @Override
    public T findByField(String field, String value) {
        return findByField(field, null, value);
    }

    /**
     * Search first item by filter
     *
     * @param filter filter
     * @return First item found
     */
    @Override
    public T findFirst(Map filter) {
        return findFirst(filter, null, null);
    }

    /**
     * Search first item by filter
     *
     * @param filter Filter
     * @param sort   Sort field
     * @param dir    Sort direction
     * @return First item found
     */
    @Override
    public T findFirst(Map filter, String sort, String dir) {
        List<T> items = findAll(-1, 0, getSort(sort, dir), filter, "", new HashMap());
        return items != null && items.size() > 0 ? items.get(0) : null;
    }

    /**
     * Find all beans
     *
     * @param offset      Start index
     * @param limit       List size
     * @param sort        Sorting fields
     * @param filter      Filter to apply
     * @param extraQuery  Extra query
     * @param extraFilter Extra filter
     * @return Bean list
     */
    @Override
    public List<T> findAll(long offset, int limit, SqlSort sort, Map filter, String extraQuery, Map extraFilter) {
        //Cloning filters to be able to modify without collateral impact
        Map<String, Object> clonedFilters = filter != null ? new HashMap<>(filter) : new HashMap<>();

        CacheTable cacheTable = getCacheTable(getTypeTableName());
        String where = getWhere(clonedFilters, extraQuery);
        String orderBy = sort != null ? sort.getSqlOrderBy() : "";
        AdTable table = cacheTable.getTable();
        TypedQuery<T> query = AdTableService.TABLESOURCE_SQL.equals(table.getDataOrigin()) ?
                (TypedQuery<T>) entityManager.createNativeQuery(table.getSqlQuery() + where + orderBy, type) :
                entityManager.createQuery("from " + type.getName() + " " + TABLE_ENTITY_ALIAS + " " + where + orderBy, type);

        if (limit > 0) {
            query.setFirstResult((int) offset);
            query.setMaxResults(limit);
        }

        AdBaseTools.setQueryParams(query, clonedFilters);
        AdBaseTools.setQueryParams(query, extraFilter);

        List<T> beanList = query.getResultList();
        return beanList;
    }

    @Override
    public List<T> findAll(long offset, int limit, SqlSort sort, Map filter) {
        return findAll(offset, limit, sort, filter, null, null);
    }

    @Override
    public List<T> findAll(SqlSort sort, Map filter) {
        return findAll(0, 0, sort, filter, null, null);
    }

    @Override
    public List<T> findAll(SqlSort sort) {
        return findAll(0, 0, sort, null, null, null);
    }

    @Override
    public List<T> findAll(long offset, int limit) {
        return findAll(offset, limit, null, null, null, null);
    }

    @Override
    public List<T> findAll(Map filter) {
        return findAll(0, 0, null, filter, null, null);
    }

    @Override
    public List<T> findAll() {
        return findAll(0, 0, null, null, null, null);
    }


    /**
     * Paged list of sorted beans.
     *
     * @param request     Paging request
     * @param extraQuery  Extra query
     * @param extraFilter Extra filter
     * @return List of beans
     */
    @Override
    public Page<T> findAll(PageSearch request, String extraQuery, Map extraFilter) {
        long offset = request.getOffset();
        int limit = request.getPageSize();
        List<T> content = findAll(offset, limit, getSort(request), request.searchs, extraQuery, extraFilter);
        long count = count(request.searchs, extraQuery, extraFilter);

        Page<T> result = new PageImpl<>(content, request, count);
        return result;
    }

    @Override
    public Page<T> findAll(PageSearch request, boolean useNamedQuery) {
        long offset = request.getOffset();
        int limit = request.getPageSize();
        Map filter = request.searchs;
        SqlSort sort = getSort(request);
        Map<String, Object> clonedFilters = filter != null ? new HashMap<>(filter) : new HashMap<>();

        CacheTable cacheTable = getCacheTable(getTypeTableName());
        String where = getWhere(clonedFilters, null);
        String orderBy = sort != null ? sort.getSqlOrderBy() : "";
        AdTable table = cacheTable.getTable();
        TypedQuery<T> query = entityManager.createNamedQuery("ExtendActtplTemplateAction", type);

        if (limit > 0) {
            query.setFirstResult((int) offset);
            query.setMaxResults(limit);
        }

//        AdBaseTools.setQueryParams(query, clonedFilters);

        List<T> beanList = query.getResultList();
        Page<T> result = new PageImpl<>(beanList, request, beanList.size());
        return result;
    }

    @Override
    public Page<T> findAll(PageSearch request) {
        return findAll(request, null, null);
    }

    /**
     * Gets the sort information
     *
     * @param request Page request
     * @return Sort information to add to the ORDER BY clause
     */
    @Override
    public SqlSort getSort(PageSearch request) {
        return getSort(request.getSort());
    }

    @Override
    public SqlSort getSort(Sort sort) {
        SqlSort result = null;
        if (sort != null) {
            result = new SqlSort();
            Iterator<Sort.Order> it = sort.iterator();
            while (it.hasNext()) {
                Sort.Order order = it.next();
                result.addSorter(order.getProperty(), order.getDirection() == Sort.Direction.ASC ? "asc" : "desc");
            }
        }
        return result;
    }

    @Override
    public SqlSort getSort(String sort, String dir) {
        SqlSort result = null;
        if (sort != null) {
            result = new SqlSort();
            result.addSorter(sort, dir);
        }
        return result;
    }

    /**
     * Obtains the value of the ORDER BY clause (Multiple)
     *
     * @param request Page request
     * @return ORDER BY information
     */
    protected String getOrderby(PageSearch request) {
        Sort sortBy = request.getSort();
        String orderby = "";
        if (sortBy != null) {
            for (Sort.Order order : sortBy) {
                if (!orderby.equals(""))
                    orderby += ", ";
                orderby += order.getProperty() + " " + (order.getDirection() == Sort.Direction.ASC ? "ASC" : "DESC");
            }
        }
        return orderby.equals("") ? "" : " \nORDER BY " + orderby;
    }

    /**
     * Gets a single item page
     *
     * @param bean Item
     * @return Page with item
     */
    @Override
    public Page<T> getItem(T bean) {
        List<T> items = new ArrayList<T>();
        items.add(bean);
        return new PageImpl<T>(items, new PageSearch(1, 1), 1);
    }

    /**
     * Counts the rows
     *
     * @param filter Filter to apply
     * @return Total elements
     */
    @Override
    public long count(Map filter) {
        return count(filter, null, null);
    }

    /**
     * Counts the rows
     *
     * @param filter      Filter to apply
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Total elements
     */
    @Override
    public long count(Map filter, String extraWhere, Map extraFilter) {
        // We make a copy so we can modify without collateral damage.
        CacheTable cacheTable = getCacheTable(getTypeTableName());
        filter = filter == null ? new HashMap(): new HashMap(filter);
        String where = getWhere(filter, extraWhere);
        Query query;
        if (AdTableService.TABLESOURCE_SQL.equals(cacheTable.getTable().getDataOrigin())) {
            String select = cacheTable.getTable().getSqlQuery().replaceAll("\\n", " ");
            int fromIndex = select.toLowerCase().indexOf(" from ");
            query = entityManager.createNativeQuery("select count(*) " + select.substring(fromIndex) + where);
        } else {
            query = entityManager.createQuery("select count(*) from " + type.getName() + " " + TABLE_ENTITY_ALIAS + " " + where);
        }

        AdBaseTools.setQueryParams(query, filter);
        AdBaseTools.setQueryParams(query, extraFilter);
        Object result = query.getSingleResult();

        return (result instanceof Number) ? ((Number) result).longValue() : 0L;
    }

    /**
     * Performs a sum of the fields according to a filter
     *
     * @param filter Filter
     * @param field  Field to analyze
     * @return Sum result
     */
    @Override
    public double sum(String field, Map filter) {
        // We make a copy so we can modify without colateral damage.
        filter = new HashMap(filter);

        String where = buildWhere(filter);
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(where)) {
            where = " where " + where;
        }
        Query query = entityManager.createQuery("select sum(" + field + ") from " + type.getName() + where);
        AdBaseTools.setQueryParams(query, filter);
        Object result = query.getSingleResult();

        return (result instanceof Number) ? ((Number) result).doubleValue() : 0L;
    }

    /**
     * Performs a sum of the fields according to a filter
     *
     * @param filter      Filter
     * @param field       Field to analyze
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Sum result
     */
    @Override
    public long sum(String field, Map filter, String extraWhere, Map extraFilter) {
        // We make a copy so we can modify without colateral damage.
        filter = new HashMap(filter);
        if (extraWhere == null || extraWhere.isEmpty())
            extraWhere = "";
        String where = buildWhere(filter);
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(where)) {
            where = " where " + where + (extraWhere != null ? extraWhere : "");
        }
        Query query = entityManager.createQuery("select sum(" + field + ") from " + type.getName() + where);
        AdBaseTools.setQueryParams(query, filter);
        AdBaseTools.setQueryParams(query, extraFilter);
        Object result = query.getSingleResult();

        return (result instanceof Number) ? ((Number) result).longValue() : 0L;
    }

    /**
     * Executes a custom query
     *
     * @param customQuery SELECT section of the query
     * @param filter      Filter
     * @param extraWhere  Extra where
     * @param extraFilter Extra filter
     * @return Query result
     */
    @Override
    public List customQuery(String customQuery, Map filter, String extraWhere, Map extraFilter) {
        // We make a copy so we can modify without colateral damage.
        filter = new HashMap(filter);
        if (extraWhere == null || extraWhere.isEmpty())
            extraWhere = "";
        String where = buildWhere(filter);
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(where)) {
            where = " where " + where + (extraWhere != null ? extraWhere : "");
        }
        Query query = entityManager.createQuery(customQuery + " from " + type.getName() + where);
        AdBaseTools.setQueryParams(query, filter);
        AdBaseTools.setQueryParams(query, extraFilter);
        List result = query.getResultList();
        return result;
    }

    /**
     * Executes a custom query
     *
     * @param customQuery Custom query
     * @param filter      Filter
     * @param offset      List start
     * @param limit       List size
     * @return Query result
     */
    @Override
    public List query(String customQuery, Map filter, int offset, int limit) {
        // We make a copy so we can modify without colateral damage.
        filter = filter == null ? new HashMap() : new HashMap(filter);
        Query query = entityManager.createQuery(customQuery);
        AdBaseTools.setQueryParams(query, filter);
        if (offset > 0) {
            query.setFirstResult(offset);
        }
        if (limit > 0) {
            query.setMaxResults(limit);
        }
        List result = query.getResultList();
        return result;
    }

    @Override
    public List query(String customQuery, Map filter, int limit) {
        return query(customQuery, filter, -1, limit);
    }

    @Override
    public List query(String customQuery, Map filter) {
        return query(customQuery, filter, -1, -1);
    }

    @Override
    public List query(String customQuery) {
        return query(customQuery, null, -1, -1);
    }

    /**
     * Executes a custom native query
     *
     * @param customQuery SQL to execute
     * @param filter      Filter
     * @param offset      First element
     * @param limit       List size
     * @return Rows
     */
    @Override
    public List nativeQuery(String customQuery, Map filter, int offset, int limit) {
        Query query = entityManager.createNativeQuery(customQuery);
        AdBaseTools.setQueryParams(query, filter);
        if (limit > 0) {
            query.setFirstResult(offset);
            query.setMaxResults(limit);
        }
        return query.getResultList();
    }

    @Override
    public List nativeQuery(String customQuery, Map filter, int limit) {
        return nativeQuery(customQuery, filter, 0, limit);
    }

    @Override
    public List nativeQuery(String customQuery, Map filter) {
        return nativeQuery(customQuery, filter, 0, 0);
    }

    @Override
    public List nativeQuery(String customQuery) {
        return nativeQuery(customQuery, null, 0, 0);
    }

    /**
     *  Get the result and column names of the executed query
     *
     * @param customQuery SQL to execute
     * @param limit       List size
     * @return Rows
     */
    @Override
    public List<Map<String, Object>> executeNativeSelectSql(String customQuery, int limit) throws SQLException {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Session session = entityManager.unwrap(Session.class);
        java.sql.Connection cnn = ((SessionImpl)session).connection();
        Statement st = cnn.createStatement();
        if (limit > 0) {
            st.setMaxRows(limit);
        }
        ResultSet rs = st.executeQuery(customQuery);
        while (rs.next()){
            HashMap<String, Object> row = new HashMap<>();
            for(int i=1; i<= rs.getMetaData().getColumnCount(); i++){
                row.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
            }
            list.add(row);
        }
        return list;
    }

    /**
     * Create a custom query
     *
     * @param customQuery Update sentence
     * @return Query
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Query getUpdateQuery(String customQuery) {
        return entityManager.createQuery(customQuery);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Query getNativeUpdateQuery(String customQuery) {
        return entityManager.createNativeQuery(customQuery);
    }

    /**
     * Executes a UPDATE
     *
     * @param params Parameters for update
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int applyUpdate(String customQuery, Map params) {
        Query query = entityManager.createQuery(customQuery);
        if (params != null) {
            Iterator itKeys = params.entrySet().iterator();
            while (itKeys.hasNext()) {
                Map.Entry pairs = (Map.Entry) itKeys.next();
                query.setParameter((String) pairs.getKey(), pairs.getValue());
            }
        }
//        logger.debug("Query:"+customQuery);
        return query.executeUpdate();
    }

    /**
     * Execute a UPDATE
     *
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int applyUpdate(String customQuery) {
        return applyUpdate(customQuery, null);
    }

    /**
     * Execute a native UPDATE
     *
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int applyNativeUpdate(String customQuery) {
        return applyNativeUpdate(customQuery, null);
    }

    /**
     * Executes a UPDATE
     *
     * @param params Parameters for update
     * @param customQuery Update to execute
     * @return Total of updated files
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public int applyNativeUpdate(String customQuery, Map params) {
        Query query = entityManager.createNativeQuery(customQuery);
        setQueryParameters(query, params);
        return query.executeUpdate();
    }

    /**
     * Updates the bean
     *
     * @param bean Entity
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(T bean) {
        if (bean.getCreated() == null) {
            T storedBean = this.findById(bean.getId());
            bean.setCreated(storedBean.getCreated());
        }
        entityManager.merge(bean);
        entityManager.flush();
    }

    /**
     * Saves the bean
     *
     * @param bean Entity
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PK save(T bean) {
        return create(bean).getId();
    }

    /**
     * Creates the bean
     *
     * @param bean Entity
     */
    @Override
    public T create(T bean) {
        entityManager.persist(bean);
        entityManager.flush();
        return bean;
    }

    /**
     * Saves the bean (if exists, otherwise it creates it)
     *
     * @param bean Entity
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PK merge(T bean) {
        T item = findById(bean.getId());
        if (item != null) {
            update(item);
            return item.getId();
        } else {
            return save(bean);
        }
    }

    /**
     * Clears the cache
     */
    @Override
    public void flush() {
        entityManager.flush();
    }

    /**
     * Deletes the bean
     *
     * @param id Bean ID
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(PK id) {
        T bean = findById(id);
        delete(bean);
    }

    /**
     * Deletes the bean
     *
     * @param bean Entity
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(T bean) {
        entityManager.remove(bean);
        flush();
    }

    /**
     * Deletes all beans that comply with filter
     *
     * @param filter Filter
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAll(Map filter) {

        List<T> res = findAll(filter);
        for (T current : res) {
            delete(current.getId());
        }
    }

    /**
     * Get generic type
     *
     * @return Type
     */
    @Override
    public Class<T> getType() {
        return type;
    }

    /**
     * Get table name for service
     *
     * @return Table name
     */
    @Override
    public String getTypeTableName() {
        Table table = type.getAnnotation(Table.class);
        String tableName = table.name();
        return tableName;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Gets a page of entities
     *
     * @param request Page request
     * @param query   Query to obtain the items
     * @param count   Query to count the items
     * @return Page of items
     */
    public Page<T> getPageResult(PageSearch request, Query query, Query count) {
        int limit = request.getPageSize();
        if (limit > 0) {
            query.setFirstResult((int) request.getOffset());
            query.setMaxResults(limit);
        }
        List<T> list = query.getResultList();
        List<Number> total = count.getResultList();
        int totalItems = total.size() > 0 ? total.get(0).intValue() : 0;
        Page<T> result = new PageImpl<T>(list, request, totalItems);
        return result;

    }

    /**
     * Get where condition for filter and extra query
     *
     * @param filter     Filter
     * @param extraQuery Extra query
     * @return Where condition
     */
    protected String getWhere(Map filter, String extraQuery) {
        if (filter != null && filter.containsKey(PageSearch.EXTENDED_SQLCOND)) {
            extraQuery = " (" + getExtendSQLCond((String) filter.get(PageSearch.EXTENDED_SQLCOND)) + (extraQuery == null ? "" : " and " + extraQuery) + ")";
            filter.remove(PageSearch.EXTENDED_SQLCOND);
        }

        String where = buildWhere(filter);
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(where) || !com.vincomobile.fw.basic.tools.Converter.isEmpty(extraQuery)) {
            where = " where " + where + (!com.vincomobile.fw.basic.tools.Converter.isEmpty(extraQuery) ? (com.vincomobile.fw.basic.tools.Converter.isEmpty(where) ? "" : " and ") + extraQuery : "");
        }

        return where;
    }

    /**
     * Obtains the filter conditions
     *
     * @param filters Filters
     * @return Filter condition (WHERE)
     */
    @Override
    public String buildWhere(Map filters) {
        StringBuilder query = new StringBuilder();
        if (filters != null) {
            Map filterCopy = new HashMap(filters);
            Iterator itKeys = filterCopy.entrySet().iterator();
            while (itKeys.hasNext()) {
                Map.Entry pairs = (Map.Entry) itKeys.next();
                String condition;
                if (pairs.getKey().equals("&") || pairs.getKey().equals("|"))
                    condition = buildWhere((Map) pairs.getValue(), pairs.getKey().toString());
                else
                    condition = buildWhereCriteria(filters, pairs);
                if (condition != null && condition.trim().length() > 0) {
                    if (query.length() > 0)
                        query.append(" and ");
                    query.append(condition);
                }
            }
        }
        return query.toString();
    }

    /**
     * Obtains the filter conditions
     *
     * @param filters  Filters
     * @param operator Operador & or |
     * @return Filter condition (WHERE)
     */
    protected String buildWhere(Map filters, String operator) {
        StringBuilder query = new StringBuilder();
        if (filters != null) {
            query.append("(");
            Map filterCopy = new HashMap(filters);
            Iterator itKeys = filterCopy.entrySet().iterator();
            while (itKeys.hasNext()) {
                Map.Entry pairs = (Map.Entry) itKeys.next();
                String condition = buildWhereCriteria(filters, pairs);
                if (condition != null && condition.trim().length() > 0) {
                    if (operator.equals("&"))
                        query.append(" and ");
                    else if (operator.equals("|"))
                        query.append(" or ");
                    query.append(condition);
                }
            }
            query.append(")");
        }
        return query.toString();
    }

    /**
     * Load cache information for a table
     *
     * @param table Table
     * @return Cache information
     */
    private CacheTable loadCacheTable(AdTable table) {
        boolean groupedsearch = false;
        List<AdColumn> columns = table.getColumns();
        String search = "upper(concat(";
        for (AdColumn column : columns) {
            // Grouped search
            if (column.getGroupedsearch() && AdColumnService.SOURCE_DATABASE.equals(column.getCsource())) {
                if (groupedsearch)
                    search += ", ";
                groupedsearch = true;
                search += "coalesce(" + column.getStandardName() + ", '')";
            }
        }
        CacheTable cacheTable = new CacheTable();
        cacheTable.setSearch(groupedsearch ? search + ")) like upper(:search)" : "");
        cacheTable.setColumns(columns);
        cacheTable.setTable(table);
        CacheManager.putTable(table.getName(), cacheTable);
        return cacheTable;
    }

    /**
     * Get table information from cache. If not found info is loaded
     *
     * @param tableName Table name
     * @return Cache information
     */
    @Override
    public CacheTable getCacheTable(String tableName) {
        CacheTable data = CacheManager.getTable(tableName);
        if (data == null) {
            Query query = entityManager.createQuery("from AdTable where name = :tableName", AdTable.class);
            query.setParameter("tableName", tableName);
            data = loadCacheTable((AdTable) query.getSingleResult());
        }
        return data;
    }

    public CacheTable getCacheTable() {
        return getCacheTable(getTypeTableName());
    }

    /**
     * Get table information from cache. If not found info is loaded
     *
     * @param idTable Table identifier
     * @return Cache information
     */
    @Override
    public CacheTable getCacheTableById(String idTable) {
        CacheTable data = CacheManager.getTableById(idTable);
        if (data == null) {
            Query query = entityManager.createQuery("from AdTable where idTable = :idTable", AdTable.class);
            query.setParameter("idTable", idTable);
            data = loadCacheTable((AdTable) query.getSingleResult());
        }
        return data;
    }

    /**
     * Get search criteria for table
     *
     * @param filters Search criteria
     * @param key Key field
     * @return Search criteria
     */
    @Override
    public String getSearchCriteria(Map filters, String key) {
        CacheTable data = getCacheTable(getTypeTableName());
        return data.getSearch();
    }

    /**
     * Parse extended condition to find pattern "@(idReference;value).field@"
     *
     * @param cond Condition
     * @return Condition with pattern replaced
     */
    private String getExtendSQLCond(String cond) {
        Matcher matcherAt = patternAt.matcher(cond);
        if (matcherAt.find()) {
            String referenceInfo = cond.substring(matcherAt.start() + 1, matcherAt.end() - 1);
            Matcher matcherParenthesis = patternParenthesis.matcher(referenceInfo);
            if (matcherParenthesis.find()) {
                String referenceValue = referenceInfo.substring(matcherParenthesis.start() + 1, matcherParenthesis.end() - 1);
                String[] values = referenceValue.split(";");
                TypedQuery<AdRefTable> queryRefTable = (TypedQuery<AdRefTable>) entityManager.createQuery("from AdRefTable where idReference = :idReference");
                queryRefTable.setParameter("idReference", values[0]);
                List<AdRefTable> refTableList = queryRefTable.getResultList();
                if (!refTableList.isEmpty()) {
                    AdRefTable refTable = refTableList.get(0);
                    AdTable table = refTable.getTable();
                    for (AdColumn column : table.getColumns()) {
                        if (column.getIdColumn().equals(refTable.getIdKey())) {
                            Query query = entityManager.createQuery(
                                    "SELECT " + referenceInfo.substring(matcherParenthesis.end() + 1) +
                                    " FROM " + refTable.getTable().getCapitalizeStandardName() +
                                    " WHERE " + column.getStandardName() + " = :value");
                            query.setParameter("value", values[1]);
                            Object value = query.getSingleResult();
                            return cond.replaceAll(patternAt.pattern(), value.toString());
                        }
                    }
                }
            }
        }
        return cond;
    }

    /**
     * Gets the associated runtime type used in generics
     *
     * @return Associated type class
     */
    @Override
    public Class getAssociatedType() {
        return type.getClass();
    }

    /**
     * Get client filter
     *
     * @param idClient Client identifier
     * @return ['0', idCLient]
     */
    @Override
    public List<String> getClientFilter(String idClient) {
        List<String> clients = new ArrayList<>();
        clients.add("0");
        if (!com.vincomobile.fw.basic.tools.Converter.isEmpty(idClient)) {
            clients.add(idClient);
        }
        return clients;
    }

    /**
     * Replace the parameters in the SQL
     *
     * @param idClient Client identifier
     * @param value Value parameters to replace
     * @param param_1 Parameter extra 1
     * @param param_2 Parameter extra 2
     * @param param_3 Parameter extra 3
     * @param param_4 Parameter extra 4
     * @param param_5 Parameter extra 5
     * @return Values replaced
     */
    @Override
    public String replaceParameters(String idClient, String value, String param_1, String param_2, String param_3, String param_4, String param_5) {
        if (value == null)
            return null;
        value = value.replaceAll("@1", param_1).replaceAll("@2", param_2).replaceAll("@3", param_3).replaceAll("@4", param_4).replaceAll("@5", param_5);
        if (value.indexOf("@CLIENT") > 0) {
            value = value.replaceAll("@CLIENT", idClient);
        }
        if (value.indexOf("@TODAY") > 0) {
            value = value.replaceAll("@TODAY", com.vincomobile.fw.basic.tools.Converter.getSQLDate(new Date()));
        }
        if (value.indexOf("@NOW") > 0) {
            value = value.replaceAll("@NOW", com.vincomobile.fw.basic.tools.Converter.getSQLDateTime(new Date(), false));
        }
        if (value.indexOf("@YESTERDAY") > 0 || value.indexOf("@YEAR") > 0 || value.indexOf("@MONTH") > 0 || value.indexOf("@DAY") > 0) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            if (value.indexOf("@YESTERDAY") > 0) {
                cal.add(Calendar.DAY_OF_YEAR, -1);
                value = value.replaceAll("@YESTERDAY", com.vincomobile.fw.basic.tools.Converter.getSQLDate(cal.getTime()));
            }
            if (value.indexOf("@YEAR") > 0) {
                value = value.replaceAll("@YEAR", ""+cal.get(Calendar.YEAR));
            }
            if (value.indexOf("@MONTH") > 0) {
                value = value.replaceAll("@MONTH", ""+(cal.get(Calendar.MONTH)+1));
            }
            if (value.indexOf("@DAY") > 0) {
                value = value.replaceAll("@DAY", ""+cal.get(Calendar.DAY_OF_MONTH));
            }
        }
        value = AdBaseTools.replacePreference(idClient, value);
        return value;
    }

    /**
     * Get extended FROM
     *
     * @param qualifier Filter qualifier
     * @return Aditional tables for FROM clause
     */
    protected String getExtendedFrom(String qualifier) {
        String result = "";
        List<ExtendedFilter> extendedFilters = CacheManager.getExtendedFilter(qualifier);
        if (extendedFilters != null) {
            for (ExtendedFilter extendedFilter : extendedFilters) {
                result += extendedFilter.getExtraFrom();
            }
        }
        return result;
    }

    /**
     * Get extended FROM (for Native queries)
     *
     * @param qualifier Filter qualifier
     * @return Aditional tables for FROM clause
     */
    protected String getExtendedFromNative(String qualifier) {
        String result = "";
        List<ExtendedFilter> extendedFilters = CacheManager.getExtendedFilter(qualifier);
        if (extendedFilters != null) {
            for (ExtendedFilter extendedFilter : extendedFilters) {
                result += extendedFilter.getExtraFromNative();
            }
        }
        return result;
    }

    /**
     * Get extended filter conditions
     *
     * @param qualifier Filter qualifier
     * @return Aditional conditions for WHERE clause
     */
    protected String getExtendedWhere(String qualifier) {
        String result = "";
        List<ExtendedFilter> extendedFilters = CacheManager.getExtendedFilter(qualifier);
        if (extendedFilters != null) {
            for (ExtendedFilter extendedFilter : extendedFilters) {
                result += " AND " + extendedFilter.getExtraWhere();
            }
        }
        return result;
    }

    /**
     * Get extended filter conditions (for Native queries)
     *
     * @param qualifier Filter qualifier
     * @return Aditional conditions for WHERE clause
     */
    protected String getExtendedWhereNative(String qualifier) {
        String result = "";
        List<ExtendedFilter> extendedFilters = CacheManager.getExtendedFilter(qualifier);
        if (extendedFilters != null) {
            for (ExtendedFilter extendedFilter : extendedFilters) {
                result += " AND " + extendedFilter.getExtraWhereNative();
            }
        }
        return result;
    }

    /**
     * Gets the class of a property of an instance
     *
     * @param property Property
     * @param value    Instance to analise
     * @return Class
     */
    protected Class getPropertyClass(String property, Object value) {
        PropertyDescriptor desc = AdBaseTools.getPropertyDescription(type, property);
        Class result = desc == null ? value.getClass() : desc.getPropertyType();

        return result;
    }

    /**
     * Builds the condition for a field
     *
     * @param filters Filters
     * @param pairs   Field entry
     * @return Field condition string representation
     */
    protected String buildWhereCriteria(Map filters, Map.Entry pairs) {
        boolean not = false;
        String key = (String) pairs.getKey(), sqlCond = PageSearch.EQUAL_COND;
        Object value = pairs.getValue();
        if (key.contains("$")) {
            if (key.startsWith(PageSearch.NOT_NULL_COND)) {
                filters.remove(key);
                key = key.substring(PageSearch.NOT_NULL_COND.length());
                return key + " is not null";
            }
            if (key.startsWith(PageSearch.NULL_COND)) {
                filters.remove(key);
                key = key.substring(PageSearch.NULL_COND.length());
                return key + " is null";
            }
            for (int i = 0; i < PageSearch.SQL_COND.length; i++) {
                if (key.startsWith(PageSearch.SQL_COND[i])) {
                    sqlCond = PageSearch.SQL_SYMB_COND[i];
                    filters.remove(key);
                    key = key.substring(PageSearch.SQL_COND[i].length());
                    filters.put(key, value);
                    break;
                }
            }
        } else if (key.startsWith(PageSearch.NOT_EQUAL_COND)) {
            not = true;
            filters.remove(key);
            key = key.substring(PageSearch.NOT_EQUAL_COND.length());
            filters.put(key, value);
        }
        Class clazz = getPropertyClass(key, value);

        // Search if it is a key or an alias
        String alias = queryAlias.get(key);
        if (alias != null) {
            filters.remove(key);
            key = alias;
            clazz = getPropertyClass(alias, value);
        }

        String condition = buildWhereCriteria(filters, clazz, key, sqlCond, value);
        if (condition != null && condition.trim().length() > 0 && not) {
            condition = "not (" + condition + ")";
        }

        return condition;
    }

    /**
     * Builds the condition of a field after changing an alias.
     *
     * @param filters Filter
     * @param clazz   Class
     * @param key     Key
     * @param sqlCond SQL condition
     * @param value   Value
     * @return String representation
     */
    protected String buildWhereCriteria(Map filters, Class clazz, String key, String sqlCond, Object value) {
        String condition;
        if (Collection.class.isAssignableFrom(value.getClass())) {
            condition = buildWhereCriteriaObject(filters, clazz, key, sqlCond, value);
        } else if (String.class.isAssignableFrom(clazz)) {
            condition = buildWhereCriteriaString(filters, clazz, key, sqlCond, (String) value);
        } else {
            condition = buildWhereCriteriaObject(filters, clazz, key, sqlCond, value);
        }

        return condition;
    }


    protected String buildWhereCriteriaString(Map filters, Class clazz, String key, String sqlCond, String value) {
        if (value.contains(BaseService.QUERY_LIKE_OP)) {
            // Like
            return buildWhereCriteriaLike(filters, key);
        } else if (value.contains(BaseService.QUERY_BETWEEN_OP)) {
            // Date range
            return AdBaseTools.buildWhereCriteriaBetween(filters, clazz, key, value);
        } else {
            // Normal string
            return buildWhereCriteriaObject(filters, clazz, key, sqlCond, value);
        }
    }

    protected String buildWhereCriteriaObject(Map filters, Class clazz, String property, String sqlCond, Object value) {
        Object obj = AdBaseTools.convert(clazz, value);
        if (obj != null) {
            filters.put(property, obj);
        } else {
            return property + " is null";
        }

        if (value instanceof String && ((String) value).contains(BaseService.QUERY_BETWEEN_OP)) {
            return AdBaseTools.buildWhereCriteriaBetween(filters, clazz, property, (String) value);
        } else if (Collection.class.isAssignableFrom(obj.getClass())) {
            return property + " in :" + AdBaseTools.getKeyParam(property);
        } else {
            CacheTable data = getCacheTable(getTypeTableName());
            AdColumn column = data.getColumnByStandardName(property);
            if (column != null && column.isDateField()) {
                return "date(" + property + ")" + sqlCond + ":" + AdBaseTools.getKeyParam(property);
            } else {
                return property + sqlCond + ":" + AdBaseTools.getKeyParam(property);
            }
        }
    }

    protected String buildWhereCriteriaLike(Map filters, String key) {
        if (BaseService.FILTER_GROUPEDSEARCH.equals(key) || key.endsWith("." + BaseService.FILTER_GROUPEDSEARCH)) {
            return getSearchCriteria(filters, key);
        } else {
            return key + " like :" + AdBaseTools.getKeyParam(key);
        }
    }

    /**
     * Set query parameters
     *
     * @param query Query
     * @param params Parameters
     */
    private void setQueryParameters(Query query, Map params) {
        if (params != null) {
            Iterator itKeys = params.entrySet().iterator();
            while (itKeys.hasNext()) {
                Map.Entry pairs = (Map.Entry) itKeys.next();
                query.setParameter((String) pairs.getKey(), pairs.getValue());
            }
        }
    }
}
