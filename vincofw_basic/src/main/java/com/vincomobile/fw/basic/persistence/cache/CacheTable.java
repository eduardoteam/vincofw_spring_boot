package com.vincomobile.fw.basic.persistence.cache;

import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdTable;

import java.util.List;

public class CacheTable {

    String search;
    List<AdColumn> columns;
    AdTable table;

    public AdColumn getColumn(String name) {
        for (AdColumn column: columns) {
            if (column.getName().equals(name))
                return column;
        }
        return null;
    }

    public AdColumn getColumnByStandardName(String name) {
        for (AdColumn column: columns) {
            if (column.getStandardName().equals(name))
                return column;
        }
        return null;
    }

    /*
     * Get/Set methods
     */
    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public List<AdColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<AdColumn> columns) {
        this.columns = columns;
    }

    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }
}
