package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdBaseTools;
import com.vincomobile.fw.basic.persistence.beans.AdMultiSelectValue;
import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdField;
import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import com.vincomobile.fw.basic.persistence.model.AdTab;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Devtools.
 * Service of AD_FIELD
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdFieldServiceImpl extends BaseServiceImpl<AdField, String> implements AdFieldService {

    private final AdTabService tabService;
    private final AdColumnService columnService;
    private final AdRefTableService refTableService;

    /**
     * Constructor.
     *
     */
    public AdFieldServiceImpl(@Lazy AdTabService tabService, AdColumnService columnService, AdRefTableService refTableService) {
        super(AdField.class);
        this.tabService = tabService;
        this.columnService = columnService;
        this.refTableService = refTableService;
    }

    /**
     * Reset MultiSelect column for all field in Tab
     *
     * @param idTab Tab identifier
     */
    @Override
    public void resetMultiSelect(String idTab) {
        applyUpdate("UPDATE AdField SET multiSelect = 0 WHERE idTab = " + Converter.getSQLString(idTab, true));
    }

    /**
     * List values for multiselect field
     *
     * @param idClient Client identifier
     * @param field Field information
     * @param parentFieldId Parent field identifier
     * @param rowKey Parent rowkey
     * @return Multi select values
     */
    @Override
    public Page<AdMultiSelectValue> listMultiSelectValues(String idClient, AdField field, String parentFieldId, String rowKey) {
        AdColumn column = columnService.findById(field.getIdColumn());
        AdRefTable refTable = refTableService.getRefTableByReference(column.getIdReferenceValue());
        AdColumn keyCol = refTable.getKey();
        AdColumn displayCol = refTable.getDisplay();
        String sort = Converter.isEmpty(refTable.getSqlorderby()) ? displayCol.getName() : refTable.getSqlorderby();
        String sql = "SELECT " + keyCol.getName() + ", " + displayCol.getName() + " " +
                "FROM " + refTable.getTable().getName() + " " +
                "WHERE active = 1 AND id_client IN :idClient ORDER BY " + sort;
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("idClient", getClientFilter(idClient));
        query.setMaxResults(300);
        List<Object[]> qResult = query.getResultList();
        List<AdMultiSelectValue> result = new ArrayList<>();
        for (Object[] item : qResult) {
            result.add(new AdMultiSelectValue(AdBaseTools.getString(item, 0), AdBaseTools.getString(item, 1), false));
        }
        AdTab tab = tabService.findById(field.getIdTab());
        AdField parentField = findById(parentFieldId);
        sql = "SELECT " + keyCol.getName() + " " +
              "FROM " + tab.getTable().getName() + " " +
              "WHERE active = 1 AND id_client IN :idClient AND " + parentField.getColumn().getName() + " = :rowKey";
        query = entityManager.createNativeQuery(sql);
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("rowKey", rowKey);
        List<String> parentResult = query.getResultList();
        for (AdMultiSelectValue value : result) {
            value.setSelected(parentResult.contains(value.getId()));
        }
        return new PageImpl<>(result, new PageSearch(1, 300), result.size());
    }

}
