package com.vincomobile.fw.basic.business;

import java.util.List;

public class SortParam {

    String field;
    Long start;
    Long step;
    List<String> ids;

    public SortParam() {
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "SortParam {field:'" + field + "', start: " + start + ", step:" + step + ", ids:" + ids + "}";
    }
}
