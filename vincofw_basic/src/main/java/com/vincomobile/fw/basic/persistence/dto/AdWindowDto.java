package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdWindow;

public class AdWindowDto {

    private String idWindow;
    private String name;
    private String wtype;

    private AdTabMainDto mainTab;

    public AdWindowDto(AdWindow item, String name) {
        this.idWindow = item.getIdWindow();
        this.wtype = item.getWtype();
        this.name = name;
    }

    public String getIdWindow() {
        return idWindow;
    }

    public String getName() {
        return name;
    }

    public String getWtype() {
        return wtype;
    }

    public AdTabMainDto getMainTab() {
        return mainTab;
    }

    public void setMainTab(AdTabMainDto mainTab) {
        this.mainTab = mainTab;
    }
}

