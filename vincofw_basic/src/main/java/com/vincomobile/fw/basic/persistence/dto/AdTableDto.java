package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdColumn;
import com.vincomobile.fw.basic.persistence.model.AdTable;
import com.vincomobile.fw.basic.persistence.model.AdTableAction;

import java.util.ArrayList;
import java.util.List;

public class AdTableDto {
    private String idTable;
    private String idSortColumn;
    private String restPath;
    private String name;
    private String standardName;
    private String dataOrigin;
    private Boolean isview;
    private Long seqno;
    private Boolean sortable;
    private String privilege;
    private long version;

    private List<AdColumnDto> columns;
    private List<AdTableActionDto> actions;

    public AdTableDto(AdTable item) {
        if (item != null) {
            this.idTable = item.getIdTable();
            this.idSortColumn = item.getIdSortColumn();
            this.restPath = item.getModule().getRestPath();
            this.name = item.getName();
            this.standardName = item.getCapitalizeStandardName();
            this.dataOrigin = item.getDataOrigin();
            this.isview = item.getIsview();
            this.seqno = item.getSeqno();
            this.sortable = item.getSortable();
            this.privilege = item.getPrivilege();
            this.version = item.getVersion();
            this.columns = new ArrayList<>();
            this.actions = new ArrayList<>();
            for (AdColumn col : item.getColumns()) {
                this.columns.add(new AdColumnDto(col));
            }
            for (AdTableAction action : item.getActions()) {
                this.actions.add(new AdTableActionDto(action));
            }
        }
    }

    /*
     * Set/Get Methods
     */

    public String getIdTable() {
        return idTable;
    }

    public String getIdSortColumn() {
        return idSortColumn;
    }

    public String getRestPath() {
        return restPath;
    }

    public String getName() {
        return name;
    }

    public String getStandardName() {
        return standardName;
    }

    public String getDataOrigin() {
        return dataOrigin;
    }

    public Boolean getIsview() {
        return isview;
    }

    public Long getSeqno() {
        return seqno;
    }

    public Boolean getSortable() {
        return sortable;
    }

    public String getPrivilege() {
        return privilege;
    }

    public long getVersion() {
        return version;
    }

    public List<AdColumnDto> getColumns() {
        return columns;
    }

    public List<AdTableActionDto> getActions() {
        return actions;
    }
}

