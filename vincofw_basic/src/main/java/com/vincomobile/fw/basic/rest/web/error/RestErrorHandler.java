package com.vincomobile.fw.basic.rest.web.error;

import com.vincomobile.fw.basic.persistence.exception.FWBadRequestException;
import com.vincomobile.fw.basic.persistence.exception.FWResourceNotFoundException;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Locale;

/**
 * This class handles the exceptions thrown by our REST API.
 *
 * @author Petri Kainulainen
 */
@ControllerAdvice
public final class RestErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestErrorHandler.class);

    private static final String ERROR_CODE_ENTRY_NOT_FOUND = "entry.not.found";

    private final MessageSource messageSource;

    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Processes an error that occurs when the requested todo entry is not found.
     *
     * @param ex            The exception that was thrown when the todo entry was not found.
     * @param currentLocale The current locale.
     * @return An error object that contains the error code and message.
     */
    @ExceptionHandler(FWResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ErrorDTO handleEntryNotFound(FWResourceNotFoundException ex, Locale currentLocale) {
        LOGGER.error("Entry was not found by using id: {}", ex.getId(), ex);

        MessageSourceResolvable errorMessageRequest = createSingleErrorMessageRequest(
                ERROR_CODE_ENTRY_NOT_FOUND,
                ex.getId()
        );

        String errorMessage = "ERROR_CODE_ENTRY_NOT_FOUND";
        try {
            errorMessage = messageSource.getMessage(errorMessageRequest, currentLocale);
        }
        catch(Throwable e) {}
        return new ErrorDTO(HttpStatus.NOT_FOUND.name(), errorMessage);
    }

    /**
     *  POST /some-resource with Content-Type as application/xml
     *      In this case the value of the Content-Type header is not the one that the service supports.
     *
     * @return An error object that contains the error code and message.
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    ControllerResult handleDefault(HttpMediaTypeNotSupportedException ex, Locale currentLocale) {
        return handleGeneric(ex, currentLocale);
    }

    /**
     GET /some-resource
     In this case the HTTP Method is not the one that the service supports.
     *
     * @return An error object that contains the error code and message.
     */
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    ControllerResult handleDefault(HttpMediaTypeNotAcceptableException ex, Locale currentLocale) {
        return handleGeneric(ex, currentLocale);
    }

    /**
     GET /some-resource
     In this case the HTTP Method is not the one that the service supports.
     *
     * @return An error object that contains the error code and message.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ResponseBody
    ControllerResult handleDefault(HttpRequestMethodNotSupportedException ex, Locale currentLocale) {
        return handleGeneric(ex, currentLocale);
    }

    private DefaultMessageSourceResolvable createSingleErrorMessageRequest(String errorMessageCode, Object... params) {
        return new DefaultMessageSourceResolvable(new String[]{errorMessageCode}, params);
    }

    /**
     * Processes an error that occurs when the validation of an object fails.
     *
     * @param ex The exception that was thrown when the validation failed.
     * @return An error object that describes all validation errors.
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ControllerResult handleGlobalErrors(Exception ex, Locale currentLocale) {
        return handleGeneric(ex, currentLocale);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ControllerResult handleGeneric(Throwable ex, Locale currentLocale) {
        LOGGER.error("Error : {}", ex.getMessage(), ex);
        ControllerResult dto = new ControllerResult();
        dto.setSuccess(false);
        dto.setError(ex.getMessage());
        LOGGER.error("Error in request", ex);
        //dto.put("exception", ex);

        return dto;
    }

    /**
     * Processes an error that occurs when the validation of an object fails.
     *
     * @param ex    The exception that was thrown when the validation failed.
     * @return      An error object that describes all validation errors.
     */
    @ExceptionHandler(FWBadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO handleValidationErrors(FWBadRequestException ex, Locale currentLocale) {
        return constructValidationErrors(ex.getBindingResult(), currentLocale);
    }

    /**
     * Processes an error that occurs when the validation of an object fails.
     *
     * @param ex The exception that was thrown when the validation failed.
     * @return An error object that describes all validation errors.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDTO handleValidationErrors(MethodArgumentNotValidException ex, Locale currentLocale) {
        return constructValidationErrors(ex.getBindingResult(), currentLocale);
    }

    private ValidationErrorDTO constructValidationErrors(BindingResult result, Locale currentLocale) {
        List<FieldError> fieldErrors = result.getFieldErrors();
        List<ObjectError> objectErrors = result.getGlobalErrors();
        LOGGER.error("Found {} validation errors and {} global errors", fieldErrors.size(), objectErrors.size());

        ValidationErrorDTO dto = new ValidationErrorDTO();

        for (FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = getValidationErrorMessage(fieldError, currentLocale);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        for (ObjectError objectError: objectErrors) {
            dto.addObjectError(objectError.getObjectName(), objectError.getDefaultMessage());
        }

        return dto;
    }

    private String getValidationErrorMessage(FieldError fieldError, Locale currentLocale) {
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);

        //If the message was not found, return the most accurate field error code instead.
        //You can remove this check if you prefer to get the default error message.
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0] + "." + fieldError.getDefaultMessage();
        }

        return localizedErrorMessage;
    }
}
