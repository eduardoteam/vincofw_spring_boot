package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class AdEntityBean extends EntityBean<String> {

    protected String idClient;
    protected String idModule;

    protected AdClient client;
    protected AdModule module;

    @Column(name = "id_client")
    @NotNull
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_module")
    @NotNull
    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_client", referencedColumnName = "id_client", insertable = false, updatable = false)
    @JsonIgnore
    public AdClient getClient() {
        return client;
    }

    public void setClient(AdClient client) {
        this.client = client;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_module", referencedColumnName = "id_module", insertable = false, updatable = false)
    @JsonIgnore
    public AdModule getModule() {
        return module;
    }

    public void setModule(AdModule module) {
        this.module = module;
    }

    /**
     * Equals implementation
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        final AdEntityBean that = (AdEntityBean) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idModule == null) && (that.idModule == null)) || (idModule != null && idModule.equals(that.idModule)));
        return result;
    }

}
