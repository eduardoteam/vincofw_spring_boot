package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdRefButton;

public class AdRefButtonDto {

    private String btype;
    private String idProcess;
    private String jsCode;
    private String icon;
    private String showMode;

    public AdRefButtonDto(AdRefButton item) {
        this.btype = item.getBtype();
        this.idProcess = item.getIdProcess();
        this.jsCode = item.getJsCode();
        this.icon = item.getIcon();
        this.showMode = item.getShowMode();
    }

    public String getBtype() {
        return btype;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public String getJsCode() {
        return jsCode;
    }

    public String getIcon() {
        return icon;
    }

    public String getShowMode() {
        return showMode;
    }
}
