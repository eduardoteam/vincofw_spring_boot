package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_ref_button
 *
 * Date: 27/11/2015
 */
@Entity
@Table(name = "ad_ref_button")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRefButton extends AdEntityBean {

    private String idRefButton;
    private String idReference;
    private String btype;
    private String idProcess;
    private String jsCode;
    private String icon;
    private String showMode;

    private AdProcess process;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRefButton;
    }

    @Override
    public void setId(String id) {
        this.idRefButton = id;
    }

    @Id
    @Column(name = "id_ref_button")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRefButton() {
        return idRefButton;
    }

    public void setIdRefButton(String idRefButton) {
        this.idRefButton = idRefButton;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "btype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getBtype() {
        return btype;
    }

    public void setBtype(String btype) {
        this.btype = btype;
    }

    @Column(name = "id_process")
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "js_code", length = 100)
    @Size(min = 1, max = 100)
    public String getJsCode() {
        return jsCode;
    }

    public void setJsCode(String jsCode) {
        this.jsCode = jsCode;
    }

    @Column(name = "icon", length = 50)
    @Size(min = 1, max = 50)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "show_mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getShowMode() {
        return showMode;
    }

    public void setShowMode(String showMode) {
        this.showMode = showMode;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_process", referencedColumnName = "id_process", insertable = false, updatable = false)
    public AdProcess getProcess() {
        return process;
    }

    public void setProcess(AdProcess process) {
        this.process = process;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRefButton)) return false;

        final AdRefButton that = (AdRefButton) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRefButton == null) && (that.idRefButton == null)) || (idRefButton != null && idRefButton.equals(that.idRefButton)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((btype == null) && (that.btype == null)) || (btype != null && btype.equals(that.btype)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((jsCode == null) && (that.jsCode == null)) || (jsCode != null && jsCode.equals(that.jsCode)));
        result = result && (((icon == null) && (that.icon == null)) || (icon != null && icon.equals(that.icon)));
        result = result && (((showMode == null) && (that.showMode == null)) || (showMode != null && showMode.equals(that.showMode)));
        return result;
    }

}

