package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_REF_LIST
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_ref_list")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRefList extends AdEntityBean {

    private String idRefList;
    private String idReference;
    private String value;
    private String name;
    private String extraInfo;
    private Long seqno;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRefList;
    }

    @Override
    public void setId(String id) {
            this.idRefList = id;
    }

    @Id
    @Column(name = "id_ref_list")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRefList() {
        return idRefList;
    }

    public void setIdRefList(String idRefList) {
        this.idRefList = idRefList;
    }

    @Column(name = "id_reference")
    @NotNull
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "value", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "name", length = 250)
    @NotNull
    @Size(min = 1, max = 250)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "extra_info", length = 500)
    @Size(min = 1, max = 500)
    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRefList)) return false;

        final AdRefList that = (AdRefList) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRefList == null) && (that.idRefList == null)) || (idRefList != null && idRefList.equals(that.idRefList)));
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((value == null) && (that.value == null)) || (value != null && value.equals(that.value)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((extraInfo == null) && (that.extraInfo == null)) || (extraInfo != null && extraInfo.equals(that.extraInfo)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        return result;
    }

}

