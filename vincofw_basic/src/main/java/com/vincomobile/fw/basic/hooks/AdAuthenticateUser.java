package com.vincomobile.fw.basic.hooks;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.persistence.services.AdUserService;
import com.vincomobile.fw.basic.session.ApplicationUser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("AdAuthenticateUserFW")
@Qualifier("hookBase")
public class AdAuthenticateUser extends Hook {

    private final AdUserService userService;

    public AdAuthenticateUser(AdUserService userService) {
        this.userService = userService;
        name = FWConfig.HOOK_AUTHENTICATE_USER;
        packageName = "[VincoFW]";
    }

    /**
     * Execute a hook
     *
     * @param result    Result execution
     * @param arguments Arguments (username)
     */
    @Override
    public void execute(HookResult result, Object... arguments) {
        logger.info(packageName + " Execute hook: " + name);
        if (result.hasKey(FWConfig.HOOK_AUTHENTICATE_USER_APPUSER))
            return;
        String username = (String) arguments[0];
        AdUser user = userService.findByLogin(username);
        if (user != null) {
            ApplicationUser applicationUser = new ApplicationUser();
            applicationUser.setId(user.getIdUser());
            applicationUser.setName(user.getName());
            applicationUser.setPassword(user.getPassword());
            applicationUser.setEmail(user.getEmail());
            applicationUser.setDefaultIdRole(user.getDefaultIdRole());
            result.put(FWConfig.HOOK_AUTHENTICATE_USER_APPUSER, applicationUser);
        }
    }

}
