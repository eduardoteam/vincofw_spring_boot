package com.vincomobile.fw.basic.persistence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.PRECONDITION_REQUIRED )
public final class FWPreconditionRequiredException extends RuntimeException {

    public FWPreconditionRequiredException() {
        super();
    }

    public FWPreconditionRequiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWPreconditionRequiredException(final String message) {
        super(message);
    }

    public FWPreconditionRequiredException(final Throwable cause) {
        super(cause);
    }

}
