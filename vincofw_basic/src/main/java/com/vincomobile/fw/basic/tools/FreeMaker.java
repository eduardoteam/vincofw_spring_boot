package com.vincomobile.fw.basic.tools;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.util.Map;

public class FreeMaker {

    /**
     * Genera un Template de una tabla
     *
     * @param template Nombre del template
     * @param options Opciones de generacion
     * @return Salida del template
     */
    public static String genFreeMaker(String template, Map options) {
        File templateFile = new File(template);
        if (templateFile.exists()) {
            try {
                Configuration cfg = new Configuration();
                cfg.setDirectoryForTemplateLoading(new File(templateFile.getParent()));
                cfg.setObjectWrapper(new DefaultObjectWrapper());
                ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
                OutputStreamWriter out = new OutputStreamWriter(outBytes);
                Template temp = cfg.getTemplate(templateFile.getName());
                temp.process(options, out);
                out.flush();
                return outBytes.toString();
            } catch (Exception e) {
                Logger logger = LoggerFactory.getLogger(FreeMaker.class);
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return "";
    }


}
