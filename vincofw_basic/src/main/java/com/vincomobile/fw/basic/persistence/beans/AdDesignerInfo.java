package com.vincomobile.fw.basic.persistence.beans;

import java.util.List;

public class AdDesignerInfo {
    Long columns;
    List<AdDesignerField> fields;

    public Long getColumns() {
        return columns;
    }

    public void setColumns(Long columns) {
        this.columns = columns;
    }

    public List<AdDesignerField> getFields() {
        return fields;
    }

    public void setFields(List<AdDesignerField> fields) {
        this.fields = fields;
    }
}
