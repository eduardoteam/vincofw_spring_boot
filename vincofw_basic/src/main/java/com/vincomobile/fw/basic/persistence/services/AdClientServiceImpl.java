package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.model.AdClient;
import com.vincomobile.fw.basic.tools.Mailer;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Devtools.
 * Service of AD_CLIENT
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdClientServiceImpl extends BaseServiceImpl<AdClient, String> implements AdClientService {

    /**
     * Constructor.
     *
     */
    public AdClientServiceImpl() {
        super(AdClient.class);
    }

    /**
     * Change mailer configuration
     *
     * @param idClient Client identifier
     * @param mailer Mailer
     * @param customSender Standard mail sender
     */
    @Override
    public void setMailerConfig(String idClient, Mailer mailer, JavaMailSenderImpl customSender) {
        AdClient client = findById(idClient);
        customSender.setUsername(client.getSmtpUser());
        customSender.setPassword(client.getSmtpPassword());
        customSender.setHost(client.getSmtpHost());
        customSender.setPort(client.getSmtpPort().intValue());
        mailer.setSenderConfigSSL(customSender, client.getSmtpSsl());
    }

    /**
     * Get all active clients (exclude client: *)
     *
     * @return Client list
     */
    @Override
    public List<AdClient> getActiveClients() {
        Query query = entityManager.createQuery("FROM AdClient WHERE active = 1 AND idClient <> '0'", AdClient.class);
        return query.getResultList();
    }

    /**
     * Get client GUI
     *
     * @param idClient Client identifier
     * @return GUI
     */
    @Override
    public AdGUIDto getClientGUI(String idClient) {
        AdGUIDto gui = CacheManager.getGUI(idClient);
        if (gui == null) {
            gui = new AdGUIDto(findById(idClient));
            CacheManager.putGUI(idClient, gui);
        }
        return gui;
    }
}
