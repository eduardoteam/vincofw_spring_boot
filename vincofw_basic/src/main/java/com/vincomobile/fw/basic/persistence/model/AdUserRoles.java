package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Devtools.
 * Modelo de AD_USER_ROLES
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_user_roles")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdUserRoles extends AdEntityBean {

    private String idUserRoles;
    private String idRole;
    private String idUser;

    private AdRole role;
    private AdUser user;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idUserRoles;
    }

    @Override
    public void setId(String id) {
            this.idUserRoles = id;
    }

    @Id
    @Column(name = "id_user_roles")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdUserRoles() {
        return idUserRoles;
    }

    public void setIdUserRoles(String idUserRoles) {
        this.idUserRoles = idUserRoles;
    }


    @Column(name = "id_role")
    @NotNull
    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    @Column(name = "id_user")
    @NotNull
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_role", referencedColumnName = "id_role", insertable = false, updatable = false)
    @JsonIgnore
    public AdRole getRole() {
        return role;
    }

    public void setRole(AdRole role) {
        this.role = role;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id_user", insertable = false, updatable = false)
    @JsonIgnore
    public AdUser getUser() {
        return user;
    }

    public void setUser(AdUser user) {
        this.user = user;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdUserRoles)) return false;

        final AdUserRoles that = (AdUserRoles) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idUserRoles == null) && (that.idUserRoles == null)) || (idUserRoles != null && idUserRoles.equals(that.idUserRoles)));
        result = result && (((idRole == null) && (that.idRole == null)) || (idRole != null && idRole.equals(that.idRole)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        return result;
    }

}

