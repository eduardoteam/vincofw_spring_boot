package com.vincomobile.fw.basic.persistence.services;

public class SqlSortItem {
    String sort, dir;

    public SqlSortItem(String sort, String dir) {
        this.sort = sort;
        this.dir = dir;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
