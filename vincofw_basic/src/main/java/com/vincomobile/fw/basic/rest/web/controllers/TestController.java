package com.vincomobile.fw.basic.rest.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/vinco_core/test")
public class TestController {

    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @RequestMapping("/welcome")
    public String hello(Model model, HttpServletRequest request) {
        logger.debug("/vinco_core/test/welcome");
        HttpSession session = request.getSession(false);
        if ( session == null || session.isNew() ) {
            session = request.getSession(true);
        }
        model.addAttribute("msg", "Welcome Friends!");
        model.addAttribute("sessionId", session.getId());
        model.addAttribute("userAgent", request.getHeader("User-Agent"));
        return "test";
    }

}
