package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdMessage;

/**
 * Created by Devtools.
 * Interface of service of AD_MESSAGE
 *
 * Date: 19/02/2015
 */
public interface AdMessageService extends BaseService<AdMessage, String> {

    /**
     * Get translate message
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param value Message search key
     * @param params Message parameters
     * @return Translate message
     */
    String getMessage(String idClient, String idLanguage, String value, Object... params);
    String getMessage(String idLanguage, String value);
    String getMessage(String value);

    /**
     * Get translate message with default value
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param value Message search key
     * @param defValue Default value
     * @param params Message parameters
     * @return Translate message
     */
    String getMessageDefault(String idClient, String idLanguage, String value, String defValue, Object... params);
    String getMessageDefault(String idLanguage, String value, String defValue);
    String getMessageDefault(String value, String defValue);

    /**
     * Get message
     *
     * @param idClient Client identifier
     * @param value Message search key
     * @return AdMessage
     */
    AdMessage findMessage(String idClient, String value);
}


