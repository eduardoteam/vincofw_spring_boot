package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.persistence.model.AdModule;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_MODULE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdModuleServiceImpl extends BaseServiceImpl<AdModule, String> implements AdModuleService {

    /**
     * Constructor.
     *
     */
    public AdModuleServiceImpl() {
        super(AdModule.class);
    }

    /**
     * Get first module for client
     *
     * @param idClient Client identifier
     * @return Module
     */
    @Override
    public AdModule getClientModule(String idClient) {
        Map filter = new HashMap();
        filter.put("idClient", idClient);
        return findFirst(filter);
    }

    /**
     * Validate module prefix
     *
     * @param idModule Module identifier
     * @param value Value to check
     * @return Validation errors
     */
    @Override
    public List<ValidationError> validateModulePrefix(String idModule, String value) {
        List<ValidationError> result = new ArrayList<>();
        AdModule module = findById(idModule);
        if (module != null) {
            if (!value.toUpperCase().startsWith(module.getDbprefix().toUpperCase()+"_")) {
                result.add(new ValidationError(ValidationError.TYPE_FIELD, "value", ValidationError.CODE_CHECK_VALUE, "AD_ValidationErrorModulePrefix"));
            }
        } else {
            result.add(new ValidationError(ValidationError.TYPE_GLOBAL, "", ValidationError.CODE_MESSAGE, "AD_ValidationErrorModule"));
        }
        return result;
    }

}
