package com.vincomobile.fw.basic.rest.web.controllers;

import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.business.SortParam;
import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.hooks.HookResult;
import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.cache.CacheTable;
import com.vincomobile.fw.basic.persistence.exception.FWBadRequestException;
import com.vincomobile.fw.basic.persistence.model.*;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import com.vincomobile.fw.basic.rest.web.error.CustomError;
import com.vincomobile.fw.basic.rest.web.error.ValidationFieldError;
import com.vincomobile.fw.basic.rest.web.tools.ControllerResult;
import com.vincomobile.fw.basic.rest.web.tools.RestPreconditions;
import com.vincomobile.fw.basic.tools.Converter;
import com.vincomobile.fw.basic.tools.FileTool;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.svenson.JSONParser;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public abstract class BaseController<T extends EntityBean, PK extends Serializable> {
    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    protected static final String ERROR_USER_INVALID       = "InvalidUser";
    protected static final String ERROR_UPLOAD_FILE        = "UploadFile";
    protected static final String ERROR_UPLOAD_FILE_TYPE   = "UploadFileType";
    protected static final String ERROR_UPLOAD_FILE_SIZE   = "UploadFileSize";
    protected static final long ERROR_IMAGE_PROPORTION     = -1L;
    protected static final long ERROR_IMAGE_RESIZE         = -2L;
    protected static final long ERROR_IMAGE_UPLOAD         = -3L;
    protected static final long ERROR_FILE_UPLOAD          = -4L;

    @Autowired
    protected AdExportService exportService;

    @Autowired
    protected AdUserService userService;

    @Autowired
    protected AdTranslationService translationService;

    @Autowired
    protected AdPreferenceService preferenceService;

    @Autowired
    protected AdAuditService auditService;

    @Autowired
    protected AdApprovalService approvalService;

    /**
     * Get main service for controller
     *
     * @return Service
     */
    public abstract BaseService getService();

    /**
     * Write Audit information for a table
     *
     * @param idTable Table identifier
     * @param idClient Client identifier
     * @param idRecord Record identifier
     * @param oldEntity Old entity (Json)
     * @param newEntity New entity (Json)
     * @param operation Update/Create/Delete Operation
     */
    private void writeAudit(String idTable, String idClient, String idRecord, String oldEntity, String newEntity, String operation) {
        AdUser user = getAuthUser();
        if (idClient != null && user != null) {
            AdAudit audit = new AdAudit();
            audit.setIdClient(idClient);
            audit.setActive(true);
            audit.setOperation(operation);
            audit.setIdTable(idTable);
            audit.setIdRecord(idRecord);
            audit.setIdUser(user.getIdUser());
            audit.setCreatedBy(user.getIdUser());
            audit.setUpdatedBy(user.getIdUser());
            audit.setOldValue(oldEntity);
            audit.setNewValue(newEntity);
            auditService.save(audit);
        }
    }

    /**
     * Remove approvals for entity
     *
     * @param entity Entity
     */
    private void removeApprovals(EntityBean entity) {
        CacheTable data = getService().getCacheTable();
        if (data != null) {
            getService().applyUpdate(
                    "DELETE FROM AdApproval " +
                    "WHERE action = 'TABLE' AND idTable = '" + data.getTable().getIdTable() + "' AND rowkey = '" + entity.getIdStr() + "'"
            );
        }
    }

    /**
     * Write entity to database
     *
     * @param entity Entity bean
     */
    private void writeEntity(EntityBean entity) {
        try {
            validateEntity(entity);
            String oldItem = null;
            CacheTable data = getService().getCacheTable();
            if (data.getTable().getAudit()) {
                oldItem = getService().findById(entity.getId()).getLightJson();
            }
            getService().update(entity);
            if (data.getTable().getAudit()) {
                writeAudit(data.getTable().getIdTable(), (String) entity.getPropertyValue("idClient"), entity.getIdStr(), oldItem, entity.getLightJson(), "U");
            }
            approvalService.saveApprovals(getAuthUser(), entity.getApprovals());
        } catch (DataIntegrityViolationException ex) {
            errorHandler(ex, entity);
        } catch (ConstraintViolationException ex) {
            errorHandler(ex, entity);
        }
    }

    /**
     * Update entity and check errors
     *
     * @param entity Entity bean
     */
    protected void updateEntity(EntityBean entity) {
        RestPreconditions.checkRequestElementNotNull(entity);
        RestPreconditions.checkNotNull(getService().findById(entity.getId()));
        writeEntity(entity);
        AdUser user = getAuthUser();
        HookManager.executeHook(FWConfig.HOOK_DB_TABLE_ROW_UPDATE, user, entity);
    }

    /**
     * Update entity and check errors
     *
     * @param entity Entity bean
     * @param passwordColumn Name for password column
     */
    protected void updateEntityWithPassword(EntityBean entity, String passwordColumn) {
        RestPreconditions.checkRequestElementNotNull(entity);
        EntityBean stored = getService().findById(entity.getId());
        RestPreconditions.checkNotNull(stored);
        Object value = entity.getPropertyValue(passwordColumn);
        String password = value != null ? value.toString() : null;
        if (Converter.isEmpty(password)) {
            entity.setPropertyValue(passwordColumn, stored.getPropertyValue(passwordColumn));
        }
        writeEntity(entity);
    }

    /**
     * Create entity and check errors
     *
     * @param entity Entity bean
     * @return PrimaryKey
     */
    protected PK createEntity(EntityBean entity) {
        RestPreconditions.checkRequestElementNotNull(entity);
        PK result = null;
        try {
            validateEntity(entity);
            result = (PK) getService().save(entity);
            CacheTable data = getService().getCacheTable();
            if (data.getTable().getAudit()) {
                writeAudit(data.getTable().getIdTable(), (String) entity.getPropertyValue("idClient"), entity.getIdStr(), null, entity.getLightJson(), "I");
            }
            AdUser user = getAuthUser();
            approvalService.saveApprovals(user, entity.getApprovals());
            HookManager.executeHook(FWConfig.HOOK_DB_TABLE_ROW_CREATE, user, entity);
            return result;
        } catch (DataIntegrityViolationException ex) {
            errorHandler(ex, entity);
        } catch (ConstraintViolationException ex) {
            errorHandler(ex, entity);
        }
        return result;
    }

    /**
     * Delete entity and check errors
     *
     * @param id Bean primary key
     */
    protected void deleteEntity(PK id) {
        EntityBean entity = getService().findById(id);
        RestPreconditions.checkNotNull(entity);
        try {
            removeEntity(entity);
        } catch (DataIntegrityViolationException ex) {
            errorHandler(ex, entity);
        }
    }

    protected void deleteEntity(EntityBean entity) {
        try {
            removeEntity(entity);
        } catch (DataIntegrityViolationException ex) {
            errorHandler(ex, entity);
        }
    }

    private void removeEntity(EntityBean entity) {
        AdUser user = getAuthUser();
        HookManager.executeHook(FWConfig.HOOK_DB_TABLE_ROW_DELETE, user, entity);
        getService().delete(entity);
        CacheTable data = getService().getCacheTable();
        if (data.getTable().getAudit()) {
            writeAudit(data.getTable().getIdTable(), (String) entity.getPropertyValue("idClient"), entity.getIdStr(), entity.getLightJson(), null, "D");
        }
        removeApprovals(entity);
    }

    /**
     * Validate entity bean
     *
     * @param entity Bean
     */
    protected void validateEntity(EntityBean entity) {
        // Execute custom validations
        List<ValidationError> validationErrors = validate(entity);
        if (validationErrors != null && validationErrors.size() > 0) {
            BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(entity, entity.getClass().getSimpleName());
            for (ValidationError error : validationErrors) {
                if (ValidationError.TYPE_FIELD.equals(error.getType())) {
                    bindingResult.rejectValue(Converter.uncapitalize(error.getField()), error.getCode(), error.getMessage());
                } else {
                    bindingResult.addError(new ObjectError(error.getCode(), error.getMessage()));
                }
            }
            throw new FWBadRequestException(bindingResult);
        }
        // Validate approvals
        CacheTable table = getService().getCacheTable();
        if (table != null && !Converter.isEmpty(table.getTable().getPrivilege())) {
            String privilege = CacheManager.getPrivilegeName(table.getTable().getPrivilege());
            if (privilege != null && !userHasPrivilege(privilege)) {
                List<AdApproval> approvals = entity.getApprovals();
                if (approvals != null) {
                    for (AdApproval approval : approvals) {
                        if (privilege.equals(approval.getPrivilege())) {
                            return;
                        }
                    }
                }
                BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(entity, entity.getClass().getSimpleName());
                String desc = table.getTable().getPrivilegeDesc();
                bindingResult.addError(new ObjectError(FWConfig.APPROVAL_NEED, privilege + "$" + desc));
                throw new FWBadRequestException(bindingResult);
            }
        }
        // Validate custom approvals
        approval(entity);
        // Update row audit
        AdUser user = getAuthUser();
        if (user != null) {
            if (entity.getCreatedBy() == null) {
                entity.setCreatedBy(user.getIdUser());
            }
            entity.setUpdatedBy(user.getIdUser());
        }
    }

    /**
     * Controller redefine this method to make custom approvals
     *
     * @param entity Entity
     * @throws FWBadRequestException Throw exception when need approval
     */
    protected void approval(EntityBean entity) throws FWBadRequestException {
    }

    /**
     * Controller redefine this method to make custom validations
     *
     * @param entity Entity
     * @return Null or empty is entity is valid
     */
    protected List<ValidationError> validate(EntityBean entity) {
        return null;
    }

    /**
     * Load translations
     *
     * @param beans Bean to translate
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     */
    public void loadTranslations(List<T> beans, String idClient, String idLanguage) {
        if (!Converter.isEmpty(idLanguage) && beans.size() > 0) {
            BaseService baseService = getService();
            CacheTable table = baseService.getCacheTable(baseService.getTypeTableName());
            List<AdColumn> adColumns = new ArrayList<>();
            for (AdColumn column : table.getColumns()) {
                if (column.getTranslatable()) {
                    adColumns.add(column);
                }
            }
            for (EntityBean bean: beans) {
                String key = bean.getIdStr();
                if (key != null) {
                    for (AdColumn column: adColumns) {
                        String translation = translationService.getTranslation(idClient, table.getTable().getIdTable(), column.getIdColumn(), idLanguage, key);
                        if (translation != null) {
                            bean.setPropertyValue(column.getCapitalizeStandardName(), translation);
                        }
                    }
                }
            }
        }
    }
    /**
     * Load translations
     *
     * @param bean Bean to translate
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     */
    public void loadTranslations(T bean, String idClient, String idLanguage) {
        ArrayList<T> beans = new ArrayList<>();
        beans.add(bean);
        loadTranslations(beans, idClient, idLanguage);
    }

    /**
     * Add client constraint to page
     *
     * @param idClient Client identifier
     * @param prefix Table prefix
     * @return Client constraint
     */
    public String getClientConstraint(String idClient, String prefix) {
        String filterName = "idClient";
        AdTable table = this.getService().getCacheTable().getTable();
        if (AdTableService.TABLESOURCE_SQL.equals(table.getDataOrigin())) {
            filterName = "id_client";
        }
        if (prefix != null)
            filterName = prefix + filterName;
        return userHasPrivilege(FWSecurityConstants.Privileges.CAN_SUPERADMIN) ? "" : "," + filterName + "=[0;" + idClient + "]";
    }

    public String getClientConstraint(String idClient) {
        return getClientConstraint(idClient, null);
    }

    /**
     * Add active constraint to page
     *
     * @return Active constraint
     */
    public String getActiveConstraint() {
        return ",active=1";
    }

    /**
     * Build sort information
     *
     * @param sort Info get from caller
     * @param prefix Prefix to add the fields
     */
    public Sort getSort(String sort, String prefix) {
        if (sort != null && sort.startsWith("[")) {
            List<Sort.Order> result = new ArrayList<>();
            List<HashMap> dataList = JSONParser.defaultJSONParser().parse(List.class, sort);
            for (HashMap map : dataList) {
                String property = (String) map.get("property");
                String direction = (String) map.get("direction");
                result.add(new Sort.Order("ASC".equalsIgnoreCase(direction) || "0".equals(direction) ? Sort.Direction.ASC : Sort.Direction.DESC, (prefix != null ? prefix : "") + property));
            }
            return Sort.by(result);
        }
        return Sort.by(Sort.Direction.ASC, "id");
    }

    public Sort getSort(String sort) {
        return getSort(sort, null);
    }

    /**
     * Build sort information
     *
     * @param field Field name (Full qualified)
     * @param dir Sort direction
     */
    public Sort getOrderBy(String field, String dir) {
        List<Sort.Order> result = new ArrayList<>();
        result.add(new Sort.Order("ASC".equalsIgnoreCase(dir) || "0".equals(dir) ? Sort.Direction.ASC : Sort.Direction.DESC, field));
        return Sort.by(result);
    }

    /**
     * Get authentificated user
     *
     * @return User
     */
    public AdUser getAuthUser() {
        try {
            AdUser user = null;
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String currentUserId = (String) auth.getPrincipal();
            if (!Converter.isEmpty(currentUserId)) {
                user = userService.findById(currentUserId);
                boolean fwUser = user != null;
                if (!fwUser) {
                    HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_USER_VALIDATE, currentUserId);
                    user = (AdUser) hookResult.values.get(FWConfig.HOOK_USER_VALIDATE_USER);
                }
            }
            RestPreconditions.checkRequestElementNotNull(user);
            return user;
        } catch (AccessDeniedException ex) {
            return null;
        }
    }

    public boolean hasAuthUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String currentUserId = (String) auth.getPrincipal();
        AdUser user = userService.findById(currentUserId);
        return user != null;
    }

    /**
     * Check if current user has a privileges
     *
     * @param privilege Privilege to be checked
     * @return User has or not the privilege
     */
    protected boolean userHasPrivilege(String privilege) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Collection privileges = auth.getAuthorities();
        if (!privileges.isEmpty()) {
            Iterator it = privileges.iterator();
            while (it.hasNext()) {
                SimpleGrantedAuthority grantedAuthority = (SimpleGrantedAuthority) it.next();
                if (privilege.equals(grantedAuthority.getAuthority()))
                    return true;
            }
        }
        return false;
    }

    /**
     * Validate user
     *
     * @param userService Users manager
     * @param username User
     * @param password Password
     * @param privileges Privileges
     * @return Success or fail
     */
    public static boolean validUser(AdUserService userService, String username, String password, String privileges) {
        AdUser user = userService.findByLogin(username);
        if (user != null) {
            try {
                if (user.getPassword().equals(password)) {
                    // Check privileges
                    final List<AdRolePriv> privilegesOfUser = userService.getPrivileges(user.getDefaultIdRole());
                    for (AdRolePriv privilege : privilegesOfUser) {
                        if (privileges.indexOf(privilege.getPrivilege().getName()) >= 0)
                            return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationFieldError processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    private ValidationFieldError processFieldErrors(List<FieldError> fieldErrors) {
        ValidationFieldError dto = new ValidationFieldError();

        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }

        return dto;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        String localizedErrorMessage = fieldError.getDefaultMessage();//messageSource.getMessage(fieldError, currentLocale);

        //If the message was not found, return the most accurate field error code instead.
        //You can remove this check if you prefer to get the default error message.
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }

        return localizedErrorMessage;
    }

    /**
     * Validate file
     *
     * @param source File from navegator
     * @param contentType Valid content type
     * @param maxSize Max size (KB)
     * @return ControllerResult
     */
    public static ControllerResult validFileUpload(MultipartFile source, String contentType, Long maxSize) {
        ControllerResult result = new ControllerResult();
        if (!FileTool.validContentType(source, contentType)) {
            result.setError(ERROR_UPLOAD_FILE_TYPE);
            result.put("value1", contentType);
        } else if (!FileTool.validSize(source, maxSize)) {
            result.setError(ERROR_UPLOAD_FILE_SIZE);
            result.put("value1", maxSize);
        }
        return result;
    }

    /**
     * Save file to file system: [ApacheServerCache]/files/[tableName]/[id]/[file]
     *
     * @param idClient Client identifier
     * @param tableName Table name
     * @param id Item identifier (Optional)
     * @param contentType File content type
     * @param maxSize Max file size
     * @param fileName File name
     * @param oldFilename Old file name
     * @param file Uploaded file
     * @return Controller result (filename property has saved file name)
     */
    public ControllerResult saveFile(String idClient, String tableName, String id, String contentType, Long maxSize, String fileName, String oldFilename, MultipartFile file) {
        ControllerResult result = validFileUpload(file, contentType, maxSize != null ? maxSize : 20000L);
        if (result.isSuccess()) {
            AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
            if (baserDir != null) {
                String fileDir = baserDir.getString() + "/files/" + tableName + "/" + (id == null ? "" : id + "/");
                File fileImg = new File(fileDir + fileName);
                boolean res = FileTool.saveFile(file, fileImg.getPath()) != null;
                if (res) {
                    if (!Converter.isEmpty(oldFilename) && !oldFilename.equals(fileName)) {
                        FileTool.deleteFile(fileDir + oldFilename);
                    }
                    result.put("filename", fileName);
                } else {
                    logger.error("Can not save file: " + fileImg.getAbsolutePath());
                    result.setError(ERROR_UPLOAD_FILE);
                }
            } else {
                logger.error("Not found base directory (Preference: " + AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE + ")");
                result.setError(ERROR_UPLOAD_FILE);
            }
        }
        return result;
    }

    public ControllerResult saveFile(String idClient, String tableName, String id, String contentType, Long maxSize, String oldFilename, MultipartFile file) {
        String fileName = FileTool.cleanFileName(file.getOriginalFilename());
        return saveFile(idClient, tableName, id, contentType, maxSize, fileName, oldFilename, file);
    }

    /**
     * Move a file loaded from framework front web
     *
     * @param idClient Client identifier
     * @param tableName Table name
     * @param field Field name
     * @param id Item identifier
     * @param fileName Copy file name
     * @param bindingResults Error binding result
     * @return File name
     * @throws IOException
     */
    public String moveFile(String idClient, String tableName, String field, String id, String fileName, BindingResult bindingResults) {
        String path = getUploadFilePath(idClient);
        String cacheDir = getGlobalCacheDir(idClient);
        String[] tokens = fileName.split("\\$");
        if (tokens.length == 3) {
            if (field.equals(tokens[0])) {
                File srcFile = new File(path + "/" + tableName + "/" + field + "/" + tokens[1] + "/" + tokens[2]);
                File destFile = new File(cacheDir + "/files/" + tableName + "/" + id + "/" + tokens[2]);
                try {
                    FileUtils.moveFile(srcFile, destFile);
                    FileTool.deleteDir(path + "/" + tableName + "/" + field + "/" + tokens[1]);
                    return tokens[2];
                } catch (IOException e) {
                    logger.error("Can not move file: " + srcFile.getAbsolutePath() + " to: " + destFile.getAbsolutePath());
                    bindingResults.rejectValue(field, ERROR_UPLOAD_FILE);
                }
            }
        } else {
            logger.error("Invalid file name format: " + fileName);
            bindingResults.rejectValue(field, ERROR_UPLOAD_FILE);
        }
        throw new FWBadRequestException(bindingResults);
    }

    /**
     * Return path of base cache.
     *
     * @param idClient Client identifier
     * @return Global cache directory
     */
    protected String getGlobalCacheDir(String idClient) {
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        return baserDir != null ? baserDir.getString() : null;
    }

    /**
     * Encode image in base64
     *
     * @param idClient Client identifier
     * @param table Table name
     * @param idRow Row identifier
     * @param name Image name
     * @return Controller Result (Property: url -> Encode image)
     */
    public ControllerResult encodeImage(String idClient, String table, String idRow, String name) {
        ControllerResult result = new ControllerResult();
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            File photo = new File(baserDir.getString() + "/files/" + table + "/" + (idRow == null ? "" : idRow + "/") + name);
            try {
                RenderedImage image = ImageIO.read(photo);
                java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream();
                ImageIO.write(image, "jpg", os);
                byte[] b64 = org.apache.commons.codec.binary.Base64.encodeBase64(os.toByteArray());
                String ss = "data:image/jpeg;base64," + new String(b64);
                result.put("url", ss);
                result.put("load", true);
            } catch (IOException e) {
                logger.error("Error loading/coding image: " + photo.getPath() + "/" + photo.getName());
                result.put("load", false);
            }
        } else {
            result.put("load", false);
        }
        return result;
    }

    /**
     * Decode and save image from Base64 source
     *
     * @param idClient Client identifier
     * @param table Table name
     * @param idRow Row identifier
     * @param name Image name
     * @param base64 Base64 encode image
     * @return Sucess or fail
     */
    protected boolean saveImage(String idClient, String table, String idRow, String name, String base64) {
        AdPreference baserDir = CacheManager.getPreference(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        if (baserDir != null) {
            try {
                byte[] decodedBytes = org.apache.commons.codec.binary.Base64.decodeBase64(base64);
                ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
                BufferedImage bImage2 = ImageIO.read(bis);
                String fileDir = baserDir.getString() + "/files/" + table + "/" + (idRow == null ? "" : idRow + "/");
                File dir = new File(fileDir);
                if (!dir.exists()) {
                    FileTool.buildPath(fileDir);
                }
                File fileImg = new File(fileDir + name);
                String format = FileTool.getFileExt(fileImg);
                if (Converter.isEmpty(format))
                    format = "jpg";
                format = format.toLowerCase().trim();
                if (format.startsWith("."))
                    format = format.substring(1);
                if ("jpeg".equals(format))
                    format = "jpg";
                ImageIO.write(bImage2, format, fileImg);
                return true;
            } catch (Exception e) {
                logger.error("Error loading/decoding image: " + name);
            }
        }
        return false;
    }

    /**
     * Throw exception error
     *
     * @param valid Controller result
     * @param bindingResults Binding result error
     */
    protected void throwError(ControllerResult valid, BindingResult bindingResults) {
        if (valid.getErrCode() < 0) {
            bindingResults.addError(new ObjectError("Message", valid.getMessage()));
        } else {
            bindingResults.rejectValue("name", valid.getMessage(), "" + valid.getProperties().get("value1"));
        }
        throw new FWBadRequestException(bindingResults);
    }

    /**
     * Error handler for DataIntegrityViolationException
     *
     * @param ex Exception
     * @param entityBean Entity bean
     */
    protected void errorHandler(ConstraintViolationException ex, EntityBean entityBean) {
        logger.error(ex.getMessage());
        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        String objectName = entityBean.getClass().getSimpleName();
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(entityBean, objectName);
        String errorCode = ValidationError.CODE_UNKNOW;
        String message = ex.getMessage();
        if (violations.size() > 0) {
            for (ConstraintViolation<?> constraint : violations) {
                if (constraint.getMessageTemplate().contains(".Size.")) {
                    bindingResult.rejectValue(Converter.removeUnderscores(constraint.getPropertyPath().toString()), ValidationError.CODE_SIZE, constraint.getMessage());
                }
                if (constraint.getMessageTemplate().contains(".NotNull.")) {
                    bindingResult.rejectValue(Converter.removeUnderscores(constraint.getPropertyPath().toString()), ValidationError.CODE_NOT_NULL, constraint.getMessage());
                }
            }
            throw new FWBadRequestException(bindingResult);
        } else {
            bindingResult.addError(new ObjectError(errorCode, message));
            throw new FWBadRequestException(bindingResult);
        }
    }

    /**
     * Error handler for DataIntegrityViolationException
     *
     * @param ex Exception
     * @param entityBean Entity bean
     */
    protected void errorHandler(DataIntegrityViolationException ex, EntityBean entityBean) {
        logger.error(ex.getMessage());
        Throwable throwable = ex.getRootCause();
        logger.error(throwable.getMessage());
        String errorCode = ValidationError.CODE_UNKNOW;
        String objectName = entityBean.getClass().getSimpleName();
        String field = null;
        String message = throwable.getMessage();
        BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(entityBean, objectName);
        if (throwable instanceof SQLIntegrityConstraintViolationException) {
            String sqlErr = ((SQLIntegrityConstraintViolationException) throwable).getSQLState();
            if ("23000".equals(sqlErr)) {
                String dbError = message.toLowerCase();
                if (dbError.contains("foreign key")) {
                    errorCode = dbError.contains("delete or update") ? ValidationError.CODE_FK_NOT_DELETE : ValidationError.CODE_FK_NOT_FOUND;
                    int indx = dbError.indexOf("references");
                    if (indx > 0) {
                        String references = dbError.substring(indx);
                        int indxOpen = references.indexOf('('), indxClose = references.indexOf(')');
                        if (indxOpen > 0 && indxClose > indxOpen) {
                            String[] tokens = references.substring(indxOpen + 1, indxClose).split(" ");
                            for (int i = 0; i < tokens.length; i++) {
                                tokens[i] = tokens[i].replaceAll("`", "");
                            }
                            String[] tableTokens = references.split(" ");
                            String fkTable = tableTokens[1].replaceAll("`", "");
                            for (String token : tokens) {
                                bindingResult.rejectValue(Converter.removeUnderscores(token), errorCode, fkTable);
                            }
                            throw new FWBadRequestException(bindingResult);
                        }
                    }
                } else if (dbError.contains("duplicate")) {
                    errorCode = ValidationError.CODE_UNIQUE;
                    String reason = getLastMatch("'\\w+", message.toLowerCase());
                    CustomError customError = getCustomError(sqlErr, message, errorCode, Converter.removeUnderscores(reason));
                    if (customError != null) {
                        bindingResult.addError(new ObjectError(ValidationError.CODE_MESSAGE, customError.getMessage()));
                        throw new FWBadRequestException(bindingResult);
                    }
                    if (!Converter.isEmpty(reason)) {
                        reason = Converter.removeUnderscores(Converter.capitalize(reason.startsWith("'") ? reason.substring(1) : reason));
                        int indx = reason.indexOf(objectName);
                        if (indx >= 0) {
                            field = reason.substring(indx + objectName.length());
                            if (!Converter.isEmpty(field) && entityBean.hasProperty(field)) {
                                bindingResult.rejectValue(Converter.uncapitalize(field), errorCode, message);
                                throw new FWBadRequestException(bindingResult);
                            }
                        }
                    }
                }
            }
            bindingResult.addError(new ObjectError(errorCode, message));
            throw new FWBadRequestException(bindingResult);
        } else if (throwable instanceof MysqlDataTruncation) {
            String sqlErr = ((MysqlDataTruncation) throwable).getSQLState();
            if ("22001".equals(sqlErr)) {
                errorCode = ValidationError.CODE_DATA_TRUNCATION;
                if (message.indexOf("Out of range value for column") > 0) {
                    field = message.substring(message.indexOf("'") + 1);
                    field = Converter.removeUnderscores(field.substring(0, field.indexOf("'")));
                    if (entityBean.hasProperty(field)) {
                        bindingResult.rejectValue(Converter.uncapitalize(field), errorCode, message);
                        throw new FWBadRequestException(bindingResult);
                    }
                }
            }
            bindingResult.addError(new ObjectError(errorCode, message));
            throw new FWBadRequestException(bindingResult);
        }
    }

    private String getLastMatch(String regex, String value) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        String result = "";
        while (matcher.find()) {
            result = value.substring(matcher.start() + 1, matcher.end());
        }
        return result;
    }

    /**
     * Get custom error message
     *
     * @param sqlErr Dadabase error
     * @param message Exception message
     * @param errorCode Detected error code (Unknow, FkNotDelete, FkNotFound, Unique)
     * @param reason Database fail constraint
     * @return Custom error message
     */
    protected CustomError getCustomError(String sqlErr, String message, String errorCode, String reason) {
        return null;
    }

    /**
     * Get custom row audit
     *
     * @param id Row identifier
     * @return Audit information
     */
    protected ControllerResult getCustomRowAudit(String id) {
        return null;
    }

    @RequestMapping(value = "/row_audit", method = RequestMethod.GET)
    @ResponseBody
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public ControllerResult rowAudit(@RequestParam(value = "id") String id) {
        ControllerResult result = getCustomRowAudit(id);
        if (result == null) {
            EntityBean entity = RestPreconditions.checkNotNull(getService().findById(id));
            result = new ControllerResult();
            result.put("id", entity.getIdStr());
            result.put("created", entity.getCreated());
            result.put("updated", entity.getUpdated());
            AdUser user = userService.findById(entity.getCreatedBy());
            result.put("createdBy", user != null ? user.getName() : "-");
            user = userService.findById(entity.getUpdatedBy());
            result.put("updatedBy", user != null ? user.getName() : "-");
        }
        return result;
    }

    @RequestMapping(value = "/export/{idTab}", method = RequestMethod.GET)
    @Secured(FWSecurityConstants.Privileges.CAN_AD_READ)
    public HttpEntity<byte[]> export(
            @PathVariable("idClient") String idClient,
            @PathVariable("idTab") String idTab,
            @RequestParam(value = "idUser") String idUser,
            @RequestParam(value = "idLanguage", required = false, defaultValue = "D804EFC2B38FEEA39FA751540709D0BA") String idLanguage,
            @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "q", required = false) String constraints,
            HttpServletResponse response
    ) throws Exception {
        logger.debug("GET export(" + constraints + ")");
        byte[] documentBody = exportService.getExport(constraints + getClientConstraint(idClient), getSort(sort), idTab, idClient, idUser, idLanguage, this.getService());
        CacheTable table = CacheManager.getTable(this.getService().getTypeTableName());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.setContentLength(documentBody.length);
        response.setHeader("Content-Disposition", "attachment; filename=" + Converter.capitalize2(table.getTable().getStandardName()) + ".xls");
        return new HttpEntity(documentBody, header);
    }

    /**
     * Sort items
     *
     * @param idClient Client identifier
     * @param entity Entity
     * @return ControllerResult
     */
    protected ControllerResult sortItems(String idClient, SortParam entity) {
        ControllerResult result = new ControllerResult();
        Long seqno = entity.getStart();
        Long step = entity.getStep();
        if (seqno == null) {
            seqno = 10L;
        }
        if (step == null || step == 0) {
            step = 10L;
        }
        for (String id : entity.getIds()) {
            EntityBean item = getService().findById(id);
            if (item != null) {
                Long current = (Long) item.getPropertyValue(entity.getField());
                if (current == null || !current.equals(seqno)) {
                    item.setPropertyValue(entity.getField(), seqno);
                    getService().update(item);
                }
                seqno += step;
            }
        }
        HookManager.executeHook(FWConfig.HOOK_TABLE_SORT, idClient, getAuthUser(), getService(), entity);
        return result;
    }

    /**
     * Deletes selected items
     *
     * @param idClient Client identifier
     * @param ids Items IDs to delete
     */
    protected void deleteItems(String idClient, String[] ids) {
        for (String id: ids) {
            EntityBean entity = getService().findById(id);
            deleteEntity(entity);
        }
        HookManager.executeHook(FWConfig.HOOK_TABLE_DELETE_BATCH, idClient, getAuthUser(), getService(), ids);
    }

    /**
     * Get upload file path
     *
     * @param idClient Client identifier
     * @return Directory path
     */
    protected String getUploadFilePath(String idClient) {
        HookResult hookResult = HookManager.executeHook(FWConfig.HOOK_GET_BASE_DIRECTORY, idClient);
        String path;
        if (hookResult.success && hookResult.hasKey(FWConfig.HOOK_GET_BASE_DIRECTORY_REPOSITIRY)) {
            path = hookResult.get(FWConfig.HOOK_GET_BASE_DIRECTORY_REPOSITIRY) + "/file_upload/";
        } else {
            path = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_APACHE_SERVER_CACHE);
        }
        return path;
    }
}
