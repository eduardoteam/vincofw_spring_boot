package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdProcess;
import com.vincomobile.fw.basic.persistence.model.AdProcessParam;

import java.util.ArrayList;
import java.util.List;

public class AdProcessDto {

    private String idProcess;
    private String name;
    private String description;
    private String module;
    private String ptype;
    private String uipattern;
    private Boolean pdf;
    private Boolean excel;
    private Boolean html;
    private Boolean word;
    private Boolean showinparams;
    private String privilege;
    private String privilegeDesc;
    private Boolean showConfirm;
    private String confirmMsg;
    private Long columnsInput;
    private Long columnsOutput;
    private List<AdProcessParamDto> params;
    private AdGUIDto gui;

    public AdProcessDto(AdProcess item, AdGUIDto gui) {
        this.idProcess = item.getIdProcess();
        this.name = item.getName();
        this.description = item.getDescription();
        this.module = item.getModule().getIdModule();
        this.ptype = item.getPtype();
        this.uipattern = item.getUipattern();
        this.pdf = item.getPdf();
        this.excel = item.getExcel();
        this.html = item.getHtml();
        this.word = item.getWord();
        this.showinparams = item.getShowinparams();
        this.privilege = item.getPrivilege();
        this.privilegeDesc = item.getPrivilegeDesc();
        this.showConfirm = item.getShowConfirm();
        this.confirmMsg = item.getConfirmMsg();
        this.columnsInput = item.getColumnsInput();
        this.columnsOutput = item.getColumnsOutput();
        this.gui = gui;
        this.params = new ArrayList<>();
        for (AdProcessParam param : item.getParams()) {
            this.params.add(new AdProcessParamDto(param));
        }
    }

    public String getIdProcess() {
        return idProcess;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getModule() {
        return module;
    }

    public String getPtype() {
        return ptype;
    }

    public String getUipattern() {
        return uipattern;
    }

    public Boolean getPdf() {
        return pdf;
    }

    public Boolean getExcel() {
        return excel;
    }

    public Boolean getHtml() {
        return html;
    }

    public Boolean getWord() {
        return word;
    }

    public Boolean getShowinparams() {
        return showinparams;
    }

    public String getPrivilege() {
        return privilege;
    }

    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public String getConfirmMsg() {
        return confirmMsg;
    }

    public Long getColumnsInput() {
        return columnsInput;
    }

    public Long getColumnsOutput() {
        return columnsOutput;
    }

    public List<AdProcessParamDto> getParams() {
        return params;
    }

    public AdGUIDto getGui() {
        return gui;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    public void setConfirmMsg(String confirmMsg) {
        this.confirmMsg = confirmMsg;
    }
}
