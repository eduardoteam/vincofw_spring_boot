package com.vincomobile.fw.basic.rest.security;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * Created by tomascouso on 19/08/14.
 */
@Component
public class AuthenticationFacadeImpl implements AuthenticationFacade {

    @Override
    public Authentication getAuthentication() {
       return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public UserDetails getMyUser() {
        Object principal = getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return (UserDetails) principal;
        } else {
            throw new AccessDeniedException("UserDetails not exists in security context: " + principal);
        }
    }
}
