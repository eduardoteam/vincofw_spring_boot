package com.vincomobile.fw.basic.persistence.services;

public interface ExtendedFilter {

    /**
     * Get extended filter qualifier
     *
     * @return Unique name for filter
     */
    String getQualifier();

    /**
     * Get extra FROM part
     *
     * @return Extra from to included
     */
    String getExtraFrom();

    /**
     * Get extra FROM part (Native SQL)
     *
     * @return Extra from to included
     */
    String getExtraFromNative();

    /**
     * Get extra WHERE part
     *
     * @return Extra filter to apply
     */
    String getExtraWhere();

    /**
     * Get extra WHERE part (Native SQL)
     *
     * @return Extra filter to apply
     */
    String getExtraWhereNative();
}
