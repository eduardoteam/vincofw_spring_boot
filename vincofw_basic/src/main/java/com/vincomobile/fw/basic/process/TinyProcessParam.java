package com.vincomobile.fw.basic.process;

import lombok.ToString;

import java.util.List;

@ToString
public class TinyProcessParam {
    String idClient;
    String idModule;
    String idUser;
    String idLanguage;
    String idProcess;

    List<ProcessParam> params;

    public void addParam(ProcessParam item) {
        this.params.add(item);
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    public List<ProcessParam> getParams() {
        return params;
    }

    public void setParams(List<ProcessParam> params) {
        this.params = params;
    }
}
