package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.JSONColumn;
import com.vincomobile.fw.basic.JSONTable;
import com.vincomobile.fw.basic.persistence.dto.AdTableDto;
import com.vincomobile.fw.basic.persistence.model.AdTable;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_TABLE
 * <p/>
 * Date: 19/02/2015
 */
public interface AdTableService extends BaseService<AdTable, String> {

    String TABLESOURCE_CODE     = "CODE";
    String TABLESOURCE_SQL      = "SQL";
    String TABLESOURCE_TABLE    = "TABLE";

    /**
     * Checks if the name belongs to a table or view
     *
     * @param tableName Table or view name
     * @param view      True to check if it is a view. False to check if it is a table
     * @param dataOrigin Origen de los datos
     * @return True if the table of view exists, false otherwise
     * @throws SQLException SQL Exception thrown if problem occurs while accesing database
     */
    boolean isValidTable(String tableName, boolean view, String dataOrigin) throws SQLException;

    /**
     * Returns information about tables not in AD_*
     *
     * @throws SQLException SQL Exception thrown if problem occurs while accesing database
     */
    List<AdTable> readTablesInformation() throws SQLException;

    /**
     * Gets the name of a file replacing parameters @[name]
     *
     * @param table Table information
     * @param name  File name
     * @return Real name
     */
    String getFileName(AdTable table, String name);

    /**
     * Get JSONTable structure by file path
     *
     * @param filePath Directory to store files into
     * @return JSONTable structure
     */
    JSONTable getJSONTableByFile(String filePath);

    /**
     * Get JSONColumn structure by file path
     *
     * @param filePath Directory to store files into
     * @return JSONColumn structure
     */
    JSONColumn getJSONColumsByFile(String filePath);

    /**
     * Get table information
     *
     * @param name Table name
     * @return AdTable
     */
    AdTable getTable(String name);

    /**
     * Load table (Meta Data)
     *
     * @param idTable Table identifier
     * @return AdTableDto
     */
    AdTableDto loadTable(String idTable);

    /**
     * Update a row in new transaction
     *
     * @param sql Update sentence
     * @return Update rows count
     */
    int updateRow(String sql);

    /**
     * Get module tables information
     *
     * @param idModule Module identifier
     * @return Tables for module
     */
    List<AdTable> getModuleTable(String idModule);

    /**
     * Validate table structure and references
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param table Table information
     * @return Null or error
     */
    String validateTable(String idClient, String idLanguage, AdTable table);

}


