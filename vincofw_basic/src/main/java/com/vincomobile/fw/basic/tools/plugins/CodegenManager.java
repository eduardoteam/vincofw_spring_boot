package com.vincomobile.fw.basic.tools.plugins;

import java.util.ArrayList;
import java.util.List;

public class CodegenManager {

    private List<CodegenFiles> codeFiles = new ArrayList<>();

    /**
     * Add a file to list or replace existing file for a property
     *
     * @param codeFile File information
     */
    public void addFile(CodegenFiles codeFile) {
        for (CodegenFiles item : codeFiles) {
            if (item.getProperty().equals(codeFile.getProperty())) {
                codeFiles.remove(item);
            }
        }
        codeFiles.add(codeFile);
    }

    public List<CodegenFiles> getCodeFiles() {
        return codeFiles;
    }
}
