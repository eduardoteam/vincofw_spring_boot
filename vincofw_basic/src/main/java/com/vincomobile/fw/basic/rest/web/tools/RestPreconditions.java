package com.vincomobile.fw.basic.rest.web.tools;

import com.vincomobile.fw.basic.persistence.exception.FWConflictException;
import com.vincomobile.fw.basic.persistence.exception.FWForbiddenException;
import com.vincomobile.fw.basic.persistence.exception.FWPreconditionRequiredException;
import com.vincomobile.fw.basic.persistence.exception.FWResourceNotFoundException;

/**
 * Simple static methods to be called at the start of your own methods to verify correct arguments and state. If the Precondition fails, an {@link org.springframework.http.HttpStatus} code is thrown
 */
public final class RestPreconditions {

    private RestPreconditions() {
        throw new AssertionError();
    }

    // API

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     * 
     * @param reference
     *            an object reference
     * @return the non-null reference that was validated
     */
    public static <T> T checkNotNull(final T reference) {
        if (reference == null) {
            throw new FWResourceNotFoundException();
        }
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling method is not null.
     * 
     * @param reference an object reference
     * @return the non-null reference that was validated
     */
    public static <T> T checkRequestElementNotNull(final T reference) {
        if (reference == null) {
            throw new FWConflictException();
        }
        return reference;
    }

    /**
     * Ensures the truth of an expression
     * 
     * @param expression a boolean expression
     */
    public static void checkRequestState(final boolean expression) {
        if (!expression) {
            throw new FWConflictException();
        }
    }

    /**
     * Check if some value was found, otherwise throw exception.
     * 
     * @param expression has value true if found, otherwise false
     */
    public static void checkFound(final boolean expression) {
        if (!expression) {
            throw new FWResourceNotFoundException();
        }
    }

    /**
     * Check if some value was found, otherwise throw exception.
     *
     * @param expression has value true if found, otherwise false
     */
    public static void checkPreconditionRequired(final boolean expression) {
        if (!expression) {
            throw new FWPreconditionRequiredException();
        }
    }

    /**
     * Check if some value was found, otherwise throw exception.
     * 
     * @param expression has value true if found, otherwise false
     */
    public static void checkAllowed(final boolean expression) {
        if (!expression) {
            throw new FWForbiddenException();
        }
    }

}
