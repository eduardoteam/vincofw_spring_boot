package com.vincomobile.fw.basic.persistence.exception;

/**
 * Thrown when validation conflict error is found. Message used to describe the validation error.
 */
public class FWValidationException extends RuntimeException {

    public FWValidationException(final String message) {
        super(message);
    }

}
