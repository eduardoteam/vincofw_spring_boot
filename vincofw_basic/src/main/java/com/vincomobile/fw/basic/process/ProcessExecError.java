package com.vincomobile.fw.basic.process;

public class ProcessExecError {

    boolean success;
    String error;

    public ProcessExecError() {
        success = true;
        error = "";
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.success = false;
        this.error = error;
    }
}
