package com.vincomobile.fw.basic.rest.jwt;

import com.google.common.base.Preconditions;
import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.hooks.HookManager;
import com.vincomobile.fw.basic.hooks.HookResult;
import com.vincomobile.fw.basic.persistence.beans.AdPrincipal;
import com.vincomobile.fw.basic.persistence.model.AdRolePriv;
import com.vincomobile.fw.basic.persistence.services.AdUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JWTUserDetailsService implements UserDetailsService {

    @Autowired
    private AdUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Preconditions.checkNotNull(username);
        HookResult result = HookManager.executeHook(FWConfig.HOOK_AUTHENTICATE_PRINCIPAL, username);
        if (result.hasKey(FWConfig.HOOK_AUTHENTICATE_PRINCIPAL_USER)) {
            final AdPrincipal principal = (AdPrincipal) result.get(FWConfig.HOOK_AUTHENTICATE_PRINCIPAL_USER);
            if (principal != null) {
                final List<AdRolePriv> privilegesOfUser = userService.getPrivileges(principal.getRoleId());
                final List<GrantedAuthority> auths = AuthorityUtils.createAuthorityList();
                for (AdRolePriv rolePriv : privilegesOfUser) {
                    auths.add(new SimpleGrantedAuthority(rolePriv.getPrivilege().getName()));
                }
                return new User(principal.getName(), principal.getPassword(), auths);
            }
        }
        throw new UsernameNotFoundException("Username was not found: " + username);
    }

}
