package com.vincomobile.fw.basic.persistence.beans;

import com.vincomobile.fw.basic.persistence.model.AdPrivilege;

public class AdPrivilegeValue {

    private String idPrivilege;
    private String name;

    public AdPrivilegeValue(AdPrivilege item) {
        this.idPrivilege = item.getIdPrivilege();
        this.name = item.getName();
    }

    public String getIdPrivilege() {
        return idPrivilege;
    }

    public String getName() {
        return name;
    }

}
