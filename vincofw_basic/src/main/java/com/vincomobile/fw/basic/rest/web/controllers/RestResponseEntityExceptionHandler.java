package com.vincomobile.fw.basic.rest.web.controllers;

import com.vincomobile.fw.basic.persistence.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.io.PrintWriter;
import java.io.StringWriter;

//@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    public RestResponseEntityExceptionHandler() {
        super();
    }

    private static void logError(String bodyOfResponse, Exception ex, HttpStatus httpStatus) {
        logger.error("ERROR: "+bodyOfResponse);
        logger.error("HttpStatus: "+httpStatus);
        logger.error(ex.getMessage());
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        logger.error(errors.toString());
    }
    // API

    // 400

    @ExceptionHandler({ ConstraintViolationException.class, FWBadRequestException.class })
    public ResponseEntity<Object> handleBadRequest(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "ConstraintViolationException.class, FWBadRequestException.class";
        logError(bodyOfResponse, ex, HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String bodyOfResponse = "handleHttpMessageNotReadable";
        // ex.getCause() instanceof JsonMappingException, JsonParseException // for additional information later on
        logError(bodyOfResponse, ex, HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final String bodyOfResponse = "handleMethodArgumentNotValid";
        logError(bodyOfResponse, ex, HttpStatus.BAD_REQUEST);
        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.BAD_REQUEST, request);
    }

    // 403

    @ExceptionHandler({ FWForbiddenException.class })
    public ResponseEntity<Object> handleForbidden(final FWForbiddenException ex, final WebRequest request) {
        final String bodyOfResponse = "FWForbiddenException.class";
        logError(bodyOfResponse, ex, HttpStatus.FORBIDDEN);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    // 404

    @ExceptionHandler({ FWEntityNotFoundException.class })
    protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "FWEntityNotFoundException.class";
        logError(bodyOfResponse, ex, HttpStatus.NOT_FOUND);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({ FWResourceNotFoundException.class })
    public ResponseEntity<Object> handleNotFound(final FWResourceNotFoundException ex, final WebRequest request) {
        final String bodyOfResponse = "FWResourceNotFoundException.class";
        logError(bodyOfResponse, ex, HttpStatus.NOT_FOUND);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(value = { EntityNotFoundException.class })
    protected ResponseEntity<Object> handleBadRequest(final EntityNotFoundException ex, final WebRequest request) {
        final String bodyOfResponse = "EntityNotFoundException.class";
        logError(bodyOfResponse, ex, HttpStatus.NOT_FOUND);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    // 409

    @ExceptionHandler({ InvalidDataAccessApiUsageException.class, DataIntegrityViolationException.class, DataAccessException.class, FWConflictException.class })
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "InvalidDataAccessApiUsageException.class, DataIntegrityViolationException.class, DataAccessException.class, FWConflictException.class";
        logError(bodyOfResponse, ex, HttpStatus.CONFLICT);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // 4xx

    @ExceptionHandler({ FWPreconditionFailedException.class })
    /*412*/protected ResponseEntity<Object> handlePreconditionFailed(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "FWPreconditionFailedException.class";
        logError(bodyOfResponse, ex, HttpStatus.PRECONDITION_FAILED);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.PRECONDITION_FAILED, request);
    }

    @ExceptionHandler({ NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class })
    /*500*/public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        final String bodyOfResponse = "NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class";
        logError(bodyOfResponse, ex, HttpStatus.INTERNAL_SERVER_ERROR);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
