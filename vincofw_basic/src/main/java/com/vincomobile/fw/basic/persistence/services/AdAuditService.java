package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdAudit;

/**
 * Created by Vincomobile FW on 02/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_audit
 */
public interface AdAuditService extends BaseService<AdAudit, String> {

}


