package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.model.AdTab;
import com.vincomobile.fw.basic.persistence.services.AdTranslationService;

import java.util.ArrayList;
import java.util.List;

public class AdTabMainDto {

    private String idTab;
    private String idTable;
    private String idWindow;
    private String name;
    private String displaylogic;
    private String ttype;
    private String uipattern;
    private AdTabDto tab;
    private List<AdTabMainDto> childTabs;

    public AdTabMainDto(AdTab item, String name, boolean isComplete, AdGUIDto gui) {
        this.tab = isComplete ? new AdTabDto(item, gui) : null;
        this.idTab = item.getIdTab();
        this.idTable = item.getIdTable();
        this.idWindow = item.getIdWindow();
        this.name = name;
        this.displaylogic = item.getDisplaylogic();
        this.ttype = item.getTtype();
        this.uipattern = item.getUipattern();
        this.childTabs = new ArrayList<>();
    }

    public void addTabs(String idClient, String idLanguage, AdTranslationService translationService, List<AdTab> items) {
        for (AdTab tab : items) {
            this.childTabs.add(new AdTabMainDto(
                    tab,
                    translationService.getTranslation(idClient, FWConfig.TABLE_ID_TAB, FWConfig.COLUMN_ID_TAB_NAME, idLanguage, tab.getIdTab(), tab.getName()),
                    false, null
            ));
        }
    }

    /*
     * Set/Get Methods
     */
    public String getIdTab() {
        return idTab;
    }

    public String getIdTable() {
        return idTable;
    }

    public String getIdWindow() {
        return idWindow;
    }

    public String getName() {
        return name;
    }

    public AdTabDto getTab() {
        return tab;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public String getTtype() {
        return ttype;
    }

    public String getUipattern() {
        return uipattern;
    }

    public List<AdTabMainDto> getChildTabs() {
        return childTabs;
    }
}

