package com.vincomobile.fw.basic.rest.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.vincomobile.fw.basic.rest.security.FWSecurityConstants;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private String tokenSecret;

    JWTAuthorizationFilter(AuthenticationManager authManager, String tokenSecret) {
        super(authManager);
        this.tokenSecret = tokenSecret;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(FWSecurityConstants.HEADER_STRING);

        if (header == null || !header.startsWith(FWSecurityConstants.TOKEN_PREFIX)) {
            SecurityContextHolder.getContext().setAuthentication(createAnonymousAuthentication());
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(FWSecurityConstants.HEADER_STRING);
        if (token != null) {
            // parse the token.
            Map<String, Claim> claims = JWT.require(Algorithm.HMAC512(tokenSecret.getBytes()))
                    .build()
                    .verify(token.replace(FWSecurityConstants.TOKEN_PREFIX, ""))
                    .getClaims();
            if (claims != null && claims.containsKey("sub")) {
                if (claims.containsKey("modules")) {
                    String pathInfo = request.getPathInfo();
                    if (pathInfo == null) {
                        String contextPath = request.getContextPath();
                        String requestURI = request.getRequestURI();
                        pathInfo = requestURI.substring(contextPath.length());
                    }
                    String module = pathInfo.substring(1, pathInfo.indexOf("/", 1));
                    List<String> modules = claims.get("modules").asList(String.class);
                    if (!modules.contains(module)) {
                        logger.error("User: " + claims.get("sub").asString() + " do not access to module: " + module);
                        return null;
                    }
                } else {
                    logger.error("User: " + claims.get("sub").asString() + " do not access to any module");
                    return null;
                }
                if (claims.containsKey("privileges")) {
                    String principal = claims.get("sub").asString();
                    List<GrantedAuthority> auths = AuthorityUtils.createAuthorityList();
                    for (String auth: claims.get("privileges").asList(String.class)) {
                        auths.add(new SimpleGrantedAuthority(auth));
                    }
                    return new UsernamePasswordAuthenticationToken(principal, null, auths);
                }
            }
            return null;
        }
        return null;
    }

    private UsernamePasswordAuthenticationToken createAnonymousAuthentication() {
        return new UsernamePasswordAuthenticationToken(null, null, null);
    }
}