package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdProcessParam;

public class AdProcessParamDto {
    private String idProcessParam;
    private String name;
    private String description;
    private String valuemin;
    private String valuemax;
    private String valuedefault;
    private Boolean mandatory;
    private Boolean ranged;
    private Boolean displayed;
    private String saveType;
    private String ptype;
    private String caption;
    private String displaylogic;
    private Long span;
    private AdReferenceDto reference;

    public AdProcessParamDto(AdProcessParam item) {
        this.idProcessParam = item.getIdProcessParam();
        this.name = item.getName();
        this.description = item.getDescription();
        this.valuemin = item.getValuemin();
        this.valuemax = item.getValuemax();
        this.valuedefault = item.getValuedefault();
        this.mandatory = item.getMandatory();
        this.ranged = item.getRanged();
        this.displayed = item.getDisplayed();
        this.saveType = item.getSaveType();
        this.caption = item.getCaption();
        this.displaylogic = item.getDisplaylogic();
        this.span = item.getSpan();
        this.reference = new AdReferenceDto(item.getReference(), item.getIdReferenceValue());
    }

    public String getIdProcessParam() {
        return idProcessParam;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getValuemin() {
        return valuemin;
    }

    public String getValuemax() {
        return valuemax;
    }

    public String getValuedefault() {
        return valuedefault;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public Boolean getRanged() {
        return ranged;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public String getSaveType() {
        return saveType;
    }

    public String getPtype() {
        return ptype;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public Long getSpan() {
        return span;
    }

    public AdReferenceDto getReference() {
        return reference;
    }
}
