package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.cache.CacheManager;
import com.vincomobile.fw.basic.persistence.model.AdRolePriv;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import com.vincomobile.fw.basic.persistence.model.AdUserHistPwd;
import com.vincomobile.fw.basic.tools.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Devtools.
 * Service of AD_USER
 * <p>
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdUserServiceImpl extends BaseServiceImpl<AdUser, String> implements AdUserService {
    private Logger logger = LoggerFactory.getLogger(AdUserServiceImpl.class);

    private final AdRolePrivService rolePrivService;
    private final AdMessageService messageService;
    private final AdUserHistPwdService userHistPwdService;

    /**
     * Constructor.
     */
    public AdUserServiceImpl(AdRolePrivService rolePrivService, AdMessageService messageService, AdUserHistPwdService userHistPwdService) {
        super(AdUser.class);
        this.rolePrivService = rolePrivService;
        this.messageService = messageService;
        this.userHistPwdService = userHistPwdService;
    }

    /**
     * Gets the user by login
     *
     * @param idClient Client identifier
     * @param login Login to search
     * @return User data
     */
    @Override
    public AdUser findByLogin(String idClient, String login) {
        if (login == null)
            return null;
        Map filter = new HashMap();
        filter.put("username", login);
        if (!Converter.isEmpty(idClient)) {
            filter.put("idClient", getClientFilter(idClient));
        }
        return findFirst(filter);
    }

    @Override
    public AdUser findByLogin(String login) {
        return findByLogin(null, login);
    }

    /**
     * Gets the user by email
     *
     * @param email Email to search
     * @return User data
     */
    @Override
    public AdUser findByEmail(String email) {
        if (email == null)
            return null;
        Map filter = new HashMap();
        filter.put("email", email);
        return findFirst(filter);
    }

    /**
     * Gets the privileges of a role
     *
     * @param idRole Role ID
     * @return Privilege list
     */
    @Override
    public List<AdRolePriv> getPrivileges(String idRole) {
        Map<String, Object> filter = new HashMap<>();
        filter.put("idRole", idRole);
        return rolePrivService.findAll(filter);
    }

    /**
     * Validate security restrictions for a user
     *
     * @param idClient Client identifier
     * @param lastUpdate Last password update
     * @param idLanguage Language identifier
     * @param size Password size
     * @param security Security level satisfy
     * @param checkValidDays Check valid days
     * @return Error or null
     */
    @Override
    public String checkSecurityPassword(String idClient, Date lastUpdate, String idLanguage, Long size, String security, boolean checkValidDays) {
        String mode = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_PASSWORD_CHECK_MODE, AdPreferenceService.PASSWORD_CHECK_MODE_BASIC);
        Long sizeMin = CacheManager.getPreferenceLong(idClient, AdPreferenceService.GLOBAL_PASSWORD_SIZE_MIN, 0L);
        Long sizeMax = CacheManager.getPreferenceLong(idClient, AdPreferenceService.GLOBAL_PASSWORD_SIZE_MAX, 0L);
        if (sizeMin > size) {
            return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdMin", sizeMin);
        }
        if (sizeMax < size) {
            return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdMax", sizeMax);
        }
        if (AdPreferenceService.PASSWORD_CHECK_MODE_STANDARD.equals(mode)) {
            if (security.equals(AdPreferenceService.PASSWORD_CHECK_MODE_BASIC)) {
                return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdCharStandard");
            }
        } else if (AdPreferenceService.PASSWORD_CHECK_MODE_HIGH.equals(mode) || AdPreferenceService.PASSWORD_CHECK_MODE_SECURE.equals(mode)) {
            if (AdPreferenceService.PASSWORD_CHECK_MODE_HIGH.equals(mode)) {
                if (!security.equals(AdPreferenceService.PASSWORD_CHECK_MODE_HIGH) && !security.equals(AdPreferenceService.PASSWORD_CHECK_MODE_SECURE)) {
                    return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdCharHigh");
                }
            } else if (!security.equals(AdPreferenceService.PASSWORD_CHECK_MODE_SECURE)) {
                return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdCharSecure");
            }
            if (checkValidDays) {
                // Check valid days
                int validDay = CacheManager.getPreferenceLong(idClient, AdPreferenceService.GLOBAL_PASSWORD_VALID_DAYS, 0L).intValue();
                Calendar cal = Calendar.getInstance();
                cal.setTime(lastUpdate);
                cal.add(Calendar.DAY_OF_YEAR, validDay);
                if (cal.getTime().before(new Date())) {
                    return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdExpire", validDay);
                }
            }
        }
        return null;
    }

    /**
     * Validate security restrictions for a user password
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @param newPassword New password
     * @return Error or null
     */
    @Override
    public String checkSecurityPasswordHistory(String idClient, String idUser, String idLanguage, String newPassword) {
        String mode = CacheManager.getPreferenceString(idClient, AdPreferenceService.GLOBAL_PASSWORD_CHECK_MODE);
        if (AdPreferenceService.PASSWORD_CHECK_MODE_HIGH.equals(mode) || AdPreferenceService.PASSWORD_CHECK_MODE_SECURE.equals(mode)) {
            Long storeCount = CacheManager.getPreferenceLong(idClient, AdPreferenceService.GLOBAL_PASSWORD_STORE_COUNT, 10L);
            List<AdUserHistPwd> userPasswd = userHistPwdService.listUserPassword(idClient, idUser);
            if (storeCount < userPasswd.size()) {
                // Remove extra password store
                for (long i = storeCount - 1; i < userPasswd.size(); i++) {
                    userHistPwdService.delete(userPasswd.get((int) i));
                }
                long pwSize = userPasswd.size();
                for (long i = storeCount - 1; i < pwSize; i++) {
                    userPasswd.remove((int) i);
                }
            }
            // Check if password already used
            for (AdUserHistPwd pwd : userPasswd) {
                if (pwd.getPasswd().equals(newPassword)) {
                    return messageService.getMessage(idClient, idLanguage, "AD_SecurityErrorPasswdStoreExist");
                }
            }
            // Store the new password
            if (storeCount < userPasswd.size()) {
                AdUserHistPwd pwd = new AdUserHistPwd();
                pwd.setIdClient(idClient);
                pwd.setActive(true);
                pwd.setIdUser(idUser);
                pwd.setPasswd(newPassword);
                userHistPwdService.save(pwd);
            } else {
                AdUserHistPwd pwd = userPasswd.get(userPasswd.size() - 1);
                pwd.setPasswd(newPassword);
                userHistPwdService.update(pwd);
            }
        }
        return null;
    }

}
