package com.vincomobile.fw.basic.persistence.beans;

public class AdRefListValue {

    private String value;
    private String name;
    private String extraInfo;

    public AdRefListValue(String value, String name, String extraInfo) {
        this.value = value;
        this.name = name;
        this.extraInfo = extraInfo;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }
}
