package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdColumn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Devtools.
 * Service of AD_COLUMN
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdColumnServiceImpl extends BaseServiceImpl<AdColumn, String> implements AdColumnService {

    /**
     * Constructor.
     *
     */
    public AdColumnServiceImpl() {
        super(AdColumn.class);
    }

}
