package com.vincomobile.fw.basic.business;

import com.vincomobile.fw.basic.persistence.model.AdProcessParam;

public class ProcessUtils {

    /**
     * Get input parameter
     *
     * @param idProcess Process identifier
     * @param name Parameter name
     * @param value Parameter value
     * @return Parameter
     */
    public static AdProcessParam getInParam(String idProcess, String name, String value) {
        AdProcessParam newParam = new AdProcessParam();
        newParam.setIdProcess(idProcess);
        newParam.setPtype("IN");
        newParam.setName(name);
        newParam.setValue(value);
        return newParam;
    }

}
