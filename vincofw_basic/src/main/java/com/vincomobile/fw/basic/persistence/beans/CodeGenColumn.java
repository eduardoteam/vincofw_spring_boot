package com.vincomobile.fw.basic.persistence.beans;

import com.vincomobile.fw.basic.tools.Converter;

public class CodeGenColumn {

    String fieldName;
    String referenceType;
    String referencedName;
    String referencedColumnName;

    public CodeGenColumn(String fieldName, String referenceType, String referencedName, String referencedColumnName) {
        this.fieldName = fieldName;
        this.referenceType = referenceType;
        this.referencedName = referencedName;
        this.referencedColumnName = referencedColumnName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldNameCap() {
        return Converter.capitalize2(fieldName);
    }

    public String getReferenceType() {
        return referenceType;
    }

    public String getReferencedName() {
        return referencedName;
    }

    public String getReferencedColumnName() {
        return referencedColumnName;
    }
}
