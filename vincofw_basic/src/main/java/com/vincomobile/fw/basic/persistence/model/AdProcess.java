package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by Devtools.
 * Modelo de ad_process
 *
 * Date: 06/11/2015
 */
@Entity
@Table(name = "ad_process")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdProcess extends AdEntityBean {

    private String idProcess;
    private String name;
    private String description;
    private String ptype;
    private String classname;
    private String classmethod;
    private Boolean background;
    private String uipattern;
    private String jrxml;
    private String jrxmlExcel;
    private String jrxmlWord;
    private Boolean pdf;
    private Boolean excel;
    private Boolean html;
    private Boolean word;
    private Boolean showinparams;
    private Boolean multiple;
    private String evalSql;
    private Long seqno;
    private String privilege;
    private String privilegeDesc;
    private Boolean showConfirm;
    private String confirmMsg;
    private Long columnsInput;
    private Long columnsOutput;

    private List<AdProcessParam> params;

    private String cronExpression;
    private String executingIdUser;
    private String executingIdClient;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcess;
    }

    @Override
    public void setId(String id) {
            this.idProcess = id;
    }

    @Id
    @Column(name = "id_process")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "ptype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    @Column(name = "classname", length = 150)
    @Size(min = 1, max = 150)
    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Column(name = "classmethod", length = 60)
    @Size(min = 1, max = 60)
    public String getClassmethod() {
        return classmethod;
    }

    public void setClassmethod(String classmethod) {
        this.classmethod = classmethod;
    }

    @Column(name = "background")
    @NotNull
    public Boolean getBackground() {
        return background;
    }

    public void setBackground(Boolean background) {
        this.background = background;
    }

    @Column(name = "uipattern", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getUipattern() {
        return uipattern;
    }

    public void setUipattern(String uipattern) {
        this.uipattern = uipattern;
    }

    @Column(name = "jrxml", length = 100)
    @Size(min = 1, max = 100)
    public String getJrxml() {
        return jrxml;
    }

    public void setJrxml(String jrxml) {
        this.jrxml = jrxml;
    }

    @Column(name = "jrxml_excel", length = 100)
    @Size(min = 1, max = 100)
    public String getJrxmlExcel() {
        return jrxmlExcel;
    }

    public void setJrxmlExcel(String jrxmlExcel) {
        this.jrxmlExcel = jrxmlExcel;
    }

    @Column(name = "jrxml_word", length = 100)
    @Size(min = 1, max = 100)
    public String getJrxmlWord() {
        return jrxmlWord;
    }

    public void setJrxmlWord(String jrxmlWord) {
        this.jrxmlWord = jrxmlWord;
    }

    @Column(name = "pdf")
    public Boolean getPdf() {
        return pdf;
    }

    public void setPdf(Boolean pdf) {
        this.pdf = pdf;
    }

    @Column(name = "excel")
    public Boolean getExcel() {
        return excel;
    }

    public void setExcel(Boolean excel) {
        this.excel = excel;
    }

    @Column(name = "html")
    public Boolean getHtml() {
        return html;
    }

    public void setHtml(Boolean html) {
        this.html = html;
    }

    @Column(name = "word")
    public Boolean getWord() {
        return word;
    }

    public void setWord(Boolean word) {
        this.word = word;
    }

    @Column(name = "showinparams")
    @NotNull
    public Boolean getShowinparams() {
        return showinparams;
    }

    public void setShowinparams(Boolean showinparams) {
        this.showinparams = showinparams;
    }

    @Column(name = "eval_sql", length = 2000)
    @Size(min = 1, max = 2000)
    public String getEvalSql() {
        return evalSql;
    }

    public void setEvalSql(String evalSql) {
        this.evalSql = evalSql;
    }

    @Column(name = "multiple")
    public Boolean getMultiple() {
        return multiple != null ? multiple : false;
    }

    public void setMultiple(Boolean multiple) {
        this.multiple = multiple;
    }

    @Column(name = "seqno")
    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    @Column(name = "privilege", length = 50)
    @Size(min = 1, max = 50)
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Column(name = "privilege_desc")
    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    @Column(name = "show_confirm", length = 1000)
    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    @Column(name = "confirm_msg", length = 1000)
    @Size(min = 1, max = 1000)
    public String getConfirmMsg() {
        return confirmMsg;
    }

    public void setConfirmMsg(String confirmMsg) {
        this.confirmMsg = confirmMsg;
    }


    @Column(name = "columns_input")
    @NotNull
    public Long getColumnsInput() {
        return columnsInput;
    }

    public void setColumnsInput(Long columnsInput) {
        this.columnsInput = columnsInput;
    }

    @Column(name = "columns_output")
    @NotNull
    public Long getColumnsOutput() {
        return columnsOutput;
    }

    public void setColumnsOutput(Long columnsOutput) {
        this.columnsOutput = columnsOutput;
    }

    @OneToMany
    @JoinColumn(name = "id_process", referencedColumnName = "id_process", insertable = false, updatable = false)
    @OrderBy("seqno")
    @JsonIgnore
    public List<AdProcessParam> getParams() {
        return params;
    }

    public void setParams(List<AdProcessParam> params) {
        this.params = params;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdProcess)) return false;

        final AdProcess that = (AdProcess) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((ptype == null) && (that.ptype == null)) || (ptype != null && ptype.equals(that.ptype)));
        result = result && (((classname == null) && (that.classname == null)) || (classname != null && classname.equals(that.classname)));
        result = result && (((classmethod == null) && (that.classmethod == null)) || (classmethod != null && classmethod.equals(that.classmethod)));
        result = result && (((background == null) && (that.background == null)) || (background != null && background.equals(that.background)));
        result = result && (((uipattern == null) && (that.uipattern == null)) || (uipattern != null && uipattern.equals(that.uipattern)));
        result = result && (((jrxml == null) && (that.jrxml == null)) || (jrxml != null && jrxml.equals(that.jrxml)));
        result = result && (((jrxmlExcel == null) && (that.jrxmlExcel == null)) || (jrxmlExcel != null && jrxmlExcel.equals(that.jrxmlExcel)));
        result = result && (((jrxmlWord == null) && (that.jrxmlWord == null)) || (jrxmlWord != null && jrxmlWord.equals(that.jrxmlWord)));
        result = result && (((pdf == null) && (that.pdf == null)) || (pdf != null && pdf.equals(that.pdf)));
        result = result && (((excel == null) && (that.excel == null)) || (excel != null && excel.equals(that.excel)));
        result = result && (((html == null) && (that.html == null)) || (html != null && html.equals(that.html)));
        result = result && (((word == null) && (that.word == null)) || (word != null && word.equals(that.word)));
        result = result && (((showinparams == null) && (that.showinparams == null)) || (showinparams != null && showinparams.equals(that.showinparams)));
        result = result && (((multiple == null) && (that.multiple == null)) || (multiple != null && multiple.equals(that.multiple)));
        result = result && (((evalSql == null) && (that.evalSql == null)) || (evalSql != null && evalSql.equals(that.evalSql)));
        result = result && (((seqno == null) && (that.seqno == null)) || (seqno != null && seqno.equals(that.seqno)));
        result = result && (((privilege == null) && (that.privilege == null)) || (privilege != null && privilege.equals(that.privilege)));
        result = result && (((privilegeDesc == null) && (that.privilegeDesc == null)) || (privilegeDesc != null && privilegeDesc.equals(that.privilegeDesc)));
        result = result && (((showConfirm == null) && (that.showConfirm == null)) || (showConfirm != null && showConfirm.equals(that.showConfirm)));
        result = result && (((confirmMsg == null) && (that.confirmMsg == null)) || (confirmMsg != null && confirmMsg.equals(that.confirmMsg)));
        result = result && (((columnsInput == null) && (that.columnsInput == null)) || (columnsInput != null && columnsInput.equals(that.columnsInput)));
        result = result && (((columnsOutput == null) && (that.columnsOutput == null)) || (columnsOutput != null && columnsOutput.equals(that.columnsOutput)));
        return result;
    }

    /**
     * Gets the Quartz scheduler executing id client
     * @return Quartz scheduler executing id client
     */
    @Transient
    public String getExecutingIdClient() {
        return executingIdClient;
    }

    /**
     * Sets the Quarts scheduler executing id client
     * @param executingIdClient Executing id client
     */
    public void setExecutingIdClient(String executingIdClient) {
        this.executingIdClient = executingIdClient;
    }

    /**
     * Gets the Quartz scheduler executing id user
     * @return Quartz scheduler executing id user
     */
    @Transient
    public String getExecutingIdUser() {
        return executingIdUser;
    }

    /**
     * Sets the Quarts scheduler executing id user
     * @param executingIdUser Executing id user
     */
    public void setExecutingIdUser(String executingIdUser) {
        this.executingIdUser = executingIdUser;
    }

    @Transient
    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    @Transient
    public String getCaption() {
        return "[" + module.getRestPath() + "] " + name;
    }

}

