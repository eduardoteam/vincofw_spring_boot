package com.vincomobile.fw.basic.persistence.beans;

public class AdLanguageInfo {

    String idLanguage;
    String iso2;

    public AdLanguageInfo(String idLanguage, String iso2) {
        this.idLanguage = idLanguage;
        this.iso2 = iso2;
    }

    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }
}
