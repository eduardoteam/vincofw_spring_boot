package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_REFERENCE
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_reference")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdReference extends AdEntityBean {

    private String idReference;
    private String name;
    private String rtype;
    private Boolean base;

    private AdRefButton refButton;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idReference;
    }

    @Override
    public void setId(String id) {
            this.idReference = id;
    }

    @Id
    @Column(name = "id_reference")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    @Column(name = "name", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "rtype", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getRtype() {
        return rtype;
    }

    public void setRtype(String rtype) {
        this.rtype = rtype;
    }

    @Column(name = "base")
    @NotNull
    public Boolean getBase() {
        return base;
    }

    public void setBase(Boolean base) {
        this.base = base;
    }

    @OneToOne()
    @JoinColumn(name = "id_reference", referencedColumnName = "id_reference", insertable = false, updatable = false)
    public AdRefButton getRefButton() {
        return refButton;
    }

    public void setRefButton(AdRefButton refButton) {
        this.refButton = refButton;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdReference)) return false;

        final AdReference that = (AdReference) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idReference == null) && (that.idReference == null)) || (idReference != null && idReference.equals(that.idReference)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((rtype == null) && (that.rtype == null)) || (rtype != null && rtype.equals(that.rtype)));
        result = result && (((base == null) && (that.base == null)) || (base != null && base.equals(that.base)));
        return result;
    }

    @Transient
    public String getCaption() {
        return "[" + module.getRestPath() + "] " + name;
    }

}

