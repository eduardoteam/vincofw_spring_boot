package com.vincomobile.fw.basic.persistence.beans;

public class AdMessageTranslation {
    String idClient;
    String idModule;
    String value;
    String text;

    public AdMessageTranslation(Object[] item) {
        this.idClient = AdBaseTools.getString(item, 3);
        this.idModule = AdBaseTools.getString(item, 4);
        this.value = AdBaseTools.getString(item, 0);
        String translation = AdBaseTools.getString(item, 2);
        this.text =  translation == null ? AdBaseTools.getString(item, 1) : translation;
    }

    public String getIdClient() {
        return idClient;
    }

    public String getIdModule() {
        return idModule;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
