package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdChart;

public class AdChartDto {

    private String idChart;
    private String ctype;
    private String displaylogic;
    private String mode;
    private String name;
    private Long seqno;
    private Long span;
    private Boolean startnewrow;

    public AdChartDto(AdChart item) {
        this.idChart = item.getIdChart();
        this.ctype = item.getCtype();
        this.displaylogic = item.getDisplaylogic();
        this.mode = item.getMode();
        this.name = item.getName();
        this.seqno = item.getSeqno();
        this.startnewrow = item.getStartnewrow();
    }

    /*
     * Set/Get Methods
     */

    public String getIdChart() {
        return idChart;
    }

    public String getCtype() {
        return ctype;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public String getMode() {
        return mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSeqno() {
        return seqno;
    }

    public Long getSpan() {
        return span;
    }

    public Boolean getStartnewrow() {
        return startnewrow;
    }
}

