package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdTableAction;

/**
 * Created by Vincomobile FW on 16/02/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_table_action
 */
public interface AdTableActionService extends BaseService<AdTableAction, String> {

    String TYPE_PROCESS     = "PROCESS";
    String TYPE_CUSTOM      = "CUSTOM";
    String TYPE_LINK        = "LINK";
    String TYPE_WINDOW      = "WINDOW";

}


