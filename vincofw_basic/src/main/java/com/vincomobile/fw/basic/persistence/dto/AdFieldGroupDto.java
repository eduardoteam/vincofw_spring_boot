package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdFieldGroup;

import java.util.ArrayList;
import java.util.List;

public class AdFieldGroupDto {

    private String idFieldGroup;
    private String name;
    private Boolean collapsed;
    private Long columns;
    private String displaylogic;
    private List<AdFieldDto> fields;

    public AdFieldGroupDto(AdFieldGroup item) {
        this.idFieldGroup = item.getIdFieldGroup();
        this.name = item.getName();
        this.collapsed = item.getCollapsed();
        this.columns = item.getColumns();
        this.displaylogic = item.getDisplaylogic();
        this.fields = new ArrayList<>();
    }

    /*
     * Set/Get Methods
     */

    public String getIdFieldGroup() {
        return idFieldGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCollapsed() {
        return collapsed;
    }

    public Long getColumns() {
        return columns;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public List<AdFieldDto> getFields() {
        return fields;
    }
}

