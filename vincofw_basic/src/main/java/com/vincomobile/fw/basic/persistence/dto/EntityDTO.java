package com.vincomobile.fw.basic.persistence.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public abstract class EntityDTO<PK extends Serializable> {
    protected Date created;
    protected Date updated;
    protected String createdBy;
    protected String updatedBy;
    protected Boolean active;

    public abstract PK getId();
}
