package com.vincomobile.fw.basic.persistence.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vincomobile.fw.basic.persistence.model.AdEntityBean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de ad_process_log
 *
 * Date: 28/11/2015
 */
@Entity
@Table(name = "ad_process_log")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdProcessLog extends AdEntityBean {

    private String idProcessLog;
    private String idProcessExec;
    private String ltype;
    private String log;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idProcessLog;
    }

    @Override
    public void setId(String id) {
            this.idProcessLog = id;
    }

    @Id
    @Column(name = "id_process_log")
    public String getIdProcessLog() {
        return idProcessLog;
    }

    public void setIdProcessLog(String idProcessLog) {
        this.idProcessLog = idProcessLog;
    }

    @Column(name = "id_process_exec")
    @NotNull
    public String getIdProcessExec() {
        return idProcessExec;
    }

    public void setIdProcessExec(String idProcessExec) {
        this.idProcessExec = idProcessExec;
    }

    @Column(name = "ltype", length = 50)
    @Size(min = 1, max = 50)
    public String getLtype() {
        return ltype;
    }

    public void setLtype(String ltype) {
        this.ltype = ltype;
    }

    @Column(name = "log", columnDefinition = "TEXT")
    @NotNull
    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdProcessLog)) return false;

        final AdProcessLog that = (AdProcessLog) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idProcessLog == null) && (that.idProcessLog == null)) || (idProcessLog != null && idProcessLog.equals(that.idProcessLog)));
        result = result && (((idProcessExec == null) && (that.idProcessExec == null)) || (idProcessExec != null && idProcessExec.equals(that.idProcessExec)));
        result = result && (((ltype == null) && (that.ltype == null)) || (ltype != null && ltype.equals(that.ltype)));
        result = result && (((log == null) && (that.log == null)) || (log != null && log.equals(that.log)));
        return result;
    }

}

