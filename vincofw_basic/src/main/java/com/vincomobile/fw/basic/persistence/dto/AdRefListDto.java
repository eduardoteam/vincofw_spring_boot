package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.beans.AdRefListValue;

public class AdRefListDto {

    private String value;
    private String name;

    public AdRefListDto(AdRefListValue item) {
        this.value = item.getValue();
        this.name = item.getName();
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
