package com.vincomobile.fw.basic.persistence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public final class FWConflictException extends RuntimeException {

    public FWConflictException() {
        super();
    }

    public FWConflictException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWConflictException(final String message) {
        super(message);
    }

    public FWConflictException(final Throwable cause) {
        super(cause);
    }

}
