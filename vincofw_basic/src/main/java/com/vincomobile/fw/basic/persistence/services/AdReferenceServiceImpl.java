package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdRefListValue;
import com.vincomobile.fw.basic.persistence.dto.AdRefButtonDto;
import com.vincomobile.fw.basic.persistence.dto.AdRefTableDto;
import com.vincomobile.fw.basic.persistence.dto.AdReferenceDto;
import com.vincomobile.fw.basic.persistence.model.AdRefButton;
import com.vincomobile.fw.basic.persistence.model.AdRefTable;
import com.vincomobile.fw.basic.persistence.model.AdReference;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_REFERENCE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdReferenceServiceImpl extends BaseServiceImpl<AdReference, String> implements AdReferenceService {

    private final AdRefTableService refTableService;
    private final AdRefListService refListService;
    private final AdRefButtonService refButtonService;

    public AdReferenceServiceImpl(AdRefTableService refTableService, AdRefListService refListService, AdRefButtonService refButtonService) {
        super(AdReference.class);
        this.refTableService = refTableService;
        this.refListService = refListService;
        this.refButtonService = refButtonService;
    }

    /**
     * Load reference information
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param idReference Reference identifier
     * @return AdReferenceDto
     */
    @Override
    public AdReferenceDto loadReference(String idClient, String idLanguage, String idReference) {
        AdReferenceDto result = null;
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idReference", idReference);
        filter.put("active", true);
        AdReference reference = findFirst(filter);
        if (reference != null) {
            result = new AdReferenceDto(reference);
            if (RTYPE_TABLE.equals(reference.getRtype()) || RTYPE_TABLEDIR.equals(reference.getRtype()) || RTYPE_SEARCH.equals(reference.getRtype())) {
                AdRefTable refTable = refTableService.getRefTableByReference(idReference);
                if (refTable != null)
                    result.setRefTable(new AdRefTableDto(refTable));
            } else if (RTYPE_LIST.equals(reference.getRtype()) || RTYPE_LISTMULTIPLE.equals(reference.getRtype())) {
                List<AdRefListValue> values = refListService.listValues(idClient, idReference, idLanguage);
                result.addRefList(values);
            } else if (RTYPE_BUTTON.equals(reference.getRtype())) {
                AdRefButton button = refButtonService.getRefButtonByReference(idReference);
                if (button != null)
                    result.setRefButton(new AdRefButtonDto(button));
            }
        }
        return result;
    }

    /**
     * Load references information
     *
     * @param idClient Client identifier
     * @param idLanguage Language identifier
     * @param references References identifiers
     * @return List of AdReferenceDto
     */
    @Override
    public List<AdReferenceDto> loadReferences(String idClient, String idLanguage, String[] references) {
        List<AdReferenceDto> result = new ArrayList<>();
        for (String idReference : references) {
            AdReferenceDto ref = loadReference(idClient, idLanguage, idReference);
            if (ref != null) {
                result.add(ref);
            }
        }
        return result;
    }

}
