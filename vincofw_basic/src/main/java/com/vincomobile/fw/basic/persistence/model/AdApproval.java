package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Vincomobile FW on 23/11/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Model for table ad_approval
 */
@Entity
@Table(name = "ad_approval")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdApproval extends EntityBean<String> {

    private String idClient;
    private String idApproval;
    private String action;
    private String privilege;
    private String rowkey;
    private String idUser;
    private String idTable;
    private String idTableAction;
    private String idProcess;

    private AdTable table;
    private AdTableAction tableAction;
    private AdProcess process;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idApproval;
    }

    @Override
    public void setId(String id) {
            this.idApproval = id;
    }

    @Id
    @Column(name = "id_approval")
    public String getIdApproval() {
        return idApproval;
    }

    public void setIdApproval(String idApproval) {
        this.idApproval = idApproval;
    }

    @Column(name = "id_client")
    @NotNull
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "action", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Column(name = "privilege", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    @Column(name = "rowkey", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getRowkey() {
        return rowkey;
    }

    public void setRowkey(String rowkey) {
        this.rowkey = rowkey;
    }

    @Column(name = "id_user", length = 32)
    @NotNull
    @Size(min = 1, max = 32)
    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Column(name = "id_table", length = 32)
    @Size(min = 1, max = 32)
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_table_action", length = 32)
    @Size(min = 1, max = 32)
    public String getIdTableAction() {
        return idTableAction;
    }

    public void setIdTableAction(String idTableAction) {
        this.idTableAction = idTableAction;
    }

    @Column(name = "id_process", length = 32)
    @Size(min = 1, max = 32)
    public String getIdProcess() {
        return idProcess;
    }

    public void setIdProcess(String idProcess) {
        this.idProcess = idProcess;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table", referencedColumnName = "id_table", insertable = false, updatable = false)
    public AdTable getTable() {
        return table;
    }

    public void setTable(AdTable table) {
        this.table = table;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_table_action", referencedColumnName = "id_table_action", insertable = false, updatable = false)
    public AdTableAction getTableAction() {
        return tableAction;
    }

    public void setTableAction(AdTableAction tableAction) {
        this.tableAction = tableAction;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_process", referencedColumnName = "id_process", insertable = false, updatable = false)
    public AdProcess getProcess() {
        return process;
    }

    public void setProcess(AdProcess process) {
        this.process = process;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdApproval)) return false;

        final AdApproval that = (AdApproval) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idApproval == null) && (that.idApproval == null)) || (idApproval != null && idApproval.equals(that.idApproval)));
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((action == null) && (that.action == null)) || (action != null && action.equals(that.action)));
        result = result && (((privilege == null) && (that.privilege == null)) || (privilege != null && privilege.equals(that.privilege)));
        result = result && (((rowkey == null) && (that.rowkey == null)) || (rowkey != null && rowkey.equals(that.rowkey)));
        result = result && (((idUser == null) && (that.idUser == null)) || (idUser != null && idUser.equals(that.idUser)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idTableAction == null) && (that.idTableAction == null)) || (idTableAction != null && idTableAction.equals(that.idTableAction)));
        result = result && (((idProcess == null) && (that.idProcess == null)) || (idProcess != null && idProcess.equals(that.idProcess)));
        return result;
    }


}

