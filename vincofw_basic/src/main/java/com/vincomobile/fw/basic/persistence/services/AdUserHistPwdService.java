package com.vincomobile.fw.basic.persistence.services;


import com.vincomobile.fw.basic.persistence.model.AdUserHistPwd;

import java.util.List;

/**
 * Created by Vincomobile FW on 07/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_user_hist_pwd
 */
public interface AdUserHistPwdService extends BaseService<AdUserHistPwd, String> {

    /**
     * Get user passwords store
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @return List
     */
    List<AdUserHistPwd> listUserPassword(String idClient, String idUser);
}


