package com.vincomobile.fw.basic.persistence.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus( value = HttpStatus.BAD_REQUEST )
public final class FWBadRequestException extends RuntimeException {

    private BindingResult bindingResult;

    public FWBadRequestException(BindingResult bindingResult) {
        super();
        this.bindingResult = bindingResult;
    }

    public FWBadRequestException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWBadRequestException(final String message) {
        super(message);
    }

    public FWBadRequestException(final Throwable cause) {
        super(cause);
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }

    public void setBindingResult(BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

}
