package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.business.ValidationError;
import com.vincomobile.fw.basic.persistence.model.AdModule;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_MODULE
 *
 * Date: 19/02/2015
 */
public interface AdModuleService extends BaseService<AdModule, String> {

    String MODULE_ID_FW                 = "D351EB083EC7E8C4891CD7B25B1E274E";

    /**
     * Get first module for client
     *
     * @param idClient Client identifier
     * @return Module
     */
    AdModule getClientModule(String idClient);

    /**
     * Validate module prefix
     *
     * @param idModule Module identifier
     * @param value Value to check
     * @return Validation errors
     */
    List<ValidationError> validateModulePrefix(String idModule, String value);
}


