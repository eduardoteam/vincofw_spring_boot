package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.model.AdUserLog;
import com.vincomobile.fw.basic.session.UserLog;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vincomobile FW on 05/11/2018.
 * Copyright © 2018 Vincomobile. All rights reserved.
 *
 * Service layer implementation for ad_user_log
 */
@Repository
@Transactional(readOnly = true)
public class AdUserLogServiceImpl extends BaseServiceImpl<AdUserLog, String> implements AdUserLogService {

    /**
     * Constructor.
     */
    public AdUserLogServiceImpl() {
        super(AdUserLog.class);
    }

    /**
     * Update user log
     *
     * @param log Log information
     */
    @Override
    public void updateLogout(UserLog log) {
        Map filter = new HashMap();
        filter.put("idUser", log.getIdUser());
        filter.put("sessionId", log.getSessionId());
        AdUserLog userLog = findFirst(filter);
        if (userLog != null) {
            userLog.setLogoutTime(log.getLastUpdate());
            update(userLog);
        }
    }

    /**
     * Reset all logoutTime with null value
     */
    @Override
    public int resetLogout() {
        Map filter = new HashMap();
        filter.put("created", FWConfig.appStartTime);
        List<AdUserLog> values = query("FROM AdUserLog WHERE logoutTime IS NULL AND created < :created", filter);
        Calendar cal = Calendar.getInstance();
        for (AdUserLog item : values) {
            cal.setTime(item.getCreated());
            cal.add(Calendar.MINUTE, 30);
            item.setLogoutTime(cal.getTime().before(FWConfig.appStartTime) ? cal.getTime() : FWConfig.appStartTime);
            update(item);
        }
        return values.size();
    }

}
