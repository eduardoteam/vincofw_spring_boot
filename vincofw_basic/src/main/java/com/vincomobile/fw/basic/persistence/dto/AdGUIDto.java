package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdClient;
import com.vincomobile.fw.basic.persistence.model.AdGUIBase;
import com.vincomobile.fw.basic.tools.Converter;

public class AdGUIDto {

    private String button;
    private String formField;
    private String filterApply;
    private String filterMode;
    private String tableType;
    private String tableMode;

    public AdGUIDto() {
    }

    public AdGUIDto(AdClient item) {
        this.button = item.getGuiButton();
        this.formField = item.getGuiFormField();
        this.filterApply = item.getGuiFilterApply();
        this.filterMode = item.getGuiFilterMode();
        this.tableType = item.getGuiTableType();
        this.tableMode = item.getGuiTableMode();
    }

    public void merge(AdGUIBase item) {
        if (item == null)
            return;
        if (!Converter.isEmpty(item.getGuiButton()))
            this.button = item.getGuiButton();
        if (!Converter.isEmpty(item.getGuiFormField()))
            this.formField = item.getGuiFormField();
        if (!Converter.isEmpty(item.getGuiFilterApply()))
            this.filterApply = item.getGuiFilterApply();
        if (!Converter.isEmpty(item.getGuiFilterMode()))
            this.filterMode = item.getGuiFilterMode();
        if (!Converter.isEmpty(item.getGuiTableType()))
            this.tableType = item.getGuiTableType();
        if (!Converter.isEmpty(item.getGuiTableMode()))
            this.tableMode = item.getGuiTableMode();
    }

    public AdGUIDto clone() {
        AdGUIDto result = new AdGUIDto();
        result.button = this.button;
        result.formField = this.formField;
        result.filterApply = this.filterApply;
        result.filterMode = this.filterMode;
        result.tableType = this.tableType;
        result.tableMode = this.tableMode;
        return result;
    }

    public String getButton() {
        return button;
    }

    public String getFormField() {
        return formField;
    }

    public String getFilterApply() {
        return filterApply;
    }

    public String getFilterMode() {
        return filterMode;
    }

    public String getTableMode() {
        return tableMode;
    }

    public String getTableType() {
        return tableType;
    }
}
