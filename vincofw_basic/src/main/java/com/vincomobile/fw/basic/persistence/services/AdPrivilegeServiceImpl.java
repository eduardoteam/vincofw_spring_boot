package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdPrivilege;
import com.vincomobile.fw.basic.persistence.model.AdUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_PRIVILEGE
 *
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdPrivilegeServiceImpl extends BaseServiceImpl<AdPrivilege, String> implements AdPrivilegeService {

    /**
     * Constructor.
     *
     */
    public AdPrivilegeServiceImpl() {
        super(AdPrivilege.class);
    }

    /**
     * Obtains all the users that have a selected privilege
     *
     * @param privilegeName Name of the privilege
     * @return List of users that have the selected privilege
     */
    @Override
    public List<AdUser> obtainUsers(String privilegeName){
        Map filter = new HashMap();
        filter.put("name", privilegeName);
        List<AdUser> users = query("SELECT USER FROM AdUser USER WHERE USER.idUser in (SELECT ROL.idUser FROM AdUserRoles ROL WHERE ROL.idRole in (SELECT PRIV.idRole FROM AdRolePriv PRIV WHERE PRIV.idPrivilege IN (SELECT P.idPrivilege FROM AdPrivilege P WHERE P.name = :name) ))", filter);
        for(AdUser user: users){
            entityManager.detach(user);
            user.setPassword("");
        }
        return users;
    }
}
