package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Devtools.
 * Modelo de AD_ROLE_PRIV
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_role_priv")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdRolePriv extends AdEntityBean {

    private String idRolePriv;
    private String idRole;
    private String idPrivilege;

    private AdPrivilege privilege;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idRolePriv;
    }

    @Override
    public void setId(String id) {
            this.idRolePriv = id;
    }

    @Id
    @Column(name = "id_role_priv")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdRolePriv() {
        return idRolePriv;
    }

    public void setIdRolePriv(String idRolePriv) {
        this.idRolePriv = idRolePriv;
    }


    @Column(name = "id_role")
    @NotNull
    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    @Column(name = "id_privilege")
    @NotNull
    public String getIdPrivilege() {
        return idPrivilege;
    }

    public void setIdPrivilege(String idPrivilege) {
        this.idPrivilege = idPrivilege;
    }

    @ManyToOne
    @JoinColumn(name = "id_privilege", referencedColumnName = "id_privilege", insertable = false, updatable = false)
    public AdPrivilege getPrivilege() {
        return privilege;
    }

    public void setPrivilege(AdPrivilege privilege) {
        this.privilege = privilege;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdRolePriv)) return false;

        final AdRolePriv that = (AdRolePriv) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idRolePriv == null) && (that.idRolePriv == null)) || (idRolePriv != null && idRolePriv.equals(that.idRolePriv)));
        result = result && (((idRole == null) && (that.idRole == null)) || (idRole != null && idRole.equals(that.idRole)));
        result = result && (((idPrivilege == null) && (that.idPrivilege == null)) || (idPrivilege != null && idPrivilege.equals(that.idPrivilege)));
        return result;
    }

}

