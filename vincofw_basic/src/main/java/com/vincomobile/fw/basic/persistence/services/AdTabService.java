package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdDesignerInfo;
import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.dto.AdTabMainDto;
import com.vincomobile.fw.basic.persistence.model.AdField;
import com.vincomobile.fw.basic.persistence.model.AdTab;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_TAB
 *
 * Date: 19/02/2015
 */
public interface AdTabService extends BaseService<AdTab, String> {

    String TYPE_TABLE                       = "TABLE";
    String TYPE_USERDEFINED                 = "USERDEFINED";
    String TYPE_IMAGE                       = "IMAGE";
    String TYPE_FILE                        = "FILE";
    String TYPE_CHARACTERISTIC              = "CHARACTERISTIC";
    String TYPE_CHARACTERISTIC_ADVANCED     = "CHARACTERISTIC_ADVANCED";
    String TYPE_CHART                       = "CHART";

    String UIPATTERN_STANDARD               = "STANDARD";
    String UIPATTERN_EDIT                   = "EDIT";
    String UIPATTERN_EDIT_DELETE            = "EDIT_DELETE";
    String UIPATTERN_READONLY               = "READONLY";
    String UIPATTERN_SORTABLE               = "SORTABLE";
    String UIPATTERN_MULTISELECT            = "MULTISELECT ";

    /**
     * List all fields for Tab
     *
     * @param idTab Tab identifier
     * @return Field list
     */
    List<AdField> listFields(String idTab);

    /**
     * List all fields for Tab (GridView)
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @return Field list
     */
    List<AdField> listGridFields(String idClient, String idTab, String idUser);

    /**
     * Clear view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    void clearViewDefault(String idWindow, String idTable, long tabLevel);

    /**
     * Check and fix the view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    void checkViewDefault(String idWindow, String idTable, long tabLevel);

    /**
     * Get the first editable tab for user and role
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idRole Role identifier
     * @param idTable Table identifier
     * @return Tab
     */
    AdTab getUserEditableTab(String idClient, String idUser, String idRole, String idTable);

    /**
     * Load main tabs of window and level (Meta Data)
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idWindow Window identifier
     * @param idTab Tab identifier (if is null load tab with level 0)
     * @param idLanguage Language identifier
     * @param gui GUI inherit properties
     * @return AdTabMainDto
     */
    AdTabMainDto loadMainTab(String idClient, String idUser, String idWindow, String idTab, String idLanguage, AdGUIDto gui);

    /**
     * Save designer tab changes
     *
     * @param idTab Tab identifier
     * @param designer Designer information
     */
    void saveTabDesign(String idTab, AdDesignerInfo designer);
}


