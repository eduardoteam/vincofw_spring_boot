package com.vincomobile.fw.basic.config;

import com.vincomobile.fw.basic.annotations.FWProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class FWScannerFactoryBean implements BeanDefinitionRegistryPostProcessor, PriorityOrdered {

    protected Logger logger = LoggerFactory.getLogger(FWScannerFactoryBean.class);

    protected abstract void addProcessPackagesForScan(List<String> packages);

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(FWProcess.class));

        logger.info("Scanning process beans ...");
        List<String> processPackages = new ArrayList<>();
        processPackages.add("com.vincomobile.fw.backend.process");
        addProcessPackagesForScan(processPackages);

        for (String processPackage : processPackages) {
            logger.info("\tPackage: " + processPackage);
            Set<BeanDefinition> definitions = scanner.findCandidateComponents(processPackage);
            for (BeanDefinition bean : definitions) {
                registerClass(bean, registry);
            }
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("FWScannerFactoryBean.postProcessBeanFactory");
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * Register class
     *
     * @param bean Bean definition
     * @param registry Bean definition registry
     */
    private void registerClass(BeanDefinition bean, BeanDefinitionRegistry registry) {
        logger.info("\t\t" + bean.getBeanClassName());
        bean.setFactoryMethodName("getService");
        bean.setLazyInit(true);
        registry.registerBeanDefinition(bean.getBeanClassName(), bean);
    }

}
