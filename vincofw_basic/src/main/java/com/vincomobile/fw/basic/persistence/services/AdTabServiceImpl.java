package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.FWConfig;
import com.vincomobile.fw.basic.persistence.beans.AdDesignerField;
import com.vincomobile.fw.basic.persistence.beans.AdDesignerInfo;
import com.vincomobile.fw.basic.persistence.dto.*;
import com.vincomobile.fw.basic.persistence.model.AdField;
import com.vincomobile.fw.basic.persistence.model.AdFieldGrid;
import com.vincomobile.fw.basic.persistence.model.AdTab;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Devtools.
 * Service of AD_TAB
 * <p/>
 * Date: 19/02/2015
 */
@Repository
@Transactional(readOnly = true)
public class AdTabServiceImpl extends BaseServiceImpl<AdTab, String> implements AdTabService {

    private final AdFieldService fieldService;
    private final AdFieldGridService fieldGridService;
    private final AdTranslationService translationService;

    /**
     * Constructor.
     */
    public AdTabServiceImpl(AdFieldService fieldService, @Lazy AdFieldGridService fieldGridService, AdTranslationService translationService) {
        super(AdTab.class);
        this.fieldService = fieldService;
        this.fieldGridService = fieldGridService;
        this.translationService = translationService;
    }

    /**
     * List all fields for Tab
     *
     * @param idTab Tab identifier
     * @return Field list
     */
    @Override
    public List<AdField> listFields(String idTab) {
        Query query = entityManager.createQuery("FROM AdField WHERE idTab = :idTab ORDER BY gridSeqno", AdField.class);
        query.setParameter("idTab", idTab);
        return query.getResultList();
    }

    /**
     * List all fields for Tab (GridView)
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @return Field list
     */
    @Override
    public List<AdField> listGridFields(String idClient, String idTab, String idUser) {
        Query query = entityManager.createQuery(
                "FROM AdFieldGrid WHERE idClient IN :idClient AND idTab = :idTab AND idUser = :idUser ORDER BY seqno",
                AdFieldGrid.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idTab", idTab);
        query.setParameter("idUser", idUser);
        List<AdFieldGrid> fieldGrids = query.getResultList();
        if (fieldGrids.size() == 0) {
            query = entityManager.createQuery("FROM AdField WHERE showingrid = true AND idTab = :idTab ORDER BY gridSeqno", AdField.class);
            query.setParameter("idTab", idTab);
            return query.getResultList();
        }
        List<AdField> result = new ArrayList<>();
        for (AdFieldGrid fieldGrid : fieldGrids) {
            result.add(fieldGrid.getField());
        }
        return result;
    }

    /**
     * Clear view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    @Override
    public void clearViewDefault(String idWindow, String idTable, long tabLevel) {
        if (!Converter.isEmpty(idWindow)) {
            Map params = new HashMap();
            params.put("idWindow", idWindow);
            params.put("idTable", idTable);
            params.put("tabLevel", tabLevel);
            applyUpdate("UPDATE AdTab SET viewDefault = 0 WHERE idWindow = :idWindow AND idTable = :idTable  AND tablevel = :tabLevel", params);
        }
    }

    /**
     * Check and fix the view default to all tabs of level
     *
     * @param idWindow Window identifier
     * @param idTable Table identifier
     * @param tabLevel Tab level
     */
    @Override
    public void checkViewDefault(String idWindow, String idTable, long tabLevel) {
        if (!Converter.isEmpty(idWindow)) {
            Map filter = new HashMap();
            filter.put("idWindow", idWindow);
            filter.put("idTable", idTable);
            filter.put("tabLevel", tabLevel);
            List<AdTab> tabs = findAll(filter);
            boolean viewDefault = false;
            for (AdTab tab : tabs) {
                if (tab.getViewDefault()) {
                    viewDefault = true;
                    break;
                }
            }
            if (!viewDefault) {
                for (AdTab tab : tabs) {
                    if (!AdTabService.UIPATTERN_SORTABLE.equals(tab.getUipattern()) && !AdTabService.UIPATTERN_MULTISELECT.equals(tab.getUipattern())) {
                        tab.setViewDefault(true);
                        update(tab);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Get the first editable tab for user and role
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idRole Role identifier
     * @param idTable Table identifier
     * @return Tab
     */
    @Override
    public AdTab getUserEditableTab(String idClient, String idUser, String idRole, String idTable) {
        Query query = entityManager.createQuery(
                "SELECT T\n" +
                "FROM AdTab T, AdWindow W, AdMenu M, AdMenuRoles MR, AdUserRoles UR\n" +
                "WHERE T.idWindow = W.idWindow AND W.idWindow = M.idWindow AND T.idClient IN :idClient\n" +
                "AND M.active = 1 AND T.active = 1 AND W.active = 1 AND MR.active = 1 AND UR.active = 1 \n" +
                "AND M.action = 'WINDOW' AND T.uipattern IN ('STANDARD', 'EDIT', 'EDIT_DELETE') \n" +
                "AND M.idMenu = MR.idMenu AND MR.idRole = UR.idRole \n" +
                "AND T.idTable = :idTable AND MR.idRole = :idRole AND UR.idUser = :idUser",
                AdTab.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idUser", idUser);
        query.setParameter("idRole", idRole);
        query.setParameter("idTable", idTable);
        query.setFirstResult(0);
        query.setMaxResults(1);
        List<AdTab> result = query.getResultList();
        return result.size() > 0 ? result.get(0) : null;
    }

    /**
     * Load main tabs of window and level (Meta Data)
     *
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idWindow Window identifier
     * @param idTab Tab identifier (if is null load tab with level 0)
     * @param idLanguage Language identifier
     * @param gui GUI inherit properties
     * @return AdTabMainDto
     */
    @Override
    public AdTabMainDto loadMainTab(String idClient, String idUser, String idWindow, String idTab, String idLanguage, AdGUIDto gui) {
        Map filter = new HashMap();
        filter.put("idClient", getClientFilter(idClient));
        filter.put("idWindow", idWindow);
        filter.put("active", true);
        if (idTab == null) {
            filter.put("tablevel", 0l);
        } else {
            filter.put("idTab", idTab);
        }
        AdTabMainDto result = null;
        List<AdTab> tabs = findAll(new SqlSort("seqno", "asc"), filter);
        List<AdTab> extraTabs = new ArrayList<>();
        for (AdTab tab: tabs) {
            if (idTab == null) {
                if (UIPATTERN_STANDARD.equals(tab.getUipattern())) {
                    if (result == null) {
                        result = loadTabInfo(idClient, idUser, idWindow, tab, idLanguage, gui);
                    }
                } else {
                    extraTabs.add(tab);
                }
            } else {
                result = loadTabInfo(idClient, idUser, idWindow, tab, idLanguage, gui);
            }
        }
        if (result != null && !extraTabs.isEmpty()) {
            result.addTabs(idClient, idLanguage, translationService, extraTabs);
        }
        return result;
    }

    /**
     * Save designer tab changes
     *
     * @param idTab Tab identifier
     * @param designer Designer information
     */
    @Override
    public void saveTabDesign(String idTab, AdDesignerInfo designer) {
        AdTab tab = findById(idTab);
        if (!tab.getColumns().equals(designer.getColumns())) {
            tab.setColumns(designer.getColumns());
            update(tab);
        }
        for (AdDesignerField item : designer.getFields()) {
            for (AdField field : tab.getFields()) {
                if (item.getIdField().equals(field.getIdField())) {
                    field.setCaption(item.getCaption());
                    field.setIdFieldGroup(item.getIdFieldGroup());
                    field.setSpan(item.getSpan());
                    field.setSeqno(item.getSeqno());
                    field.setGridSeqno(item.getGridSeqno());
                    field.setDisplayed(item.getDisplayed());
                    field.setShowingrid(item.getShowingrid());
                    field.setStartnewrow(item.getStartnewrow());
                    field.setReadonly(item.getReadonly());
                    fieldService.update(field);
                }
            }
        }
    }

    private AdTabMainDto loadTabInfo(String idClient, String idUser, String idWindow, AdTab tab, String idLanguage, AdGUIDto gui) {
        gui.merge(tab);
        AdTabMainDto result = new AdTabMainDto(
                tab,
                translationService.getTranslation(idClient, FWConfig.TABLE_ID_TAB, FWConfig.COLUMN_ID_TAB_NAME, idLanguage, tab.getIdTab(), tab.getName()),
                true, gui
        );
        if (!TYPE_USERDEFINED.equals(tab.getTtype())) {
            // Set translations for current tab
            for (AdFieldGroupDto item : result.getTab().getFieldGroups()) {
                item.setName(translationService.getTranslation(idClient, FWConfig.TABLE_ID_FIELD_GROUP, FWConfig.COLUMN_ID_FIELD_GROUP_NAME, idLanguage, item.getIdFieldGroup(), item.getName()));
            }
            for (AdFieldDto item : result.getTab().getFields()) {
                item.setCaption(translationService.getTranslation(idClient, FWConfig.TABLE_ID_FIELD, FWConfig.COLUMN_ID_FIELD_CAPTION, idLanguage, item.getIdField(), item.getCaption()));
            }
            for (AdColumnDto item : result.getTab().getTable().getColumns()) {
                item.setDescription(translationService.getTranslation(idClient, FWConfig.TABLE_ID_COLUMN, FWConfig.COLUMN_ID_COLUMN_DESCRIPTION, idLanguage, item.getIdColumn(), item.getDescription()));
            }
            for (AdChartDto item : result.getTab().getCharts()) {
                item.setName(translationService.getTranslation(idClient, FWConfig.TABLE_ID_CHART, FWConfig.COLUMN_ID_CHART_NAME, idLanguage, item.getIdChart(), item.getName()));
            }
            for (AdChartFilterDto item : result.getTab().getChartFilters()) {
                item.setCaption(translationService.getTranslation(idClient, FWConfig.TABLE_ID_CHART_FILTER, FWConfig.COLUMN_ID_CHART_FILTER_CAPTION, idLanguage, item.getIdChartFilter(), item.getCaption()));
                item.setDescription(translationService.getTranslation(idClient, FWConfig.TABLE_ID_CHART_FILTER, FWConfig.COLUMN_ID_CHART_FILTER_DESCRIPTION, idLanguage, item.getIdChartFilter(), item.getDescription()));
            }
            List<AdFieldGrid> fieldGrids = fieldGridService.getUserColumns(idClient, tab.getIdTab(), idUser);
            if (!fieldGrids.isEmpty()) {
                for (AdFieldDto fld: result.getTab().getFields()) {
                    AdFieldGrid fieldGrid = fieldGrids.stream().filter(fldGrid -> fldGrid.getIdField().equals(fld.getIdField())).findAny().orElse(null);
                    fld.setShowingrid(fieldGrid != null);
                    fld.setGridSeqno(fieldGrid == null ? 0L : fieldGrid.getSeqno());
                }
            }
        }
        // Load tabs for next tablevel
        Query query = entityManager.createQuery(
                "SELECT T FROM AdTab T\n" +
                        "WHERE T.idClient IN :idClient AND T.idWindow = :idWindow AND T.tablevel = :tablevel AND T.active = 1\n" +
                        "  AND (T.ttype = 'USERDEFINED' OR exists (SELECT 1 FROM AdColumn C\n" +
                        "              WHERE C.idTable = T.idTable AND C.linkParent = 1 " +
                        "                AND C.idReference IN ('A05ACA9B2E1A435CA2829CD738279502', 'A05ACA9B2E1A435CA2829CD738279503', 'D44DF6BE72EC49A2855C7C08323B0285') \n" +
                        "                AND (SELECT idTable FROM AdRefTable RT WHERE RT.idReference = C.idReferenceValue) = :idTable)) \n" +
                        "ORDER BY T.seqno",
                AdTab.class
        );
        query.setParameter("idClient", getClientFilter(idClient));
        query.setParameter("idWindow", idWindow);
        query.setParameter("tablevel", tab.getTablevel() + 1);
        query.setParameter("idTable", tab.getIdTable());
        result.addTabs(idClient, idLanguage, translationService, query.getResultList());
        return result;
    }
}
