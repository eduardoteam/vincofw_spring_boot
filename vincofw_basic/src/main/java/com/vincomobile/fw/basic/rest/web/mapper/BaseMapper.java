package com.vincomobile.fw.basic.rest.web.mapper;

import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
public class BaseMapper<BEAN, DTO> {

    private Class<BEAN> beanClass;
    private Class<DTO> dtoClass;

    public BaseMapper(Class<BEAN> beanClass, Class<DTO> dtoClass) {
        this.beanClass = beanClass;
        this.dtoClass = dtoClass;
    }

    protected ModelMapper buildMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper
                .getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper;
    }

    public DTO toDto(BEAN bean) {
        ModelMapper modelMapper = buildMapper();
        DTO dto = modelMapper
                .map(bean, dtoClass);

        return dto;
    }

    public BEAN toBean(DTO dto) {
        ModelMapper modelMapper = buildMapper();
        BEAN bean = modelMapper.map(dto, beanClass);

        return bean;
    }

    public List<DTO> toDto(List<BEAN> beans) {
        ModelMapper modelMapper = buildMapper();
        List<DTO> result = beans.stream().map(item -> modelMapper.map(item, dtoClass)).collect(Collectors.toList());

        return result;
    }

    public List<BEAN> toBean(List<DTO> dtos) {
        ModelMapper modelMapper = buildMapper();
        List<BEAN> result = dtos.stream().map(item -> modelMapper.map(item, beanClass)).collect(Collectors.toList());

        return result;
    }

    public Page<DTO> toDto(Page<BEAN> page) {
        ModelMapper modelMapper = buildMapper();
        List<DTO> content = page.getContent().stream().map(item -> modelMapper.map(item, dtoClass)).collect(Collectors.toList());

        return new PageImpl<DTO>(content, PageRequest.of(page.getNumber(), page.getSize()), page.getTotalElements());
    }

    public Page<BEAN> toBean(Page<DTO> page) {
        ModelMapper modelMapper = buildMapper();
        List<BEAN> content = page.getContent().stream().map(item -> modelMapper.map(item, beanClass)).collect(Collectors.toList());

        return new PageImpl<BEAN>(content, PageRequest.of(page.getNumber(), page.getSize()), page.getTotalElements());
    }



}
