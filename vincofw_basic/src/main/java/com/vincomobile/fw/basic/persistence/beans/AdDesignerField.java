package com.vincomobile.fw.basic.persistence.beans;

public class AdDesignerField {
    String idField;
    String caption;
    String idFieldGroup;
    Long span;
    Long seqno;
    Long gridSeqno;
    Boolean displayed;
    Boolean showingrid;
    Boolean startnewrow;
    Boolean readonly;

    public String getIdField() {
        return idField;
    }

    public void setIdField(String idField) {
        this.idField = idField;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getIdFieldGroup() {
        return idFieldGroup;
    }

    public void setIdFieldGroup(String idFieldGroup) {
        this.idFieldGroup = idFieldGroup;
    }

    public Long getSpan() {
        return span;
    }

    public void setSpan(Long span) {
        this.span = span;
    }

    public Long getSeqno() {
        return seqno;
    }

    public void setSeqno(Long seqno) {
        this.seqno = seqno;
    }

    public Long getGridSeqno() {
        return gridSeqno;
    }

    public void setGridSeqno(Long gridSeqno) {
        this.gridSeqno = gridSeqno;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public void setDisplayed(Boolean displayed) {
        this.displayed = displayed;
    }

    public Boolean getShowingrid() {
        return showingrid;
    }

    public void setShowingrid(Boolean showingrid) {
        this.showingrid = showingrid;
    }

    public Boolean getStartnewrow() {
        return startnewrow;
    }

    public void setStartnewrow(Boolean startnewrow) {
        this.startnewrow = startnewrow;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public void setReadonly(Boolean readonly) {
        this.readonly = readonly;
    }
}
