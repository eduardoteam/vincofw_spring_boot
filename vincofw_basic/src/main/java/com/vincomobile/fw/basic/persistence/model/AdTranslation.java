package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Devtools.
 * Modelo de AD_TRANSLATION
 *
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_translation")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdTranslation extends AdEntityBean {

    private String idTranslation;
    private String idLanguage;
    private String idTable;
    private String idColumn;
    private String rowkey;
    private String translation;

    private AdLanguage language;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idTranslation;
    }

    @Override
    public void setId(String id) {
            this.idTranslation = id;
    }

    @Id
    @Column(name = "id_translation")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdTranslation() {
        return idTranslation;
    }

    public void setIdTranslation(String idTranslation) {
        this.idTranslation = idTranslation;
    }

    @Column(name = "id_language")
    @NotNull
    public String getIdLanguage() {
        return idLanguage;
    }

    public void setIdLanguage(String idLanguage) {
        this.idLanguage = idLanguage;
    }

    @Column(name = "id_table")
    @NotNull
    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    @Column(name = "id_column")
    @NotNull
    public String getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(String idColumn) {
        this.idColumn = idColumn;
    }

    @Column(name = "rowkey")
    @NotNull
    public String getRowkey() {
        return rowkey;
    }

    public void setRowkey(String rowkey) {
        this.rowkey = rowkey;
    }

    @Column(name = "translation", columnDefinition = "TEXT")
    @NotNull
    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @ManyToOne
    @JoinColumn(name = "id_language", referencedColumnName = "id_language", insertable = false, updatable = false)
    public AdLanguage getLanguage() {
        return language;
    }

    public void setLanguage(AdLanguage language) {
        this.language = language;
    }

    /**
     * Implementa el equals
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdTranslation)) return false;

        final AdTranslation that = (AdTranslation) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idTranslation == null) && (that.idTranslation == null)) || (idTranslation != null && idTranslation.equals(that.idTranslation)));
        result = result && (((idLanguage == null) && (that.idLanguage == null)) || (idLanguage != null && idLanguage.equals(that.idLanguage)));
        result = result && (((idTable == null) && (that.idTable == null)) || (idTable != null && idTable.equals(that.idTable)));
        result = result && (((idColumn == null) && (that.idColumn == null)) || (idColumn != null && idColumn.equals(that.idColumn)));
        result = result && (((rowkey == null) && (that.rowkey == null)) || (rowkey != null && rowkey.equals(that.rowkey)));
        result = result && (((translation == null) && (that.translation == null)) || (translation != null && translation.equals(that.translation)));
        return result;
    }

    @Transient
    public String getPath() {
        return rowkey + "/translation/" + idLanguage + "/" + translation;
    }

}

