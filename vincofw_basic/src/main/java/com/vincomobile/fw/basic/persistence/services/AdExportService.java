package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdEntityBean;
import org.springframework.data.domain.Sort;

public interface AdExportService extends BaseService<AdEntityBean, String> {

    /**
     * Export associated table data to file
     *
     * @param constraints Filter request
     * @param sort Sort criteria
     * @param idTab Tab being exported
     * @param idClient Client identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @param service Service manager
     * @return Associated table data (currently CSV formatted file)
     */
    byte[] getExport(String constraints, Sort sort, String idTab, String idClient, String idUser, String idLanguage, BaseService service) throws Exception;


}


