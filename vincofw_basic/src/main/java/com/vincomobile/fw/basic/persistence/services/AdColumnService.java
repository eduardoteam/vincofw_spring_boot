package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.model.AdColumn;

/**
 * Created by Devtools.
 * Interface of service of AD_COLUMN
 *
 * Date: 19/02/2015
 */
public interface AdColumnService extends BaseService<AdColumn, String> {

    String TYPE_AUTOINCREMENT   = "AUTOINCREMENT";
    String TYPE_BOOLEAN         = "BOOLEAN";
    String TYPE_DATE            = "DATE";
    String TYPE_DATETIME        = "DATETIME";
    String TYPE_INTEGER         = "INTEGER";
    String TYPE_NUMBER          = "NUMBER";
    String TYPE_STRING          = "STRING";
    String TYPE_TEXT            = "TEXT";

    String SOURCE_DATABASE      = "DATABASE";
    String SOURCE_BUTTON        = "BUTTON";
    String SOURCE_CALCULATE     = "CALCULATE";
    String SOURCE_TRANSIENT     = "TRANSIENT";

    String CORE_FIELDS          = "created;updated;created_by;updated_by;active";

}


