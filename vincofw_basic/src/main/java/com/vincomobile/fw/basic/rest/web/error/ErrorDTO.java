package com.vincomobile.fw.basic.rest.web.error;

/**
 * This class contains the information of an error that occurred when the API tried
 * to perform the operation requested by the client.
 *
 * @author Petri Kainulainen
 */
final class ErrorDTO {

    private final String code;
    private final String message;

    ErrorDTO(String code, String message) {
        PreCondition.notNull(code, "Code cannot be null.");
        PreCondition.notEmpty(code, "Code cannot be empty.");

        PreCondition.notNull(message, "Message cannot be null.");
        PreCondition.notEmpty(message, "Message cannot be empty");

        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
