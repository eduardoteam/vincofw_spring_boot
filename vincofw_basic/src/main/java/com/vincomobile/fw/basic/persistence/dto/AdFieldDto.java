package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdField;

public class AdFieldDto {

    private String idField;
    private String idColumn;
    private String idFieldGroup;
    private String caption;
    private String placeholder;
    private String hint;
    private Long seqno;
    private Long gridSeqno;
    private Boolean readonly;
    private Boolean displayed;
    private Boolean showingrid;
    private Boolean startnewrow;
    private Boolean showinstatusbar;
    private Boolean usedInChild;
    private String onchangefunction;
    private String displaylogic;
    private Long span;
    private Boolean filterRange;
    private String filterType;
    private Boolean sortable;
    private String cssClass;
    private Boolean multiSelect;
    private Long guiFlexGrow;
    private Long guiMinWidth;
    private Long guiMaxWidth;
    private Long guiWidth;
    private String columnName;

    public AdFieldDto(AdField item) {
        this.idField = item.getIdField();
        this.idColumn = item.getIdColumn();
        this.idFieldGroup = item.getIdFieldGroup();
        this.caption = item.getCaption();
        this.placeholder = item.getPlaceholder();
        this.hint = item.getHint();
        this.seqno = item.getSeqno();
        this.gridSeqno = item.getGridSeqno();
        this.readonly = item.getReadonly();
        this.displayed = item.getDisplayed();
        this.showingrid = item.getShowingrid();
        this.startnewrow = item.getStartnewrow();
        this.showinstatusbar = item.getShowinstatusbar();
        this.usedInChild = item.getUsedInChild();
        this.onchangefunction = item.getOnchangefunction();
        this.displaylogic = item.getDisplaylogic();
        this.span = item.getSpan();
        this.filterRange = item.getFilterRange();
        this.filterType = item.getFilterType();
        this.sortable = item.getSortable();
        this.cssClass = item.getCssClass();
        this.multiSelect = item.getMultiSelect();
        this.guiFlexGrow = item.getGuiFlexGrow();
        this.guiMinWidth = item.getGuiMinWidth();
        this.guiMaxWidth = item.getGuiMaxWidth();
        this.guiWidth = item.getGuiWidth();
        this.columnName = item.getColumn().getStandardName();
    }

    /*
     * Set/Get Methods
     */

    public String getIdField() {
        return idField;
    }

    public String getIdColumn() {
        return idColumn;
    }

    public String getIdFieldGroup() {
        return idFieldGroup;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String getHint() {
        return hint;
    }

    public Long getSeqno() {
        return seqno;
    }

    public Long getGridSeqno() {
        return gridSeqno;
    }

    public void setGridSeqno(Long gridSeqno) {
        this.gridSeqno = gridSeqno;
    }

    public Boolean getReadonly() {
        return readonly;
    }

    public Boolean getDisplayed() {
        return displayed;
    }

    public Boolean getShowingrid() {
        return showingrid;
    }

    public void setShowingrid(Boolean showingrid) {
        this.showingrid = showingrid;
    }

    public Boolean getStartnewrow() {
        return startnewrow;
    }

    public Boolean getShowinstatusbar() {
        return showinstatusbar;
    }

    public Boolean getUsedInChild() {
        return usedInChild;
    }

    public String getOnchangefunction() {
        return onchangefunction;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public Long getSpan() {
        return span;
    }

    public Boolean getFilterRange() {
        return filterRange;
    }

    public String getFilterType() {
        return filterType;
    }

    public Boolean getSortable() {
        return sortable;
    }

    public String getCssClass() {
        return cssClass;
    }

    public Boolean getMultiSelect() {
        return multiSelect;
    }

    public Long getGuiFlexGrow() {
        return guiFlexGrow;
    }

    public Long getGuiMinWidth() {
        return guiMinWidth;
    }

    public Long getGuiMaxWidth() {
        return guiMaxWidth;
    }

    public Long getGuiWidth() {
        return guiWidth;
    }

    public String getColumnName() {
        return columnName;
    }
}

