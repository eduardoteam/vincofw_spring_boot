package com.vincomobile.fw.basic.persistence.services;


import com.vincomobile.fw.basic.persistence.model.AdClient;
import com.vincomobile.fw.basic.persistence.model.AdClientModule;
import com.vincomobile.fw.basic.persistence.model.AdModule;

import java.util.List;

/**
 * Created by Devtools.
 * Interface del servicio de ad_client_module
 *
 * Date: 28/11/2015
 */
public interface AdClientModuleService extends BaseService<AdClientModule, String> {

    /**
     * List all clients for module
     *
     * @param idModule Module identifier
     * @return Clients list
     */
    List<AdClient> listClients(String idModule);

    /**
     * List all modules for client
     *
     * @param idClient Client identifier
     * @return Modules list
     */
    List<AdModule> listModules(String idClient);
}


