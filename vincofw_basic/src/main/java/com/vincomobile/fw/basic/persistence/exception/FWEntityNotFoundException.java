package com.vincomobile.fw.basic.persistence.exception;

public final class FWEntityNotFoundException extends RuntimeException {

    public FWEntityNotFoundException() {
        super();
    }

    public FWEntityNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FWEntityNotFoundException(final String message) {
        super(message);
    }

    public FWEntityNotFoundException(final Throwable cause) {
        super(cause);
    }

}
