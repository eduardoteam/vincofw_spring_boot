package com.vincomobile.fw.basic.persistence.services;


import com.vincomobile.fw.basic.persistence.model.AdFieldGrid;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by Vincomobile FW on 02/06/2017.
 * Copyright © 2017 Vincomobile. All rights reserved.
 *
 * Service layer interface for ad_field_grid
 */
public interface AdFieldGridService extends BaseService<AdFieldGrid, String> {

    /**
     * List columns for User and Tab
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @return Columns
     */
    List<AdFieldGrid> getUserColumns(String idClient, String idTab, String idUser);

    /**
     * List all columns for User and Tab
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @param idLanguage Language identifier
     * @return Columns
     */
    Page<AdFieldGrid> getColumns(String idClient, String idTab, String idUser, String idLanguage);

    /**
     * Save columns
     *
     * @param idClient Client identifier
     * @param idTab Tab identifier
     * @param idUser User identifier
     * @param fields Field list
     */
    void saveColumns(String idClient, String idTab, String idUser, String[] fields);

    /**
     * Delete columns
     *
     * @param idTab Tab identifier
     * @param idUser User identifier
     */
    void deleteColumns(String idTab, String idUser);
}


