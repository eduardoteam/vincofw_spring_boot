package com.vincomobile.fw.basic.tools;

public class Constants {

    public static String realAppPath        = "";               // Directorio fisico donde se almacena la aplicacion
    
    // Gestión de sesiones
    public static long SESSION_MAX_IDLE_TIME_VALUE  = (30 * 60 * 1000); // 30 minutos (en milisegundos: 30 * 60 * 1000)

    // Constantes de DB
    public static final int DB_TYPE_MYSQL   = 1;                // Servidor de DB MySQL
    public static final int DB_TYPE_MSSQL   = 2;                // Servidor de DB Microsft SQL Server
    public static final int DB_TYPE_ORACLE  = 3;                // Servidor de DB Oracle
    public static int DB_TYPE               = DB_TYPE_MYSQL;    // Tipo de la DB
    public static String DB_SYSDATE         = "sysdate()";      // Funcion para obtener la fecha actual
    public static String SQL_DATE_OUTPUT_FORMAT     = "yyyy-MM-dd";
    public static String SQL_DATE_INPUT_FORMAT      = "dd/MM/yyyy";
    public static String SQL_DATETIME_OUTPUT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static String SQL_DATETIME_INPUT_FORMAT  = "dd/MM/yyyy HH:mm:ss";
    public static String SQL_SELECT_LASTID          = "SELECT LAST_INSERT_ID()";
    public static String SQL_DATE_MOBILE_FORMAT     = "yyyy-MM-dd";

    // Codigo de excepciones tratadas
    public static int DB_ERROR_PRIMARYKEY;
    public static int DB_ERROR_FOREINGKEY;


    public static String APP_NAME;                          // Nombre de la aplicacion
    public static int PAGE_NUMBERS          = 5;            // Numero de paginas a mostrar
    public static int MAX_LENGTH_READ       = 150 * 1024;    // Gestion de ficheros
    public static int COOKIE_MAX_AGE        = 31;           // Maximo numero de dias que es valida una Cookie
    public static long DEFAUL_IDI_COD          = 1;          // Id. del idioma por defecto

    // Parametro que identifica el tipo de salida de los reportes
    public static String OUTPUT_REPORT_TYPE = "outputType";

    /*
     * Selecciona las opciones segun el tipo de base de datos
     */
    public static void setDBOptions(int dbType) {
        DB_TYPE = dbType;
        switch (DB_TYPE) {
            case DB_TYPE_MYSQL:
                DB_SYSDATE                  = "sysdate()";
                SQL_DATE_OUTPUT_FORMAT      = "yyyy-MM-dd";
                SQL_DATE_INPUT_FORMAT       = "dd/MM/yyyy";
                SQL_DATETIME_OUTPUT_FORMAT  = "yyyy-MM-dd HH:mm:ss";
                SQL_DATETIME_INPUT_FORMAT   = "dd/MM/yyyy HH:mm:ss";
                SQL_SELECT_LASTID           = "SELECT LAST_INSERT_ID()";
                DB_ERROR_PRIMARYKEY         = 1062;
                DB_ERROR_FOREINGKEY         = 1451;
                break;

            case DB_TYPE_MSSQL:
                DB_SYSDATE                  = "getdate()";
                SQL_DATE_OUTPUT_FORMAT      = "yyyy-MM-dd";
                SQL_DATE_INPUT_FORMAT       = "dd/MM/yyyy";
                SQL_DATETIME_OUTPUT_FORMAT  = "yyyy-MM-dd HH:mm:ss";
                SQL_DATETIME_INPUT_FORMAT   = "dd/MM/yyyy HH:mm:ss";
                SQL_SELECT_LASTID           = "SELECT SCOPE_IDENTITY()";
                DB_ERROR_PRIMARYKEY         = 2627;
                DB_ERROR_FOREINGKEY         = 547;
                break;
        }

    }
}
