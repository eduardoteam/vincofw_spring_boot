package com.vincomobile.fw.basic.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Devtools.
 * Modelo de AD_CLIENT
 * <p/>
 * Date: 19/02/2015
 */
@Entity
@Table(name = "ad_client")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, ignoreUnknown = true)
public class AdClient extends EntityBean<String> {

    private String idClient;
    private String idModule;
    private String name;
    private String description;
    private String nick;
    private String email;
    private String smtpHost;
    private Long smtpPort;
    private Boolean smtpSsl;
    private String smtpUser;
    private String smtpPassword;
    private String guiTableType;
    private String guiTableMode;
    private String guiButton;
    private String guiFormField;
    private String guiFilterMode;
    private String guiFilterApply;
    private String guiColorTheme;
    private String guiWidth;
    private String toolbarPosition;
    private String toolbarColor;
    private String toolbarColorVariant;
    private Boolean footerHidden;
    private String footerPosition;
    private String footerColor;
    private String footerColorVariant;
    private String navbarPosition;
    private String navbarPrimaryColor;
    private String navbarPrimaryColorVariant;
    private String navbarSecondaryColor;
    private String navbarSecondaryColorVariant;

    private AdModule module;

    /*
     * Set/Get Methods
     */

    @Override
    @Transient
    public String getId() {
        return idClient;
    }

    @Override
    public void setId(String id) {
        this.idClient = id;
    }

    @Id
    @Column(name = "id_client")
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Column(name = "id_module")
    @NotNull
    public String getIdModule() {
        return idModule;
    }

    public void setIdModule(String idModule) {
        this.idModule = idModule;
    }

    @Column(name = "name", length = 100, unique = true)
    @NotNull
    @Size(min = 1, max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", length = 250)
    @Size(min = 1, max = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "nick", length = 20)
    @Size(min = 1, max = 20)
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Column(name = "email", length = 150, unique = true)
    @Email
    @Size(min = 1, max = 150)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = com.vincomobile.fw.basic.tools.Converter.isEmpty(email) ? null : email;
    }

    @Column(name = "smtp_host", length = 100)
    @Size(min = 1, max = 100)
    public String getSmtpHost() {
        return smtpHost;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    @Column(name = "smtp_port")
    public Long getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(Long smtpPort) {
        this.smtpPort = smtpPort;
    }

    @Column(name = "smtp_ssl")
    public Boolean getSmtpSsl() {
        return smtpSsl;
    }

    public void setSmtpSsl(Boolean smtpSsl) {
        this.smtpSsl = smtpSsl;
    }

    @Column(name = "smtp_user", length = 45)
    @Size(min = 1, max = 45)
    public String getSmtpUser() {
        return smtpUser;
    }

    public void setSmtpUser(String smtpUser) {
        this.smtpUser = smtpUser;
    }

    @Column(name = "smtp_password", length = 20)
    @Size(min = 1, max = 20)
    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    @Column(name = "gui_table_type", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiTableType() {
        return guiTableType;
    }

    public void setGuiTableType(String guiTableType) {
        this.guiTableType = guiTableType;
    }

    @Column(name = "gui_table_mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiTableMode() {
        return guiTableMode;
    }

    public void setGuiTableMode(String guiTableMode) {
        this.guiTableMode = guiTableMode;
    }

    @Column(name = "gui_button", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiButton() {
        return guiButton;
    }

    public void setGuiButton(String guiButton) {
        this.guiButton = guiButton;
    }

    @Column(name = "gui_form_field", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiFormField() {
        return guiFormField;
    }

    public void setGuiFormField(String guiFormField) {
        this.guiFormField = guiFormField;
    }

    @Column(name = "gui_filter_mode", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiFilterMode() {
        return guiFilterMode;
    }

    public void setGuiFilterMode(String guiFilterMode) {
        this.guiFilterMode = guiFilterMode;
    }

    @Column(name = "gui_filter_apply", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiFilterApply() {
        return guiFilterApply;
    }

    public void setGuiFilterApply(String guiFilterApply) {
        this.guiFilterApply = guiFilterApply;
    }

    @Column(name = "gui_color_theme", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiColorTheme() {
        return guiColorTheme;
    }

    public void setGuiColorTheme(String guiColorTheme) {
        this.guiColorTheme = guiColorTheme;
    }

    @Column(name = "gui_width", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getGuiWidth() {
        return guiWidth;
    }

    public void setGuiWidth(String guiWidth) {
        this.guiWidth = guiWidth;
    }

    @Column(name = "toolbar_position", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getToolbarPosition() {
        return toolbarPosition;
    }

    public void setToolbarPosition(String toolbarPosition) {
        this.toolbarPosition = toolbarPosition;
    }

    @Column(name = "toolbar_color", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getToolbarColor() {
        return toolbarColor;
    }

    public void setToolbarColor(String toolbarColor) {
        this.toolbarColor = toolbarColor;
    }

    @Column(name = "toolbar_color_variant", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getToolbarColorVariant() {
        return toolbarColorVariant;
    }

    public void setToolbarColorVariant(String toolbarColorVariant) {
        this.toolbarColorVariant = toolbarColorVariant;
    }

    @Column(name = "footer_hidden")
    @NotNull
    public Boolean getFooterHidden() {
        return footerHidden;
    }

    public void setFooterHidden(Boolean footerHidden) {
        this.footerHidden = footerHidden;
    }

    @Column(name = "footer_position", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getFooterPosition() {
        return footerPosition;
    }

    public void setFooterPosition(String footerPosition) {
        this.footerPosition = footerPosition;
    }

    @Column(name = "footer_color", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getFooterColor() {
        return footerColor;
    }

    public void setFooterColor(String footerColor) {
        this.footerColor = footerColor;
    }

    @Column(name = "footer_color_variant", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getFooterColorVariant() {
        return footerColorVariant;
    }

    public void setFooterColorVariant(String footerColorVariant) {
        this.footerColorVariant = footerColorVariant;
    }

    @Column(name = "navbar_position", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getNavbarPosition() {
        return navbarPosition;
    }

    public void setNavbarPosition(String navbarPosition) {
        this.navbarPosition = navbarPosition;
    }

    @Column(name = "navbar_primary_color", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getNavbarPrimaryColor() {
        return navbarPrimaryColor;
    }

    public void setNavbarPrimaryColor(String navbarPrimaryColor) {
        this.navbarPrimaryColor = navbarPrimaryColor;
    }

    @Column(name = "navbar_primary_color_variant", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getNavbarPrimaryColorVariant() {
        return navbarPrimaryColorVariant;
    }

    public void setNavbarPrimaryColorVariant(String navbarPrimaryColorVariant) {
        this.navbarPrimaryColorVariant = navbarPrimaryColorVariant;
    }

    @Column(name = "navbar_secondary_color", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getNavbarSecondaryColor() {
        return navbarSecondaryColor;
    }

    public void setNavbarSecondaryColor(String navbarSecondaryColor) {
        this.navbarSecondaryColor = navbarSecondaryColor;
    }

    @Column(name = "navbar_secondary_color_variant", length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    public String getNavbarSecondaryColorVariant() {
        return navbarSecondaryColorVariant;
    }

    public void setNavbarSecondaryColorVariant(String navbarSecondaryColorVariant) {
        this.navbarSecondaryColorVariant = navbarSecondaryColorVariant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_module", referencedColumnName = "id_module", insertable = false, updatable = false)
    public AdModule getModule() {
        return module;
    }

    public void setModule(AdModule module) {
        this.module = module;
    }

    /**
     * Implementa el equals
     *
     * @param aThat Object to compare with
     * @return true/false
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdClient)) return false;

        final AdClient that = (AdClient) aThat;
        boolean result = super.equals(aThat);
        result = result && (((idClient == null) && (that.idClient == null)) || (idClient != null && idClient.equals(that.idClient)));
        result = result && (((idModule == null) && (that.idModule == null)) || (idModule != null && idModule.equals(that.idModule)));
        result = result && (((name == null) && (that.name == null)) || (name != null && name.equals(that.name)));
        result = result && (((description == null) && (that.description == null)) || (description != null && description.equals(that.description)));
        result = result && (((nick == null) && (that.nick == null)) || (nick != null && nick.equals(that.nick)));
        result = result && (((email == null) && (that.email == null)) || (email != null && email.equals(that.email)));
        result = result && (((smtpHost == null) && (that.smtpHost == null)) || (smtpHost != null && smtpHost.equals(that.smtpHost)));
        result = result && (((smtpPort == null) && (that.smtpPort == null)) || (smtpPort != null && smtpPort.equals(that.smtpPort)));
        result = result && (((smtpSsl == null) && (that.smtpSsl == null)) || (smtpSsl != null && smtpSsl.equals(that.smtpSsl)));
        result = result && (((smtpUser == null) && (that.smtpUser == null)) || (smtpUser != null && smtpUser.equals(that.smtpUser)));
        result = result && (((smtpPassword == null) && (that.smtpPassword == null)) || (smtpPassword != null && smtpPassword.equals(that.smtpPassword)));
        result = result && (((guiTableType == null) && (that.guiTableType == null)) || (guiTableType != null && guiTableType.equals(that.guiTableType)));
        result = result && (((guiTableMode == null) && (that.guiTableMode == null)) || (guiTableMode != null && guiTableMode.equals(that.guiTableMode)));
        result = result && (((guiButton == null) && (that.guiButton == null)) || (guiButton != null && guiButton.equals(that.guiButton)));
        result = result && (((guiFormField == null) && (that.guiFormField == null)) || (guiFormField != null && guiFormField.equals(that.guiFormField)));
        result = result && (((guiFilterMode == null) && (that.guiFilterMode == null)) || (guiFilterMode != null && guiFilterMode.equals(that.guiFilterMode)));
        result = result && (((guiFilterApply == null) && (that.guiFilterApply == null)) || (guiFilterApply != null && guiFilterApply.equals(that.guiFilterApply)));
        result = result && (((guiColorTheme == null) && (that.guiColorTheme == null)) || (guiColorTheme != null && guiColorTheme.equals(that.guiColorTheme)));
        result = result && (((guiWidth == null) && (that.guiWidth == null)) || (guiWidth != null && guiWidth.equals(that.guiWidth)));
        result = result && (((toolbarPosition == null) && (that.toolbarPosition == null)) || (toolbarPosition != null && toolbarPosition.equals(that.toolbarPosition)));
        result = result && (((toolbarColor == null) && (that.toolbarColor == null)) || (toolbarColor != null && toolbarColor.equals(that.toolbarColor)));
        result = result && (((toolbarColorVariant == null) && (that.toolbarColorVariant == null)) || (toolbarColorVariant != null && toolbarColorVariant.equals(that.toolbarColorVariant)));
        result = result && (((footerHidden == null) && (that.footerHidden == null)) || (footerHidden != null && footerHidden.equals(that.footerHidden)));
        result = result && (((footerPosition == null) && (that.footerPosition == null)) || (footerPosition != null && footerPosition.equals(that.footerPosition)));
        result = result && (((footerColor == null) && (that.footerColor == null)) || (footerColor != null && footerColor.equals(that.footerColor)));
        result = result && (((footerColorVariant == null) && (that.footerColorVariant == null)) || (footerColorVariant != null && footerColorVariant.equals(that.footerColorVariant)));
        result = result && (((navbarPosition == null) && (that.navbarPosition == null)) || (navbarPosition != null && navbarPosition.equals(that.navbarPosition)));
        result = result && (((navbarPrimaryColor == null) && (that.navbarPrimaryColor == null)) || (navbarPrimaryColor != null && navbarPrimaryColor.equals(that.navbarPrimaryColor)));
        result = result && (((navbarPrimaryColorVariant == null) && (that.navbarPrimaryColorVariant == null)) || (navbarPrimaryColorVariant != null && navbarPrimaryColorVariant.equals(that.navbarPrimaryColorVariant)));
        result = result && (((navbarSecondaryColor == null) && (that.navbarSecondaryColor == null)) || (navbarSecondaryColor != null && navbarSecondaryColor.equals(that.navbarSecondaryColor)));
        result = result && (((navbarSecondaryColorVariant == null) && (that.navbarSecondaryColorVariant == null)) || (navbarSecondaryColorVariant != null && navbarSecondaryColorVariant.equals(that.navbarSecondaryColorVariant)));
        return result;
    }

    @Transient
    public boolean isSmtpConfigured() {
        return !com.vincomobile.fw.basic.tools.Converter.isEmpty(smtpHost) && !com.vincomobile.fw.basic.tools.Converter.isEmpty(smtpUser)
                && !com.vincomobile.fw.basic.tools.Converter.isEmpty(smtpPassword) && !com.vincomobile.fw.basic.tools.Converter.isEmpty(smtpPort);
    }
}

