package com.vincomobile.fw.basic.persistence.cache;

import com.vincomobile.fw.basic.persistence.dto.AdGUIDto;
import com.vincomobile.fw.basic.persistence.model.AdPreference;
import com.vincomobile.fw.basic.persistence.model.AdPrivilege;
import com.vincomobile.fw.basic.persistence.model.AdRefList;
import com.vincomobile.fw.basic.persistence.services.*;
import com.vincomobile.fw.basic.tools.Converter;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class CacheManager {

    static Map<String, CacheTable> cacheTables = new HashMap();
    static Map<String, CacheTable> cacheTablesById = new HashMap();
    static Map<String, AdPreference> cachePrefences = new HashMap();
    static Map<String, AdRefList> cacheRefList = new HashMap();
    static Map<String, String> cachePrivilegeNames = new HashMap();
    static Map<String, AdGUIDto> cacheGUI = new HashMap();
    static Map<String, Object> cacheCustom = new HashMap();
    static ExecutorService executor = Executors.newFixedThreadPool(10);
    static Stack<Map> stackPool = new Stack<>();

    public static Map<String, List<ExtendedFilter>> cacheExtendedFilters = new HashMap<>();
    public static List<String> extendedFilters = new ArrayList<>();
    public static AdPreferenceService preferenceService;
    public static AdRefListService refListService;
    public static AdPrivilegeService privilegeService;
    public static AdMessageService messageService;

    public static void clearAll() {
        cacheTables.clear();
        cacheTablesById.clear();
        cachePrefences.clear();
        cacheRefList.clear();
    }

    public static ExecutorService getExecutor() {
        return executor;
    }

    public static void destroy() {
        executor.shutdownNow();
    }

    /*
     * Pool stack
     */
    public static synchronized void stackPush(Map mail) {
        stackPool.push(mail);
    }

    public static synchronized Map stackPop() {
        return stackPool.pop();
    }

    public static synchronized int stackSize() {
        return stackPool.size();
    }

    /*
     * Table cache
     */
    public static CacheTable getTable(String key) {
        return cacheTables.get(key);
    }

    public static CacheTable getTableById(String key) {
        return cacheTablesById.get(key);
    }

    public static void putTable(String key, CacheTable data) {
        cacheTables.put(key, data);
        cacheTablesById.put(data.table.getIdTable(), data);
    }

    public static void removeTable(String key) {
        CacheTable data = cacheTables.get(key);
        if (data != null) {
            cacheTables.remove(key);
            cacheTablesById.remove(data.table.getIdTable());
        }
    }

    /*
     * GUI cache
     */
    public static AdGUIDto getGUI(String key) {
        return cacheGUI.get(key);
    }

    public static void putGUI(String key, AdGUIDto data) {
        cacheGUI.put(key, data);
    }

    public static void removeGUI(String key) {
        cacheGUI.remove(key);
    }

    /*
     * Preference cache
     */
    public static boolean hasPreference(String idClient, String name, String visibleAtcustom) {
        String key = idClient + "$" + name + (visibleAtcustom != null ? "$" + visibleAtcustom : "");
        return cachePrefences.containsKey(key);
    }

    public static boolean hasPreference(String idClient, String name) {
        return hasPreference(idClient, name, null);
    }

    public static AdPreference getPreference(String idClient, String name, String defaultValue, String visibleAtcustom) {
        String key = idClient + "$" + name + (visibleAtcustom != null ? "$" + visibleAtcustom : "");
        AdPreference preference = cachePrefences.get(key);
        if (preference == null) {
            preference = preferenceService == null ? new AdPreference() : (visibleAtcustom == null ? preferenceService.getPreference(name, idClient) : preferenceService.getPreference(name, idClient, null, null, visibleAtcustom));
            if (preference.getValue() == null) {
                preference.setValue(defaultValue);
            }
            if (preferenceService != null)
                cachePrefences.put(key, preference);
        }
        return preference;
    }

    public static AdPreference getPreference(String idClient, String name, String defaultValue) {
        return getPreference(idClient, name, defaultValue, null);
    }

    public static AdPreference getPreference(String idClient, String name) {
        return getPreference(idClient, name, "", null);
    }

    public static void removePreference(String name) {
        List<String> keysToRemove = new ArrayList<>();
        Iterator it = cachePrefences.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getKey().toString().endsWith("$" + name) || pair.getKey().toString().contains("$" + name + "$")) {
                keysToRemove.add(pair.getKey().toString());
            }
        }
        for (String key : keysToRemove) {
            cachePrefences.remove(key);
        }
    }

    public static String getPreferenceString(String idClient, String name, String defaultValue, String visibleAtcustom) {
        return getPreference(idClient, name, defaultValue, visibleAtcustom).getString();
    }

    public static String getPreferenceString(String idClient, String name, String defaultValue) {
        return getPreference(idClient, name, defaultValue).getString();
    }

    public static String getPreferenceString(String idClient, String name) {
        return getPreference(idClient, name, "").getString();
    }

    public static Long getPreferenceLong(String idClient, String name, Long defaultValue, String visibleAtcustom) {
        return Converter.getLong(getPreference(idClient, name, "", visibleAtcustom).getString(), defaultValue);
    }

    public static Long getPreferenceLong(String idClient, String name, Long defaultValue) {
        return Converter.getLong(getPreference(idClient, name).getString(), defaultValue);
    }

    public static Long getPreferenceLong(String idClient, String name) {
        return getPreferenceLong(idClient, name, 0L);
    }

    public static boolean getPreferenceBoolean(String idClient, String name, boolean defaultValue, String visibleAtcustom) {
        return Converter.getBoolean(getPreference(idClient, name, "", visibleAtcustom).getString(), defaultValue);
    }

    public static boolean getPreferenceBoolean(String idClient, String name, boolean defaultValue) {
        return Converter.getBoolean(getPreference(idClient, name).getString(), defaultValue);
    }

    public static boolean getPreferenceBoolean(String idClient, String name) {
        return getPreferenceBoolean(idClient, name, false);
    }

    /*
     * Reference list cache
     */
    public static String getRefListString(String idClient, String idReference, String value) {
        String key = idClient + "$" + idReference + "$" + value;
        AdRefList refList = cacheRefList.get(key);
        if (refList == null) {
            refList = refListService.getRefList(idClient, idReference, value);
            if (refList != null)
                cacheRefList.put(key, refList);
            else
                return "";
        }
        return refList.getName();
    }

    /*
     * ExtendedFilter cache
     */
    public static List<ExtendedFilter> getExtendedFilter(String qualifier) {
        return cacheExtendedFilters.get(qualifier);
    }

    public static void addExtendedFilter(String filterClassName) {
        extendedFilters.add(filterClassName);
    }

    /**
     * Get privilege name
     *
     * @param idPrivilege Privilege identifier
     * @return Privilege name
     */
    public static String getPrivilegeName(String idPrivilege) {
        String name = cachePrivilegeNames.get(idPrivilege);
        if (name == null) {
            AdPrivilege privilege = privilegeService.findById(idPrivilege);
            if (privilege != null) {
                cachePrivilegeNames.put(idPrivilege, privilege.getName());
                name = privilege.getName();
            }
        }
        return name;
    }

    /**
     * Get value from Custom Cache
     *
     * @param key Key
     * @return Value
     */
    public static Object getCustom(String key) {
        return cacheCustom.get(key);
    }

    /**
     * Put a value in Custom Cache
     *
     * @param key Key
     * @param value Value
     */
    public static void putCustom(String key, Object value) {
        cacheCustom.put(key, value);
    }

    /**
     * Get translate message
     *
     * @param idClient Client id.
     * @param idLanguage Language id.
     * @param value Message search key
     * @param params Message parameters
     * @return Translate message
     */
    public static String getMessage(String idClient, String idLanguage, String value, Object... params) {
        return messageService.getMessage(idClient, idLanguage, value, params);
    }

    public static String getMessage(String idLanguage, String value) {
        return messageService.getMessage(idLanguage, value);
    }

    public static String getMessage(String value) {
        return messageService.getMessage(value);
    }

}
