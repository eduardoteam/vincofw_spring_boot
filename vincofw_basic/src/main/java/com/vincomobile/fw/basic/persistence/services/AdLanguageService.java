package com.vincomobile.fw.basic.persistence.services;

import com.vincomobile.fw.basic.persistence.beans.AdLanguageInfo;
import com.vincomobile.fw.basic.persistence.model.AdLanguage;

import java.util.List;

/**
 * Created by Devtools.
 * Interface of service of AD_LANGUAGE
 *
 * Date: 19/02/2015
 */
public interface AdLanguageService extends BaseService<AdLanguage, String> {

    /**
     * Get language information by identifier, if not found return default langague
     *
     * @param idLanguage Language identifier
     * @return Language information
     */
    AdLanguageInfo getLanguageInfo(String idLanguage);

    /**
     * Get a language by identifier, if not found return default langague
     *
     * @param idLanguage Language identifier
     * @return Language
     */
    AdLanguage getLanguage(String idLanguage);

    /**
     * Finds a language by its ISO code
     *
     * @param idClient Client identifier
     * @param iso ISO code
     * @return Language. Returns null if no language matches the ISO code
     */
    AdLanguage findByISO(String idClient, String iso);
    AdLanguage findByISO(String iso);

    /**
     * Get language identifier
     *
     * @param language Language ISO2
     * @return Language identifier
     */
    String getIdLanguageByISO2(String language);

    /**
     * Finds a language by its ISO3 code
     *
     * @param idClient Client identifier
     * @param iso3 ISO code
     * @return Language. Returns null if no language matches the ISO3 code
     */
    AdLanguage findByISO3(String idClient, String iso3);

    /**
     * Get all active languages
     *
     * @return Language list
     */
    List<AdLanguage> getActiveLanguages();

}


