package com.vincomobile.fw.basic.persistence.beans;

public class AdPermission {

    String property;
    Boolean allow;

    public AdPermission(String property, Boolean allow) {
        this.property = property;
        this.allow = allow;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Boolean getAllow() {
        return allow;
    }

    public void setAllow(Boolean allow) {
        this.allow = allow;
    }
}
