package com.vincomobile.fw.basic.persistence.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class AdGUIBase extends AdEntityBean {

    private String guiTableType;
    private String guiTableMode;
    private String guiButton;
    private String guiFormField;
    private String guiFilterMode;
    private String guiFilterApply;

    /*
     * Set/Get Methods
     */

    @Column(name = "gui_table_type", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiTableType() {
        return guiTableType;
    }

    public void setGuiTableType(String guiTableType) {
        this.guiTableType = guiTableType;
    }

    @Column(name = "gui_table_mode", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiTableMode() {
        return guiTableMode;
    }

    public void setGuiTableMode(String guiTableMode) {
        this.guiTableMode = guiTableMode;
    }

    @Column(name = "gui_button", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiButton() {
        return guiButton;
    }

    public void setGuiButton(String guiButton) {
        this.guiButton = guiButton;
    }

    @Column(name = "gui_form_field", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiFormField() {
        return guiFormField;
    }

    public void setGuiFormField(String guiFormField) {
        this.guiFormField = guiFormField;
    }

    @Column(name = "gui_filter_mode", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiFilterMode() {
        return guiFilterMode;
    }

    public void setGuiFilterMode(String guiFilterMode) {
        this.guiFilterMode = guiFilterMode;
    }

    @Column(name = "gui_filter_apply", length = 50)
    @Size(min = 1, max = 50)
    public String getGuiFilterApply() {
        return guiFilterApply;
    }

    public void setGuiFilterApply(String guiFilterApply) {
        this.guiFilterApply = guiFilterApply;
    }

    /**
     * Equals implementation
     *
     * @see Object#equals(Object)
     * @param aThat Object to compare with
     * @return true/false
     */
    @Override
    public boolean equals(final Object aThat) {
        if (this == aThat) return true;
        if (aThat == null) return false;
        if (!(aThat instanceof AdGUIBase)) return false;

        final AdGUIBase that = (AdGUIBase) aThat;
        boolean result = super.equals(aThat);
        result = result && (((guiTableType == null) && (that.guiTableType == null)) || (guiTableType != null && guiTableType.equals(that.guiTableType)));
        result = result && (((guiTableMode == null) && (that.guiTableMode == null)) || (guiTableMode != null && guiTableMode.equals(that.guiTableMode)));
        result = result && (((guiButton == null) && (that.guiButton == null)) || (guiButton != null && guiButton.equals(that.guiButton)));
        result = result && (((guiFormField == null) && (that.guiFormField == null)) || (guiFormField != null && guiFormField.equals(that.guiFormField)));
        result = result && (((guiFilterMode == null) && (that.guiFilterMode == null)) || (guiFilterMode != null && guiFilterMode.equals(that.guiFilterMode)));
        result = result && (((guiFilterApply == null) && (that.guiFilterApply == null)) || (guiFilterApply != null && guiFilterApply.equals(that.guiFilterApply)));
        return result;
    }
}

