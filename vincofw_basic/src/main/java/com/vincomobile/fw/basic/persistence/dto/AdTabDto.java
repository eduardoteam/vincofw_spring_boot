package com.vincomobile.fw.basic.persistence.dto;

import com.vincomobile.fw.basic.persistence.model.AdChart;
import com.vincomobile.fw.basic.persistence.model.AdChartFilter;
import com.vincomobile.fw.basic.persistence.model.AdField;
import com.vincomobile.fw.basic.persistence.model.AdTab;
import com.vincomobile.fw.basic.tools.Converter;

import java.util.ArrayList;
import java.util.List;

public class AdTabDto {

    private String idTab;
    private Long tablevel;
    private Long columns;
    private Long sortColumns;
    private Long seqno;
    private String uipattern;
    private String ttype;
    private String sqlwhere;
    private String sqlorderby;
    private String displaylogic;
    private String command;
    private String idParentTable;
    private String viewMode;
    private Boolean viewDefault;
    private String groupMode;
    private boolean hasFilters;

    private AdGUIDto gui;
    private AdTableDto table;
    private List<AdFieldDto> fields;
    private List<AdChartDto> charts;
    private List<AdChartFilterDto> chartFilters;
    private List<AdFieldGroupDto> fieldGroups;

    public AdTabDto(AdTab item, AdGUIDto gui) {
        this.idTab = item.getIdTab();
        this.idParentTable = item.getIdParentTable();
        this.tablevel = item.getTablevel();
        this.columns = item.getColumns();
        this.sortColumns = item.getSortColumns();
        this.seqno = item.getSeqno();
        this.uipattern = item.getUipattern();
        this.ttype = item.getTtype();
        this.sqlwhere = item.getSqlwhere();
        this.sqlorderby = item.getSqlorderby();
        this.displaylogic = item.getDisplaylogic();
        this.command = item.getCommand();
        this.viewMode = item.getViewMode();
        this.viewDefault = item.getViewDefault();
        this.groupMode = item.getGroupMode();
        this.gui = gui;
        this.table = new AdTableDto(item.getTable());
        this.fields = new ArrayList<>();
        this.charts = new ArrayList<>();
        this.chartFilters = new ArrayList<>();
        this.fieldGroups = new ArrayList<>();
        this.hasFilters = false;
        for (AdField fld : item.getFields()) {
            this.fields.add(new AdFieldDto(fld));
            if (fld.getColumn().getShowinsearch() != null && fld.getColumn().getShowinsearch()) {
                this.hasFilters = true;
            }
            if (!Converter.isEmpty(fld.getIdFieldGroup())) {
                boolean found = false;
                for (AdFieldGroupDto groupDto : this.fieldGroups) {
                    if (groupDto.getIdFieldGroup().equals(fld.getIdFieldGroup())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    this.fieldGroups.add(new AdFieldGroupDto(fld.getFieldGroup()));
                }
            }
        }
        for (AdChart chart : item.getCharts()) {
            this.charts.add(new AdChartDto(chart));
        }
        for (AdChartFilter chartFilter : item.getChartFilters()) {
            this.chartFilters.add(new AdChartFilterDto(chartFilter));
        }
    }

    /*
     * Set/Get Methods
     */

    public String getIdTab() {
        return idTab;
    }

    public AdGUIDto getGui() {
        return gui;
    }

    public Long getTablevel() {
        return tablevel;
    }

    public Long getColumns() {
        return columns;
    }

    public Long getSortColumns() {
        return sortColumns;
    }

    public Long getSeqno() {
        return seqno;
    }

    public String getUipattern() {
        return uipattern;
    }

    public String getTtype() {
        return ttype;
    }

    public String getSqlwhere() {
        return sqlwhere;
    }

    public String getSqlorderby() {
        return sqlorderby;
    }

    public String getDisplaylogic() {
        return displaylogic;
    }

    public String getCommand() {
        return command;
    }

    public String getIdParentTable() {
        return idParentTable;
    }

    public String getViewMode() {
        return viewMode;
    }

    public Boolean getViewDefault() {
        return viewDefault;
    }

    public String getGroupMode() {
        return groupMode;
    }

    public boolean isHasFilters() {
        return hasFilters;
    }

    public AdTableDto getTable() {
        return table;
    }

    public List<AdFieldDto> getFields() {
        return fields;
    }

    public List<AdChartDto> getCharts() {
        return charts;
    }

    public List<AdChartFilterDto> getChartFilters() {
        return chartFilters;
    }

    public List<AdFieldGroupDto> getFieldGroups() {
        return fieldGroups;
    }
}

